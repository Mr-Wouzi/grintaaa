Comparateur

=======================

Install using Vagrant
---------------------

From the host :

    git clone http://gitlab.linaya.net/comparateur/comparateur-de-prix.git
    cd comparateur-de-prix
    vagrant up
    vagrant ssh


Inside the VM :

    cd /var/www/projet
    composer update --optimize-autoloader
    make install

Edit your hosts file (/etc/hosts) and add :

    192.168.56.103  projet.dev www.projet.dev

You can now access project page at

[https://projet.dev/](https://projet.dev/) (prod env)

[https://projet.dev/app_dev.php](https://projet.dev/app_dev.php) (dev env)

[http://www.projet.dev:1080/](http://www.projet.dev:1080/) (mailcatcher)

Admin Mysql : [adminer](http://192.168.56.103/adminer/)