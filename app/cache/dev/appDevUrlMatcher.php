<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // application_site_vote_default_index
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'application_site_vote_default_index')), array (  '_controller' => 'Application\\Site\\VoteBundle\\Controller\\DefaultController::indexAction',));
        }

        // dcs_rating_add_vote
        if (0 === strpos($pathinfo, '/vote/add') && preg_match('#^/vote/add/(?P<id>[^/]++)/(?P<value>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'dcs_rating_add_vote')), array (  '_controller' => 'DCS\\RatingBundle\\Controller\\RatingController::addVoteAction',));
        }

        // home
        if (preg_match('#^/(?P<_locale>fr|en)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'home')), array (  '_controller' => 'Comparator\\Bundle\\BaseBundle\\Controller\\BaseController::homeAction',  '_locale' => 'fr',));
        }

        if (0 === strpos($pathinfo, '/p')) {
            // personne
            if (0 === strpos($pathinfo, '/personne_type') && preg_match('#^/personne_type(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'personne')), array (  '_controller' => 'Comparator\\Bundle\\BaseBundle\\Controller\\BaseController::personneAction',  '_locale' => 'fr',));
            }

            // presentation
            if (0 === strpos($pathinfo, '/presentation') && preg_match('#^/presentation(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'presentation')), array (  '_controller' => 'Comparator\\Bundle\\BaseBundle\\Controller\\BaseController::presentationAction',  '_locale' => 'fr',));
            }

        }

        if (0 === strpos($pathinfo, '/event_statut')) {
            // event_statut
            if (rtrim($pathinfo, '/') === '/event_statut') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'event_statut');
                }

                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\StatutEventController::indexAction',  '_route' => 'event_statut',);
            }

            // event_statut_show
            if (preg_match('#^/event_statut/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_statut_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\StatutEventController::showAction',));
            }

            // event_statut_new
            if ($pathinfo === '/event_statut/new') {
                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\StatutEventController::newAction',  '_route' => 'event_statut_new',);
            }

            // event_statut_create
            if ($pathinfo === '/event_statut/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_event_statut_create;
                }

                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\StatutEventController::createAction',  '_route' => 'event_statut_create',);
            }
            not_event_statut_create:

            // event_statut_edit
            if (preg_match('#^/event_statut/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_statut_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\StatutEventController::editAction',));
            }

            // event_statut_update
            if (preg_match('#^/event_statut/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_event_statut_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_statut_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\StatutEventController::updateAction',));
            }
            not_event_statut_update:

            // event_statut_delete
            if (preg_match('#^/event_statut/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_event_statut_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_statut_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\StatutEventController::deleteAction',));
            }
            not_event_statut_delete:

        }

        if (0 === strpos($pathinfo, '/grintaaa_')) {
            if (0 === strpos($pathinfo, '/grintaaa_interest')) {
                // grintaaa_interest
                if (rtrim($pathinfo, '/') === '/grintaaa_interest') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'grintaaa_interest');
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\InterestController::indexAction',  '_route' => 'grintaaa_interest',);
                }

                // grintaaa_interest_show
                if ($pathinfo === '/grintaaa_interest/show') {
                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\InterestController::showAction',  '_route' => 'grintaaa_interest_show',);
                }

                // grintaaa_interest_new
                if ($pathinfo === '/grintaaa_interest/new') {
                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\InterestController::newAction',  '_route' => 'grintaaa_interest_new',);
                }

                // grintaaa_interest_create
                if ($pathinfo === '/grintaaa_interest/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_grintaaa_interest_create;
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\InterestController::createAction',  '_route' => 'grintaaa_interest_create',);
                }
                not_grintaaa_interest_create:

                // grintaaa_interest_edit
                if (preg_match('#^/grintaaa_interest/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_interest_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\InterestController::editAction',));
                }

                // grintaaa_interest_update
                if (preg_match('#^/grintaaa_interest/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_grintaaa_interest_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_interest_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\InterestController::updateAction',));
                }
                not_grintaaa_interest_update:

                // grintaaa_interest_delete
                if (preg_match('#^/grintaaa_interest/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_grintaaa_interest_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_interest_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\InterestController::deleteAction',));
                }
                not_grintaaa_interest_delete:

            }

            if (0 === strpos($pathinfo, '/grintaaa_co')) {
                if (0 === strpos($pathinfo, '/grintaaa_configuration_notification_email')) {
                    // grintaaa_configuration_email
                    if (rtrim($pathinfo, '/') === '/grintaaa_configuration_notification_email') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'grintaaa_configuration_email');
                        }

                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ConfigurationController::indexAction',  '_route' => 'grintaaa_configuration_email',);
                    }

                    // grintaaa_configuration_email_show
                    if ($pathinfo === '/grintaaa_configuration_notification_email/show') {
                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ConfigurationController::showAction',  '_route' => 'grintaaa_configuration_email_show',);
                    }

                }

                if (0 === strpos($pathinfo, '/grintaaa_coaching')) {
                    // grintaaa_coaching
                    if (rtrim($pathinfo, '/') === '/grintaaa_coaching') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'grintaaa_coaching');
                        }

                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CoachingController::indexAction',  '_route' => 'grintaaa_coaching',);
                    }

                    // grintaaa_coaching_show
                    if ($pathinfo === '/grintaaa_coaching/show') {
                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CoachingController::showAction',  '_route' => 'grintaaa_coaching_show',);
                    }

                    // grintaaa_coaching_new
                    if ($pathinfo === '/grintaaa_coaching/new') {
                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CoachingController::newAction',  '_route' => 'grintaaa_coaching_new',);
                    }

                    // grintaaa_coaching_create
                    if ($pathinfo === '/grintaaa_coaching/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_grintaaa_coaching_create;
                        }

                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CoachingController::createAction',  '_route' => 'grintaaa_coaching_create',);
                    }
                    not_grintaaa_coaching_create:

                    // grintaaa_coaching_edit
                    if (preg_match('#^/grintaaa_coaching/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_coaching_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CoachingController::editAction',));
                    }

                    // grintaaa_coaching_update
                    if (preg_match('#^/grintaaa_coaching/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_grintaaa_coaching_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_coaching_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CoachingController::updateAction',));
                    }
                    not_grintaaa_coaching_update:

                    // grintaaa_coaching_delete
                    if (preg_match('#^/grintaaa_coaching/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_grintaaa_coaching_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_coaching_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CoachingController::deleteAction',));
                    }
                    not_grintaaa_coaching_delete:

                }

            }

            if (0 === strpos($pathinfo, '/grintaaa_msg')) {
                // grintaaa_msg
                if (rtrim($pathinfo, '/') === '/grintaaa_msg') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'grintaaa_msg');
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::indexAction',  '_route' => 'grintaaa_msg',);
                }

                // grintaaa_msg_show
                if (preg_match('#^/grintaaa_msg/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_msg_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::showAction',));
                }

                // grintaaa_msg_new
                if ($pathinfo === '/grintaaa_msg/new') {
                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::newAction',  '_route' => 'grintaaa_msg_new',);
                }

                // grintaaa_msg_create
                if ($pathinfo === '/grintaaa_msg/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_grintaaa_msg_create;
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::createAction',  '_route' => 'grintaaa_msg_create',);
                }
                not_grintaaa_msg_create:

                // grintaaa_msg_edit
                if (preg_match('#^/grintaaa_msg/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_msg_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::editAction',));
                }

                // grintaaa_msg_update
                if (preg_match('#^/grintaaa_msg/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_grintaaa_msg_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_msg_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::updateAction',));
                }
                not_grintaaa_msg_update:

                // grintaaa_msg_delete
                if (preg_match('#^/grintaaa_msg/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_grintaaa_msg_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_msg_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::deleteAction',));
                }
                not_grintaaa_msg_delete:

                // send_message
                if (0 === strpos($pathinfo, '/grintaaa_msg/send') && preg_match('#^/grintaaa_msg/send/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'send_message')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::sendMessageAction',));
                }

                // receive_message
                if ($pathinfo === '/grintaaa_msg/receive') {
                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::receiveMessageAction',  '_route' => 'receive_message',);
                }

                // friend_message
                if (preg_match('#^/grintaaa_msg/(?P<userid>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'friend_message')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MessagesController::listMessageAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/multimedia')) {
            // grintaaa_multimedia_video
            if ($pathinfo === '/multimedia/video') {
                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MurController::listVideosAction',  '_route' => 'grintaaa_multimedia_video',);
            }

            // grintaaa_multimedia_image
            if ($pathinfo === '/multimedia/image') {
                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MurController::listImagesAction',  '_route' => 'grintaaa_multimedia_image',);
            }

        }

        if (0 === strpos($pathinfo, '/grintaaa_')) {
            if (0 === strpos($pathinfo, '/grintaaa_notification')) {
                // grintaaa_notification
                if (rtrim($pathinfo, '/') === '/grintaaa_notification') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'grintaaa_notification');
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\NotificationController::indexAction',  '_route' => 'grintaaa_notification',);
                }

                // grintaaa_notification_show
                if (preg_match('#^/grintaaa_notification/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_notification_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\NotificationController::showAction',));
                }

                // grintaaa_notification_new
                if ($pathinfo === '/grintaaa_notification/new') {
                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\NotificationController::newAction',  '_route' => 'grintaaa_notification_new',);
                }

                // grintaaa_notification_create
                if ($pathinfo === '/grintaaa_notification/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_grintaaa_notification_create;
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\NotificationController::createAction',  '_route' => 'grintaaa_notification_create',);
                }
                not_grintaaa_notification_create:

                // grintaaa_notification_edit
                if (preg_match('#^/grintaaa_notification/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_notification_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\NotificationController::editAction',));
                }

                // grintaaa_notification_update
                if (preg_match('#^/grintaaa_notification/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_grintaaa_notification_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_notification_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\NotificationController::updateAction',));
                }
                not_grintaaa_notification_update:

                // grintaaa_notification_delete
                if (preg_match('#^/grintaaa_notification/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_grintaaa_notification_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_notification_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\NotificationController::deleteAction',));
                }
                not_grintaaa_notification_delete:

            }

            if (0 === strpos($pathinfo, '/grintaaa_comment')) {
                // grintaaa_comment
                if (rtrim($pathinfo, '/') === '/grintaaa_comment') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'grintaaa_comment');
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentController::indexAction',  '_route' => 'grintaaa_comment',);
                }

                // grintaaa_comment_show
                if (preg_match('#^/grintaaa_comment/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_comment_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentController::showAction',));
                }

                // grintaaa_comment_new
                if ($pathinfo === '/grintaaa_comment/new') {
                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentController::newAction',  '_route' => 'grintaaa_comment_new',);
                }

                // grintaaa_comment_create
                if ($pathinfo === '/grintaaa_comment/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_grintaaa_comment_create;
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentController::createAction',  '_route' => 'grintaaa_comment_create',);
                }
                not_grintaaa_comment_create:

                // grintaaa_comment_edit
                if (preg_match('#^/grintaaa_comment/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_comment_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentController::editAction',));
                }

                // grintaaa_comment_update
                if (preg_match('#^/grintaaa_comment/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_grintaaa_comment_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_comment_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentController::updateAction',));
                }
                not_grintaaa_comment_update:

                // grintaaa_comment_delete
                if (preg_match('#^/grintaaa_comment/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_grintaaa_comment_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_comment_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentController::deleteAction',));
                }
                not_grintaaa_comment_delete:

                // add_comment
                if (preg_match('#^/grintaaa_comment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_comment')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentController::addCommentAction',));
                }

                if (0 === strpos($pathinfo, '/grintaaa_comment_user')) {
                    // grintaaa_comment_user
                    if (rtrim($pathinfo, '/') === '/grintaaa_comment_user') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'grintaaa_comment_user');
                        }

                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentUserController::indexAction',  '_route' => 'grintaaa_comment_user',);
                    }

                    // grintaaa_comment_user_show
                    if (preg_match('#^/grintaaa_comment_user/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_comment_user_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentUserController::showAction',));
                    }

                    // grintaaa_comment_user_new
                    if ($pathinfo === '/grintaaa_comment_user/new') {
                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentUserController::newAction',  '_route' => 'grintaaa_comment_user_new',);
                    }

                    // grintaaa_comment_user_create
                    if ($pathinfo === '/grintaaa_comment_user/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_grintaaa_comment_user_create;
                        }

                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentUserController::createAction',  '_route' => 'grintaaa_comment_user_create',);
                    }
                    not_grintaaa_comment_user_create:

                    // grintaaa_comment_user_edit
                    if (preg_match('#^/grintaaa_comment_user/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_comment_user_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentUserController::editAction',));
                    }

                    // grintaaa_comment_user_update
                    if (preg_match('#^/grintaaa_comment_user/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_grintaaa_comment_user_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_comment_user_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentUserController::updateAction',));
                    }
                    not_grintaaa_comment_user_update:

                    // grintaaa_comment_user_delete
                    if (preg_match('#^/grintaaa_comment_user/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                            $allow = array_merge($allow, array('POST', 'DELETE'));
                            goto not_grintaaa_comment_user_delete;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_comment_user_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentUserController::deleteAction',));
                    }
                    not_grintaaa_comment_user_delete:

                    // add_comment_user
                    if (preg_match('#^/grintaaa_comment_user/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_comment_user')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\CommentUserController::addCommentAction',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/grintaaa_friend')) {
                // grintaaa_friend
                if (rtrim($pathinfo, '/') === '/grintaaa_friend') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'grintaaa_friend');
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::indexAction',  '_route' => 'grintaaa_friend',);
                }

                // grintaaa__list_friend
                if ($pathinfo === '/grintaaa_friend/friends_list') {
                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::listFriendAction',  '_route' => 'grintaaa__list_friend',);
                }

                if (0 === strpos($pathinfo, '/grintaaa_friend/search')) {
                    // grintaaa__list_friend_search
                    if ($pathinfo === '/grintaaa_friend/search') {
                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::searchFriendAction',  '_route' => 'grintaaa__list_friend_search',);
                    }

                    // grintaaa__list_friend_searchs
                    if ($pathinfo === '/grintaaa_friend/searchs') {
                        return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::searchFriendsAction',  '_route' => 'grintaaa__list_friend_searchs',);
                    }

                }

                // grintaaa_friend_show
                if (preg_match('#^/grintaaa_friend/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_friend_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::showAction',));
                }

                // grintaaa_friend_new
                if ($pathinfo === '/grintaaa_friend/new') {
                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::newAction',  '_route' => 'grintaaa_friend_new',);
                }

                // grintaaa_friend_create
                if ($pathinfo === '/grintaaa_friend/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_grintaaa_friend_create;
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::createAction',  '_route' => 'grintaaa_friend_create',);
                }
                not_grintaaa_friend_create:

                // grintaaa_friend_edit
                if (preg_match('#^/grintaaa_friend/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_friend_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::editAction',));
                }

                // grintaaa_friend_update
                if (preg_match('#^/grintaaa_friend/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_grintaaa_friend_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_friend_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::updateAction',));
                }
                not_grintaaa_friend_update:

                // grintaaa_friend_delete
                if (preg_match('#^/grintaaa_friend/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_grintaaa_friend_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_friend_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::deleteAction',));
                }
                not_grintaaa_friend_delete:

                // add_friend
                if (preg_match('#^/grintaaa_friend/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_friend')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\FriendsController::addFriendAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/favoris')) {
            // favoris
            if (rtrim($pathinfo, '/') === '/favoris') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'favoris');
                }

                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ParticipeEventController::indexAction',  '_route' => 'favoris',);
            }

            // favoris_show
            if (preg_match('#^/favoris/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ParticipeEventController::showAction',));
            }

            // favoris_new
            if ($pathinfo === '/favoris/new') {
                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ParticipeEventController::newAction',  '_route' => 'favoris_new',);
            }

            // favoris_create
            if ($pathinfo === '/favoris/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_favoris_create;
                }

                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ParticipeEventController::createAction',  '_route' => 'favoris_create',);
            }
            not_favoris_create:

            // favoris_edit
            if (preg_match('#^/favoris/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ParticipeEventController::editAction',));
            }

            // favoris_update
            if (preg_match('#^/favoris/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_favoris_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ParticipeEventController::updateAction',));
            }
            not_favoris_update:

            // favoris_confirm
            if (preg_match('#^/favoris/(?P<id>[^/]++)/confirm$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_favoris_confirm;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_confirm')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ParticipeEventController::confirmAction',));
            }
            not_favoris_confirm:

            // favoris_delete
            if (preg_match('#^/favoris/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_favoris_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'favoris_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ParticipeEventController::deleteAction',));
            }
            not_favoris_delete:

            // add_favorite
            if (preg_match('#^/favoris/(?P<enabled>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_favorite')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\ParticipeEventController::addParticipeAction',));
            }

        }

        if (0 === strpos($pathinfo, '/like')) {
            if (0 === strpos($pathinfo, '/liked')) {
                // liked
                if (0 === strpos($pathinfo, '/liked/countlik') && preg_match('#^/liked/countlik/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'liked')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\LikedController::indexAction',));
                }

                // add_liked
                if (preg_match('#^/liked/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_liked')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\LikedController::addLikedAction',));
                }

                // del_liked
                if (0 === strpos($pathinfo, '/liked/dell') && preg_match('#^/liked/dell/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_del_liked;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'del_liked')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\LikedController::delLikedAction',));
                }
                not_del_liked:

                // test_liked
                if (0 === strpos($pathinfo, '/liked/count') && preg_match('#^/liked/count/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'test_liked')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\LikedController::testLikedAction',));
                }

            }

            if (0 === strpos($pathinfo, '/likem')) {
                // likem
                if (0 === strpos($pathinfo, '/likem/countlik') && preg_match('#^/likem/countlik/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'likem')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\LikemController::indexAction',));
                }

                // add_likem
                if (preg_match('#^/likem/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_likem')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\LikemController::addLikemAction',));
                }

                // del_likem
                if (0 === strpos($pathinfo, '/likem/del') && preg_match('#^/likem/del/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_del_likem;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'del_likem')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\LikemController::delLikemAction',));
                }
                not_del_likem:

                // test_likem
                if (0 === strpos($pathinfo, '/likem/count') && preg_match('#^/likem/count/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'test_likem')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\LikemController::testLikemAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/murs')) {
            // mures
            if (rtrim($pathinfo, '/') === '/murs') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'mures');
                }

                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MurController::murEventFriendsAction',  '_route' => 'mures',);
            }

            // mures_statut_delete
            if (preg_match('#^/murs/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'mures_statut_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\MurController::deleteAction',));
            }

        }

        if (0 === strpos($pathinfo, '/event')) {
            // event
            if (preg_match('#^/event(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::listEventAction',  '_locale' => 'fr',));
            }

            // event_confirmed
            if (preg_match('#^/event/(?P<slug>[^/]++)/confirmed(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_confirmed')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::confirmedAction',  '_locale' => 'fr',));
            }

            // event_user
            if (0 === strpos($pathinfo, '/event/competitions') && preg_match('#^/event/competitions/(?P<username>[^/]++)(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_user')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::listEventByUserAction',  '_locale' => 'fr',));
            }

            // event_search
            if (0 === strpos($pathinfo, '/event/search') && preg_match('#^/event/search(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_search')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::searchAction',  '_locale' => 'fr',));
            }

            // event_show
            if (preg_match('#^/event/(?P<slug>[^/]++)/show(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::showAction',  '_locale' => 'fr',));
            }

            // event_send_show
            if (preg_match('#^/event/(?P<slug>[^/]++)/send(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_send_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::sendEventAction',  '_locale' => 'fr',));
            }

            // event_send_notif_show
            if (preg_match('#^/event/(?P<slug>[^/]++)/sendnotif(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_send_notif_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::sendNotifEventAction',  '_locale' => 'fr',));
            }

            // event_send_mail_show
            if (preg_match('#^/event/(?P<slug>[^/]++)/sendMail(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_send_mail_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::sendEventMailAction',  '_locale' => 'fr',));
            }

            // event_new
            if (0 === strpos($pathinfo, '/event/new') && preg_match('#^/event/new(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_new')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::newAction',  '_locale' => 'fr',));
            }

            // event_create
            if ($pathinfo === '/event/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_event_create;
                }

                return array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::createAction',  '_route' => 'event_create',);
            }
            not_event_create:

            // event_edit
            if (preg_match('#^/event/(?P<id>[^/]++)/edit(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_edit')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::editAction',  '_locale' => 'fr',));
            }

            // event_update
            if (preg_match('#^/event/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_event_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_update')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::updateAction',));
            }
            not_event_update:

            // event_delete
            if (preg_match('#^/event/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_event_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_delete')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::deleteAction',));
            }
            not_event_delete:

            // events_file_create
            if (0 === strpos($pathinfo, '/event/createFile') && preg_match('#^/event/createFile/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'events_file_create')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::createFileAction',));
            }

            // events_statut_create
            if (0 === strpos($pathinfo, '/event/statut') && preg_match('#^/event/statut/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'events_statut_create')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::addStatutAction',));
            }

            // events_video_create
            if (0 === strpos($pathinfo, '/event/video') && preg_match('#^/event/video/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'events_video_create')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\EventController::addVideoAction',));
            }

        }

        if (0 === strpos($pathinfo, '/user_')) {
            if (0 === strpos($pathinfo, '/user_file')) {
                // user_file
                if (rtrim($pathinfo, '/') === '/user_file') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'user_file');
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::indexAction',  '_route' => 'user_file',);
                }

                // user_file_show
                if ($pathinfo === '/user_file/show') {
                    return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::showAction',  '_route' => 'user_file_show',);
                }

                // user_file_profile_logo
                if (preg_match('#^/user_file/(?P<id>[^/]++)/image$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_file_profile_logo')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::profileLogoAction',));
                }

                // user_file_new
                if ($pathinfo === '/user_file/new') {
                    return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::newAction',  '_route' => 'user_file_new',);
                }

                // user_file_create
                if ($pathinfo === '/user_file/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_user_file_create;
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::createAction',  '_route' => 'user_file_create',);
                }
                not_user_file_create:

                // user_file_edit
                if (preg_match('#^/user_file/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_file_edit')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::editAction',));
                }

                // user_file_update
                if (preg_match('#^/user_file/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_user_file_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_file_update')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::updateAction',));
                }
                not_user_file_update:

                if (0 === strpos($pathinfo, '/user_file/avatar')) {
                    if (0 === strpos($pathinfo, '/user_file/avatarFemme')) {
                        // user_file_avatarFemme1
                        if ($pathinfo === '/user_file/avatarFemme1') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme1Action',  '_route' => 'user_file_avatarFemme1',);
                        }

                        // user_file_avatarFemme2
                        if ($pathinfo === '/user_file/avatarFemme2') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme2Action',  '_route' => 'user_file_avatarFemme2',);
                        }

                        // user_file_avatarFemme3
                        if ($pathinfo === '/user_file/avatarFemme3') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme3Action',  '_route' => 'user_file_avatarFemme3',);
                        }

                        // user_file_avatarFemme4
                        if ($pathinfo === '/user_file/avatarFemme4') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme4Action',  '_route' => 'user_file_avatarFemme4',);
                        }

                        // user_file_avatarFemme5
                        if ($pathinfo === '/user_file/avatarFemme5') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme5Action',  '_route' => 'user_file_avatarFemme5',);
                        }

                        // user_file_avatarFemme6
                        if ($pathinfo === '/user_file/avatarFemme6') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme6Action',  '_route' => 'user_file_avatarFemme6',);
                        }

                        // user_file_avatarFemme7
                        if ($pathinfo === '/user_file/avatarFemme7') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme7Action',  '_route' => 'user_file_avatarFemme7',);
                        }

                        // user_file_avatarFemme8
                        if ($pathinfo === '/user_file/avatarFemme8') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme8Action',  '_route' => 'user_file_avatarFemme8',);
                        }

                        // user_file_avatarFemme9
                        if ($pathinfo === '/user_file/avatarFemme9') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme9Action',  '_route' => 'user_file_avatarFemme9',);
                        }

                        if (0 === strpos($pathinfo, '/user_file/avatarFemme1')) {
                            // user_file_avatarFemme10
                            if ($pathinfo === '/user_file/avatarFemme10') {
                                return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme10Action',  '_route' => 'user_file_avatarFemme10',);
                            }

                            // user_file_avatarFemme11
                            if ($pathinfo === '/user_file/avatarFemme11') {
                                return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarFemme11Action',  '_route' => 'user_file_avatarFemme11',);
                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/user_file/avatarHommes')) {
                        // user_file_avatarHommes1
                        if ($pathinfo === '/user_file/avatarHommes1') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes1Action',  '_route' => 'user_file_avatarHommes1',);
                        }

                        // user_file_avatarHommes2
                        if ($pathinfo === '/user_file/avatarHommes2') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes2Action',  '_route' => 'user_file_avatarHommes2',);
                        }

                        // user_file_avatarHommes3
                        if ($pathinfo === '/user_file/avatarHommes3') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes3Action',  '_route' => 'user_file_avatarHommes3',);
                        }

                        // user_file_avatarHommes4
                        if ($pathinfo === '/user_file/avatarHommes4') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes4Action',  '_route' => 'user_file_avatarHommes4',);
                        }

                        // user_file_avatarHommes5
                        if ($pathinfo === '/user_file/avatarHommes5') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes5Action',  '_route' => 'user_file_avatarHommes5',);
                        }

                        // user_file_avatarHommes6
                        if ($pathinfo === '/user_file/avatarHommes6') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes6Action',  '_route' => 'user_file_avatarHommes6',);
                        }

                        // user_file_avatarHommes7
                        if ($pathinfo === '/user_file/avatarHommes7') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes7Action',  '_route' => 'user_file_avatarHommes7',);
                        }

                        // user_file_avatarHommes8
                        if ($pathinfo === '/user_file/avatarHommes8') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes8Action',  '_route' => 'user_file_avatarHommes8',);
                        }

                        // user_file_avatarHommes9
                        if ($pathinfo === '/user_file/avatarHommes9') {
                            return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes9Action',  '_route' => 'user_file_avatarHommes9',);
                        }

                        if (0 === strpos($pathinfo, '/user_file/avatarHommes1')) {
                            // user_file_avatarHommes10
                            if ($pathinfo === '/user_file/avatarHommes10') {
                                return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes10Action',  '_route' => 'user_file_avatarHommes10',);
                            }

                            // user_file_avatarHommes11
                            if ($pathinfo === '/user_file/avatarHommes11') {
                                return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::avatarHommes11Action',  '_route' => 'user_file_avatarHommes11',);
                            }

                        }

                    }

                }

                // user_file_delete
                if (preg_match('#^/user_file/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_file_delete')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\FileController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/user_album')) {
                // user_album
                if (rtrim($pathinfo, '/') === '/user_album') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'user_album');
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\AlbumController::indexAction',  '_route' => 'user_album',);
                }

                // user_album_show
                if (preg_match('#^/user_album/(?P<slug>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_album_show')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\AlbumController::showAction',  'slug' => NULL,));
                }

                // user_album_new
                if ($pathinfo === '/user_album/new') {
                    return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\AlbumController::newAction',  '_route' => 'user_album_new',);
                }

                // user_album_create
                if ($pathinfo === '/user_album/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_user_album_create;
                    }

                    return array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\AlbumController::createAction',  '_route' => 'user_album_create',);
                }
                not_user_album_create:

                // user_file_news
                if (preg_match('#^/user_album/(?P<slug>[^/]++)/news$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_file_news')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\AlbumController::newsAction',  'slug' => NULL,));
                }

                // user_file_creates
                if (preg_match('#^/user_album/(?P<slug>[^/]++)/creates$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_user_file_creates;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_file_creates')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\AlbumController::createsAction',  'slug' => NULL,));
                }
                not_user_file_creates:

                // user_album_edit
                if (preg_match('#^/user_album/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_album_edit')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\AlbumController::editAction',));
                }

                // user_album_update
                if (preg_match('#^/user_album/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_user_album_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_album_update')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\AlbumController::updateAction',));
                }
                not_user_album_update:

                // user_album_delete
                if (preg_match('#^/user_album/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_user_album_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_album_delete')), array (  '_controller' => 'Comparator\\Bundle\\MultimediaBundle\\Controller\\AlbumController::deleteAction',));
                }
                not_user_album_delete:

            }

        }

        if (0 === strpos($pathinfo, '/statut_file')) {
            // event_file
            if (rtrim($pathinfo, '/') === '/statut_file') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'event_file');
                }

                return array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\FileController::indexAction',  '_route' => 'event_file',);
            }

            // event_file_show
            if (preg_match('#^/statut_file/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_file_show')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\FileController::showAction',));
            }

            // event_file_new
            if ($pathinfo === '/statut_file/new') {
                return array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\FileController::newAction',  '_route' => 'event_file_new',);
            }

            // event_file_create
            if ($pathinfo === '/statut_file/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_event_file_create;
                }

                return array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\FileController::createAction',  '_route' => 'event_file_create',);
            }
            not_event_file_create:

            // event_file_edit
            if (preg_match('#^/statut_file/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_file_edit')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\FileController::editAction',));
            }

            // event_file_update
            if (preg_match('#^/statut_file/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_event_file_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_file_update')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\FileController::updateAction',));
            }
            not_event_file_update:

            // event_file_delete
            if (preg_match('#^/statut_file/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_event_file_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'event_file_delete')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\FileController::deleteAction',));
            }
            not_event_file_delete:

        }

        if (0 === strpos($pathinfo, '/user_statut')) {
            // user_statut
            if (rtrim($pathinfo, '/') === '/user_statut') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'user_statut');
                }

                return array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\StatutController::indexAction',  '_route' => 'user_statut',);
            }

            // user_statut_show
            if (preg_match('#^/user_statut/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_statut_show')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\StatutController::showAction',));
            }

            // user_statut_new
            if ($pathinfo === '/user_statut/new') {
                return array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\StatutController::newAction',  '_route' => 'user_statut_new',);
            }

            // user_statut_create
            if ($pathinfo === '/user_statut/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_user_statut_create;
                }

                return array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\StatutController::createAction',  '_route' => 'user_statut_create',);
            }
            not_user_statut_create:

            // user_statut_edit
            if (preg_match('#^/user_statut/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_statut_edit')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\StatutController::editAction',));
            }

            // user_statut_update
            if (preg_match('#^/user_statut/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_user_statut_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_statut_update')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\StatutController::updateAction',));
            }
            not_user_statut_update:

            // user_statut_delete
            if (preg_match('#^/user_statut/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_user_statut_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_statut_delete')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\StatutController::deleteAction',));
            }
            not_user_statut_delete:

            // mur_statut_create
            if ($pathinfo === '/user_statut/statut') {
                return array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\StatutController::addStatutAction',  '_route' => 'mur_statut_create',);
            }

            // mur_video_create
            if ($pathinfo === '/user_statut/video') {
                return array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\StatutController::addVideoAction',  '_route' => 'mur_video_create',);
            }

        }

        // fos_user_security_login
        if ($pathinfo === '/api') {
            return array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\SecurityController::apiAction',  '_route' => 'fos_user_security_login',);
        }

        // fos_user_security_check
        if (preg_match('#^/(?P<_locale>[^/]++)/login_check$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_fos_user_security_check;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_security_check')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\SecurityController::checkAction',));
        }
        not_fos_user_security_check:

        // fos_user_security_logout
        if ($pathinfo === '/logout') {
            return array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
        }

        if (0 === strpos($pathinfo, '/resetting')) {
            // fos_user_resetting_request
            if ($pathinfo === '/resetting/request') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_request;
                }

                return array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_send_email
            if ($pathinfo === '/resetting/send-email') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fos_user_resetting_send_email;
                }

                return array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ($pathinfo === '/resetting/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_check_email;
                }

                return array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
            }
            not_fos_user_resetting_check_email:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_resetting_reset;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ResettingController::resetAction',));
            }
            not_fos_user_resetting_reset:

        }

        if (0 === strpos($pathinfo, '/profil')) {
            // fos_user_profile_show
            if (preg_match('#^/profil(?:/(?P<_locale>[^/]++))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_profile_show')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ProfileFOSUser1Controller::showAction',));
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit_authentication
            if (preg_match('#^/profil/(?P<_locale>[^/]++)/edit\\-authentication$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_profile_edit_authentication')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ProfileFOSUser1Controller::editAuthenticationAction',));
            }

            // fos_user_profile_edit
            if (preg_match('#^/profil/(?P<_locale>[^/]++)/edit\\-profile$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_profile_edit')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ProfileFOSUser1Controller::editProfileAction',));
            }

            // sonata_user_profile_show
            if (preg_match('#^/profil(?:/(?P<_locale>[^/]++))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_sonata_user_profile_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_user_profile_show')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ProfileFOSUser1Controller::showAction',));
            }
            not_sonata_user_profile_show:

            // sonata_user_profile_edit_authentication
            if (preg_match('#^/profil/(?P<_locale>[^/]++)/edit\\-authentication$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_user_profile_edit_authentication')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ProfileFOSUser1Controller::editAuthenticationAction',));
            }

            // sonata_user_profile_edit
            if (preg_match('#^/profil/(?P<_locale>[^/]++)/edit\\-profile$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_user_profile_edit')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ProfileFOSUser1Controller::editProfileAction',));
            }

        }

        if (0 === strpos($pathinfo, '/grintaaa_user')) {
            // grintaaa_user
            if (preg_match('#^/grintaaa_user/(?P<username>[^/]++)/(?P<id>[^/]++)/friends/show(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_user')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\UserController::indexAction',  '_locale' => 'fr',));
            }

            // grintaaa_user_show
            if (preg_match('#^/grintaaa_user/(?P<username>[^/]++)/(?P<id>[^/]++)/show(?:/(?P<_locale>fr|en))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'grintaaa_user_show')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\UserController::showAction',  '_locale' => 'fr',));
            }

        }

        if (0 === strpos($pathinfo, '/register')) {
            // fos_user_registration_register
            if (preg_match('#^/register(?:/(?P<_locale>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_register')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\RegistrationController::registerAction',));
            }

            // fos_user_registration_check_email
            if (preg_match('#^/register/(?P<_locale>[^/]++)/check\\-email$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_check_email;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_check_email')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\RegistrationController::checkEmailAction',));
            }
            not_fos_user_registration_check_email:

            // fos_user_registration_confirm
            if (preg_match('#^/register/(?P<_locale>[^/]++)/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirm;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\RegistrationController::confirmAction',));
            }
            not_fos_user_registration_confirm:

            // fos_user_registration_confirmed
            if (preg_match('#^/register/(?P<_locale>[^/]++)/confirmed$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirmed;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirmed')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\RegistrationController::confirmedAction',));
            }
            not_fos_user_registration_confirmed:

            // fos_user_registration_register1
            if (0 === strpos($pathinfo, '/register1') && preg_match('#^/register1(?:/(?P<_locale>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_register1')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\RegistrationController::register1Action',));
            }

            // fos_user_registration_register2
            if (0 === strpos($pathinfo, '/register2') && preg_match('#^/register2(?:/(?P<_locale>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_register2')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\RegistrationController::register2Action',));
            }

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_change_password
            if (preg_match('#^/profile/(?P<_locale>[^/]++)/change\\-password$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_change_password;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_change_password')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ChangePasswordFOSUser1Controller::changePasswordAction',));
            }
            not_fos_user_change_password:

            // sonata_user_change_password
            if (preg_match('#^/profile/(?P<_locale>[^/]++)/change\\-password$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_sonata_user_change_password;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_user_change_password')), array (  '_locale' => 'fr',  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\ChangePasswordFOSUser1Controller::changePasswordAction',));
            }
            not_sonata_user_change_password:

        }

        if (0 === strpos($pathinfo, '/sonata')) {
            if (0 === strpos($pathinfo, '/sonata/cache')) {
                // sonata_cache_esi
                if (0 === strpos($pathinfo, '/sonata/cache/esi') && preg_match('#^/sonata/cache/esi/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_cache_esi')), array (  '_controller' => 'sonata.cache.esi:cacheAction',));
                }

                // sonata_cache_ssi
                if (0 === strpos($pathinfo, '/sonata/cache/ssi') && preg_match('#^/sonata/cache/ssi/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_cache_ssi')), array (  '_controller' => 'sonata.cache.ssi:cacheAction',));
                }

                if (0 === strpos($pathinfo, '/sonata/cache/js-')) {
                    // sonata_cache_js_async
                    if ($pathinfo === '/sonata/cache/js-async') {
                        return array (  '_controller' => 'sonata.cache.js_async:cacheAction',  '_route' => 'sonata_cache_js_async',);
                    }

                    // sonata_cache_js_sync
                    if ($pathinfo === '/sonata/cache/js-sync') {
                        return array (  '_controller' => 'sonata.cache.js_sync:cacheAction',  '_route' => 'sonata_cache_js_sync',);
                    }

                }

                // sonata_cache_apc
                if (0 === strpos($pathinfo, '/sonata/cache/apc') && preg_match('#^/sonata/cache/apc/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_cache_apc')), array (  '_controller' => 'sonata.cache.apc:cacheAction',));
                }

                // sonata_cache_symfony
                if (0 === strpos($pathinfo, '/sonata/cache/symfony') && preg_match('#^/sonata/cache/symfony/(?P<token>[^/]++)/(?P<type>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_cache_symfony')), array (  '_controller' => 'sonata.cache.symfony:cacheAction',));
                }

            }

            if (0 === strpos($pathinfo, '/sonata/page/cache')) {
                // sonata_page_cache_esi
                if (0 === strpos($pathinfo, '/sonata/page/cache/esi') && preg_match('#^/sonata/page/cache/esi/(?P<_token>[^/]++)/(?P<manager>[^/]++)/(?P<page_id>[^/]++)/(?P<block_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_page_cache_esi')), array (  '_controller' => 'sonata.page.cache.esi:cacheAction',));
                }

                // sonata_page_cache_ssi
                if (0 === strpos($pathinfo, '/sonata/page/cache/ssi') && preg_match('#^/sonata/page/cache/ssi/(?P<_token>[^/]++)/(?P<manager>[^/]++)/(?P<page_id>[^/]++)/(?P<block_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_page_cache_ssi')), array (  '_controller' => 'sonata.page.cache.ssi:cacheAction',));
                }

                if (0 === strpos($pathinfo, '/sonata/page/cache/js-')) {
                    // sonata_page_js_async_cache
                    if ($pathinfo === '/sonata/page/cache/js-async') {
                        return array (  '_controller' => 'sonata.page.cache.js_async:cacheAction',  '_route' => 'sonata_page_js_async_cache',);
                    }

                    // sonata_page_js_sync_cache
                    if ($pathinfo === '/sonata/page/cache/js-sync') {
                        return array (  '_controller' => 'sonata.page.cache.js_sync:cacheAction',  '_route' => 'sonata_page_js_sync_cache',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/page/exceptions')) {
            // sonata_page_exceptions_list
            if ($pathinfo === '/page/exceptions/list') {
                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageController::exceptionsListAction',  '_route' => 'sonata_page_exceptions_list',);
            }

            // sonata_page_exceptions_edit
            if (0 === strpos($pathinfo, '/page/exceptions/edit') && preg_match('#^/page/exceptions/edit/(?P<code>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_page_exceptions_edit')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageController::exceptionEditAction',));
            }

        }

        if (0 === strpos($pathinfo, '/media')) {
            if (0 === strpos($pathinfo, '/media/gallery')) {
                // sonata_media_gallery_index
                if (rtrim($pathinfo, '/') === '/media/gallery') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'sonata_media_gallery_index');
                    }

                    return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryController::indexAction',  '_route' => 'sonata_media_gallery_index',);
                }

                // sonata_media_gallery_view
                if (0 === strpos($pathinfo, '/media/gallery/view') && preg_match('#^/media/gallery/view/(?P<id>.*)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_gallery_view')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryController::viewAction',));
                }

            }

            // sonata_media_view
            if (0 === strpos($pathinfo, '/media/view') && preg_match('#^/media/view/(?P<id>[^/]++)(?:/(?P<format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_view')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaController::viewAction',  'format' => 'reference',));
            }

            // sonata_media_download
            if (0 === strpos($pathinfo, '/media/download') && preg_match('#^/media/download/(?P<id>.*)(?:/(?P<format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_download')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaController::downloadAction',  'format' => 'reference',));
            }

        }

        if (0 === strpos($pathinfo, '/blog')) {
            if (0 === strpos($pathinfo, '/blog/a')) {
                // sonata_news_add_comment
                if (0 === strpos($pathinfo, '/blog/add-comment') && preg_match('#^/blog/add\\-comment/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_news_add_comment')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\PostController::addCommentAction',));
                }

                // sonata_news_archive_monthly
                if (0 === strpos($pathinfo, '/blog/archive') && preg_match('#^/blog/archive/(?P<year>\\d+)/(?P<month>\\d+)(?:\\.(?P<_format>html|rss))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_news_archive_monthly')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\PostController::archiveMonthlyAction',  '_format' => 'html',));
                }

            }

            // sonata_news_tag
            if (0 === strpos($pathinfo, '/blog/tag') && preg_match('#^/blog/tag/(?P<tag>[^/\\.]++)(?:\\.(?P<_format>html|rss))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_news_tag')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\PostController::tagAction',  '_format' => 'html',));
            }

            // sonata_news_collection
            if (0 === strpos($pathinfo, '/blog/collection') && preg_match('#^/blog/collection/(?P<collection>[^/\\.]++)(?:\\.(?P<_format>html|rss))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_news_collection')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\PostController::collectionAction',  '_format' => 'html',));
            }

            if (0 === strpos($pathinfo, '/blog/archive')) {
                // sonata_news_archive_yearly
                if (preg_match('#^/blog/archive/(?P<year>\\d+)(?:\\.(?P<_format>html|rss))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_news_archive_yearly')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\PostController::archiveYearlyAction',  '_format' => 'html',));
                }

                // sonata_news_archive
                if (preg_match('#^/blog/archive(?:\\.(?P<_format>html|rss))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_news_archive')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\PostController::archiveAction',  '_format' => 'html',));
                }

            }

            // sonata_news_comment_moderation
            if (0 === strpos($pathinfo, '/blog/comment/moderation') && preg_match('#^/blog/comment/moderation/(?P<commentId>[^/]++)/(?P<hash>[^/]++)/(?P<status>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_news_comment_moderation')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\PostController::commentModerationAction',));
            }

            // sonata_news_view
            if (preg_match('#^/blog/(?P<permalink>.+?)(?:\\.(?P<_format>html|rss))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_news_view')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\PostController::viewAction',  '_format' => 'html',));
            }

            // sonata_news_home
            if (rtrim($pathinfo, '/') === '/blog') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'sonata_news_home');
                }

                return array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\PostController::homeAction',  '_route' => 'sonata_news_home',);
            }

        }

        if (0 === strpos($pathinfo, '/connect')) {
            // hwi_oauth_service_redirect
            if (preg_match('#^/connect/(?P<service>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'hwi_oauth_service_redirect')), array (  '_controller' => 'Application\\OAuthBundle\\Controller\\ConnectController::redirectToServiceAction',));
            }

            // hwi_oauth_connect
            if (rtrim($pathinfo, '/') === '/connect') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'hwi_oauth_connect');
                }

                return array (  '_controller' => 'Application\\OAuthBundle\\Controller\\ConnectController::connectAction',  '_route' => 'hwi_oauth_connect',);
            }

            // hwi_oauth_connect_service
            if (0 === strpos($pathinfo, '/connect/service') && preg_match('#^/connect/service/(?P<service>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'hwi_oauth_connect_service')), array (  '_controller' => 'Application\\OAuthBundle\\Controller\\ConnectController::connectServiceAction',));
            }

            // hwi_oauth_connect_registration
            if (0 === strpos($pathinfo, '/connect/registration') && preg_match('#^/connect/registration/(?P<key>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'hwi_oauth_connect_registration')), array (  '_controller' => 'Application\\OAuthBundle\\Controller\\ConnectController::registrationAction',));
            }

        }

        // hwi_facebook_login
        if ($pathinfo === '/login/check-facebook') {
            return array('_route' => 'hwi_facebook_login');
        }

        if (0 === strpos($pathinfo, '/comments/threads')) {
            // fos_comment_new_threads
            if (0 === strpos($pathinfo, '/comments/threads/new') && preg_match('#^/comments/threads/new(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_new_threads;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_new_threads')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::newThreadsAction',  '_format' => 'html',));
            }
            not_fos_comment_new_threads:

            // fos_comment_edit_thread_commentable
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/commentable/edit(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_edit_thread_commentable;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_edit_thread_commentable')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::editThreadCommentableAction',  '_format' => 'html',));
            }
            not_fos_comment_edit_thread_commentable:

            // fos_comment_new_thread_comments
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments/new(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_new_thread_comments;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_new_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::newThreadCommentsAction',  '_format' => 'html',));
            }
            not_fos_comment_new_thread_comments:

            // fos_comment_remove_thread_comment
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/remove(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_remove_thread_comment;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_remove_thread_comment')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::removeThreadCommentAction',  '_format' => 'html',));
            }
            not_fos_comment_remove_thread_comment:

            // fos_comment_edit_thread_comment
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/edit(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_edit_thread_comment;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_edit_thread_comment')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::editThreadCommentAction',  '_format' => 'html',));
            }
            not_fos_comment_edit_thread_comment:

            // fos_comment_new_thread_comment_votes
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/votes/new(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_new_thread_comment_votes;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_new_thread_comment_votes')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::newThreadCommentVotesAction',  '_format' => 'html',));
            }
            not_fos_comment_new_thread_comment_votes:

            // fos_comment_get_thread
            if (preg_match('#^/comments/threads/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_get_thread;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadAction',  '_format' => 'html',));
            }
            not_fos_comment_get_thread:

            // fos_comment_get_threads
            if (preg_match('#^/comments/threads(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_get_threads;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_threads')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadsActions',  '_format' => 'html',));
            }
            not_fos_comment_get_threads:

            // fos_comment_post_threads
            if (preg_match('#^/comments/threads(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fos_comment_post_threads;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_post_threads')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::postThreadsAction',  '_format' => 'html',));
            }
            not_fos_comment_post_threads:

            // fos_comment_patch_thread_commentable
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/commentable(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PATCH') {
                    $allow[] = 'PATCH';
                    goto not_fos_comment_patch_thread_commentable;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_patch_thread_commentable')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::patchThreadCommentableAction',  '_format' => 'html',));
            }
            not_fos_comment_patch_thread_commentable:

            // fos_comment_get_thread_comment
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_get_thread_comment;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread_comment')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadCommentAction',  '_format' => 'html',));
            }
            not_fos_comment_get_thread_comment:

            // fos_comment_patch_thread_comment_state
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/state(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PATCH') {
                    $allow[] = 'PATCH';
                    goto not_fos_comment_patch_thread_comment_state;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_patch_thread_comment_state')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::patchThreadCommentStateAction',  '_format' => 'html',));
            }
            not_fos_comment_patch_thread_comment_state:

            // fos_comment_put_thread_comments
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_fos_comment_put_thread_comments;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_put_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::putThreadCommentsAction',  '_format' => 'html',));
            }
            not_fos_comment_put_thread_comments:

            // fos_comment_get_thread_comments
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_get_thread_comments;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadCommentsAction',  '_format' => 'html',));
            }
            not_fos_comment_get_thread_comments:

            // fos_comment_post_thread_comments
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fos_comment_post_thread_comments;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_post_thread_comments')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::postThreadCommentsAction',  '_format' => 'html',));
            }
            not_fos_comment_post_thread_comments:

            // fos_comment_get_thread_comment_votes
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/votes(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_comment_get_thread_comment_votes;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_get_thread_comment_votes')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::getThreadCommentVotesAction',  '_format' => 'html',));
            }
            not_fos_comment_get_thread_comment_votes:

            // fos_comment_post_thread_comment_votes
            if (preg_match('#^/comments/threads/(?P<id>[^/]++)/comments/(?P<commentId>[^/]++)/votes(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fos_comment_post_thread_comment_votes;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_comment_post_thread_comment_votes')), array (  '_controller' => 'FOS\\CommentBundle\\Controller\\ThreadController::postThreadCommentVotesAction',  '_format' => 'html',));
            }
            not_fos_comment_post_thread_comment_votes:

        }

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/admin')) {
                // sonata_admin_redirect
                if (rtrim($pathinfo, '/') === '/admin') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'sonata_admin_redirect');
                    }

                    return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonata_admin_dashboard',  'permanent' => 'true',  '_route' => 'sonata_admin_redirect',);
                }

                // sonata_admin_dashboard
                if ($pathinfo === '/admin/dashboard') {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard',);
                }

                if (0 === strpos($pathinfo, '/admin/core')) {
                    // sonata_admin_retrieve_form_element
                    if ($pathinfo === '/admin/core/get-form-field-element') {
                        return array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  '_route' => 'sonata_admin_retrieve_form_element',);
                    }

                    // sonata_admin_append_form_element
                    if ($pathinfo === '/admin/core/append-form-field-element') {
                        return array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  '_route' => 'sonata_admin_append_form_element',);
                    }

                    // sonata_admin_short_object_information
                    if (0 === strpos($pathinfo, '/admin/core/get-short-object-description') && preg_match('#^/admin/core/get\\-short\\-object\\-description(?:\\.(?P<_format>html|json))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_short_object_information')), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_format' => 'html',));
                    }

                    // sonata_admin_set_object_field_value
                    if ($pathinfo === '/admin/core/set-object-field-value') {
                        return array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  '_route' => 'sonata_admin_set_object_field_value',);
                    }

                }

                // sonata_admin_search
                if ($pathinfo === '/admin/search') {
                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',  '_route' => 'sonata_admin_search',);
                }

                // sonata_admin_retrieve_autocomplete_items
                if ($pathinfo === '/admin/core/get-autocomplete-items') {
                    return array (  '_controller' => 'sonata.admin.controller.admin:retrieveAutocompleteItemsAction',  '_route' => 'sonata_admin_retrieve_autocomplete_items',);
                }

                if (0 === strpos($pathinfo, '/admin/sonata')) {
                    if (0 === strpos($pathinfo, '/admin/sonata/user')) {
                        if (0 === strpos($pathinfo, '/admin/sonata/user/user')) {
                            // admin_sonata_user_user_list
                            if ($pathinfo === '/admin/sonata/user/user/list') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_list',  '_route' => 'admin_sonata_user_user_list',);
                            }

                            // admin_sonata_user_user_create
                            if ($pathinfo === '/admin/sonata/user/user/create') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_create',  '_route' => 'admin_sonata_user_user_create',);
                            }

                            // admin_sonata_user_user_batch
                            if ($pathinfo === '/admin/sonata/user/user/batch') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_batch',  '_route' => 'admin_sonata_user_user_batch',);
                            }

                            // admin_sonata_user_user_edit
                            if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_edit',));
                            }

                            // admin_sonata_user_user_delete
                            if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_delete',));
                            }

                            // admin_sonata_user_user_show
                            if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_show',));
                            }

                            // admin_sonata_user_user_export
                            if ($pathinfo === '/admin/sonata/user/user/export') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_export',  '_route' => 'admin_sonata_user_user_export',);
                            }

                            // admin_sonata_user_user_history
                            if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_history',));
                            }

                            // admin_sonata_user_user_history_view_revision
                            if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_history_view_revision',));
                            }

                            // admin_sonata_user_user_history_compare_revisions
                            if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_history_compare_revisions',));
                            }

                            // admin_sonata_user_user_acl
                            if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_acl',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/sonata/user/group')) {
                            // admin_sonata_user_group_list
                            if ($pathinfo === '/admin/sonata/user/group/list') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_list',  '_route' => 'admin_sonata_user_group_list',);
                            }

                            // admin_sonata_user_group_create
                            if ($pathinfo === '/admin/sonata/user/group/create') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_create',  '_route' => 'admin_sonata_user_group_create',);
                            }

                            // admin_sonata_user_group_batch
                            if ($pathinfo === '/admin/sonata/user/group/batch') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_batch',  '_route' => 'admin_sonata_user_group_batch',);
                            }

                            // admin_sonata_user_group_edit
                            if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_edit',));
                            }

                            // admin_sonata_user_group_delete
                            if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_delete',));
                            }

                            // admin_sonata_user_group_show
                            if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_show',));
                            }

                            // admin_sonata_user_group_export
                            if ($pathinfo === '/admin/sonata/user/group/export') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_export',  '_route' => 'admin_sonata_user_group_export',);
                            }

                            // admin_sonata_user_group_history
                            if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_history',));
                            }

                            // admin_sonata_user_group_history_view_revision
                            if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_history_view_revision',));
                            }

                            // admin_sonata_user_group_history_compare_revisions
                            if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_history_compare_revisions',));
                            }

                            // admin_sonata_user_group_acl
                            if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_acl',));
                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/sonata/page')) {
                        if (0 === strpos($pathinfo, '/admin/sonata/page/page')) {
                            // admin_sonata_page_page_list
                            if ($pathinfo === '/admin/sonata/page/page/list') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::listAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_list',  '_route' => 'admin_sonata_page_page_list',);
                            }

                            // admin_sonata_page_page_create
                            if ($pathinfo === '/admin/sonata/page/page/create') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::createAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_create',  '_route' => 'admin_sonata_page_page_create',);
                            }

                            // admin_sonata_page_page_batch
                            if ($pathinfo === '/admin/sonata/page/page/batch') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::batchAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_batch',  '_route' => 'admin_sonata_page_page_batch',);
                            }

                            // admin_sonata_page_page_edit
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_edit')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::editAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_edit',));
                            }

                            // admin_sonata_page_page_delete
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_delete')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::deleteAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_delete',));
                            }

                            // admin_sonata_page_page_show
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_show')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::showAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_show',));
                            }

                            // admin_sonata_page_page_export
                            if ($pathinfo === '/admin/sonata/page/page/export') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::exportAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_export',  '_route' => 'admin_sonata_page_page_export',);
                            }

                            // admin_sonata_page_page_history
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_history')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::historyAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_history',));
                            }

                            // admin_sonata_page_page_history_view_revision
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_history_view_revision')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_history_view_revision',));
                            }

                            // admin_sonata_page_page_history_compare_revisions
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_history_compare_revisions')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_history_compare_revisions',));
                            }

                            // admin_sonata_page_page_acl
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_acl')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::aclAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_acl',));
                            }

                            // admin_sonata_page_page_block_list
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/list$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_list')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::listAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_list',));
                            }

                            // admin_sonata_page_page_block_create
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/create$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_create')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::createAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_create',));
                            }

                            // admin_sonata_page_page_block_batch
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/batch$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_batch')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::batchAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_batch',));
                            }

                            // admin_sonata_page_page_block_edit
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/(?P<childId>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_edit')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::editAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_edit',));
                            }

                            // admin_sonata_page_page_block_delete
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/(?P<childId>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_delete')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::deleteAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_delete',));
                            }

                            // admin_sonata_page_page_block_show
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/(?P<childId>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_show')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::showAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_show',));
                            }

                            // admin_sonata_page_page_block_export
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/export$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_export')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::exportAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_export',));
                            }

                            // admin_sonata_page_page_block_history
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/(?P<childId>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_history')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::historyAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_history',));
                            }

                            // admin_sonata_page_page_block_history_view_revision
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/(?P<childId>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_history_view_revision')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_history_view_revision',));
                            }

                            // admin_sonata_page_page_block_history_compare_revisions
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/(?P<childId>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_history_compare_revisions')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_history_compare_revisions',));
                            }

                            // admin_sonata_page_page_block_acl
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/(?P<childId>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_acl')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::aclAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_acl',));
                            }

                            // admin_sonata_page_page_block_savePosition
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/save\\-position$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_savePosition')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::savePositionAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_savePosition',));
                            }

                            // admin_sonata_page_page_block_view
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/(?P<childId>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_view')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::viewAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_view',));
                            }

                            // admin_sonata_page_page_block_switchParent
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/block/switch\\-parent$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_block_switchParent')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::switchParentAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_page_block_switchParent',));
                            }

                            // admin_sonata_page_page_snapshot_list
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/list$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_list')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::listAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_list',));
                            }

                            // admin_sonata_page_page_snapshot_create
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/create$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_create')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::createAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_create',));
                            }

                            // admin_sonata_page_page_snapshot_batch
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/batch$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_batch')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::batchAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_batch',));
                            }

                            // admin_sonata_page_page_snapshot_edit
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/(?P<childId>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_edit')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::editAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_edit',));
                            }

                            // admin_sonata_page_page_snapshot_delete
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/(?P<childId>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_delete')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::deleteAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_delete',));
                            }

                            // admin_sonata_page_page_snapshot_show
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/(?P<childId>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_show')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::showAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_show',));
                            }

                            // admin_sonata_page_page_snapshot_export
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/export$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_export')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::exportAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_export',));
                            }

                            // admin_sonata_page_page_snapshot_history
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/(?P<childId>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_history')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::historyAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_history',));
                            }

                            // admin_sonata_page_page_snapshot_history_view_revision
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/(?P<childId>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_history_view_revision')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_history_view_revision',));
                            }

                            // admin_sonata_page_page_snapshot_history_compare_revisions
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/(?P<childId>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_history_compare_revisions')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_history_compare_revisions',));
                            }

                            // admin_sonata_page_page_snapshot_acl
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/snapshot/(?P<childId>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_snapshot_acl')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::aclAction',  '_sonata_admin' => 'sonata.page.admin.page|sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_page_snapshot_acl',));
                            }

                            // admin_sonata_page_page_compose
                            if (preg_match('#^/admin/sonata/page/page/(?P<id>[^/]++)/compose$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_compose')), array (  'id' => NULL,  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::composeAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_compose',));
                            }

                            // admin_sonata_page_page_compose_container_show
                            if (0 === strpos($pathinfo, '/admin/sonata/page/page/compose/container') && preg_match('#^/admin/sonata/page/page/compose/container(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_page_compose_container_show')), array (  'id' => NULL,  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::composeContainerShowAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_compose_container_show',));
                            }

                            // admin_sonata_page_page_tree
                            if ($pathinfo === '/admin/sonata/page/page/tree') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\PageAdminController::treeAction',  '_sonata_admin' => 'sonata.page.admin.page',  '_sonata_name' => 'admin_sonata_page_page_tree',  '_route' => 'admin_sonata_page_page_tree',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/sonata/page/block')) {
                            // admin_sonata_page_block_list
                            if ($pathinfo === '/admin/sonata/page/block/list') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::listAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_list',  '_route' => 'admin_sonata_page_block_list',);
                            }

                            // admin_sonata_page_block_create
                            if ($pathinfo === '/admin/sonata/page/block/create') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::createAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_create',  '_route' => 'admin_sonata_page_block_create',);
                            }

                            // admin_sonata_page_block_batch
                            if ($pathinfo === '/admin/sonata/page/block/batch') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::batchAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_batch',  '_route' => 'admin_sonata_page_block_batch',);
                            }

                            // admin_sonata_page_block_edit
                            if (preg_match('#^/admin/sonata/page/block/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_block_edit')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::editAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_edit',));
                            }

                            // admin_sonata_page_block_delete
                            if (preg_match('#^/admin/sonata/page/block/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_block_delete')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::deleteAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_delete',));
                            }

                            // admin_sonata_page_block_show
                            if (preg_match('#^/admin/sonata/page/block/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_block_show')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::showAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_show',));
                            }

                            // admin_sonata_page_block_export
                            if ($pathinfo === '/admin/sonata/page/block/export') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::exportAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_export',  '_route' => 'admin_sonata_page_block_export',);
                            }

                            // admin_sonata_page_block_history
                            if (preg_match('#^/admin/sonata/page/block/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_block_history')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::historyAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_history',));
                            }

                            // admin_sonata_page_block_history_view_revision
                            if (preg_match('#^/admin/sonata/page/block/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_block_history_view_revision')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_history_view_revision',));
                            }

                            // admin_sonata_page_block_history_compare_revisions
                            if (preg_match('#^/admin/sonata/page/block/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_block_history_compare_revisions')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_history_compare_revisions',));
                            }

                            // admin_sonata_page_block_acl
                            if (preg_match('#^/admin/sonata/page/block/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_block_acl')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::aclAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_acl',));
                            }

                            // admin_sonata_page_block_savePosition
                            if ($pathinfo === '/admin/sonata/page/block/save-position') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::savePositionAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_savePosition',  '_route' => 'admin_sonata_page_block_savePosition',);
                            }

                            // admin_sonata_page_block_view
                            if (preg_match('#^/admin/sonata/page/block/(?P<id>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_block_view')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::viewAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_view',));
                            }

                            // admin_sonata_page_block_switchParent
                            if ($pathinfo === '/admin/sonata/page/block/switch-parent') {
                                return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\BlockAdminController::switchParentAction',  '_sonata_admin' => 'sonata.page.admin.block',  '_sonata_name' => 'admin_sonata_page_block_switchParent',  '_route' => 'admin_sonata_page_block_switchParent',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/sonata/page/s')) {
                            if (0 === strpos($pathinfo, '/admin/sonata/page/snapshot')) {
                                // admin_sonata_page_snapshot_list
                                if ($pathinfo === '/admin/sonata/page/snapshot/list') {
                                    return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::listAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_list',  '_route' => 'admin_sonata_page_snapshot_list',);
                                }

                                // admin_sonata_page_snapshot_create
                                if ($pathinfo === '/admin/sonata/page/snapshot/create') {
                                    return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::createAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_create',  '_route' => 'admin_sonata_page_snapshot_create',);
                                }

                                // admin_sonata_page_snapshot_batch
                                if ($pathinfo === '/admin/sonata/page/snapshot/batch') {
                                    return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::batchAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_batch',  '_route' => 'admin_sonata_page_snapshot_batch',);
                                }

                                // admin_sonata_page_snapshot_edit
                                if (preg_match('#^/admin/sonata/page/snapshot/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_snapshot_edit')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::editAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_edit',));
                                }

                                // admin_sonata_page_snapshot_delete
                                if (preg_match('#^/admin/sonata/page/snapshot/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_snapshot_delete')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::deleteAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_delete',));
                                }

                                // admin_sonata_page_snapshot_show
                                if (preg_match('#^/admin/sonata/page/snapshot/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_snapshot_show')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::showAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_show',));
                                }

                                // admin_sonata_page_snapshot_export
                                if ($pathinfo === '/admin/sonata/page/snapshot/export') {
                                    return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::exportAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_export',  '_route' => 'admin_sonata_page_snapshot_export',);
                                }

                                // admin_sonata_page_snapshot_history
                                if (preg_match('#^/admin/sonata/page/snapshot/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_snapshot_history')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::historyAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_history',));
                                }

                                // admin_sonata_page_snapshot_history_view_revision
                                if (preg_match('#^/admin/sonata/page/snapshot/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_snapshot_history_view_revision')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_history_view_revision',));
                                }

                                // admin_sonata_page_snapshot_history_compare_revisions
                                if (preg_match('#^/admin/sonata/page/snapshot/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_snapshot_history_compare_revisions')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_history_compare_revisions',));
                                }

                                // admin_sonata_page_snapshot_acl
                                if (preg_match('#^/admin/sonata/page/snapshot/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_snapshot_acl')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SnapshotAdminController::aclAction',  '_sonata_admin' => 'sonata.page.admin.snapshot',  '_sonata_name' => 'admin_sonata_page_snapshot_acl',));
                                }

                            }

                            if (0 === strpos($pathinfo, '/admin/sonata/page/site')) {
                                // admin_sonata_page_site_list
                                if ($pathinfo === '/admin/sonata/page/site/list') {
                                    return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::listAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_list',  '_route' => 'admin_sonata_page_site_list',);
                                }

                                // admin_sonata_page_site_create
                                if ($pathinfo === '/admin/sonata/page/site/create') {
                                    return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::createAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_create',  '_route' => 'admin_sonata_page_site_create',);
                                }

                                // admin_sonata_page_site_batch
                                if ($pathinfo === '/admin/sonata/page/site/batch') {
                                    return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::batchAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_batch',  '_route' => 'admin_sonata_page_site_batch',);
                                }

                                // admin_sonata_page_site_edit
                                if (preg_match('#^/admin/sonata/page/site/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_site_edit')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::editAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_edit',));
                                }

                                // admin_sonata_page_site_delete
                                if (preg_match('#^/admin/sonata/page/site/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_site_delete')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::deleteAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_delete',));
                                }

                                // admin_sonata_page_site_show
                                if (preg_match('#^/admin/sonata/page/site/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_site_show')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::showAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_show',));
                                }

                                // admin_sonata_page_site_export
                                if ($pathinfo === '/admin/sonata/page/site/export') {
                                    return array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::exportAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_export',  '_route' => 'admin_sonata_page_site_export',);
                                }

                                // admin_sonata_page_site_history
                                if (preg_match('#^/admin/sonata/page/site/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_site_history')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::historyAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_history',));
                                }

                                // admin_sonata_page_site_history_view_revision
                                if (preg_match('#^/admin/sonata/page/site/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_site_history_view_revision')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_history_view_revision',));
                                }

                                // admin_sonata_page_site_history_compare_revisions
                                if (preg_match('#^/admin/sonata/page/site/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_site_history_compare_revisions')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_history_compare_revisions',));
                                }

                                // admin_sonata_page_site_acl
                                if (preg_match('#^/admin/sonata/page/site/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_site_acl')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::aclAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_acl',));
                                }

                                // admin_sonata_page_site_snapshots
                                if (preg_match('#^/admin/sonata/page/site/(?P<id>[^/]++)/snapshots$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_page_site_snapshots')), array (  '_controller' => 'Sonata\\PageBundle\\Controller\\SiteAdminController::snapshotsAction',  '_sonata_admin' => 'sonata.page.admin.site',  '_sonata_name' => 'admin_sonata_page_site_snapshots',));
                                }

                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/sonata/news')) {
                        if (0 === strpos($pathinfo, '/admin/sonata/news/post')) {
                            // admin_sonata_news_post_list
                            if ($pathinfo === '/admin/sonata/news/post/list') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_list',  '_route' => 'admin_sonata_news_post_list',);
                            }

                            // admin_sonata_news_post_create
                            if ($pathinfo === '/admin/sonata/news/post/create') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_create',  '_route' => 'admin_sonata_news_post_create',);
                            }

                            // admin_sonata_news_post_batch
                            if ($pathinfo === '/admin/sonata/news/post/batch') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_batch',  '_route' => 'admin_sonata_news_post_batch',);
                            }

                            // admin_sonata_news_post_edit
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_edit',));
                            }

                            // admin_sonata_news_post_delete
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_delete',));
                            }

                            // admin_sonata_news_post_show
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_show',));
                            }

                            // admin_sonata_news_post_export
                            if ($pathinfo === '/admin/sonata/news/post/export') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_export',  '_route' => 'admin_sonata_news_post_export',);
                            }

                            // admin_sonata_news_post_history
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_history',));
                            }

                            // admin_sonata_news_post_history_view_revision
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_history_view_revision',));
                            }

                            // admin_sonata_news_post_history_compare_revisions
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_history_compare_revisions',));
                            }

                            // admin_sonata_news_post_acl
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.news.admin.post',  '_sonata_name' => 'admin_sonata_news_post_acl',));
                            }

                            // admin_sonata_news_post_comment_list
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/list$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_list')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::listAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_list',));
                            }

                            // admin_sonata_news_post_comment_create
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/create$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_create')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::createAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_create',));
                            }

                            // admin_sonata_news_post_comment_batch
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/batch$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_batch')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::batchAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_batch',));
                            }

                            // admin_sonata_news_post_comment_edit
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_edit')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::editAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_edit',));
                            }

                            // admin_sonata_news_post_comment_delete
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_delete')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::deleteAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_delete',));
                            }

                            // admin_sonata_news_post_comment_show
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_show')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::showAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_show',));
                            }

                            // admin_sonata_news_post_comment_export
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/export$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_export')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::exportAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_export',));
                            }

                            // admin_sonata_news_post_comment_history
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_history')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::historyAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_history',));
                            }

                            // admin_sonata_news_post_comment_history_view_revision
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_history_view_revision')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_history_view_revision',));
                            }

                            // admin_sonata_news_post_comment_history_compare_revisions
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_history_compare_revisions')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_history_compare_revisions',));
                            }

                            // admin_sonata_news_post_comment_acl
                            if (preg_match('#^/admin/sonata/news/post/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_post_comment_acl')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::aclAction',  '_sonata_admin' => 'sonata.news.admin.post|sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_post_comment_acl',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/sonata/news/comment')) {
                            // admin_sonata_news_comment_list
                            if ($pathinfo === '/admin/sonata/news/comment/list') {
                                return array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::listAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_list',  '_route' => 'admin_sonata_news_comment_list',);
                            }

                            // admin_sonata_news_comment_create
                            if ($pathinfo === '/admin/sonata/news/comment/create') {
                                return array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::createAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_create',  '_route' => 'admin_sonata_news_comment_create',);
                            }

                            // admin_sonata_news_comment_batch
                            if ($pathinfo === '/admin/sonata/news/comment/batch') {
                                return array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::batchAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_batch',  '_route' => 'admin_sonata_news_comment_batch',);
                            }

                            // admin_sonata_news_comment_edit
                            if (preg_match('#^/admin/sonata/news/comment/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_comment_edit')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::editAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_edit',));
                            }

                            // admin_sonata_news_comment_delete
                            if (preg_match('#^/admin/sonata/news/comment/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_comment_delete')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::deleteAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_delete',));
                            }

                            // admin_sonata_news_comment_show
                            if (preg_match('#^/admin/sonata/news/comment/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_comment_show')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::showAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_show',));
                            }

                            // admin_sonata_news_comment_export
                            if ($pathinfo === '/admin/sonata/news/comment/export') {
                                return array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::exportAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_export',  '_route' => 'admin_sonata_news_comment_export',);
                            }

                            // admin_sonata_news_comment_history
                            if (preg_match('#^/admin/sonata/news/comment/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_comment_history')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::historyAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_history',));
                            }

                            // admin_sonata_news_comment_history_view_revision
                            if (preg_match('#^/admin/sonata/news/comment/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_comment_history_view_revision')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_history_view_revision',));
                            }

                            // admin_sonata_news_comment_history_compare_revisions
                            if (preg_match('#^/admin/sonata/news/comment/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_comment_history_compare_revisions')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_history_compare_revisions',));
                            }

                            // admin_sonata_news_comment_acl
                            if (preg_match('#^/admin/sonata/news/comment/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_news_comment_acl')), array (  '_controller' => 'Sonata\\NewsBundle\\Controller\\CommentAdminController::aclAction',  '_sonata_admin' => 'sonata.news.admin.comment',  '_sonata_name' => 'admin_sonata_news_comment_acl',));
                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/sonata/media')) {
                        if (0 === strpos($pathinfo, '/admin/sonata/media/media')) {
                            // admin_sonata_media_media_list
                            if ($pathinfo === '/admin/sonata/media/media/list') {
                                return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::listAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_list',  '_route' => 'admin_sonata_media_media_list',);
                            }

                            // admin_sonata_media_media_create
                            if ($pathinfo === '/admin/sonata/media/media/create') {
                                return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::createAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_create',  '_route' => 'admin_sonata_media_media_create',);
                            }

                            // admin_sonata_media_media_batch
                            if ($pathinfo === '/admin/sonata/media/media/batch') {
                                return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::batchAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_batch',  '_route' => 'admin_sonata_media_media_batch',);
                            }

                            // admin_sonata_media_media_edit
                            if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_edit')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::editAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_edit',));
                            }

                            // admin_sonata_media_media_delete
                            if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_delete')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::deleteAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_delete',));
                            }

                            // admin_sonata_media_media_show
                            if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_show')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::showAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_show',));
                            }

                            // admin_sonata_media_media_export
                            if ($pathinfo === '/admin/sonata/media/media/export') {
                                return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::exportAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_export',  '_route' => 'admin_sonata_media_media_export',);
                            }

                            // admin_sonata_media_media_history
                            if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_history')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::historyAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_history',));
                            }

                            // admin_sonata_media_media_history_view_revision
                            if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_history_view_revision')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_history_view_revision',));
                            }

                            // admin_sonata_media_media_history_compare_revisions
                            if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_history_compare_revisions')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_history_compare_revisions',));
                            }

                            // admin_sonata_media_media_acl
                            if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_acl')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::aclAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_acl',));
                            }

                            if (0 === strpos($pathinfo, '/admin/sonata/media/media/ckeditor_')) {
                                // admin_sonata_media_media_ckeditor_browser
                                if ($pathinfo === '/admin/sonata/media/media/ckeditor_browser') {
                                    return array (  '_controller' => 'Sonata\\FormatterBundle\\Controller\\CkeditorAdminController::browserAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_ckeditor_browser',  '_route' => 'admin_sonata_media_media_ckeditor_browser',);
                                }

                                // admin_sonata_media_media_ckeditor_upload
                                if ($pathinfo === '/admin/sonata/media/media/ckeditor_upload') {
                                    return array (  '_controller' => 'Sonata\\FormatterBundle\\Controller\\CkeditorAdminController::uploadAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_ckeditor_upload',  '_route' => 'admin_sonata_media_media_ckeditor_upload',);
                                }

                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/sonata/media/gallery')) {
                            // admin_sonata_media_gallery_list
                            if ($pathinfo === '/admin/sonata/media/gallery/list') {
                                return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::listAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_list',  '_route' => 'admin_sonata_media_gallery_list',);
                            }

                            // admin_sonata_media_gallery_create
                            if ($pathinfo === '/admin/sonata/media/gallery/create') {
                                return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::createAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_create',  '_route' => 'admin_sonata_media_gallery_create',);
                            }

                            // admin_sonata_media_gallery_batch
                            if ($pathinfo === '/admin/sonata/media/gallery/batch') {
                                return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::batchAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_batch',  '_route' => 'admin_sonata_media_gallery_batch',);
                            }

                            // admin_sonata_media_gallery_edit
                            if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_edit')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::editAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_edit',));
                            }

                            // admin_sonata_media_gallery_delete
                            if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_delete')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::deleteAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_delete',));
                            }

                            // admin_sonata_media_gallery_show
                            if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_show')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::showAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_show',));
                            }

                            // admin_sonata_media_gallery_export
                            if ($pathinfo === '/admin/sonata/media/gallery/export') {
                                return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::exportAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_export',  '_route' => 'admin_sonata_media_gallery_export',);
                            }

                            // admin_sonata_media_gallery_history
                            if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_history')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::historyAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_history',));
                            }

                            // admin_sonata_media_gallery_history_view_revision
                            if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_history_view_revision')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_history_view_revision',));
                            }

                            // admin_sonata_media_gallery_history_compare_revisions
                            if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_history_compare_revisions')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_history_compare_revisions',));
                            }

                            // admin_sonata_media_gallery_acl
                            if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_acl')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::aclAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_acl',));
                            }

                            if (0 === strpos($pathinfo, '/admin/sonata/media/galleryhasmedia')) {
                                // admin_sonata_media_galleryhasmedia_list
                                if ($pathinfo === '/admin/sonata/media/galleryhasmedia/list') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_list',  '_route' => 'admin_sonata_media_galleryhasmedia_list',);
                                }

                                // admin_sonata_media_galleryhasmedia_create
                                if ($pathinfo === '/admin/sonata/media/galleryhasmedia/create') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_create',  '_route' => 'admin_sonata_media_galleryhasmedia_create',);
                                }

                                // admin_sonata_media_galleryhasmedia_batch
                                if ($pathinfo === '/admin/sonata/media/galleryhasmedia/batch') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_batch',  '_route' => 'admin_sonata_media_galleryhasmedia_batch',);
                                }

                                // admin_sonata_media_galleryhasmedia_edit
                                if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_edit',));
                                }

                                // admin_sonata_media_galleryhasmedia_delete
                                if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_delete',));
                                }

                                // admin_sonata_media_galleryhasmedia_show
                                if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_show',));
                                }

                                // admin_sonata_media_galleryhasmedia_export
                                if ($pathinfo === '/admin/sonata/media/galleryhasmedia/export') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_export',  '_route' => 'admin_sonata_media_galleryhasmedia_export',);
                                }

                                // admin_sonata_media_galleryhasmedia_history
                                if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_history',));
                                }

                                // admin_sonata_media_galleryhasmedia_history_view_revision
                                if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_history_view_revision',));
                                }

                                // admin_sonata_media_galleryhasmedia_history_compare_revisions
                                if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_history_compare_revisions',));
                                }

                                // admin_sonata_media_galleryhasmedia_acl
                                if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_acl',));
                                }

                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/sonata/c')) {
                        if (0 === strpos($pathinfo, '/admin/sonata/comment')) {
                            if (0 === strpos($pathinfo, '/admin/sonata/comment/comment')) {
                                // admin_sonata_comment_comment_list
                                if ($pathinfo === '/admin/sonata/comment/comment/list') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_list',  '_route' => 'admin_sonata_comment_comment_list',);
                                }

                                // admin_sonata_comment_comment_create
                                if ($pathinfo === '/admin/sonata/comment/comment/create') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_create',  '_route' => 'admin_sonata_comment_comment_create',);
                                }

                                // admin_sonata_comment_comment_batch
                                if ($pathinfo === '/admin/sonata/comment/comment/batch') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_batch',  '_route' => 'admin_sonata_comment_comment_batch',);
                                }

                                // admin_sonata_comment_comment_edit
                                if (preg_match('#^/admin/sonata/comment/comment/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_comment_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_edit',));
                                }

                                // admin_sonata_comment_comment_delete
                                if (preg_match('#^/admin/sonata/comment/comment/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_comment_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_delete',));
                                }

                                // admin_sonata_comment_comment_show
                                if (preg_match('#^/admin/sonata/comment/comment/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_comment_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_show',));
                                }

                                // admin_sonata_comment_comment_export
                                if ($pathinfo === '/admin/sonata/comment/comment/export') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_export',  '_route' => 'admin_sonata_comment_comment_export',);
                                }

                                // admin_sonata_comment_comment_history
                                if (preg_match('#^/admin/sonata/comment/comment/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_comment_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_history',));
                                }

                                // admin_sonata_comment_comment_history_view_revision
                                if (preg_match('#^/admin/sonata/comment/comment/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_comment_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_history_view_revision',));
                                }

                                // admin_sonata_comment_comment_history_compare_revisions
                                if (preg_match('#^/admin/sonata/comment/comment/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_comment_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_history_compare_revisions',));
                                }

                                // admin_sonata_comment_comment_acl
                                if (preg_match('#^/admin/sonata/comment/comment/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_comment_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_comment_acl',));
                                }

                            }

                            if (0 === strpos($pathinfo, '/admin/sonata/comment/thread')) {
                                // admin_sonata_comment_thread_list
                                if ($pathinfo === '/admin/sonata/comment/thread/list') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_list',  '_route' => 'admin_sonata_comment_thread_list',);
                                }

                                // admin_sonata_comment_thread_create
                                if ($pathinfo === '/admin/sonata/comment/thread/create') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_create',  '_route' => 'admin_sonata_comment_thread_create',);
                                }

                                // admin_sonata_comment_thread_batch
                                if ($pathinfo === '/admin/sonata/comment/thread/batch') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_batch',  '_route' => 'admin_sonata_comment_thread_batch',);
                                }

                                // admin_sonata_comment_thread_edit
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_edit',));
                                }

                                // admin_sonata_comment_thread_delete
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_delete',));
                                }

                                // admin_sonata_comment_thread_show
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_show',));
                                }

                                // admin_sonata_comment_thread_export
                                if ($pathinfo === '/admin/sonata/comment/thread/export') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_export',  '_route' => 'admin_sonata_comment_thread_export',);
                                }

                                // admin_sonata_comment_thread_history
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_history',));
                                }

                                // admin_sonata_comment_thread_history_view_revision
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_history_view_revision',));
                                }

                                // admin_sonata_comment_thread_history_compare_revisions
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_history_compare_revisions',));
                                }

                                // admin_sonata_comment_thread_acl
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.comment.admin.thread',  '_sonata_name' => 'admin_sonata_comment_thread_acl',));
                                }

                                // admin_sonata_comment_thread_comment_list
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/list$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_list',));
                                }

                                // admin_sonata_comment_thread_comment_create
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/create$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_create',));
                                }

                                // admin_sonata_comment_thread_comment_batch
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/batch$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_batch',));
                                }

                                // admin_sonata_comment_thread_comment_edit
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_edit',));
                                }

                                // admin_sonata_comment_thread_comment_delete
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_delete',));
                                }

                                // admin_sonata_comment_thread_comment_show
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/show$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_show',));
                                }

                                // admin_sonata_comment_thread_comment_export
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/export$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_export',));
                                }

                                // admin_sonata_comment_thread_comment_history
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/history$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_history',));
                                }

                                // admin_sonata_comment_thread_comment_history_view_revision
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_history_view_revision',));
                                }

                                // admin_sonata_comment_thread_comment_history_compare_revisions
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_history_compare_revisions',));
                                }

                                // admin_sonata_comment_thread_comment_acl
                                if (preg_match('#^/admin/sonata/comment/thread/(?P<id>[^/]++)/comment/(?P<childId>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_comment_thread_comment_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.comment.admin.thread|sonata.comment.admin.comment',  '_sonata_name' => 'admin_sonata_comment_thread_comment_acl',));
                                }

                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/sonata/classification')) {
                            if (0 === strpos($pathinfo, '/admin/sonata/classification/category')) {
                                // admin_sonata_classification_category_list
                                if ($pathinfo === '/admin/sonata/classification/category/list') {
                                    return array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::listAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_list',  '_route' => 'admin_sonata_classification_category_list',);
                                }

                                // admin_sonata_classification_category_create
                                if ($pathinfo === '/admin/sonata/classification/category/create') {
                                    return array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::createAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_create',  '_route' => 'admin_sonata_classification_category_create',);
                                }

                                // admin_sonata_classification_category_batch
                                if ($pathinfo === '/admin/sonata/classification/category/batch') {
                                    return array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::batchAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_batch',  '_route' => 'admin_sonata_classification_category_batch',);
                                }

                                // admin_sonata_classification_category_edit
                                if (preg_match('#^/admin/sonata/classification/category/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_category_edit')), array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::editAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_edit',));
                                }

                                // admin_sonata_classification_category_delete
                                if (preg_match('#^/admin/sonata/classification/category/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_category_delete')), array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::deleteAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_delete',));
                                }

                                // admin_sonata_classification_category_show
                                if (preg_match('#^/admin/sonata/classification/category/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_category_show')), array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::showAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_show',));
                                }

                                // admin_sonata_classification_category_export
                                if ($pathinfo === '/admin/sonata/classification/category/export') {
                                    return array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::exportAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_export',  '_route' => 'admin_sonata_classification_category_export',);
                                }

                                // admin_sonata_classification_category_history
                                if (preg_match('#^/admin/sonata/classification/category/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_category_history')), array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::historyAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_history',));
                                }

                                // admin_sonata_classification_category_history_view_revision
                                if (preg_match('#^/admin/sonata/classification/category/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_category_history_view_revision')), array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_history_view_revision',));
                                }

                                // admin_sonata_classification_category_history_compare_revisions
                                if (preg_match('#^/admin/sonata/classification/category/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_category_history_compare_revisions')), array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_history_compare_revisions',));
                                }

                                // admin_sonata_classification_category_acl
                                if (preg_match('#^/admin/sonata/classification/category/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_category_acl')), array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::aclAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_acl',));
                                }

                                // admin_sonata_classification_category_tree
                                if ($pathinfo === '/admin/sonata/classification/category/tree') {
                                    return array (  '_controller' => 'Sonata\\ClassificationBundle\\Controller\\CategoryAdminController::treeAction',  '_sonata_admin' => 'sonata.classification.admin.category',  '_sonata_name' => 'admin_sonata_classification_category_tree',  '_route' => 'admin_sonata_classification_category_tree',);
                                }

                            }

                            if (0 === strpos($pathinfo, '/admin/sonata/classification/tag')) {
                                // admin_sonata_classification_tag_list
                                if ($pathinfo === '/admin/sonata/classification/tag/list') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_list',  '_route' => 'admin_sonata_classification_tag_list',);
                                }

                                // admin_sonata_classification_tag_create
                                if ($pathinfo === '/admin/sonata/classification/tag/create') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_create',  '_route' => 'admin_sonata_classification_tag_create',);
                                }

                                // admin_sonata_classification_tag_batch
                                if ($pathinfo === '/admin/sonata/classification/tag/batch') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_batch',  '_route' => 'admin_sonata_classification_tag_batch',);
                                }

                                // admin_sonata_classification_tag_edit
                                if (preg_match('#^/admin/sonata/classification/tag/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_tag_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_edit',));
                                }

                                // admin_sonata_classification_tag_delete
                                if (preg_match('#^/admin/sonata/classification/tag/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_tag_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_delete',));
                                }

                                // admin_sonata_classification_tag_show
                                if (preg_match('#^/admin/sonata/classification/tag/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_tag_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_show',));
                                }

                                // admin_sonata_classification_tag_export
                                if ($pathinfo === '/admin/sonata/classification/tag/export') {
                                    return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_export',  '_route' => 'admin_sonata_classification_tag_export',);
                                }

                                // admin_sonata_classification_tag_history
                                if (preg_match('#^/admin/sonata/classification/tag/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_tag_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_history',));
                                }

                                // admin_sonata_classification_tag_history_view_revision
                                if (preg_match('#^/admin/sonata/classification/tag/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_tag_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_history_view_revision',));
                                }

                                // admin_sonata_classification_tag_history_compare_revisions
                                if (preg_match('#^/admin/sonata/classification/tag/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_tag_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_history_compare_revisions',));
                                }

                                // admin_sonata_classification_tag_acl
                                if (preg_match('#^/admin/sonata/classification/tag/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_tag_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.classification.admin.tag',  '_sonata_name' => 'admin_sonata_classification_tag_acl',));
                                }

                            }

                            if (0 === strpos($pathinfo, '/admin/sonata/classification/co')) {
                                if (0 === strpos($pathinfo, '/admin/sonata/classification/collection')) {
                                    // admin_sonata_classification_collection_list
                                    if ($pathinfo === '/admin/sonata/classification/collection/list') {
                                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_list',  '_route' => 'admin_sonata_classification_collection_list',);
                                    }

                                    // admin_sonata_classification_collection_create
                                    if ($pathinfo === '/admin/sonata/classification/collection/create') {
                                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_create',  '_route' => 'admin_sonata_classification_collection_create',);
                                    }

                                    // admin_sonata_classification_collection_batch
                                    if ($pathinfo === '/admin/sonata/classification/collection/batch') {
                                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_batch',  '_route' => 'admin_sonata_classification_collection_batch',);
                                    }

                                    // admin_sonata_classification_collection_edit
                                    if (preg_match('#^/admin/sonata/classification/collection/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_collection_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_edit',));
                                    }

                                    // admin_sonata_classification_collection_delete
                                    if (preg_match('#^/admin/sonata/classification/collection/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_collection_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_delete',));
                                    }

                                    // admin_sonata_classification_collection_show
                                    if (preg_match('#^/admin/sonata/classification/collection/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_collection_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_show',));
                                    }

                                    // admin_sonata_classification_collection_export
                                    if ($pathinfo === '/admin/sonata/classification/collection/export') {
                                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_export',  '_route' => 'admin_sonata_classification_collection_export',);
                                    }

                                    // admin_sonata_classification_collection_history
                                    if (preg_match('#^/admin/sonata/classification/collection/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_collection_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_history',));
                                    }

                                    // admin_sonata_classification_collection_history_view_revision
                                    if (preg_match('#^/admin/sonata/classification/collection/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_collection_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_history_view_revision',));
                                    }

                                    // admin_sonata_classification_collection_history_compare_revisions
                                    if (preg_match('#^/admin/sonata/classification/collection/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_collection_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_history_compare_revisions',));
                                    }

                                    // admin_sonata_classification_collection_acl
                                    if (preg_match('#^/admin/sonata/classification/collection/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_collection_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.classification.admin.collection',  '_sonata_name' => 'admin_sonata_classification_collection_acl',));
                                    }

                                }

                                if (0 === strpos($pathinfo, '/admin/sonata/classification/context')) {
                                    // admin_sonata_classification_context_list
                                    if ($pathinfo === '/admin/sonata/classification/context/list') {
                                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_list',  '_route' => 'admin_sonata_classification_context_list',);
                                    }

                                    // admin_sonata_classification_context_create
                                    if ($pathinfo === '/admin/sonata/classification/context/create') {
                                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_create',  '_route' => 'admin_sonata_classification_context_create',);
                                    }

                                    // admin_sonata_classification_context_batch
                                    if ($pathinfo === '/admin/sonata/classification/context/batch') {
                                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_batch',  '_route' => 'admin_sonata_classification_context_batch',);
                                    }

                                    // admin_sonata_classification_context_edit
                                    if (preg_match('#^/admin/sonata/classification/context/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_context_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_edit',));
                                    }

                                    // admin_sonata_classification_context_delete
                                    if (preg_match('#^/admin/sonata/classification/context/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_context_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_delete',));
                                    }

                                    // admin_sonata_classification_context_show
                                    if (preg_match('#^/admin/sonata/classification/context/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_context_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_show',));
                                    }

                                    // admin_sonata_classification_context_export
                                    if ($pathinfo === '/admin/sonata/classification/context/export') {
                                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_export',  '_route' => 'admin_sonata_classification_context_export',);
                                    }

                                    // admin_sonata_classification_context_history
                                    if (preg_match('#^/admin/sonata/classification/context/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_context_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_history',));
                                    }

                                    // admin_sonata_classification_context_history_view_revision
                                    if (preg_match('#^/admin/sonata/classification/context/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_context_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_history_view_revision',));
                                    }

                                    // admin_sonata_classification_context_history_compare_revisions
                                    if (preg_match('#^/admin/sonata/classification/context/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_context_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_history_compare_revisions',));
                                    }

                                    // admin_sonata_classification_context_acl
                                    if (preg_match('#^/admin/sonata/classification/context/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_classification_context_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.classification.admin.context',  '_sonata_name' => 'admin_sonata_classification_context_acl',));
                                    }

                                }

                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/sonata/notification/message')) {
                        // admin_sonata_notification_message_list
                        if ($pathinfo === '/admin/sonata/notification/message/list') {
                            return array (  '_controller' => 'Sonata\\NotificationBundle\\Controller\\MessageAdminController::listAction',  '_sonata_admin' => 'sonata.notification.admin.message',  '_sonata_name' => 'admin_sonata_notification_message_list',  '_route' => 'admin_sonata_notification_message_list',);
                        }

                        // admin_sonata_notification_message_batch
                        if ($pathinfo === '/admin/sonata/notification/message/batch') {
                            return array (  '_controller' => 'Sonata\\NotificationBundle\\Controller\\MessageAdminController::batchAction',  '_sonata_admin' => 'sonata.notification.admin.message',  '_sonata_name' => 'admin_sonata_notification_message_batch',  '_route' => 'admin_sonata_notification_message_batch',);
                        }

                        // admin_sonata_notification_message_delete
                        if (preg_match('#^/admin/sonata/notification/message/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_notification_message_delete')), array (  '_controller' => 'Sonata\\NotificationBundle\\Controller\\MessageAdminController::deleteAction',  '_sonata_admin' => 'sonata.notification.admin.message',  '_sonata_name' => 'admin_sonata_notification_message_delete',));
                        }

                        // admin_sonata_notification_message_show
                        if (preg_match('#^/admin/sonata/notification/message/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_notification_message_show')), array (  '_controller' => 'Sonata\\NotificationBundle\\Controller\\MessageAdminController::showAction',  '_sonata_admin' => 'sonata.notification.admin.message',  '_sonata_name' => 'admin_sonata_notification_message_show',));
                        }

                        // admin_sonata_notification_message_export
                        if ($pathinfo === '/admin/sonata/notification/message/export') {
                            return array (  '_controller' => 'Sonata\\NotificationBundle\\Controller\\MessageAdminController::exportAction',  '_sonata_admin' => 'sonata.notification.admin.message',  '_sonata_name' => 'admin_sonata_notification_message_export',  '_route' => 'admin_sonata_notification_message_export',);
                        }

                        // admin_sonata_notification_message_history_view_revision
                        if (preg_match('#^/admin/sonata/notification/message/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_notification_message_history_view_revision')), array (  '_controller' => 'Sonata\\NotificationBundle\\Controller\\MessageAdminController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.notification.admin.message',  '_sonata_name' => 'admin_sonata_notification_message_history_view_revision',));
                        }

                        // admin_sonata_notification_message_history_compare_revisions
                        if (preg_match('#^/admin/sonata/notification/message/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_notification_message_history_compare_revisions')), array (  '_controller' => 'Sonata\\NotificationBundle\\Controller\\MessageAdminController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.notification.admin.message',  '_sonata_name' => 'admin_sonata_notification_message_history_compare_revisions',));
                        }

                        // admin_sonata_notification_message_acl
                        if (preg_match('#^/admin/sonata/notification/message/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_notification_message_acl')), array (  '_controller' => 'Sonata\\NotificationBundle\\Controller\\MessageAdminController::aclAction',  '_sonata_admin' => 'sonata.notification.admin.message',  '_sonata_name' => 'admin_sonata_notification_message_acl',));
                        }

                    }

                }

                if (0 === strpos($pathinfo, '/admin/comparator')) {
                    if (0 === strpos($pathinfo, '/admin/comparator/base/site')) {
                        // admin_comparator_base_site_list
                        if ($pathinfo === '/admin/comparator/base/site/list') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_list',  '_route' => 'admin_comparator_base_site_list',);
                        }

                        // admin_comparator_base_site_create
                        if ($pathinfo === '/admin/comparator/base/site/create') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_create',  '_route' => 'admin_comparator_base_site_create',);
                        }

                        // admin_comparator_base_site_batch
                        if ($pathinfo === '/admin/comparator/base/site/batch') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_batch',  '_route' => 'admin_comparator_base_site_batch',);
                        }

                        // admin_comparator_base_site_edit
                        if (preg_match('#^/admin/comparator/base/site/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_base_site_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_edit',));
                        }

                        // admin_comparator_base_site_delete
                        if (preg_match('#^/admin/comparator/base/site/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_base_site_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_delete',));
                        }

                        // admin_comparator_base_site_show
                        if (preg_match('#^/admin/comparator/base/site/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_base_site_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_show',));
                        }

                        // admin_comparator_base_site_export
                        if ($pathinfo === '/admin/comparator/base/site/export') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_export',  '_route' => 'admin_comparator_base_site_export',);
                        }

                        // admin_comparator_base_site_history
                        if (preg_match('#^/admin/comparator/base/site/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_base_site_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_history',));
                        }

                        // admin_comparator_base_site_history_view_revision
                        if (preg_match('#^/admin/comparator/base/site/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_base_site_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_history_view_revision',));
                        }

                        // admin_comparator_base_site_history_compare_revisions
                        if (preg_match('#^/admin/comparator/base/site/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_base_site_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_history_compare_revisions',));
                        }

                        // admin_comparator_base_site_acl
                        if (preg_match('#^/admin/comparator/base/site/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_base_site_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.base.admin.site',  '_sonata_name' => 'admin_comparator_base_site_acl',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/comparator/event')) {
                        if (0 === strpos($pathinfo, '/admin/comparator/event/event')) {
                            // admin_comparator_event_event_list
                            if ($pathinfo === '/admin/comparator/event/event/list') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_list',  '_route' => 'admin_comparator_event_event_list',);
                            }

                            // admin_comparator_event_event_create
                            if ($pathinfo === '/admin/comparator/event/event/create') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_create',  '_route' => 'admin_comparator_event_event_create',);
                            }

                            // admin_comparator_event_event_batch
                            if ($pathinfo === '/admin/comparator/event/event/batch') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_batch',  '_route' => 'admin_comparator_event_event_batch',);
                            }

                            // admin_comparator_event_event_edit
                            if (preg_match('#^/admin/comparator/event/event/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_event_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_edit',));
                            }

                            // admin_comparator_event_event_delete
                            if (preg_match('#^/admin/comparator/event/event/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_event_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_delete',));
                            }

                            // admin_comparator_event_event_show
                            if (preg_match('#^/admin/comparator/event/event/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_event_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_show',));
                            }

                            // admin_comparator_event_event_export
                            if ($pathinfo === '/admin/comparator/event/event/export') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_export',  '_route' => 'admin_comparator_event_event_export',);
                            }

                            // admin_comparator_event_event_history
                            if (preg_match('#^/admin/comparator/event/event/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_event_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_history',));
                            }

                            // admin_comparator_event_event_history_view_revision
                            if (preg_match('#^/admin/comparator/event/event/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_event_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_history_view_revision',));
                            }

                            // admin_comparator_event_event_history_compare_revisions
                            if (preg_match('#^/admin/comparator/event/event/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_event_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_history_compare_revisions',));
                            }

                            // admin_comparator_event_event_acl
                            if (preg_match('#^/admin/comparator/event/event/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_event_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.event.admin.event',  '_sonata_name' => 'admin_comparator_event_event_acl',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/comparator/event/category')) {
                            // admin_comparator_event_category_list
                            if ($pathinfo === '/admin/comparator/event/category/list') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_list',  '_route' => 'admin_comparator_event_category_list',);
                            }

                            // admin_comparator_event_category_create
                            if ($pathinfo === '/admin/comparator/event/category/create') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_create',  '_route' => 'admin_comparator_event_category_create',);
                            }

                            // admin_comparator_event_category_batch
                            if ($pathinfo === '/admin/comparator/event/category/batch') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_batch',  '_route' => 'admin_comparator_event_category_batch',);
                            }

                            // admin_comparator_event_category_edit
                            if (preg_match('#^/admin/comparator/event/category/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_category_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_edit',));
                            }

                            // admin_comparator_event_category_delete
                            if (preg_match('#^/admin/comparator/event/category/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_category_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_delete',));
                            }

                            // admin_comparator_event_category_show
                            if (preg_match('#^/admin/comparator/event/category/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_category_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_show',));
                            }

                            // admin_comparator_event_category_export
                            if ($pathinfo === '/admin/comparator/event/category/export') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_export',  '_route' => 'admin_comparator_event_category_export',);
                            }

                            // admin_comparator_event_category_history
                            if (preg_match('#^/admin/comparator/event/category/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_category_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_history',));
                            }

                            // admin_comparator_event_category_history_view_revision
                            if (preg_match('#^/admin/comparator/event/category/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_category_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_history_view_revision',));
                            }

                            // admin_comparator_event_category_history_compare_revisions
                            if (preg_match('#^/admin/comparator/event/category/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_category_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_history_compare_revisions',));
                            }

                            // admin_comparator_event_category_acl
                            if (preg_match('#^/admin/comparator/event/category/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_category_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.event.admin.category',  '_sonata_name' => 'admin_comparator_event_category_acl',));
                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/comparator/event/ville')) {
                            // admin_comparator_event_ville_list
                            if ($pathinfo === '/admin/comparator/event/ville/list') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_list',  '_route' => 'admin_comparator_event_ville_list',);
                            }

                            // admin_comparator_event_ville_create
                            if ($pathinfo === '/admin/comparator/event/ville/create') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_create',  '_route' => 'admin_comparator_event_ville_create',);
                            }

                            // admin_comparator_event_ville_batch
                            if ($pathinfo === '/admin/comparator/event/ville/batch') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_batch',  '_route' => 'admin_comparator_event_ville_batch',);
                            }

                            // admin_comparator_event_ville_edit
                            if (preg_match('#^/admin/comparator/event/ville/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_ville_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_edit',));
                            }

                            // admin_comparator_event_ville_delete
                            if (preg_match('#^/admin/comparator/event/ville/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_ville_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_delete',));
                            }

                            // admin_comparator_event_ville_show
                            if (preg_match('#^/admin/comparator/event/ville/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_ville_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_show',));
                            }

                            // admin_comparator_event_ville_export
                            if ($pathinfo === '/admin/comparator/event/ville/export') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_export',  '_route' => 'admin_comparator_event_ville_export',);
                            }

                            // admin_comparator_event_ville_history
                            if (preg_match('#^/admin/comparator/event/ville/(?P<id>[^/]++)/history$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_ville_history')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_history',));
                            }

                            // admin_comparator_event_ville_history_view_revision
                            if (preg_match('#^/admin/comparator/event/ville/(?P<id>[^/]++)/history/(?P<revision>[^/]++)/view$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_ville_history_view_revision')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyViewRevisionAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_history_view_revision',));
                            }

                            // admin_comparator_event_ville_history_compare_revisions
                            if (preg_match('#^/admin/comparator/event/ville/(?P<id>[^/]++)/history/(?P<base_revision>[^/]++)/(?P<compare_revision>[^/]++)/compare$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_ville_history_compare_revisions')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::historyCompareRevisionsAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_history_compare_revisions',));
                            }

                            // admin_comparator_event_ville_acl
                            if (preg_match('#^/admin/comparator/event/ville/(?P<id>[^/]++)/acl$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_comparator_event_ville_acl')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::aclAction',  '_sonata_admin' => 'sonata.event.admin.ville',  '_sonata_name' => 'admin_comparator_event_ville_acl',));
                            }

                        }

                    }

                }

                if (0 === strpos($pathinfo, '/admin/log')) {
                    if (0 === strpos($pathinfo, '/admin/login')) {
                        // sonata_user_admin_security_login
                        if ($pathinfo === '/admin/login') {
                            return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::loginAction',  '_route' => 'sonata_user_admin_security_login',);
                        }

                        // sonata_user_admin_security_check
                        if ($pathinfo === '/admin/login_check') {
                            return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::checkAction',  '_route' => 'sonata_user_admin_security_check',);
                        }

                    }

                    // sonata_user_admin_security_logout
                    if ($pathinfo === '/admin/logout') {
                        return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::logoutAction',  '_route' => 'sonata_user_admin_security_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/media/pixlr')) {
                    // sonata_media_pixlr_edit
                    if (0 === strpos($pathinfo, '/admin/media/pixlr/edit') && preg_match('#^/admin/media/pixlr/edit/(?P<id>[^/]++)/(?P<mode>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_pixlr_edit')), array (  '_controller' => 'sonata.media.extra.pixlr:editAction',));
                    }

                    // sonata_media_pixlr_target
                    if (0 === strpos($pathinfo, '/admin/media/pixlr/target') && preg_match('#^/admin/media/pixlr/target/(?P<hash>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_pixlr_target')), array (  '_controller' => 'sonata.media.extra.pixlr:targetAction',));
                    }

                    // sonata_media_pixlr_exit
                    if (0 === strpos($pathinfo, '/admin/media/pixlr/exit') && preg_match('#^/admin/media/pixlr/exit/(?P<hash>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_pixlr_exit')), array (  '_controller' => 'sonata.media.extra.pixlr:exitAction',));
                    }

                    // sonata_media_pixlr_open_editor
                    if (0 === strpos($pathinfo, '/admin/media/pixlr/open') && preg_match('#^/admin/media/pixlr/open/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_pixlr_open_editor')), array (  '_controller' => 'sonata.media.extra.pixlr:openEditorAction',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/api')) {
                // nelmio_api_doc_index
                if (rtrim($pathinfo, '/') === '/api/doc') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_nelmio_api_doc_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'nelmio_api_doc_index');
                    }

                    return array (  '_controller' => 'Nelmio\\ApiDocBundle\\Controller\\ApiDocController::indexAction',  '_route' => 'nelmio_api_doc_index',);
                }
                not_nelmio_api_doc_index:

                if (0 === strpos($pathinfo, '/api/classification')) {
                    if (0 === strpos($pathinfo, '/api/classification/c')) {
                        if (0 === strpos($pathinfo, '/api/classification/categories')) {
                            // sonata_api_classification_category_get_categories
                            if (preg_match('#^/api/classification/categories(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_sonata_api_classification_category_get_categories;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_category_get_categories')), array (  '_controller' => 'sonata.classification.controller.api.category:getCategoriesAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_category_get_categories:

                            // sonata_api_classification_category_get_category
                            if (preg_match('#^/api/classification/categories/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_sonata_api_classification_category_get_category;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_category_get_category')), array (  '_controller' => 'sonata.classification.controller.api.category:getCategoryAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_category_get_category:

                            // sonata_api_classification_category_post_category
                            if (preg_match('#^/api/classification/categories(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_sonata_api_classification_category_post_category;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_category_post_category')), array (  '_controller' => 'sonata.classification.controller.api.category:postCategoryAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_category_post_category:

                            // sonata_api_classification_category_put_category
                            if (preg_match('#^/api/classification/categories/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'PUT') {
                                    $allow[] = 'PUT';
                                    goto not_sonata_api_classification_category_put_category;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_category_put_category')), array (  '_controller' => 'sonata.classification.controller.api.category:putCategoryAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_category_put_category:

                            // sonata_api_classification_category_delete_category
                            if (preg_match('#^/api/classification/categories/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'DELETE') {
                                    $allow[] = 'DELETE';
                                    goto not_sonata_api_classification_category_delete_category;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_category_delete_category')), array (  '_controller' => 'sonata.classification.controller.api.category:deleteCategoryAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_category_delete_category:

                        }

                        if (0 === strpos($pathinfo, '/api/classification/collections')) {
                            // sonata_api_classification_collection_get_collections
                            if (preg_match('#^/api/classification/collections(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_sonata_api_classification_collection_get_collections;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_collection_get_collections')), array (  '_controller' => 'sonata.classification.controller.api.collection:getCollectionsAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_collection_get_collections:

                            // sonata_api_classification_collection_get_collection
                            if (preg_match('#^/api/classification/collections/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_sonata_api_classification_collection_get_collection;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_collection_get_collection')), array (  '_controller' => 'sonata.classification.controller.api.collection:getCollectionAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_collection_get_collection:

                            // sonata_api_classification_collection_post_collection
                            if (preg_match('#^/api/classification/collections(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_sonata_api_classification_collection_post_collection;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_collection_post_collection')), array (  '_controller' => 'sonata.classification.controller.api.collection:postCollectionAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_collection_post_collection:

                            // sonata_api_classification_collection_put_collection
                            if (preg_match('#^/api/classification/collections/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'PUT') {
                                    $allow[] = 'PUT';
                                    goto not_sonata_api_classification_collection_put_collection;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_collection_put_collection')), array (  '_controller' => 'sonata.classification.controller.api.collection:putCollectionAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_collection_put_collection:

                            // sonata_api_classification_collection_delete_collection
                            if (preg_match('#^/api/classification/collections/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'DELETE') {
                                    $allow[] = 'DELETE';
                                    goto not_sonata_api_classification_collection_delete_collection;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_collection_delete_collection')), array (  '_controller' => 'sonata.classification.controller.api.collection:deleteCollectionAction',  '_format' => NULL,));
                            }
                            not_sonata_api_classification_collection_delete_collection:

                        }

                    }

                    if (0 === strpos($pathinfo, '/api/classification/tags')) {
                        // sonata_api_classification_tag_get_tags
                        if (preg_match('#^/api/classification/tags(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_classification_tag_get_tags;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_tag_get_tags')), array (  '_controller' => 'sonata.classification.controller.api.tag:getTagsAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_tag_get_tags:

                        // sonata_api_classification_tag_get_tag
                        if (preg_match('#^/api/classification/tags/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_classification_tag_get_tag;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_tag_get_tag')), array (  '_controller' => 'sonata.classification.controller.api.tag:getTagAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_tag_get_tag:

                        // sonata_api_classification_tag_post_tag
                        if (preg_match('#^/api/classification/tags(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_classification_tag_post_tag;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_tag_post_tag')), array (  '_controller' => 'sonata.classification.controller.api.tag:postTagAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_tag_post_tag:

                        // sonata_api_classification_tag_put_tag
                        if (preg_match('#^/api/classification/tags/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_classification_tag_put_tag;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_tag_put_tag')), array (  '_controller' => 'sonata.classification.controller.api.tag:putTagAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_tag_put_tag:

                        // sonata_api_classification_tag_delete_tag
                        if (preg_match('#^/api/classification/tags/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_classification_tag_delete_tag;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_tag_delete_tag')), array (  '_controller' => 'sonata.classification.controller.api.tag:deleteTagAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_tag_delete_tag:

                    }

                    if (0 === strpos($pathinfo, '/api/classification/contexts')) {
                        // sonata_api_classification_context_get_contexts
                        if (preg_match('#^/api/classification/contexts(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_classification_context_get_contexts;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_context_get_contexts')), array (  '_controller' => 'sonata.classification.controller.api.context:getContextsAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_context_get_contexts:

                        // sonata_api_classification_context_get_context
                        if (preg_match('#^/api/classification/contexts/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_classification_context_get_context;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_context_get_context')), array (  '_controller' => 'sonata.classification.controller.api.context:getContextAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_context_get_context:

                        // sonata_api_classification_context_post_context
                        if (preg_match('#^/api/classification/contexts(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_classification_context_post_context;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_context_post_context')), array (  '_controller' => 'sonata.classification.controller.api.context:postContextAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_context_post_context:

                        // sonata_api_classification_context_put_context
                        if (preg_match('#^/api/classification/contexts/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_classification_context_put_context;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_context_put_context')), array (  '_controller' => 'sonata.classification.controller.api.context:putContextAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_context_put_context:

                        // sonata_api_classification_context_delete_context
                        if (preg_match('#^/api/classification/contexts/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_classification_context_delete_context;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_classification_context_delete_context')), array (  '_controller' => 'sonata.classification.controller.api.context:deleteContextAction',  '_format' => NULL,));
                        }
                        not_sonata_api_classification_context_delete_context:

                    }

                }

                if (0 === strpos($pathinfo, '/api/news')) {
                    if (0 === strpos($pathinfo, '/api/news/posts')) {
                        // sonata_api_news_post_get_posts
                        if (preg_match('#^/api/news/posts(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_news_post_get_posts;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_post_get_posts')), array (  '_controller' => 'sonata.news.controller.api.post:getPostsAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_post_get_posts:

                        // sonata_api_news_post_get_post
                        if (preg_match('#^/api/news/posts/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_news_post_get_post;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_post_get_post')), array (  '_controller' => 'sonata.news.controller.api.post:getPostAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_post_get_post:

                        // sonata_api_news_post_post_post
                        if (preg_match('#^/api/news/posts(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_news_post_post_post;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_post_post_post')), array (  '_controller' => 'sonata.news.controller.api.post:postPostAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_post_post_post:

                        // sonata_api_news_post_put_post
                        if (preg_match('#^/api/news/posts/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_news_post_put_post;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_post_put_post')), array (  '_controller' => 'sonata.news.controller.api.post:putPostAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_post_put_post:

                        // sonata_api_news_post_delete_post
                        if (preg_match('#^/api/news/posts/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_news_post_delete_post;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_post_delete_post')), array (  '_controller' => 'sonata.news.controller.api.post:deletePostAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_post_delete_post:

                        // sonata_api_news_post_get_post_comments
                        if (preg_match('#^/api/news/posts/(?P<id>[^/]++)/comments(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_news_post_get_post_comments;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_post_get_post_comments')), array (  '_controller' => 'sonata.news.controller.api.post:getPostCommentsAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_post_get_post_comments:

                        // sonata_api_news_post_post_post_comments
                        if (preg_match('#^/api/news/posts/(?P<id>[^/]++)/comments(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_news_post_post_post_comments;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_post_post_post_comments')), array (  '_controller' => 'sonata.news.controller.api.post:postPostCommentsAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_post_post_post_comments:

                        // sonata_api_news_post_put_post_comments
                        if (preg_match('#^/api/news/posts/(?P<postId>[^/]++)/comments/(?P<commentId>[^/\\.]++)(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_news_post_put_post_comments;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_post_put_post_comments')), array (  '_controller' => 'sonata.news.controller.api.post:putPostCommentsAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_post_put_post_comments:

                    }

                    if (0 === strpos($pathinfo, '/api/news/comments')) {
                        // sonata_api_news_comment_get_comment
                        if (preg_match('#^/api/news/comments/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_news_comment_get_comment;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_comment_get_comment')), array (  '_controller' => 'sonata.news.controller.api.comment:getCommentAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_comment_get_comment:

                        // sonata_api_news_comment_delete_comment
                        if (preg_match('#^/api/news/comments/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_news_comment_delete_comment;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_news_comment_delete_comment')), array (  '_controller' => 'sonata.news.controller.api.comment:deleteCommentAction',  '_format' => NULL,));
                        }
                        not_sonata_api_news_comment_delete_comment:

                    }

                }

                if (0 === strpos($pathinfo, '/api/media')) {
                    if (0 === strpos($pathinfo, '/api/media/galleries')) {
                        // sonata_api_media_gallery_get_galleries
                        if (preg_match('#^/api/media/galleries(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_media_gallery_get_galleries;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_get_galleries')), array (  '_controller' => 'sonata.media.controller.api.gallery:getGalleriesAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_get_galleries:

                        // sonata_api_media_gallery_get_gallery
                        if (preg_match('#^/api/media/galleries/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_media_gallery_get_gallery;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_get_gallery')), array (  '_controller' => 'sonata.media.controller.api.gallery:getGalleryAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_get_gallery:

                        // sonata_api_media_gallery_get_gallery_medias
                        if (preg_match('#^/api/media/galleries/(?P<id>[^/]++)/medias(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_media_gallery_get_gallery_medias;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_get_gallery_medias')), array (  '_controller' => 'sonata.media.controller.api.gallery:getGalleryMediasAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_get_gallery_medias:

                        // sonata_api_media_gallery_get_gallery_galleryhasmedias
                        if (preg_match('#^/api/media/galleries/(?P<id>[^/]++)/galleryhasmedias(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_media_gallery_get_gallery_galleryhasmedias;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_get_gallery_galleryhasmedias')), array (  '_controller' => 'sonata.media.controller.api.gallery:getGalleryGalleryhasmediasAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_get_gallery_galleryhasmedias:

                        // sonata_api_media_gallery_post_gallery
                        if (preg_match('#^/api/media/galleries(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_media_gallery_post_gallery;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_post_gallery')), array (  '_controller' => 'sonata.media.controller.api.gallery:postGalleryAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_post_gallery:

                        // sonata_api_media_gallery_put_gallery
                        if (preg_match('#^/api/media/galleries/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_media_gallery_put_gallery;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_put_gallery')), array (  '_controller' => 'sonata.media.controller.api.gallery:putGalleryAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_put_gallery:

                        // sonata_api_media_gallery_post_gallery_media_galleryhasmedia
                        if (preg_match('#^/api/media/galleries/(?P<galleryId>[^/]++)/media/(?P<mediaId>[^/]++)/galleryhasmedia(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_media_gallery_post_gallery_media_galleryhasmedia;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_post_gallery_media_galleryhasmedia')), array (  '_controller' => 'sonata.media.controller.api.gallery:postGalleryMediaGalleryhasmediaAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_post_gallery_media_galleryhasmedia:

                        // sonata_api_media_gallery_put_gallery_media_galleryhasmedia
                        if (preg_match('#^/api/media/galleries/(?P<galleryId>[^/]++)/media/(?P<mediaId>[^/]++)/galleryhasmedia(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_media_gallery_put_gallery_media_galleryhasmedia;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_put_gallery_media_galleryhasmedia')), array (  '_controller' => 'sonata.media.controller.api.gallery:putGalleryMediaGalleryhasmediaAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_put_gallery_media_galleryhasmedia:

                        // sonata_api_media_gallery_delete_gallery_media_galleryhasmedia
                        if (preg_match('#^/api/media/galleries/(?P<galleryId>[^/]++)/media/(?P<mediaId>[^/]++)/galleryhasmedia(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_media_gallery_delete_gallery_media_galleryhasmedia;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_delete_gallery_media_galleryhasmedia')), array (  '_controller' => 'sonata.media.controller.api.gallery:deleteGalleryMediaGalleryhasmediaAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_delete_gallery_media_galleryhasmedia:

                        // sonata_api_media_gallery_delete_gallery
                        if (preg_match('#^/api/media/galleries/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_media_gallery_delete_gallery;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_gallery_delete_gallery')), array (  '_controller' => 'sonata.media.controller.api.gallery:deleteGalleryAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_gallery_delete_gallery:

                    }

                    if (0 === strpos($pathinfo, '/api/media/media')) {
                        // sonata_api_media_media_get_media
                        if (preg_match('#^/api/media/media(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_media_media_get_media;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_media_get_media')), array (  '_controller' => 'sonata.media.controller.api.media:getMediaAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_media_get_media:

                        // sonata_api_media_media_get_medium
                        if (preg_match('#^/api/media/media/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_media_media_get_medium;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_media_get_medium')), array (  '_controller' => 'sonata.media.controller.api.media:getMediumAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_media_get_medium:

                        // sonata_api_media_media_get_medium_formats
                        if (preg_match('#^/api/media/media/(?P<id>[^/]++)/formats(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_media_media_get_medium_formats;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_media_get_medium_formats')), array (  '_controller' => 'sonata.media.controller.api.media:getMediumFormatsAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_media_get_medium_formats:

                        // sonata_api_media_media_get_medium_binary
                        if (preg_match('#^/api/media/media/(?P<id>[^/]++)/binaries/(?P<format>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_media_media_get_medium_binary;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_media_get_medium_binary')), array (  '_controller' => 'sonata.media.controller.api.media:getMediumBinaryAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_media_get_medium_binary:

                        // sonata_api_media_media_delete_medium
                        if (preg_match('#^/api/media/media/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_media_media_delete_medium;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_media_delete_medium')), array (  '_controller' => 'sonata.media.controller.api.media:deleteMediumAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_media_delete_medium:

                        // sonata_api_media_media_put_medium
                        if (preg_match('#^/api/media/media/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_media_media_put_medium;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_media_put_medium')), array (  '_controller' => 'sonata.media.controller.api.media:putMediumAction',  '_format' => NULL,));
                        }
                        not_sonata_api_media_media_put_medium:

                    }

                    // sonata_api_media_media_post_provider_medium
                    if (0 === strpos($pathinfo, '/api/media/providers') && preg_match('#^/api/media/providers/(?P<provider>[A-Za-z0-9.]*)/media(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_sonata_api_media_media_post_provider_medium;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_media_post_provider_medium')), array (  '_controller' => 'sonata.media.controller.api.media:postProviderMediumAction',  '_format' => NULL,));
                    }
                    not_sonata_api_media_media_post_provider_medium:

                    // sonata_api_media_media_put_medium_binary_content
                    if (0 === strpos($pathinfo, '/api/media/media') && preg_match('#^/api/media/media/(?P<id>[^/]++)/binary/content(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'PUT') {
                            $allow[] = 'PUT';
                            goto not_sonata_api_media_media_put_medium_binary_content;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_media_media_put_medium_binary_content')), array (  '_controller' => 'sonata.media.controller.api.media:putMediumBinaryContentAction',  '_format' => NULL,));
                    }
                    not_sonata_api_media_media_put_medium_binary_content:

                }

                if (0 === strpos($pathinfo, '/api/page')) {
                    if (0 === strpos($pathinfo, '/api/page/blocks')) {
                        // sonata_api_block_get_block
                        if (preg_match('#^/api/page/blocks/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_block_get_block;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_block_get_block')), array (  '_controller' => 'sonata.page.controller.api.block:getBlockAction',  '_format' => NULL,));
                        }
                        not_sonata_api_block_get_block:

                        // sonata_api_block_put_block
                        if (preg_match('#^/api/page/blocks/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_block_put_block;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_block_put_block')), array (  '_controller' => 'sonata.page.controller.api.block:putBlockAction',  '_format' => NULL,));
                        }
                        not_sonata_api_block_put_block:

                        // sonata_api_block_delete_block
                        if (preg_match('#^/api/page/blocks/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_block_delete_block;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_block_delete_block')), array (  '_controller' => 'sonata.page.controller.api.block:deleteBlockAction',  '_format' => NULL,));
                        }
                        not_sonata_api_block_delete_block:

                    }

                    if (0 === strpos($pathinfo, '/api/page/pages')) {
                        // sonata_api_page_get_pages
                        if (preg_match('#^/api/page/pages(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_page_get_pages;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_get_pages')), array (  '_controller' => 'sonata.page.controller.api.page:getPagesAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_get_pages:

                        // sonata_api_page_get_page
                        if (preg_match('#^/api/page/pages/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_page_get_page;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_get_page')), array (  '_controller' => 'sonata.page.controller.api.page:getPageAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_get_page:

                        // sonata_api_page_get_page_blocks
                        if (preg_match('#^/api/page/pages/(?P<id>[^/]++)/blocks(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_page_get_page_blocks;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_get_page_blocks')), array (  '_controller' => 'sonata.page.controller.api.page:getPageBlocksAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_get_page_blocks:

                        // sonata_api_page_get_page_pages
                        if (preg_match('#^/api/page/pages/(?P<id>[^/]++)/pages(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_page_get_page_pages;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_get_page_pages')), array (  '_controller' => 'sonata.page.controller.api.page:getPagePagesAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_get_page_pages:

                        // sonata_api_page_post_page_block
                        if (preg_match('#^/api/page/pages/(?P<id>[^/]++)/blocks(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_page_post_page_block;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_post_page_block')), array (  '_controller' => 'sonata.page.controller.api.page:postPageBlockAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_post_page_block:

                        // sonata_api_page_post_page
                        if (preg_match('#^/api/page/pages(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_page_post_page;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_post_page')), array (  '_controller' => 'sonata.page.controller.api.page:postPageAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_post_page:

                        // sonata_api_page_put_page
                        if (preg_match('#^/api/page/pages/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_page_put_page;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_put_page')), array (  '_controller' => 'sonata.page.controller.api.page:putPageAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_put_page:

                        // sonata_api_page_delete_page
                        if (preg_match('#^/api/page/pages/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_page_delete_page;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_delete_page')), array (  '_controller' => 'sonata.page.controller.api.page:deletePageAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_delete_page:

                        // sonata_api_page_post_page_snapshot
                        if (preg_match('#^/api/page/pages/(?P<id>[^/]++)/snapshots(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_page_post_page_snapshot;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_post_page_snapshot')), array (  '_controller' => 'sonata.page.controller.api.page:postPageSnapshotAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_post_page_snapshot:

                        // sonata_api_page_post_pages_snapshots
                        if (0 === strpos($pathinfo, '/api/page/pages/snapshots') && preg_match('#^/api/page/pages/snapshots(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_page_post_pages_snapshots;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_page_post_pages_snapshots')), array (  '_controller' => 'sonata.page.controller.api.page:postPagesSnapshotsAction',  '_format' => NULL,));
                        }
                        not_sonata_api_page_post_pages_snapshots:

                    }

                    if (0 === strpos($pathinfo, '/api/page/sites')) {
                        // sonata_api_site_get_sites
                        if (preg_match('#^/api/page/sites(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_site_get_sites;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_site_get_sites')), array (  '_controller' => 'sonata.page.controller.api.site:getSitesAction',  '_format' => NULL,));
                        }
                        not_sonata_api_site_get_sites:

                        // sonata_api_site_get_site
                        if (preg_match('#^/api/page/sites/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_site_get_site;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_site_get_site')), array (  '_controller' => 'sonata.page.controller.api.site:getSiteAction',  '_format' => NULL,));
                        }
                        not_sonata_api_site_get_site:

                        // sonata_api_site_put_site
                        if (preg_match('#^/api/page/sites/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_site_put_site;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_site_put_site')), array (  '_controller' => 'sonata.page.controller.api.site:putSiteAction',  '_format' => NULL,));
                        }
                        not_sonata_api_site_put_site:

                        // sonata_api_site_delete_site
                        if (preg_match('#^/api/page/sites/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_site_delete_site;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_site_delete_site')), array (  '_controller' => 'sonata.page.controller.api.site:deleteSiteAction',  '_format' => NULL,));
                        }
                        not_sonata_api_site_delete_site:

                    }

                }

                if (0 === strpos($pathinfo, '/api/user')) {
                    if (0 === strpos($pathinfo, '/api/user/users')) {
                        // sonata_api_user_user_get_users
                        if (preg_match('#^/api/user/users(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_user_user_get_users;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_user_get_users')), array (  '_controller' => 'sonata.user.controller.api.user:getUsersAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_user_get_users:

                        // sonata_api_user_user_get_user
                        if (preg_match('#^/api/user/users/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_user_user_get_user;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_user_get_user')), array (  '_controller' => 'sonata.user.controller.api.user:getUserAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_user_get_user:

                        // sonata_api_user_user_post_user
                        if (preg_match('#^/api/user/users(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_user_user_post_user;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_user_post_user')), array (  '_controller' => 'sonata.user.controller.api.user:postUserAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_user_post_user:

                        // sonata_api_user_user_put_user
                        if (preg_match('#^/api/user/users/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_user_user_put_user;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_user_put_user')), array (  '_controller' => 'sonata.user.controller.api.user:putUserAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_user_put_user:

                        // sonata_api_user_user_delete_user
                        if (preg_match('#^/api/user/users/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_user_user_delete_user;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_user_delete_user')), array (  '_controller' => 'sonata.user.controller.api.user:deleteUserAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_user_delete_user:

                        // sonata_api_user_user_post_user_group
                        if (preg_match('#^/api/user/users/(?P<userId>[^/]++)/groups/(?P<groupId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_user_user_post_user_group;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_user_post_user_group')), array (  '_controller' => 'sonata.user.controller.api.user:postUserGroupAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_user_post_user_group:

                        // sonata_api_user_user_delete_user_group
                        if (preg_match('#^/api/user/users/(?P<userId>[^/]++)/groups/(?P<groupId>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_user_user_delete_user_group;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_user_delete_user_group')), array (  '_controller' => 'sonata.user.controller.api.user:deleteUserGroupAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_user_delete_user_group:

                    }

                    if (0 === strpos($pathinfo, '/api/user/groups')) {
                        // sonata_api_user_group_get_groups
                        if (preg_match('#^/api/user/groups(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_user_group_get_groups;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_group_get_groups')), array (  '_controller' => 'sonata.user.controller.api.group:getGroupsAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_group_get_groups:

                        // sonata_api_user_group_get_group
                        if (preg_match('#^/api/user/groups/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_sonata_api_user_group_get_group;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_group_get_group')), array (  '_controller' => 'sonata.user.controller.api.group:getGroupAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_group_get_group:

                        // sonata_api_user_group_post_group
                        if (preg_match('#^/api/user/groups(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_sonata_api_user_group_post_group;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_group_post_group')), array (  '_controller' => 'sonata.user.controller.api.group:postGroupAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_group_post_group:

                        // sonata_api_user_group_put_group
                        if (preg_match('#^/api/user/groups/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'PUT') {
                                $allow[] = 'PUT';
                                goto not_sonata_api_user_group_put_group;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_group_put_group')), array (  '_controller' => 'sonata.user.controller.api.group:putGroupAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_group_put_group:

                        // sonata_api_user_group_delete_group
                        if (preg_match('#^/api/user/groups/(?P<id>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'DELETE') {
                                $allow[] = 'DELETE';
                                goto not_sonata_api_user_group_delete_group;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_api_user_group_delete_group')), array (  '_controller' => 'sonata.user.controller.api.group:deleteGroupAction',  '_format' => NULL,));
                        }
                        not_sonata_api_user_group_delete_group:

                    }

                    if (0 === strpos($pathinfo, '/api/users')) {
                        // get_users_check
                        if (0 === strpos($pathinfo, '/api/users/check') && preg_match('#^/api/users/check(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_get_users_check;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_users_check')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::getUsersCheckAction',  '_format' => NULL,));
                        }
                        not_get_users_check:

                        if (0 === strpos($pathinfo, '/api/users/log')) {
                            if (0 === strpos($pathinfo, '/api/users/logins')) {
                                // post_users_login_facebook
                                if (0 === strpos($pathinfo, '/api/users/logins/facebooks') && preg_match('#^/api/users/logins/facebooks(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                    if ($this->context->getMethod() != 'POST') {
                                        $allow[] = 'POST';
                                        goto not_post_users_login_facebook;
                                    }

                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_users_login_facebook')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::postUsersLoginFacebookAction',  '_format' => NULL,));
                                }
                                not_post_users_login_facebook:

                                // post_users_login
                                if (preg_match('#^/api/users/logins(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                    if ($this->context->getMethod() != 'POST') {
                                        $allow[] = 'POST';
                                        goto not_post_users_login;
                                    }

                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_users_login')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::postUsersLoginAction',  '_format' => NULL,));
                                }
                                not_post_users_login:

                            }

                            // get_users_logout
                            if (0 === strpos($pathinfo, '/api/users/logout') && preg_match('#^/api/users/logout(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_get_users_logout;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_users_logout')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::getUsersLogoutAction',  '_format' => NULL,));
                            }
                            not_get_users_logout:

                        }

                        // post_users_register
                        if (0 === strpos($pathinfo, '/api/users/registers') && preg_match('#^/api/users/registers(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_post_users_register;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_users_register')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::postUsersRegisterAction',  '_format' => NULL,));
                        }
                        not_post_users_register:

                        // post_users_edit
                        if (0 === strpos($pathinfo, '/api/users/edits') && preg_match('#^/api/users/edits(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_post_users_edit;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_users_edit')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::postUsersEditAction',  '_format' => NULL,));
                        }
                        not_post_users_edit:

                        if (0 === strpos($pathinfo, '/api/users/in')) {
                            // get_users_info
                            if (0 === strpos($pathinfo, '/api/users/info') && preg_match('#^/api/users/info(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_get_users_info;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_users_info')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::getUsersInfoAction',  '_format' => NULL,));
                            }
                            not_get_users_info:

                            if (0 === strpos($pathinfo, '/api/users/interest')) {
                                // get_users_interest
                                if (preg_match('#^/api/users/interest(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                        $allow = array_merge($allow, array('GET', 'HEAD'));
                                        goto not_get_users_interest;
                                    }

                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_users_interest')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::getUsersInterestAction',  '_format' => NULL,));
                                }
                                not_get_users_interest:

                                // post_users_interest
                                if (0 === strpos($pathinfo, '/api/users/interests') && preg_match('#^/api/users/interests(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                    if ($this->context->getMethod() != 'POST') {
                                        $allow[] = 'POST';
                                        goto not_post_users_interest;
                                    }

                                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_users_interest')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::postUsersInterestAction',  '_format' => NULL,));
                                }
                                not_post_users_interest:

                            }

                        }

                        if (0 === strpos($pathinfo, '/api/users/coaching')) {
                            // get_users_coaching
                            if (preg_match('#^/api/users/coaching(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_get_users_coaching;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_users_coaching')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::getUsersCoachingAction',  '_format' => NULL,));
                            }
                            not_get_users_coaching:

                            // post_users_coaching
                            if (0 === strpos($pathinfo, '/api/users/coachings') && preg_match('#^/api/users/coachings(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_post_users_coaching;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_users_coaching')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::postUsersCoachingAction',  '_format' => NULL,));
                            }
                            not_post_users_coaching:

                        }

                        if (0 === strpos($pathinfo, '/api/users/notif')) {
                            // get_users_notif
                            if (preg_match('#^/api/users/notif(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                    $allow = array_merge($allow, array('GET', 'HEAD'));
                                    goto not_get_users_notif;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_users_notif')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::getUsersNotifAction',  '_format' => NULL,));
                            }
                            not_get_users_notif:

                            // post_users_notif
                            if (0 === strpos($pathinfo, '/api/users/notifs') && preg_match('#^/api/users/notifs(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                                if ($this->context->getMethod() != 'POST') {
                                    $allow[] = 'POST';
                                    goto not_post_users_notif;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_users_notif')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::postUsersNotifAction',  '_format' => NULL,));
                            }
                            not_post_users_notif:

                        }

                        // post_users_image
                        if (0 === strpos($pathinfo, '/api/users/images') && preg_match('#^/api/users/images(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_post_users_image;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_users_image')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::postUsersImageAction',  '_format' => NULL,));
                        }
                        not_post_users_image:

                        // get_users_profile
                        if (preg_match('#^/api/users/(?P<id>[^/]++)/profile(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_get_users_profile;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_users_profile')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::getUsersProfileAction',  '_format' => NULL,));
                        }
                        not_get_users_profile:

                        // get_users_friends
                        if (0 === strpos($pathinfo, '/api/users/friends') && preg_match('#^/api/users/friends(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_get_users_friends;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_users_friends')), array (  '_controller' => 'Application\\Sonata\\UserBundle\\Controller\\Api\\UserController::getUsersFriendsAction',  '_format' => NULL,));
                        }
                        not_get_users_friends:

                    }

                }

                if (0 === strpos($pathinfo, '/api/events')) {
                    // post_event_create
                    if (0 === strpos($pathinfo, '/api/events/creates') && preg_match('#^/api/events/creates(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_post_event_create;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_event_create')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\EventController::postEventCreateAction',  '_format' => NULL,));
                    }
                    not_post_event_create:

                    // post_event_search
                    if (0 === strpos($pathinfo, '/api/events/searches') && preg_match('#^/api/events/searches(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_post_event_search;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_event_search')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\EventController::postEventSearchAction',  '_format' => NULL,));
                    }
                    not_post_event_search:

                    // get_event_show
                    if (preg_match('#^/api/events/(?P<slug>[^/]++)/show(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_get_event_show;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_event_show')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\EventController::getEventShowAction',  '_format' => NULL,));
                    }
                    not_get_event_show:

                    // post_event_participe
                    if (preg_match('#^/api/events/(?P<id>[^/]++)/participes/(?P<enable>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_post_event_participe;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_event_participe')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\EventController::postEventParticipeAction',  '_format' => NULL,));
                    }
                    not_post_event_participe:

                    // post_event_del_participe
                    if (preg_match('#^/api/events/(?P<id>[^/]++)/dels/participes(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_post_event_del_participe;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_event_del_participe')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\EventController::postEventDelParticipeAction',  '_format' => NULL,));
                    }
                    not_post_event_del_participe:

                }

                // get_notification
                if (0 === strpos($pathinfo, '/api/notification') && preg_match('#^/api/notification(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_get_notification;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_notification')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\NotificationController::getNotificationAction',  '_format' => NULL,));
                }
                not_get_notification:

                if (0 === strpos($pathinfo, '/api/counter')) {
                    // get_counter_all
                    if (0 === strpos($pathinfo, '/api/counter/all') && preg_match('#^/api/counter/all(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_get_counter_all;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_counter_all')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\CounterController::getCounterAllAction',  '_format' => NULL,));
                    }
                    not_get_counter_all:

                    // get_counter_notification
                    if (0 === strpos($pathinfo, '/api/counter/notification') && preg_match('#^/api/counter/notification(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_get_counter_notification;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_counter_notification')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\CounterController::getCounterNotificationAction',  '_format' => NULL,));
                    }
                    not_get_counter_notification:

                    // get_counter_notification_user
                    if (0 === strpos($pathinfo, '/api/counters') && preg_match('#^/api/counters/(?P<userid>[^/]++)/notification/user(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_get_counter_notification_user;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_counter_notification_user')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\CounterController::getCounterNotificationUserAction',  '_format' => NULL,));
                    }
                    not_get_counter_notification_user:

                }

                // get_mur
                if (0 === strpos($pathinfo, '/api/mur') && preg_match('#^/api/mur(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_get_mur;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_mur')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\MurController::getMurAction',  '_format' => NULL,));
                }
                not_get_mur:

                // get_friend
                if (0 === strpos($pathinfo, '/api/friend') && preg_match('#^/api/friend(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_get_friend;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_friend')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\FriendsController::getFriendAction',  '_format' => NULL,));
                }
                not_get_friend:

                if (0 === strpos($pathinfo, '/api/likems')) {
                    // get_likem
                    if (preg_match('#^/api/likems/(?P<userid>[^/\\.]++)(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_get_likem;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_likem')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\LikemController::getLikemAction',  '_format' => NULL,));
                    }
                    not_get_likem:

                    // get_likem_count
                    if (preg_match('#^/api/likems/(?P<userid>[^/]++)/count(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_get_likem_count;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_likem_count')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\LikemController::getLikemCountAction',  '_format' => NULL,));
                    }
                    not_get_likem_count:

                    // get_likem_del
                    if (preg_match('#^/api/likems/(?P<userid>[^/]++)/del(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_get_likem_del;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_likem_del')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\LikemController::getLikemDelAction',  '_format' => NULL,));
                    }
                    not_get_likem_del:

                    // get_likem_countlik
                    if (preg_match('#^/api/likems/(?P<userid>[^/]++)/countlik(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_get_likem_countlik;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_likem_countlik')), array (  '_controller' => 'Comparator\\Bundle\\EventBundle\\Controller\\Api\\LikemController::getLikemCountlikAction',  '_format' => NULL,));
                    }
                    not_get_likem_countlik:

                }

                if (0 === strpos($pathinfo, '/api/walls')) {
                    // post_wall_text
                    if (0 === strpos($pathinfo, '/api/walls/texts') && preg_match('#^/api/walls/texts(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_post_wall_text;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_wall_text')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\Api\\StatutController::postWallTextAction',  '_format' => NULL,));
                    }
                    not_post_wall_text:

                    // post_wall_videos
                    if (0 === strpos($pathinfo, '/api/walls/videos') && preg_match('#^/api/walls/videos(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_post_wall_videos;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_wall_videos')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\Api\\StatutController::postWallVideosAction',  '_format' => NULL,));
                    }
                    not_post_wall_videos:

                    // post_wall_picture
                    if (0 === strpos($pathinfo, '/api/walls/pictures') && preg_match('#^/api/walls/pictures(?:\\.(?P<_format>json|xml|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_post_wall_picture;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_wall_picture')), array (  '_controller' => 'Comparator\\Bundle\\StatutBundle\\Controller\\Api\\StatutController::postWallPictureAction',  '_format' => NULL,));
                    }
                    not_post_wall_picture:

                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
