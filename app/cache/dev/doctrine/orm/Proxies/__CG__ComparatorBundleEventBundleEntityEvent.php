<?php

namespace Proxies\__CG__\Comparator\Bundle\EventBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Event extends \Comparator\Bundle\EventBundle\Entity\Event implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'title', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'nbPlayer', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'startDate', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'startTime', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'endTime', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'description', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'slug', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'createdAt', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'updatedAt', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'category', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'user', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'handi', 'address', 'locality', 'country', 'lat', 'lng');
        }

        return array('__isInitialized__', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'title', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'nbPlayer', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'startDate', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'startTime', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'endTime', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'description', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'slug', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'createdAt', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'updatedAt', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'category', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'user', '' . "\0" . 'Comparator\\Bundle\\EventBundle\\Entity\\Event' . "\0" . 'handi', 'address', 'locality', 'country', 'lat', 'lng');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Event $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function __toString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', array());

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle($title)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTitle', array($title));

        return parent::setTitle($title);
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTitle', array());

        return parent::getTitle();
    }

    /**
     * {@inheritDoc}
     */
    public function setHandi($handi)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setHandi', array($handi));

        return parent::setHandi($handi);
    }

    /**
     * {@inheritDoc}
     */
    public function getHandi()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHandi', array());

        return parent::getHandi();
    }

    /**
     * {@inheritDoc}
     */
    public function setNbPlayer($nbPlayer)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNbPlayer', array($nbPlayer));

        return parent::setNbPlayer($nbPlayer);
    }

    /**
     * {@inheritDoc}
     */
    public function getNbPlayer()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNbPlayer', array());

        return parent::getNbPlayer();
    }

    /**
     * {@inheritDoc}
     */
    public function setStartDate($startDate)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStartDate', array($startDate));

        return parent::setStartDate($startDate);
    }

    /**
     * {@inheritDoc}
     */
    public function getStartDate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStartDate', array());

        return parent::getStartDate();
    }

    /**
     * {@inheritDoc}
     */
    public function setStartTime($startTime)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStartTime', array($startTime));

        return parent::setStartTime($startTime);
    }

    /**
     * {@inheritDoc}
     */
    public function getStartTime()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStartTime', array());

        return parent::getStartTime();
    }

    /**
     * {@inheritDoc}
     */
    public function setEndTime($endTime)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEndTime', array($endTime));

        return parent::setEndTime($endTime);
    }

    /**
     * {@inheritDoc}
     */
    public function getEndTime()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEndTime', array());

        return parent::getEndTime();
    }

    /**
     * {@inheritDoc}
     */
    public function setDescription($description)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDescription', array($description));

        return parent::setDescription($description);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDescription', array());

        return parent::getDescription();
    }

    /**
     * {@inheritDoc}
     */
    public function setSlug($slug)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSlug', array($slug));

        return parent::setSlug($slug);
    }

    /**
     * {@inheritDoc}
     */
    public function getSlug()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSlug', array());

        return parent::getSlug();
    }

    /**
     * {@inheritDoc}
     */
    public function setCategory(\Comparator\Bundle\EventBundle\Entity\Category $category = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCategory', array($category));

        return parent::setCategory($category);
    }

    /**
     * {@inheritDoc}
     */
    public function getCategory()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCategory', array());

        return parent::getCategory();
    }

    /**
     * {@inheritDoc}
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUser', array($user));

        return parent::setUser($user);
    }

    /**
     * {@inheritDoc}
     */
    public function getUser()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUser', array());

        return parent::getUser();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($createdAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', array($createdAt));

        return parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', array());

        return parent::getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($updatedAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', array($updatedAt));

        return parent::setUpdatedAt($updatedAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', array());

        return parent::getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setAddress($address)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAddress', array($address));

        return parent::setAddress($address);
    }

    /**
     * {@inheritDoc}
     */
    public function getAddress()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAddress', array());

        return parent::getAddress();
    }

    /**
     * {@inheritDoc}
     */
    public function setLocality($locality)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLocality', array($locality));

        return parent::setLocality($locality);
    }

    /**
     * {@inheritDoc}
     */
    public function getLocality()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLocality', array());

        return parent::getLocality();
    }

    /**
     * {@inheritDoc}
     */
    public function setCountry($country)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCountry', array($country));

        return parent::setCountry($country);
    }

    /**
     * {@inheritDoc}
     */
    public function getCountry()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountry', array());

        return parent::getCountry();
    }

    /**
     * {@inheritDoc}
     */
    public function getLat()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLat', array());

        return parent::getLat();
    }

    /**
     * {@inheritDoc}
     */
    public function setLat($lat)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLat', array($lat));

        return parent::setLat($lat);
    }

    /**
     * {@inheritDoc}
     */
    public function getLng()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLng', array());

        return parent::getLng();
    }

    /**
     * {@inheritDoc}
     */
    public function setLng($lng)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLng', array($lng));

        return parent::setLng($lng);
    }

}
