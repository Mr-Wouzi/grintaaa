<?php

/* ComparatorBaseBundle:Base:home.html.twig */
class __TwigTemplate_0192b5fee123e2721bd1a4a2b37dba0fc9d78e5b056cf03d538e42aa0c2aa727 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array())) {
            // line 3
            echo "       <!--Debut containt-->
       <div class=\"container containt-dash\"> 
           <!--Debut left-block-->
           <div class=\"col-md-2 no-padding-right\"> 
               <div class=\"block profil-block\">
                   <div class=\"profil-dash\"> 
                         <div class=\"col-md-12 no-padding\">
\t\t\t\t\t\t    <div class=\"grid\">
\t\t\t\t\t         <figure class=\"effect-winston\">
\t\t\t\t\t\t      ";
            // line 12
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
            echo "
\t\t\t\t\t\t      <figcaption>  
\t\t\t\t\t\t\t   <p> 
\t\t\t\t\t\t\t\t<a href=\"";
            // line 15
            echo $this->env->getExtension('routing')->getPath("user_file_new");
            echo "\"><i class=\"fa fa-fw\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
            echo "</i></a>
\t\t\t\t\t\t\t   </p>
\t\t\t\t\t\t      </figcaption>\t\t\t
\t\t\t\t\t         </figure>
\t\t\t\t            </div>            
                         </div>
                         <div class=\"col-md-12 no-padding\">  
                             <p class=\"titre\">";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
            echo "</p>

\t\t\t\t\t\t\t<p class=\"titre-user\">      
\t\t\t\t\t\t\t\t<a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
            echo "\"> 
\t\t\t\t\t\t\t\t\t<img src=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
            echo "\" class=\"img-loginnn\" />";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
            echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</p>  
                             <!--p class=\"pro\">Profil</p-->
                         </div>
                         
                   </div>              
                   <ul class=\"list-unstyled\">
                       <li>
                           <a href=\"";
            // line 35
            echo $this->env->getExtension('routing')->getPath("home");
            echo "\" class=\"active\">
                              <img src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
            echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
            // line 40
            echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
            echo "\">
                            ";
            // line 41
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
            echo "
                           </a>
                       </li>
                       
                       <li>
                           <a href=\"";
            // line 46
            echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
            echo "\">
                             ";
            // line 47
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
            echo "

                           </a>
                       </li>
                       <li>
                            ";
            // line 52
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
            echo "
                       </li>
                       <li>
                           <a href=\"";
            // line 55
            echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
            echo "\">
                              ";
            // line 56
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
            echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
            // line 60
            echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
            echo "\">
                               ";
            // line 61
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
            echo "
                           </a>
                       </li>
                   </ul>
               </div>
 
               ";
            // line 67
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
            echo "
               ";
            // line 68
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
            echo "


           </div>
           <!--Fin left-block-->
           <!--Debut center-block-->
           <div class=\"col-md-10\"> 
               ";
            // line 75
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:User:complete"));
            echo "
              <div class=\"block exprimer\">
                   <div class=\"titre green\">
                     <img src=\"";
            // line 78
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/pencil.png"), "html", null, true);
            echo "\">
                     <h4>";
            // line 79
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous"), "html", null, true);
            echo "</h4>
                   </div>
                   <div class=\"add-act\">
\t\t\t\t   
                      <ul class=\"nav nav-tabs\" role=\"tablist\">
                        <li role=\"presentation\" class=\"active\"><a href=\"#statut\" aria-controls=\"statut\" role=\"tab\" data-toggle=\"tab\"><img src=\"";
            // line 84
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/statut.png"), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous.staut"), "html", null, true);
            echo "</a></li>
                        <li role=\"presentation\"><a href=\"#photo\" aria-controls=\"photo\" role=\"tab\" data-toggle=\"tab\"><img src=\"";
            // line 85
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/camera-photo.png"), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous.photo"), "html", null, true);
            echo "</a></li>
                        <li role=\"presentation\"><a href=\"#video\" aria-controls=\"video\" role=\"tab\" data-toggle=\"tab\" class=\"video\"><img src=\"";
            // line 86
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/camera-video.png"), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous.video"), "html", null, true);
            echo "</a></li>
                      </ul>

\t\t\t\t\t  
                      <div class=\"tab-content\">
                        <div role=\"tabpanel\" class=\"tab-pane active\" id=\"statut\">
                        <form method=\"post\" action=\"";
            // line 92
            echo $this->env->getExtension('routing')->getPath("mur_statut_create");
            echo "\">
                                <textarea placeholder=\"";
            // line 93
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous"), "html", null, true);
            echo "\" name=\"textarea\"></textarea>
                                <div class=\"send\">
                                   <button type=\"submit\" class=\"btn btn-grinta\">";
            // line 95
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.publier"), "html", null, true);
            echo "</button>
                                </div>
                            </form>
                        </div>
                        <div role=\"tabpanel\" class=\"tab-pane\" id=\"photo\">
                             ";
            // line 100
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorStatutBundle:Statut:new"));
            echo "
                        </div>
                        <div role=\"tabpanel\" class=\"tab-pane\" id=\"video\">
                            <form method=\"post\" action=\"";
            // line 103
            echo $this->env->getExtension('routing')->getPath("mur_video_create");
            echo "\">
                                <textarea placeholder=\"";
            // line 104
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.video.youtube"), "html", null, true);
            echo "\" name=\"video\"></textarea>
                                <div class=\"send\">
                                   <button type=\"submit\" class=\"btn btn-grinta\">";
            // line 106
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.publier"), "html", null, true);
            echo "</button>
                                </div>
                            </form>
                        </div>
                      </div>

                    </div>
               </div>
           
 
               ";
            // line 116
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:murEventFriends"));
            echo "



           </div>
           <!--Fin center-block-->
           <!--Debut right-block-->
           <!--div class=\"col-sm-2\">
               <div class=\"block photo\">
                  <div class=\"title-block\">
                    <h4 class=\"green\">Album Photo</h4>
                  </div>
                  <div class=\"body-photo\">
                      <div class=\"album\">
                         <div class=\"col-xs-6 image-wrapper\">
                            <img src=\"";
            // line 131
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/photo/photo1.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                            <p>Fifa Sport</p>
                         </div>
                         <div class=\"col-xs-6 image-wrapper\">
                            <img src=\"";
            // line 135
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/photo/photo2.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                            <p>Atlantic days</p>
                         </div>
                         <div class=\"col-xs-6 image-wrapper\">
                            <img src=\"";
            // line 139
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/photo/photo3.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                            <p>Gyms</p>
                         </div>
                         <div class=\"col-xs-6 image-wrapper\">
                            <img src=\"";
            // line 143
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/photo/photo4.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                            <p>Yoga coaching</p>
                         </div>
                         <div class=\"col-xs-6 image-wrapper\">
                            <img src=\"";
            // line 147
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/photo/photo5.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                            <p>Our athletes</p>
                         </div>
                         <div class=\"col-xs-6 image-wrapper\">
                            <img src=\"";
            // line 151
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/photo/photo6.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                            <p>champions</p>
                         </div>
                      </div>
                  </div>
               </div>
               <div class=\"block video\">
                  <div class=\"title-block\">
                    <h4 class=\"green\">Vidéo</h4>
                  </div>
                  <div class=\"body-video\">
                      <div class=\"video\">
                         <video width=\"100%\" height=\"auto\" controls>
                          <source src=\"video/Messi.mp4\" type=\"video/mp4\">
                         </video>
                         <a href=\"#\"><p>La reussite a un titre</p></a>
                      </div>
                      <div class=\"video\">
                         <video width=\"100%\" height=\"auto\" controls>
                          <source src=\"";
            // line 170
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/video/PoloClub.mp4"), "html", null, true);
            echo "\" type=\"video/mp4\">
                         </video>
                         <a href=\"#\"><p>Polo Club</p></a>
                      </div>
                      <div class=\"video\">
                         <video width=\"100%\" height=\"auto\" controls>
                          <source src=\"";
            // line 176
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/video/nadal.mp4"), "html", null, true);
            echo "\" type=\"video/mp4\">
                         </video>
                         <a href=\"#\"><p>New Delhi</p></a>
                      </div>
                  </div>
               </div>
           </div>
           <!--Fin right-block-->  
       </div>
       <!--Debut containt-->
\t   <br><br><br>
   </div>
    ";
        } else {
            // line 188
            echo "  

    ";
            // line 190
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationHWIOAuthBundle:Connect:connect"));
            echo "
               <!--Debut containt-->
";
        }
        // line 193
        echo "<style>
 .footer{padding:0px;}
</style>

<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>
<!-- auto -->
<ins class=\"adsbygoogle\"
     style=\"display:block\"
     data-ad-client=\"ca-pub-3318666783044944\"
     data-ad-slot=\"6484240518\"
     data-ad-format=\"auto\"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
";
    }

    public function getTemplateName()
    {
        return "ComparatorBaseBundle:Base:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  356 => 193,  350 => 190,  346 => 188,  330 => 176,  321 => 170,  299 => 151,  292 => 147,  285 => 143,  278 => 139,  271 => 135,  264 => 131,  246 => 116,  233 => 106,  228 => 104,  224 => 103,  218 => 100,  210 => 95,  205 => 93,  201 => 92,  190 => 86,  184 => 85,  178 => 84,  170 => 79,  166 => 78,  160 => 75,  150 => 68,  146 => 67,  137 => 61,  133 => 60,  126 => 56,  122 => 55,  116 => 52,  108 => 47,  104 => 46,  96 => 41,  92 => 40,  83 => 36,  79 => 35,  65 => 26,  61 => 25,  53 => 22,  41 => 15,  35 => 12,  24 => 3,  22 => 2,  19 => 1,);
    }
}
