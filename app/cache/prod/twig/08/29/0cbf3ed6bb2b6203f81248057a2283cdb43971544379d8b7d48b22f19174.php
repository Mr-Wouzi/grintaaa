<?php

/* ComparatorEventBundle:Notification:countNotifications.html.twig */
class __TwigTemplate_08290cbf3ed6bb2b6203f81248057a2283cdb43971544379d8b7d48b22f19174 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["notification"]) ? $context["notification"] : null) > 0)) {
            // line 2
            echo "<span class=\"nbr-notif\">";
            echo twig_escape_filter($this->env, (isset($context["notification"]) ? $context["notification"] : null), "html", null, true);
            echo "</span>
";
        }
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Notification:countNotifications.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  21 => 2,  19 => 1,);
    }
}
