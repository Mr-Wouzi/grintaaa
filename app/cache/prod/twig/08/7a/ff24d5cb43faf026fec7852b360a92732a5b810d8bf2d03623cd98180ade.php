<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_087aff24d5cb43faf026fec7852b360a92732a5b810d8bf2d03623cd98180ade extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 42
        echo "<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = \"//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.6\";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>";
    }

    // line 1
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 2
        echo "
                <div class=\"containt\">

                  <div class=\"container\">
        <div id=\"fb-root\"></div>

                     <div class=\"row col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 login-error\">
                         <div class=\"login-error-form\">
\t\t\t\t\t\t\t<p class=\"text-center\">";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>

\t\t\t\t\t\t\t<a  class=\"btn btn-grinta btn-welcome\" href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.welcome"), "html", null, true);
        echo "</a><br>
\t\t\t\t\t\t\t<div align=\"center\" class=\"fb-share-button\" data-href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "schemeAndHttpHost", array()), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "getBaseURL", array(), "method"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["currentPath"]) ? $context["currentPath"] : null), "html", null, true);
        echo "\" data-layout=\"button_count\" data-size=\"small\" data-mobile-iframe=\"false\">
\t\t\t\t\t\t\t\t<a class=\"fb-xfbml-parse-ignore\" target=\"_blank\" href=\"https://www.facebook.com/sharer/sharer.php?u='";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "schemeAndHttpHost", array()), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "getBaseURL", array(), "method"), "html", null, true);
        echo "';src=sdkpreparse\">Partager</a>
\t\t\t\t\t\t\t</div>
                         </div>
                     </div>
                  </div>
               </div>
               <!--Fin containt-->
           </div>
        </div>



    ";
        // line 26
        if ( !twig_test_empty($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()))) {
            // line 27
            echo "        ";
            $context["targetUrl"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => (("_security." . $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "security", array()), "token", array()), "providerKey", array())) . ".target_path")), "method");
            // line 28
            echo "        ";
            if ( !twig_test_empty((isset($context["targetUrl"]) ? $context["targetUrl"] : null))) {
                // line 29
                echo "    <div class=\"content-login\">
        <div class=\"container\">
            <div class=\"col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 verif-login\">
                <div class=\"top-verif\">
\t\t<p><a href=\"";
                // line 33
                echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
                echo "</a></p>
                </div>

            </div>
        </div>
    </div>
\t\t";
            }
            // line 40
            echo "    ";
        }
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  104 => 40,  92 => 33,  86 => 29,  83 => 28,  80 => 27,  78 => 26,  62 => 14,  56 => 13,  50 => 12,  45 => 10,  35 => 2,  32 => 1,  22 => 42,  20 => 1,);
    }
}
