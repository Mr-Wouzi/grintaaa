<?php

/* ApplicationSonataUserBundle:Profile:edit_authentication.html.twig */
class __TwigTemplate_13235f3133af330014d6c5428b9f93f8025f369048fe8582d76d4cb298306c8c extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container containt-dash\">
           <!--Debut left-block-->
           <div class=\"col-md-2 no-padding-right\"> 
               <div class=\"block profil-block\">
                   <div class=\"profil-dash\"> 
                         <div class=\"col-md-12 no-padding\">
\t\t\t\t\t\t    <div class=\"grid\">
\t\t\t\t\t         <figure class=\"effect-winston\">
\t\t\t\t\t\t      ";
        // line 9
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
\t\t\t\t\t\t      <figcaption>  
\t\t\t\t\t\t\t   <p> 
\t\t\t\t\t\t\t\t<a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\"><i class=\"fa fa-fw\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
\t\t\t\t\t\t\t   </p>
\t\t\t\t\t\t      </figcaption>\t\t\t
\t\t\t\t\t         </figure>
\t\t\t\t            </div>       
                         </div>
                         <div class=\"col-md-12 no-padding\">  
                             <p class=\"titre\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>

                    <p class=\"titre-user\">      
\t\t\t\t\t\t<a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t<img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t</a>
\t\t\t\t\t</p>    
                             <!--p class=\"pro\">Profil</p-->
                         </div>
                      
                   </div>
                   <ul class=\"list-unstyled\">
                       <li>
                           <a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"active\">
                              <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\">
                            ";
        // line 38
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                           </a>
                       </li>
                       
                       <li>
                           <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\">
                             ";
        // line 44
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                           </a>
                       </li>
                       <li>
                            ";
        // line 49
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                       </li>
                       <li>
                           <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\">
                              ";
        // line 53
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\">
                               ";
        // line 58
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                           </a>
                       </li>
                   </ul>
               </div>
 
               ";
        // line 64
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
               ";
        // line 65
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

            <br><br>  
           </div>
           <!--Fin left-block-->
           <!--Debut center-block-->
           <div class=\"col-md-10\"> 
    <div class=\"head-compte\">
        <div class=\"\">
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
        // line 75
        echo $this->env->getExtension('routing')->getPath("sonata_user_profile_edit_authentication");
        echo "\" class=\"btn btn-compte active\">
\t\t\t \t <i class=\"fa fa-user\"></i>  <br>
                    ";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.compte"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>
            <div class=\"col-md-2 col-sm-3 no-padding\">
                <a href=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-pencil-square-o\"></i>  <br>
                    ";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change.password"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>   
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
        // line 87
        echo $this->env->getExtension('routing')->getPath("grintaaa_interest");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-star\"></i>  
\t\t\t\t <br>";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>

            ";
        // line 93
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "coach", array()) == 1)) {
            // line 94
            echo "                <div class=\"col-md-1 col-sm-3 no-padding\">
                    <a href=\"";
            // line 95
            echo $this->env->getExtension('routing')->getPath("grintaaa_coaching");
            echo "\" class=\"btn btn-compte\">
                        <i class=\"fa fa-futbol-o\"></i>
                        <br>";
            // line 97
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.coaching"), "html", null, true);
            echo "
                    </a>
                </div>
            ";
        }
        // line 101
        echo "
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
        // line 103
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-picture-o\"></i>  
\t\t\t\t <br>";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.avatar"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>

            <div class=\"col-md-3 col-sm-3 no-padding\">
                <a href=\"";
        // line 110
        echo $this->env->getExtension('routing')->getPath("grintaaa_configuration_email");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-envelope-o\"></i>  
\t\t\t\t <br>";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.email"), "html", null, true);
        echo "
\t\t        </a>
            </div>


        </div>  
    </div> 
    <div class=\"\"> 
\t<br><br>
\t\t    <div class=\"col-md-12 titre-gris\" align=\"center\">
                ";
        // line 122
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.your.email"), "html", null, true);
        echo "
\t\t\t</div>    
\t\t\t
                        <div class=\"col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 inscrit\">
\t\t\t\t\t\t  <div class=\"col-md-10 col-md-offset-1 cadre-blanc\">
        <form  action=\"";
        // line 127
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit_authentication");
        echo "\" class=\"form-inscription\" method=\"post\">
            <!--div class=\"row\">
                <div class=\"col-md-11 col-md-offset-1\">
                    <h4>Votre email : ";
        // line 130
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "email", array()), "html", null, true);
        echo "</h4>
                </div> 
            </div-->
            <div class=\"row\" ";
        // line 133
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "civility", array())) {
            echo " style=\"display: none\"";
        }
        echo ">
                <div class=\"col-md-6\">    
                    ";
        // line 135
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "civility", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                </div>
                <div class=\"col-md-6\">
                    ";
        // line 138
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "username", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.username"))));
        echo "
                </div>
            </div>    
       
            <div class=\"row\">
                <div class=\"col-md-6\">
\t\t\t\t\t<h4 class=\"titre-inscrit2\"><label class=\"label-edit\">Nom</label></h4>
                    ";
        // line 145
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "firstname", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.name"))));
        echo "
                </div>
                <div class=\"col-md-6\">
\t\t\t\t\t<h4 class=\"titre-inscrit2\"><label class=\"label-edit\">Prénom</label> </h4> 
                    ";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "lastname", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.lastname"))));
        echo "
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-6\">
\t\t\t\t\t<h4 class=\"titre-inscrit2\"><label class=\"label-edit\">Pays</label></h4> 
                    ";
        // line 156
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                </div>
                <div class=\"col-md-6\">
\t\t\t\t\t<h4 class=\"titre-inscrit2\"><label class=\"label-edit\">Ville</label></h4> 
                    ";
        // line 160
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.adress"))));
        echo "
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-12\">
\t\t\t\t\t<h4 class=\"titre-inscrit2\"><label class=\"label-edit\">Téléphone</label> </h4>
\t\t\t\t\t";
        // line 167
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.phone"))));
        echo "
                </div>
                
            </div>
            <div class=\"row\">
                <div class=\"col-md-6\">
\t\t\t\t\t<h4 class=\"titre-inscrit2\"><label class=\"label-edit\">Taille (cm)</label> </h4>
                    ";
        // line 174
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "size", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.size"))));
        echo "
                </div>
                <div class=\"col-md-6\">
\t\t\t\t\t<h4 class=\"titre-inscrit2\"><label class=\"label-edit\">Poids (kg)</label> </h4>
                    ";
        // line 178
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "weight", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.weight"))));
        echo "
                </div>
            </div>
            <div class=\"row\">
\t\t\t    <div class=\"col-md-12\"> 
\t\t\t\t  <h4 class=\"titre-inscrit2\"><label>";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.date.birth"), "html", null, true);
        echo "</label></h4>
\t\t\t\t</div>\t
                <div id=\"date_birth\">
                    ";
        // line 186
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "date_of_birth", array()), 'widget', array("attr" => array("class" => "form-control datepicker", "placeholder" => $this->env->getExtension('translator')->trans("user.date.birth"))));
        echo "
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-6\">
                    <h4 class=\"titre-inscrit\">";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "handi", array()), 'label');
        echo "</h4>
                </div>
                <div class=\"col-md-6\">
                    ";
        // line 194
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "handi", array()), 'widget');
        echo "
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-6\">
\t\t\t\t    <h4 class=\"titre-inscrit\">";
        // line 200
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coach", array()), 'label');
        echo "</h4>
                </div>
                <div class=\"col-md-6\">
                    ";
        // line 203
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coach", array()), 'widget');
        echo "
                </div>
            </div> 

            <div class=\"row\">
                <div class=\"col-md-6\">
                    <h4 class=\"titre-inscrit\">
\t\t\t\t\t\t<label class=\" control-label col-lg-3 control-label required\">
\t\t\t\t\t\t";
        // line 211
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "civility", array()) == "Homme")) {
            echo " 
\t\t\t\t\t\t   ";
            // line 212
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.msg.homme"), "html", null, true);
            echo "  
\t\t\t\t\t\t\t";
        } elseif (($this->getAttribute($this->getAttribute(        // line 213
(isset($context["app"]) ? $context["app"] : null), "user", array()), "civility", array()) == "Femme")) {
            // line 214
            echo "\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.msg.femme"), "html", null, true);
            echo "
\t\t\t\t\t\t\t";
        } else {
            // line 216
            echo "\t\t\t\t\t\t\t";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.msg.mix"), "html", null, true);
            echo "
\t\t\t\t\t\t\t";
        }
        // line 218
        echo "\t\t\t\t\t\t\t <span class=\"asterisk\">*</span>
\t\t\t\t\t\t</label>
\t\t\t\t\t</h4>
                </div>
                <div class=\"col-md-6\">
                    ";
        // line 223
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "param", array()), 'widget');
        echo "
                </div>
            </div>
            <div class=\"row\">
            </div>
                   
            ";
        // line 229
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
        echo "
            <div class=\"row\">
                <div class=\"col-md-6 col-md-offset-3\">
                    <button type=\"submit\" name=\"submit\" class=\"btn btn-grinta\">";
        // line 232
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.submit.jour"), "html", null, true);
        echo "</button>
                </div>
            </div>
        </form>
\t\t</div>
\t\t</div>
    </div>
 </div>
</div>
<script type=\"text/javascript\">

   // \$(\"#fos_user_profile_form_param_1\").attr('checked', 'checked');
   //alert(\$(\"#fos_user_profile_form_param_1\").val());
   if( \$(\"#fos_user_profile_form_param_0\").is(':checked')|| \$(\"#fos_user_profile_form_param_1\").is(':checked')){
       i=0;
   } else {
       \$(\"#fos_user_profile_form_param_1\").attr('checked', 'checked');
   }
    \$(\"#date_birth .col-lg-9\").addClass(\"col-md-4\");
\$(\"#date_birth .col-md-4\").removeClass(\"col-lg-9\");
</script>";
    }

    public function getTemplateName()
    {
        return "ApplicationSonataUserBundle:Profile:edit_authentication.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  437 => 232,  431 => 229,  422 => 223,  415 => 218,  409 => 216,  403 => 214,  401 => 213,  397 => 212,  393 => 211,  382 => 203,  376 => 200,  367 => 194,  361 => 191,  353 => 186,  347 => 183,  339 => 178,  332 => 174,  322 => 167,  312 => 160,  305 => 156,  295 => 149,  288 => 145,  278 => 138,  272 => 135,  265 => 133,  259 => 130,  253 => 127,  245 => 122,  232 => 112,  227 => 110,  219 => 105,  214 => 103,  210 => 101,  203 => 97,  198 => 95,  195 => 94,  193 => 93,  186 => 89,  181 => 87,  174 => 83,  169 => 81,  162 => 77,  157 => 75,  144 => 65,  140 => 64,  131 => 58,  127 => 57,  120 => 53,  116 => 52,  110 => 49,  102 => 44,  98 => 43,  90 => 38,  86 => 37,  77 => 33,  73 => 32,  59 => 23,  55 => 22,  47 => 19,  35 => 12,  29 => 9,  19 => 1,);
    }
}
