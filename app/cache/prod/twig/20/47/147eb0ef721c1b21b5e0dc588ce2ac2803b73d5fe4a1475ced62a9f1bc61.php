<?php

/* ApplicationHWIOAuthBundle:Connect:registrationError.html.twig */
class __TwigTemplate_2047147eb0ef721c1b21b5e0dc588ce2ac2803b73d5fe4a1475ced62a9f1bc61 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'hwi_oauth_content' => array($this, 'block_hwi_oauth_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('hwi_oauth_content', $context, $blocks);
    }

    public function block_hwi_oauth_content($context, array $blocks = array())
    {
        // line 2
        echo "
    <div class=\"containt\">
                  <div class=\"container\">

                     <div class=\"row col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 login-error\">
                         <div class=\"login-error-form row\">
\t\t\t\t\t\t  <div class=\" col-md-offset-1 col-md-10\">
\t\t\t\t\t\t   <h3 class=\"titre_connect\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("erroor.success"), "html", null, true);
        echo "</h3>

                            <p class=\"red\">";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("erroor1.success"), "html", null, true);
        echo "</p>


\t\t\t\t\t\t\t<div class=\"col-md-5\" align=\"center\">
\t\t\t\t\t\t\t\t<a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
        echo "\" class=\"btn btn-facebook btn-nnfaceoui\"> Oui</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-7\" align=\"center\">
\t\t\t\t\t\t\t\t<a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\" class=\"btn btn-facebook btn-nnface\">Non, je veux m'inscrire !</a>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
                         </div>
                     </div>
                  </div>
               </div>
               <!--Fin containt-->
           </div>
        </div>

";
    }

    public function getTemplateName()
    {
        return "ApplicationHWIOAuthBundle:Connect:registrationError.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  53 => 18,  47 => 15,  40 => 11,  35 => 9,  26 => 2,  20 => 1,);
    }
}
