<?php

/* SonataUserBundle:ChangePassword:changePassword_content.html.twig */
class __TwigTemplate_2137945e861e9281fa3c5e7fbd7459ef5114ea6a4e8cf183b39fca4577ec8a5f extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container containt-dash\">
           <!--Debut left-block-->
           <div class=\"col-md-2 no-padding-right\">
               <div class=\"block profil-block\">
                   <div class=\"profil-dash\">
                         <div class=\"col-md-12 no-padding\">
\t\t\t\t\t\t    <div class=\"grid\">
\t\t\t\t\t         <figure class=\"effect-winston\">
\t\t\t\t\t\t      ";
        // line 9
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
\t\t\t\t\t\t      <figcaption>
\t\t\t\t\t\t\t   <p>
\t\t\t\t\t\t\t\t<a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\"><i class=\"fa fa-fw\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
\t\t\t\t\t\t\t   </p>
\t\t\t\t\t\t      </figcaption>
\t\t\t\t\t         </figure>
\t\t\t\t            </div>
                         </div>  
                         <div class=\"col-md-12 no-padding\">
                             <p class=\"titre\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>

\t\t\t\t\t\t\t<p class=\"titre-user\">      
\t\t\t\t\t\t\t\t<a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</p>  
                             <!--p class=\"pro\">Profil</p-->
                         </div>
                              
                   </div>    
                   <ul class=\"list-unstyled\">
                       <li>
                           <a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"active\">
                              <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\">
                            ";
        // line 38
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                           </a>
                       </li>

                       <li>
                           <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\">
                             ";
        // line 44
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                           </a>
                       </li>
                       <li>
                            ";
        // line 49
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                       </li>
                       <li>
                           <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\">
                              ";
        // line 53
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\">
                               ";
        // line 58
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                           </a>
                       </li>
                   </ul>
               </div>

               ";
        // line 64
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
               ";
        // line 65
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

            <br><br>
           </div>
           <!--Fin left-block-->
           <!--Debut center-block-->
           <div class=\"col-md-10\">
    <div class=\"head-compte\">
        <div class=\"\">
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
        // line 75
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit_authentication");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-user\"></i>  <br>
                    ";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.compte"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>
            <div class=\"col-md-2 col-sm-3 no-padding\">
                <a href=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\" class=\"btn btn-compte active\">
\t\t\t \t <i class=\"fa fa-pencil-square-o\"></i>  <br>
                    ";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change.password"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
        // line 87
        echo $this->env->getExtension('routing')->getPath("grintaaa_interest");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-star\"></i>
\t\t\t\t <br>";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>
            ";
        // line 92
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "coach", array()) == 1)) {
            // line 93
            echo "            <div class=\"col-md-1 col-sm-3 no-padding\">
                <a href=\"";
            // line 94
            echo $this->env->getExtension('routing')->getPath("grintaaa_coaching");
            echo "\" class=\"btn btn-compte\">
                    <i class=\"fa fa-futbol-o\"></i>
                    <br>";
            // line 96
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.coaching"), "html", null, true);
            echo "
                </a>
            </div>
           ";
        }
        // line 100
        echo "            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
        // line 101
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-picture-o\"></i>
\t\t\t\t <br>";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.avatar"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>       

            <div class=\"col-md-3 col-sm-3 no-padding\">
                <a href=\"";
        // line 108
        echo $this->env->getExtension('routing')->getPath("grintaaa_configuration_email");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-envelope-o\"></i>
\t\t\t\t <br>";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.email"), "html", null, true);
        echo "
\t\t        </a>
            </div>

        </div>
    </div>
\t<br><br>
\t\t    <div class=\"col-md-12 titre-gris\" align=\"center\">
                ";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.pass"), "html", null, true);
        echo "
\t\t\t</div>
\t<div class=\"col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 inscrit\">
  \t  <div class=\"col-md-10 col-md-offset-1 cadre-blanc\">
        <form  class=\"form-inscription\"  action=\"";
        // line 122
        echo $this->env->getExtension('routing')->getPath("sonata_user_change_password");
        echo "\"";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo " method=\"POST\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    ";
        // line 125
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "current_password", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("old.pass"))));
        echo "
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    ";
        // line 130
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "new", array()), "first", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("new.pass"))));
        echo "
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-12\">
                    ";
        // line 136
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "new", array()), "second", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("confirm.pass"))));
        echo "
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-md-12\">
                    <button type=\"submit\" class=\"btn btn-grinta\"><i
                                class=\"icon-lock icon-white glyphicon glyphicon-lock\"></i>&nbsp;";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change_password.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "
                    </button>
                </div>
            </div>
            ";
        // line 147
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
        </form>
\t  </div>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "SonataUserBundle:ChangePassword:changePassword_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  290 => 147,  283 => 143,  273 => 136,  264 => 130,  256 => 125,  248 => 122,  241 => 118,  230 => 110,  225 => 108,  217 => 103,  212 => 101,  209 => 100,  202 => 96,  197 => 94,  194 => 93,  192 => 92,  186 => 89,  181 => 87,  174 => 83,  169 => 81,  162 => 77,  157 => 75,  144 => 65,  140 => 64,  131 => 58,  127 => 57,  120 => 53,  116 => 52,  110 => 49,  102 => 44,  98 => 43,  90 => 38,  86 => 37,  77 => 33,  73 => 32,  59 => 23,  55 => 22,  47 => 19,  35 => 12,  29 => 9,  19 => 1,);
    }
}
