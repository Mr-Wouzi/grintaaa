<?php

/* ApplicationHWIOAuthBundle:Connect:login.html.twig */
class __TwigTemplate_2e12b97f494b2792532d62a85e1e8860dc483a3f573673b50a731d80ca9f1c82 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'hwi_oauth_content' => array($this, 'block_hwi_oauth_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=\"containt\">
    <div class=\"container\">
        <div class=\"row big-title\">
            <div class=\"col-sm-12 text-center\">
                <h1>Organiser vos sports entre amis</h1>
            </div>
        </div>
        <div class=\"row second-big-title text-center\">
            <div class=\"col-sm-11 col-sm-offset-1\">
                <h2>
                    Grintaaa est un outil de société d'un nouveau genre.<br>
                    C'est un reseau social dedier à tous les sportifs.<br>
                    L'utilisateur pourra à tous moments trouver des partenaires ainsi que des coachs à n'importe quel endroit et pour n'importe quel sport.
                    Il pourra partager ses exploits sportifs et créer sa propre communauté.
                </h2>
            </div>
        </div>
        <div class=\"row col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 sign-in\">
            <div class=\"compte\">
                <a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("personne");
        echo "\" class=\"btn btn-grinta\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/compte.png"), "html", null, true);
        echo "\">Créer votre <span>compte</span></a>
            </div>
            <div class=\"facebook\">

                ";
        // line 25
        $this->displayBlock('hwi_oauth_content', $context, $blocks);
        // line 34
        echo "            </div>
            <div class=\"compte\">
                <a href=\"";
        // line 36
        echo $this->env->getExtension('routing')->getPath("presentation");
        echo "\" target=\"_blank\" class=\"lien-pres\"><span>Grintaaa</span> c'est quoi ?</a>
            </div>
        </div>
    </div>
</div>
<!--Fin containt-->
</div>
</div>




";
    }

    // line 25
    public function block_hwi_oauth_content($context, array $blocks = array())
    {
        // line 26
        echo "                    ";
        if ((array_key_exists("error", $context) && (isset($context["error"]) ? $context["error"] : null))) {
            // line 27
            echo "                        <span>";
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
            echo "</span>
                    ";
        }
        // line 29
        echo "                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('hwi_oauth')->getResourceOwners());
        foreach ($context['_seq'] as $context["_key"] => $context["owner"]) {
            // line 30
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('hwi_oauth')->getLoginUrl($context["owner"]), "html", null, true);
            echo "\" class=\"btn btn-facebook\"><i class=\"fa fa-facebook\"></i> connexion via <span>facebook</span></a>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['owner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "                ";
    }

    public function getTemplateName()
    {
        return "ApplicationHWIOAuthBundle:Connect:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 33,  91 => 30,  86 => 29,  80 => 27,  77 => 26,  74 => 25,  57 => 36,  53 => 34,  51 => 25,  42 => 21,  20 => 1,);
    }
}
