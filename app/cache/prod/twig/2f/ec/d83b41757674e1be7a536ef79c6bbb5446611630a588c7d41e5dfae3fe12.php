<?php

/* ApplicationSonataUserBundle:Registration:register1.html.twig */
class __TwigTemplate_2fecd83b41757674e1be7a536ef79c6bbb5446611630a588c7d41e5dfae3fe12 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('fos_user_content', $context, $blocks);
    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 3
        $this->env->loadTemplate("ApplicationSonataUserBundle:Registration:register_content1.html.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "ApplicationSonataUserBundle:Registration:register1.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  23 => 2,  20 => 1,);
    }
}
