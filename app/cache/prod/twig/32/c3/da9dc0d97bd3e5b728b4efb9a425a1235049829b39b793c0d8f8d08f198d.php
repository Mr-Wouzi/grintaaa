<?php

/* ComparatorEventBundle:Form:fields.html.twig */
class __TwigTemplate_32c3da9dc0d97bd3e5b728b4efb9a425a1235049829b39b793c0d8f8d08f198d extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'gmap_address_widget' => array($this, 'block_gmap_address_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('gmap_address_widget', $context, $blocks);
    }

    public function block_gmap_address_widget($context, array $blocks = array())
    {
        // line 2
        echo "<div id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_gmap_address_widget\"></div>
<div id=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_input\">";
        $this->displayBlock("form_widget", $context, $blocks);
        echo "</div>
<div id=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_map\" class=\"gmap_address_map\"></div>
 
<script type=\"text/javascript\">
\$(function() {
            var addresspickerMap = \$(\"#";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address", array()), "vars", array()), "id", array()), "html", null, true);
        echo "\").addresspicker({
                regionBias: \"fr\",
                elements: {
                    map: \"#";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_map\",
                    locality: '#";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locality", array()), "vars", array()), "id", array()), "html", null, true);
        echo "',
                    country:  '#";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), "vars", array()), "id", array()), "html", null, true);
        echo "',
                    lat:      \"#";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "lat", array()), "vars", array()), "id", array()), "html", null, true);
        echo "\",
                    lng:      \"#";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "lng", array()), "vars", array()), "id", array()), "html", null, true);
        echo "\"
                }
            });
            var gmarker = addresspickerMap.addresspicker(\"marker\");
            gmarker.setVisible(true);
            addresspickerMap.addresspicker(\"updatePosition\");
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Form:fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 15,  62 => 14,  58 => 13,  54 => 12,  50 => 11,  44 => 8,  37 => 4,  31 => 3,  26 => 2,  20 => 1,);
    }
}
