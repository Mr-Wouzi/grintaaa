<?php

/* ComparatorMultimediaBundle:File:logo.html.twig */
class __TwigTemplate_373859d675832d7d69138673400996f023af185b18bdae7cf0e161ace9c20fc9 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, (isset($context["logo"]) ? $context["logo"] : null)) > 0)) {
            // line 2
            echo "<img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["logo"]) ? $context["logo"] : null), 0, array(), "array"), "logo", array()), "html", null, true);
            echo "\" alt=\"avatar\">

";
        } else {
            // line 5
            echo "    <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
            echo "\" alt=\"avatar\">
";
        }
    }

    public function getTemplateName()
    {
        return "ComparatorMultimediaBundle:File:logo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 5,  21 => 2,  19 => 1,);
    }
}
