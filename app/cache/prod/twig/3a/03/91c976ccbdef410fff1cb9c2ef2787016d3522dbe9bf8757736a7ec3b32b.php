<?php

/* ApplicationHWIOAuthBundle:Connect:registration_success.html.twig */
class __TwigTemplate_3a0391c976ccbdef410fff1cb9c2ef2787016d3522dbe9bf8757736a7ec3b32b extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'hwi_oauth_content' => array($this, 'block_hwi_oauth_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('hwi_oauth_content', $context, $blocks);
        // line 23
        echo "
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = \"//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.6\";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>";
    }

    // line 1
    public function block_hwi_oauth_content($context, array $blocks = array())
    {
        // line 2
        echo "    <div class=\"containt\">
                  <div class=\"container\">

                     <div class=\"row col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 login-error\">
                         <div class=\"login-error-form\">

                    <p class=\"text-center\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["userInformation"]) ? $context["userInformation"] : null), "realName", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>

                      <a  class=\"btn btn-grinta btn-welcome\" href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("user_file");
        echo "\">Bienvenue</a><br>
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div align=\"center\" class=\"fb-share-button\" data-href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "schemeAndHttpHost", array()), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "getBaseURL", array(), "method"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["currentPath"]) ? $context["currentPath"] : null), "html", null, true);
        echo "\" data-layout=\"button_count\" data-size=\"small\" data-mobile-iframe=\"false\">
\t\t\t\t\t\t\t\t<a class=\"fb-xfbml-parse-ignore\" target=\"_blank\" href=\"https://www.facebook.com/sharer/sharer.php?u='";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "schemeAndHttpHost", array()), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "getBaseURL", array(), "method"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["currentPath"]) ? $context["currentPath"] : null), "html", null, true);
        echo "';src=sdkpreparse\">Partager</a>
\t\t\t\t\t\t\t</div>
                         </div>
                     </div>
                  </div>
               </div>
               <!--Fin containt-->
           </div>
        </div>
";
    }

    public function getTemplateName()
    {
        return "ApplicationHWIOAuthBundle:Connect:registration_success.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  60 => 13,  54 => 12,  49 => 10,  44 => 8,  36 => 2,  33 => 1,  22 => 23,  20 => 1,);
    }
}
