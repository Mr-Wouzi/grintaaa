<?php

/* ComparatorEventBundle:Configuration:index.html.twig */
class __TwigTemplate_3a7ef72346c424ed57d470f1719fb14b1e2badd775bf54a64df2e7fcb835490c extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array())) {
            // line 2
            echo " <!--Debut containt-->
<div class=\"container containt-dash\">
           <!--Debut left-block-->
           <div class=\"col-md-2 no-padding-right\"> 
               <div class=\"block profil-block\">
                   <div class=\"profil-dash\"> 
                         <div class=\"col-md-12 no-padding\">
\t\t\t\t\t\t    <div class=\"grid\">
\t\t\t\t\t         <figure class=\"effect-winston\">
\t\t\t\t\t\t      ";
            // line 11
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
            echo "
\t\t\t\t\t\t      <figcaption>  
\t\t\t\t\t\t\t   <p> 
\t\t\t\t\t\t\t\t<a href=\"";
            // line 14
            echo $this->env->getExtension('routing')->getPath("user_file_new");
            echo "\" title=\"demande partenariat sportif\"><i class=\"fa fa-fw\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
            echo "</i></a>
\t\t\t\t\t\t\t   </p>
\t\t\t\t\t\t      </figcaption>\t\t\t
\t\t\t\t\t         </figure>
\t\t\t\t            </div>       
                         </div>
                         <div class=\"col-md-12 no-padding\">  
                             <p class=\"titre\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
            echo "</p>
                             <p class=\"titre-user\">      
\t\t\t\t\t\t\t\t<a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
            echo "\"> 
\t\t\t\t\t\t\t\t\t<img src=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
            echo "\" class=\"img-loginnn\" />";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
            echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</p> 

                             <!--p class=\"pro\">Profil</p-->
                         </div>
                      
                   </div>
                   <ul class=\"list-unstyled\">
                       <li>
                           <a href=\"";
            // line 34
            echo $this->env->getExtension('routing')->getPath("home");
            echo "\" title=\"demande partenariat sportif\" class=\"active\">
                              <img src=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
            echo "\" alt=\"demande partenariat sportif\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
            echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
            // line 39
            echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
            echo "\" title=\"demande partenariat sportif\">
                            ";
            // line 40
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
            echo "
                           </a>
                       </li>
                       
                       <li>
                           <a href=\"";
            // line 45
            echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
            echo "\"title=\"demande partenariat sportif\">
                             ";
            // line 46
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
            echo "

                           </a>
                       </li>
                       <li>
                            ";
            // line 51
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
            echo "
                       </li>
                       <li>
                           <a href=\"";
            // line 54
            echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
            echo "\" title=\"demande partenariat sportif\">
                              ";
            // line 55
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
            echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
            // line 59
            echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
            echo "\" title=\"demande partenariat sportif\">
                               ";
            // line 60
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
            echo "
                           </a>
                       </li>
                   </ul>
               </div>
 
               ";
            // line 66
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
            echo "
               ";
            // line 67
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
            echo "

            <br><br>  
           </div>
           <!--Fin left-block-->
           <!--Debut center-block-->
           <div class=\"col-md-10\"> 
    <div class=\"head-compte\">
        <div class=\"\">
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
            // line 77
            echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit_authentication");
            echo "\" title=\"demande partenariat sportif\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-user\"></i>  <br>
                    ";
            // line 79
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.compte"), "html", null, true);
            echo "
\t\t\t\t</a>
            </div>
            <div class=\"col-md-2 col-sm-3 no-padding\">
                <a href=\"";
            // line 83
            echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
            echo "\" title=\"demande partenariat sportif\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-pencil-square-o\"></i>  <br>
                    ";
            // line 85
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change.password"), "html", null, true);
            echo "
\t\t\t\t</a>
            </div>
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
            // line 89
            echo $this->env->getExtension('routing')->getPath("grintaaa_interest");
            echo "\" title=\"demande partenariat sportif\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-star\"></i>  
\t\t\t\t <br>";
            // line 91
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
            echo "
\t\t\t\t</a>       
            </div>
            ";
            // line 94
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "coach", array()) == 1)) {
                echo " 
            <div class=\"col-md-1 col-sm-3 no-padding\">
                <a href=\"";
                // line 96
                echo $this->env->getExtension('routing')->getPath("grintaaa_coaching");
                echo "\" title=\"demande partenariat sportif\" class=\"btn btn-compte\">
                    <i class=\"fa fa-futbol-o\"></i>
                    <br>";
                // line 98
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.coaching"), "html", null, true);
                echo "
                </a>
            </div>
            ";
            }
            // line 102
            echo "            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
            // line 103
            echo $this->env->getExtension('routing')->getPath("user_file_new");
            echo "\" title=\"demande partenariat sportif\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-picture-o\"></i>  
\t\t\t\t <br>";
            // line 105
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.avatar"), "html", null, true);
            echo "
\t\t\t\t</a>
            </div>


            <div class=\"col-md-2 col-sm-3 no-padding\">   
                <a href=\"";
            // line 111
            echo $this->env->getExtension('routing')->getPath("grintaaa_configuration_email");
            echo "\" title=\"demande partenariat sportif\" class=\"btn btn-compte active\">
\t\t\t \t <i class=\"fa fa-envelope-o\"></i>
\t\t\t\t <br>";
            // line 113
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.email"), "html", null, true);
            echo "
\t\t        </a>
            </div>

        </div> 
    </div>
 
\t<div class=\" inscrit\">
  \t  <div class=\"cadre-blanc\">   
        <form class=\"form-inscription\" action=\"";
            // line 122
            echo $this->env->getExtension('routing')->getPath("grintaaa_configuration_email_show");
            echo "\" method=\"post\">
    
                <div class=\"row\">
                    <div class=\"col-md-12 interet222\"><br>
                     <div class=\"col-md-12 titre-gris\" align=\"center\">
                         ";
            // line 127
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.precise.email"), "html", null, true);
            echo "
\t\t\t\t     </div>
\t\t\t\t\t <br><br>

                        ";
            // line 131
            if ((isset($context["entities"]) ? $context["entities"] : null)) {
                // line 132
                echo "
\t\t\t\t\t\t";
                // line 133
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                    // line 134
                    echo "
\t\t\t\t\t\t 

\t\t\t\t\t\t  <div class=\"col-md-offset-2 col-md-8 block\">
\t\t\t\t\t\t\t<br>
                            <ul>
                            <li class=\"col-md-12\"><input type=\"checkbox\" name=\"comment\"  ";
                    // line 140
                    if ($this->getAttribute($context["entity"], "comment", array())) {
                        echo "checked ";
                    }
                    echo "> ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.pub.com"), "html", null, true);
                    echo "</li>
                            <li class=\"col-md-12\"><input type=\"checkbox\" name=\"profile\"  ";
                    // line 141
                    if ($this->getAttribute($context["entity"], "profile", array())) {
                        echo "checked ";
                    }
                    echo "> ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.pub.com.profil"), "html", null, true);
                    echo " </li>
                            <li class=\"col-md-12\"><input type=\"checkbox\" name=\"participe\" ";
                    // line 142
                    if ($this->getAttribute($context["entity"], "participe", array())) {
                        echo "checked ";
                    }
                    echo "> ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.partcipe.eve"), "html", null, true);
                    echo "  </li>
                            <li class=\"col-md-12\"><input type=\"checkbox\" name=\"confirmer\"  ";
                    // line 143
                    if ($this->getAttribute($context["entity"], "confirmer", array())) {
                        echo "checked ";
                    }
                    echo "> ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.conf.inv"), "html", null, true);
                    echo " </li>
                            <li class=\"col-md-12\"><input type=\"checkbox\" name=\"supprimer\"  ";
                    // line 144
                    if ($this->getAttribute($context["entity"], "supprimer", array())) {
                        echo "checked ";
                    }
                    echo ">  ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.supp.inv"), "html", null, true);
                    echo " </li>
                            </ul>  
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t  </div> 
                                                     
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 150
                echo "                                    
             
                        ";
            } else {
                // line 153
                echo "                            <div class=\"col-md-offset-2 col-md-8 block\">
\t\t\t\t\t\t\t\t<br>                               
                                <ul>
                                    <li class=\"col-md-12\"><input type=\"checkbox\" name=\"comment\"> ";
                // line 156
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.pub.com"), "html", null, true);
                echo " </li>
                                    <li class=\"col-md-12\"><input type=\"checkbox\" name=\"profile\"> ";
                // line 157
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.pub.com.profil"), "html", null, true);
                echo " </li>
                                    <li class=\"col-md-12\"><input type=\"checkbox\" name=\"participe\"> ";
                // line 158
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.partcipe.eve"), "html", null, true);
                echo " </li>
                                    <li class=\"col-md-12\"><input type=\"checkbox\" name=\"confirmer\"> ";
                // line 159
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.conf.inv"), "html", null, true);
                echo " </li>
                                    <li class=\"col-md-12\"><input type=\"checkbox\" name=\"supprimer\"> ";
                // line 160
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.supp.inv"), "html", null, true);
                echo " </li>
                                </ul>
\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t\t\t<br>
                            </div>
                        ";
            }
            // line 166
            echo "                    </div> 
                </div>     
            <div class=\"row\" align=\"center\">
\t\t      <div class=\"col-md-12\" align=\"center\">
                <div class=\"col-md-4\"></div>
                <div class=\"col-md-4\">
                    <input type=\"submit\" class=\"btn btn-grinta\" value=\" ";
            // line 172
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("button.enregistrer"), "html", null, true);
            echo "\">
                </div>   
              </div>\t\t\t\t
            </div>


        </form>
      </div>
    </div>
  </div>
</div>
               <!--Fin containt-->
  ";
        }
        // line 185
        echo "\t<div class=\"block-footer\"></div>





";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Configuration:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  385 => 185,  369 => 172,  361 => 166,  352 => 160,  348 => 159,  344 => 158,  340 => 157,  336 => 156,  331 => 153,  326 => 150,  310 => 144,  302 => 143,  294 => 142,  286 => 141,  278 => 140,  270 => 134,  266 => 133,  263 => 132,  261 => 131,  254 => 127,  246 => 122,  234 => 113,  229 => 111,  220 => 105,  215 => 103,  212 => 102,  205 => 98,  200 => 96,  195 => 94,  189 => 91,  184 => 89,  177 => 85,  172 => 83,  165 => 79,  160 => 77,  147 => 67,  143 => 66,  134 => 60,  130 => 59,  123 => 55,  119 => 54,  113 => 51,  105 => 46,  101 => 45,  93 => 40,  89 => 39,  80 => 35,  76 => 34,  61 => 24,  57 => 23,  50 => 21,  38 => 14,  32 => 11,  21 => 2,  19 => 1,);
    }
}
