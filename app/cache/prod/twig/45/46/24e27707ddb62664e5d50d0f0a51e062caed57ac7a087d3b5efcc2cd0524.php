<?php

/* ComparatorBaseBundle:Block:block_header.html.twig */
class __TwigTemplate_454624e27707ddb62664e5d50d0f0a51e062caed57ac7a087d3b5efcc2cd0524 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<!--Header-->
";
        // line 3
        if ( !$this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array())) {
            // line 4
            echo "    ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "home")) {
                // line 5
                echo "
        <body id=\"page\">

        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>

        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 24
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-2 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 36
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 37
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\" alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 47
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>
                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 54
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 55
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 57
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 58
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>  
        <!--Fin Header-->
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 65
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "personne")) {
                // line 66
                echo "
        <body id=\"page\">

        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>

        </ul>
    <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 85
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-2 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 97
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 98
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\" alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 106
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 108
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>
                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 115
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 116
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 118
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 119
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->

    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 127
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "hwi_oauth_connect")) {
                // line 128
                echo "        <body id=\"page\">

        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>

        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 146
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-2 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 158
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 159
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\" alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 167
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 169
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>
                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 176
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 177
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 179
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 180
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 187
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "hwi_oauth_connect_registration")) {
                // line 188
                echo "        <body id=\"page\">

        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>

        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 206
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-2 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 218
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 219
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\" alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 227
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 229
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>
                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 236
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 237
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 239
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 240
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 246
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_registration_register")) {
                // line 247
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>
        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 263
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 275
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 276
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\"
                                 alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 285
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 287
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>

                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 295
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 296
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 298
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 299
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 306
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_registration_register1")) {
                // line 307
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>
        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 323
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 335
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 336
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\"
                                 alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 345
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 347
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>

                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 355
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 356
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 358
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 359
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 366
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_registration_register2")) {
                // line 367
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>
        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 383
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 395
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 396
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\"
                                 alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 405
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 407
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>

                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 415
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 416
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 418
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 419
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 426
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_security_login")) {
                // line 427
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>

        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 444
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <a class=\"logo-login\" href=\"";
                // line 449
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 450
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\"
                                 alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <ul class=\"nav navbar-nav navbar-right navbar-login\">
                        <div class=\"slogoan2 col-md-10\">
                            <img src=\"";
                // line 456
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                        </div>
                        <div class=\"col-md-2 align-right\">
                            <a href=\"";
                // line 459
                echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
                echo "\" class=\"bt-inscrit btn-grinta\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("submit.inscription"), "html", null, true);
                echo "</a>
                        </div>
                    </ul>
                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 466
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 467
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 469
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 470
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->
        <!--Fin Header-->
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 478
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_resetting_request")) {
                // line 479
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>
        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 495
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 507
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 508
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\"
                                 alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 517
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 519
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>

                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 527
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 528
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 530
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 531
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 538
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_resetting_send_email")) {
                // line 539
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>
        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 555
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 567
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 568
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\"
                                 alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 577
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 579
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>

                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 587
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 588
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 590
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 591
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->
    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 598
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_resetting_check_email")) {
                // line 599
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>
        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 615
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 627
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 628
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\"
                                 alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 637
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 639
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>

                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 647
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 648
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 650
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 651
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->

    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 659
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_resetting_reset")) {
                // line 660
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>
        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 676
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 688
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 689
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\"
                                 alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 698
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            ";
                // line 700
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ApplicationSonataUserBundle:Security:login"));
                echo "
                        </ul>
                    </div>

                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 708
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 709
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 711
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 712
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->
        <!--Fin Header-->

    ";
            } elseif (($this->getAttribute($this->getAttribute(            // line 721
(isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_registration_check_email")) {
                // line 722
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>
        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 738
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 750
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 751
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\"
                                 alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                        <ul class=\"nav navbar-nav navbar-right navbar-login\">

                            <div class=\"slogoan col-md-3\">
                                <img src=\"";
                // line 760
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                           
                        </ul>
                    </div>

                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 770
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 771
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 773
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 774
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->
    ";
            }
        } else {
            // line 783
            echo "
    ";
            // line 784
            if ((((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "event_new") || ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "hwi_oauth_connect_registration")) || ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_registration_confirmed")) || ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "event_confirmed"))) {
                // line 785
                echo "        <body id=\"page\">
        <ul class=\"cb-slideshow\">
            <li><span>Image 01</li>
            <li><span>Image 02</li>
            <li><span>Image 03</li>
            <li><span>Image 04</li>
            <li><span>Image 05</li>
            <li><span>Image 06</li>

        </ul>
        <div class=\"row\">
        <div class=\"container-fluid\">
        <!--Debut Header-->
        <header class=\"header-login\">
            <nav class=\"navbar navbar-default\">
                <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                    <div class=\"medaille\">
                        <img src=\"";
                // line 802
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                    </div>
                </div>
                <div class=\"col-md-8 block-c\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                data-target=\"#navbar-login\" aria-expanded=\"false\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                        </button>
                        <a class=\"logo-login\" href=\"";
                // line 814
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                            <img src=\"";
                // line 815
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\" alt=\"grintaaa\"/>
                        </a>
                    </div>
                    <div class=\"collapse navbar-collapse\" id=\"navbar-login\">





                        <ul class=\"nav navbar-nav navbar-right navbar-steps\">
                            <div class=\"slogoan3 col-md-5\">
                                <img src=\"";
                // line 826
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                            </div>
                            <div class=\"col-md-7 align-right\">
                                <ul class=\"list-inline\">
                                    <li ";
                // line 830
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "event_search")) {
                    echo "class=\"links chercher active\" ";
                } else {
                    echo "class=\"links chercher\"";
                }
                echo " >
                                        <a href=\"";
                // line 831
                echo $this->env->getExtension('routing')->getPath("event");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("trouve.sport"), "html", null, true);
                echo "</a>
                                    </li>
                                    <li class=\"links organiser\" ";
                // line 833
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "event_new")) {
                    echo "class=\"links organiser active\" ";
                } else {
                    echo "class=\"links organiser\"";
                }
                echo ">
                                        <a href=\"";
                // line 834
                echo $this->env->getExtension('routing')->getPath("event_new");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("organise.sport"), "html", null, true);
                echo "</a>
                                    </li>

                                    <li class=\"dropdown notification messages\">
                                        <a href=\"";
                // line 838
                echo $this->env->getExtension('routing')->getPath("grintaaa_friend");
                echo "\" class=\"dropdown-toggle notif\">
                                            <img src=\"";
                // line 839
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/invit.png"), "html", null, true);
                echo "\"
                                                 alt=\"notification\">
                                            ";
                // line 841
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countIndex"));
                echo "
                                        </a>
                                    </li>
                                    <li class=\"dropdown notification\">
                                        <a href=\"";
                // line 845
                echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
                echo "\" class=\"dropdown-toggle notif\">
                                            <img src=\"";
                // line 846
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/notif.png"), "html", null, true);
                echo "\"
                                                 alt=\"notification\">
                                            ";
                // line 848
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotifications"));
                echo "
                                        </a>
                                    </li>
                                    <li class=\"dropdown cpt\">
                                        <a href=\"#\" class=\"dropdown-toggle compte\" data-toggle=\"dropdown\" role=\"button\"
                                           aria-haspopup=\"true\" aria-expanded=\"false\">
                                            ";
                // line 854
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
                echo "
                                            <p><div style=\"display:none;\">";
                // line 855
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
                echo "</div></p>
                                        </a>
                                        <ul class=\"dropdown-menu dropdown-compte\">
                                            <li><a href=\"";
                // line 858
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_user", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.jeux"), "html", null, true);
                echo "</a></li>
                                            <li><a href=\"";
                // line 859
                echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.friends"), "html", null, true);
                echo "</a></li>
                                            <li>";
                // line 860
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotificationss"));
                echo "</li>
                                            <li role=\"separator\" class=\"divider\"></li>
                                            <li><a href=\"";
                // line 862
                echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("param.compte"), "html", null, true);
                echo "</a></li>
                                            <li><a href=\"";
                // line 863
                echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.logout"), "html", null, true);
                echo "</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </ul>
                    </div>
                </div>
                <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                    <ul class=\"list-inline language\">
                        <li class=\"lang\"><a href=\"";
                // line 874
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                        src=\"";
                // line 875
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                        alt=\"français\"></a></li>
                        <li class=\"lang last\"><a href=\"";
                // line 877
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                        src=\"";
                // line 878
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!--Fin Header-->
    ";
            } else {
                // line 886
                echo "        <body>
        <div class=\"row gris header\">
        <!--Debut Header-->
        <div class=\"container-fluid\">
            <div class=\"header-login\">
                <nav class=\"navbar navbar-default\">
                    <div class=\"col-md-2 block-m medaille-tel\" align=\"center\">
                        <div class=\"medaille\">
                            <img src=\"";
                // line 894
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/medaille.png"), "html", null, true);
                echo "\" alt=\"médaille\">
                        </div>
                    </div>
                    <div class=\"col-md-8 block-c\">
                        <div class=\"navbar-header\">
                            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                                    data-target=\"#navbar-login\" aria-expanded=\"false\">
                                <span class=\"sr-only\">Toggle navigation</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                            <a class=\"logo-login\" href=\"";
                // line 906
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\">
                                <img src=\"";
                // line 907
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login.png"), "html", null, true);
                echo "\" alt=\"grintaaa\"/>
                            </a>
                        </div>
                        <div class=\"collapse navbar-collapse\" id=\"navbar-login\">

                            <ul class=\"nav navbar-nav navbar-right navbar-steps\">
                                <div class=\"slogoan3 col-md-5\">
                                    <img src=\"";
                // line 914
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/slogan.png"), "html", null, true);
                echo "\" />
                                </div>
                                <div class=\"col-md-7 align-right\">
                                    <ul class=\"list-inline\">
                                        <li class=\"links chercher\">
                                            <a href=\"";
                // line 919
                echo $this->env->getExtension('routing')->getPath("event");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("trouve.sport"), "html", null, true);
                echo "</a>
                                        </li>
                                        <li class=\"links organiser\">
                                            <a href=\"";
                // line 922
                echo $this->env->getExtension('routing')->getPath("event_new");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("organise.sport"), "html", null, true);
                echo "</a>
                                        </li>

                                        <li class=\"dropdown notification messages\">
                                            <a href=\"";
                // line 926
                echo $this->env->getExtension('routing')->getPath("grintaaa_friend");
                echo "\" class=\"dropdown-toggle notif\">
                                                <img src=\"";
                // line 927
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/invit.png"), "html", null, true);
                echo "\"
                                                     alt=\"notification\">
                                                ";
                // line 929
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countIndex"));
                echo "
                                            </a>
                                        </li>
                                        <li class=\"dropdown notification\">
                                            <a href=\"";
                // line 933
                echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
                echo "\" class=\"dropdown-toggle notif\">
                                                <img src=\"";
                // line 934
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/notif.png"), "html", null, true);
                echo "\"
                                                     alt=\"notification\">
                                                ";
                // line 936
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotifications"));
                echo "
                                            </a>
                                        </li>
                                        <li class=\"dropdown cpt\">
                                            <a href=\"#\" class=\"dropdown-toggle compte\" data-toggle=\"dropdown\"
                                               role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                ";
                // line 942
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
                echo "
                                                <p><div style=\"display:none;\">";
                // line 943
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
                echo "</div></p>
                                            </a>
                                            <ul class=\"dropdown-menu dropdown-compte\">
                                                <li><a href=\"";
                // line 946
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_user", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.jeux"), "html", null, true);
                echo "</a></li>
                                                <li><a href=\"";
                // line 947
                echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.friends"), "html", null, true);
                echo "</a></li>
                                                <li>";
                // line 948
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotificationss"));
                echo "</li>
                                                <li role=\"separator\" class=\"divider\"></li>
                                                <li><a href=\"";
                // line 950
                echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("param.compte"), "html", null, true);
                echo "</a></li>
                                                <li><a href=\"";
                // line 951
                echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.logout"), "html", null, true);
                echo "</a>
                                                </li>
                                            </ul>
                                        </li>   
                                    </ul>
                                </div>
                            </ul>
                        </div>
                    </div>
                    <div class=\"col-md-2 block-m langue-tel\" align=\"center\">

                        <ul class=\"list-inline language\">
                            <li class=\"lang\"><a href=\"";
                // line 963
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "fr"));
                echo "\"><img
                                            src=\"";
                // line 964
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fr.png"), "html", null, true);
                echo "\"
                                            alt=\"français\"></a></li>
                            <li class=\"lang last\"><a href=\"";
                // line 966
                echo $this->env->getExtension('routing')->getPath("home", array("_locale" => "en"));
                echo "\"><img
                                            src=\"";
                // line 967
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/en.png"), "html", null, true);
                echo "\" alt=\"anglais\"></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--Fin Header-->
    ";
            }
        }
    }

    public function getTemplateName()
    {
        return "ComparatorBaseBundle:Block:block_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1558 => 967,  1554 => 966,  1549 => 964,  1545 => 963,  1528 => 951,  1522 => 950,  1517 => 948,  1511 => 947,  1505 => 946,  1499 => 943,  1495 => 942,  1486 => 936,  1481 => 934,  1477 => 933,  1470 => 929,  1465 => 927,  1461 => 926,  1452 => 922,  1444 => 919,  1436 => 914,  1426 => 907,  1422 => 906,  1407 => 894,  1397 => 886,  1386 => 878,  1382 => 877,  1377 => 875,  1373 => 874,  1357 => 863,  1351 => 862,  1346 => 860,  1340 => 859,  1334 => 858,  1328 => 855,  1324 => 854,  1315 => 848,  1310 => 846,  1306 => 845,  1299 => 841,  1294 => 839,  1290 => 838,  1281 => 834,  1273 => 833,  1266 => 831,  1258 => 830,  1251 => 826,  1237 => 815,  1233 => 814,  1218 => 802,  1199 => 785,  1197 => 784,  1194 => 783,  1182 => 774,  1178 => 773,  1173 => 771,  1169 => 770,  1156 => 760,  1144 => 751,  1140 => 750,  1125 => 738,  1107 => 722,  1105 => 721,  1093 => 712,  1089 => 711,  1084 => 709,  1080 => 708,  1069 => 700,  1064 => 698,  1052 => 689,  1048 => 688,  1033 => 676,  1015 => 660,  1013 => 659,  1002 => 651,  998 => 650,  993 => 648,  989 => 647,  978 => 639,  973 => 637,  961 => 628,  957 => 627,  942 => 615,  924 => 599,  922 => 598,  912 => 591,  908 => 590,  903 => 588,  899 => 587,  888 => 579,  883 => 577,  871 => 568,  867 => 567,  852 => 555,  834 => 539,  832 => 538,  822 => 531,  818 => 530,  813 => 528,  809 => 527,  798 => 519,  793 => 517,  781 => 508,  777 => 507,  762 => 495,  744 => 479,  742 => 478,  731 => 470,  727 => 469,  722 => 467,  718 => 466,  706 => 459,  700 => 456,  691 => 450,  687 => 449,  679 => 444,  660 => 427,  658 => 426,  648 => 419,  644 => 418,  639 => 416,  635 => 415,  624 => 407,  619 => 405,  607 => 396,  603 => 395,  588 => 383,  570 => 367,  568 => 366,  558 => 359,  554 => 358,  549 => 356,  545 => 355,  534 => 347,  529 => 345,  517 => 336,  513 => 335,  498 => 323,  480 => 307,  478 => 306,  468 => 299,  464 => 298,  459 => 296,  455 => 295,  444 => 287,  439 => 285,  427 => 276,  423 => 275,  408 => 263,  390 => 247,  388 => 246,  379 => 240,  375 => 239,  370 => 237,  366 => 236,  356 => 229,  351 => 227,  340 => 219,  336 => 218,  321 => 206,  301 => 188,  299 => 187,  289 => 180,  285 => 179,  280 => 177,  276 => 176,  266 => 169,  261 => 167,  250 => 159,  246 => 158,  231 => 146,  211 => 128,  209 => 127,  198 => 119,  194 => 118,  189 => 116,  185 => 115,  175 => 108,  170 => 106,  159 => 98,  155 => 97,  140 => 85,  119 => 66,  117 => 65,  107 => 58,  103 => 57,  98 => 55,  94 => 54,  84 => 47,  79 => 45,  68 => 37,  64 => 36,  49 => 24,  28 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }
}
