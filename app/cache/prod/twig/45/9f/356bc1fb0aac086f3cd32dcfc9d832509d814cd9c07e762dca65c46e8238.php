<?php

/* ComparatorEventBundle:Event:new.html.twig */
class __TwigTemplate_459f356bc1fb0aac086f3cd32dcfc9d832509d814cd9c07e762dca65c46e8238 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('javascripts', $context, $blocks);
        // line 28
        echo "
    
 ";
        // line 30
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array())) {
            // line 31
            echo "
      \t  \t   <!--containt-->
                    <!--Debut containt-->
               <div class=\"containt-steps\">
                  <div class=\"container\">
                      <div class=\"col-md-10 col-md-offset-1 text-center\">
                          <h2>";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("organize.new.sport"), "html", null, true);
            echo "</h2>
                      </div>
                      <div class=\"clearfix\"></div>
                      <div class=\"wizard\" id=\"wizard\">
                        <div class=\"wizard-inner\">
                            <ul class=\"nav nav-tabs nav-inscri\" role=\"tablist\">

                                <li role=\"presentation\" class=\"col-sm-3 col-md-2 active\">
                                    <p>";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.lieu"), "html", null, true);
            echo "</p>
                                    <hr class=\"left-line\">
                                    <a href=\"#step1\" data-toggle=\"tab\" aria-controls=\"step1\" role=\"tab\"></a>
                                    <hr class=\"right-line\">
                                </li>
                                <li role=\"presentation\" class=\"col-sm-3 col-md-2 arrow disabled\">
                                    <p>";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.type"), "html", null, true);
            echo "</p>
                                    <hr class=\"left-line\">
                                    <a href=\"#step2\" data-toggle=\"tab\" aria-controls=\"step2\" role=\"tab\"></a>
                                    <hr class=\"right-line\">
                                </li>
                                <li role=\"presentation\" class=\"col-sm-3 col-md-2 arrow disabled\">
                                    <p>";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.date"), "html", null, true);
            echo "</p>
                                    <hr class=\"left-line\">
                                    <a href=\"#step3\" data-toggle=\"tab\" aria-controls=\"step3\" role=\"tab\"></a>
                                    <hr class=\"right-line\">
                                </li>
                                <li role=\"presentation\" class=\"col-sm-3 col-md-2 arrow disabled\">
                                    <p>";
            // line 63
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.details"), "html", null, true);
            echo "</p>
                                    <hr class=\"left-line\">
                                    <a href=\"#step4\" data-toggle=\"tab\" aria-controls=\"step4\" role=\"tab\"></a>
                                    <hr class=\"right-line\">
                                </li>
                                <li role=\"presentation\" class=\"col-sm-3 col-md-2 arrow disabled\">
                                    <p>";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.validation"), "html", null, true);
            echo "</p>
                                    <hr class=\"left-line\">
                                    <a href=\"#step5\" data-toggle=\"tab\" aria-controls=\"step5\" role=\"tab\"></a>
                                    <hr class=\"right-line\">
                                </li>
                            </ul>
                        </div>
                        <form role=\"form\" id=\"commentForm\" action=\"";
            // line 76
            echo $this->env->getExtension('routing')->getPath("event_create");
            echo "\" ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
            echo " method=\"post\">
                            <div class=\"tab-content content-steps\">
                                <!--step 1-->
                                <div class=\"tab-pane active\" role=\"tabpanel\" id=\"step1\">
                                    <div class=\"title-step\">
                                      <h4>";
            // line 81
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.choice.lieu"), "html", null, true);
            echo "</h4>
                                    </div>
                                    <div class=\"container-step\">
                                        <div class=\"singl-step col-md-4 col-md-offset-4\">
\t\t\t\t\t\t\t\t\t\t  <div class=\"input_div\"> 
                                             ";
            // line 86
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address", array()), 'widget', array("attr" => array("id" => "adresse", "placeholder" => "Mentionner le lieu du sport")));
            echo "
\t\t\t\t\t\t\t\t\t\t  </div>
                                                         ";
            // line 88
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "locality", array()), 'widget');
            echo "
                                                           ";
            // line 89
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget');
            echo "
                                                           ";
            // line 90
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "lat", array()), 'widget');
            echo "
                                                           ";
            // line 91
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "lng", array()), 'widget');
            echo "
                                        </div>

                                    </div>
\t\t\t\t\t\t\t\t\t<div id=\"map\"></div>
                                    <div class=\"footer-content-step\">
                                        <button type=\"button\" class=\"btn prev-step\">";
            // line 97
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.retour"), "html", null, true);
            echo "</button>
                                        <button type=\"button\" class=\"btn next-step btn-grinta\">";
            // line 98
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.suiv"), "html", null, true);
            echo "</button>
                                    </div>
                                </div>
                                <!--step 2-->
                                <div class=\"tab-pane\" role=\"tabpanel\" id=\"step2\">
                                    <div class=\"title-step\">
                                      <h4>";
            // line 104
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.choice.dis"), "html", null, true);
            echo "</h4>
                                    </div>
                                    <div class=\"container-step\">
                                        <div class=\"singl-step col-md-4 col-md-offset-4\">
\t\t\t\t\t\t\t\t\t\t  <div class=\"input_div\"> 


             <select id=\"selectcat\" name=\"categoryselected\">
                  <option value=\"\" class=\"rhth\">";
            // line 112
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.select.sport"), "html", null, true);
            echo "</option>
                    ";
            // line 113
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["disciplinesc"]) ? $context["disciplinesc"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["discipc"]) {
                // line 114
                echo "     <option class=\"des";
                echo twig_escape_filter($this->env, $this->getAttribute($context["discipc"], "id", array()), "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["discipc"], "id", array()), "html", null, true);
                echo "\" id=\"selectionone\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["discipc"], "title", array()), "html", null, true);
                echo "</option>
 ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discipc'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 116
            echo "
            </select>
                                               <br>
                                               <br>
            <select id=\"selectprod\" name=\"articleID\">
                ";
            // line 121
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["disciplinessa"]) ? $context["disciplinessa"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["discips"]) {
                // line 122
                echo "     <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["discips"], "id", array()), "html", null, true);
                echo "\" class=\"selectors des";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["discips"], "parent", array()), "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["discips"], "title", array()), "html", null, true);
                echo "</option>
 ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discips'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 124
            echo "            </select>
\t\t\t\t\t\t\t\t\t\t  </div>

                                        </div>
                                    </div>
                                    <div class=\"footer-content-step\">
                                        <button type=\"button\" class=\"btn prev-step\">";
            // line 130
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.retour"), "html", null, true);
            echo "</button>
                                        <button type=\"button\" class=\"btn next-step btn-grinta\">";
            // line 131
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.suiv"), "html", null, true);
            echo "</button>
                                    </div>
                                </div>
                                <!--step 3-->
                                <div class=\"tab-pane\" role=\"tabpanel\" id=\"step3\">
                                    <div class=\"title-step\">
                                      <h4>";
            // line 137
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.pec.date"), "html", null, true);
            echo " & ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.pec.heure"), "html", null, true);
            echo "</h4>
                                    </div>
                                    <div class=\"container-step\">
                                        <div class=\"singl-step step-date col-sm-6\">
                                            <h3>";
            // line 141
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.date"), "html", null, true);
            echo "</h3>
                                            <div class=\"col-sm-10\">
                                             ";
            // line 143
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "startDate", array()), 'widget', array("attr" => array("class" => "form-control datepicker", "placeholder" => $this->env->getExtension('translator')->trans("click.choice.date"))));
            echo "
\t\t\t\t\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t\t\t\t</div>
                                        <div class=\"singl-step col-sm-5 col-sm-offset-1 time-step\">
                                            <h3>";
            // line 147
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.heure"), "html", null, true);
            echo "</h3>
                                            <div class=\"col-md-5 \">
                                                <label>";
            // line 149
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.heure.dep"), "html", null, true);
            echo "</label>
                                                <div class=\"col-xs-12\">
                                               ";
            // line 151
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "startTime", array()), 'widget');
            echo "
                                                </div>

                                            </div>
                                            <div class=\"col-md-5\">
                                                <label>";
            // line 156
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.heure.fin"), "html", null, true);
            echo "</label>
                                                <div class=\"clearfix\"></div>
                                                <div class=\"col-xs-12\">
                                                ";
            // line 159
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "endTime", array()), 'widget');
            echo "
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                    <div class=\"footer-content-step\">
                                        <button type=\"button\" class=\"btn prev-step\">";
            // line 168
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.retour"), "html", null, true);
            echo "</button>
                                        <button type=\"button\" class=\"btn next-step btn-grinta\">";
            // line 169
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.suiv"), "html", null, true);
            echo "</button>
                                    </div>
                                </div>
                                <!--step 4-->
                                <div class=\"tab-pane\" role=\"tabpanel\" id=\"step4\">
                                    <div class=\"title-step\">
                                      <h4>";
            // line 175
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("desc.det.event.suiv"), "html", null, true);
            echo "</h4>
                                    </div>
                                    <div class=\"container-step\">
                                         <div class=\"singl-step col-md-6 detail\">
\t\t\t\t\t\t\t\t\t\t   <div class=\"col-md-4 no-padding\">
                                             <h3>";
            // line 180
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.title.sport"), "html", null, true);
            echo "</h3>
\t\t\t\t\t\t\t\t\t\t   </div> 
\t\t\t\t\t\t\t\t\t\t   <div class=\"col-md-8 no-padding\">
                                             ";
            // line 183
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "title", array()), 'widget', array("attr" => array("id" => "titre", "placeholder" => $this->env->getExtension('translator')->trans("holder.title.event"))));
            echo "
\t\t\t\t\t\t\t\t\t\t   </div>
\t\t\t\t\t\t\t\t\t\t   <div class=\"col-md-4 no-padding\">
                                             <h3 class=\"nbr-gamers\">";
            // line 186
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.nb.joueur.sport"), "html", null, true);
            echo "</h3>
\t\t\t\t\t\t\t\t\t\t   </div> 
\t\t\t\t\t\t\t\t\t\t   <div class=\"col-md-8 no-padding\">
\t\t\t\t\t\t\t\t\t\t     <div class=\"nbrej activej col-md-9\">
\t\t\t\t\t\t\t\t\t\t      <div class=\"input_div\"> 
                                               ";
            // line 191
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "nbPlayer", array()), 'widget', array("attr" => array("id" => "joueurs", "placeholder" => $this->env->getExtension('translator')->trans("holder.joueur.event"), "min" => "0")));
            echo "
\t\t\t\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t\t\t\t </div>
\t\t\t\t\t\t\t\t\t\t     <div class=\"col-md-3\">
\t\t\t\t\t\t\t\t\t\t      <input type=\"checkbox\" onclick=\"oncheck();\" class=\"illimte\" id=\"nbre_illimite\" name=\"nbre_illimite\" /> ";
            // line 195
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("joueur.illimite"), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t\t\t     </div>
\t\t\t\t\t\t\t\t\t\t   </div>

\t\t\t\t\t\t\t\t\t\t   <div class=\"col-md-8 no-padding handicap-detail\">
                                             <h3 class=\"nbr-gamers\">";
            // line 200
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("handi.sport"), "html", null, true);
            echo "</h3>
                                              ";
            // line 201
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "handi", array()), 'widget');
            echo "
\t\t\t\t\t\t\t\t\t\t   </div>

\t\t\t\t\t\t\t\t\t\t   <div class=\"col-md-12 no-padding clear-tel\">
\t\t\t\t\t\t\t\t\t\t    <a onclick=\"plus();\" class=\"ajout_option\">
\t\t\t\t\t\t\t\t\t\t\t <i class=\"fa fa-plus-circle\"></i> ";
            // line 206
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("add.option"), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t    <div id=\"cadre\"></div>
\t\t\t\t\t\t\t\t\t\t    <div class=\"col-md-12 no-padding\" id=\"sup\">
\t\t\t\t\t\t\t\t\t\t     <a onclick=\"moins();\" class=\"sup_option\">
\t\t\t\t\t\t\t\t\t\t\t  <i class=\"fa fa-minus-circle\"></i> ";
            // line 211
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("delete.option"), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t\t\t\t </a>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t   </div>
                                         </div>
                                         <div class=\"singl-step col-md-5 detail col-md-offset-1\">
                                          <h3 class=\"desc\">";
            // line 217
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("desc.event.sports"), "html", null, true);
            echo "</h3>
\t\t\t\t\t\t\t\t\t\t  <div class=\"input_div\"> 
                                           ";
            // line 219
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget', array("attr" => array("id" => "description", "placeholder" => $this->env->getExtension('translator')->trans("holder.detail.event"))));
            echo "
                                          </div>
\t\t\t\t\t\t\t\t\t\t </div>\t\t\t\t\t\t\t\t\t 
                                    </div>
                                    <div class=\"footer-content-step\">
                                        <button type=\"button\" class=\"btn prev-step\">";
            // line 224
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.retour"), "html", null, true);
            echo "</button>
                                        <input type=\"button\" class=\"btn next-step btn-grinta\" id=\"hit\" value=\"";
            // line 225
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.valide"), "html", null, true);
            echo "\">

                                    </div>
                                </div>
                                <!--step 5-->
                                <div class=\"tab-pane\" role=\"tabpanel\" id=\"step5\">
                                     <div class=\"container-step revue\">
                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 233
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lieu.sport"), "html", null, true);
            echo "</h3>
                                             <p id=\"p1\"></p>
                                        </div>
                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 237
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("type.descipline"), "html", null, true);
            echo "</h3>
                                             <p id=\"p2\"></p>
                                        </div>
                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 241
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.title.sport"), "html", null, true);
            echo "</h3>
                                             <p id=\"p3\"></p>
                                        </div>
                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 245
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.date"), "html", null, true);
            echo "</h3>
                                             <p id=\"p4\"></p>
                                        </div>
                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 249
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.heure.dep"), "html", null, true);
            echo "</h3>
                                             <p id=\"p5\"></p>
                                        </div>
                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 253
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.heure.fin"), "html", null, true);
            echo "</h3>
                                             <p id=\"p6\"></p>
                                        </div>
                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 257
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.nbr.joueur"), "html", null, true);
            echo "</h3>
                                             <p id=\"p7\"></p>
                                        </div>
                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 261
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.desc.sport"), "html", null, true);
            echo "</h3>
                                             <p id=\"p8\"></p>
                                        </div>
                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 265
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("handi.sport"), "html", null, true);
            echo "</h3>
                                             <p id=\"ph\"></p>
                                        </div>

                                        <div class=\"col-sm-6 item-revue\">
                                             <h3 class=\"rev-titre\">";
            // line 270
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("events.option.sport"), "html", null, true);
            echo "</h3>
                                             <ul>

                                                ";
            // line 273
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(0, 100));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 274
                echo "     <li id=\"l";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\"></li>
 ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 276
            echo "                                              </ul>
                                        </div>
                                    </div>
                                    <div class=\"footer-content-step\">
                                        <button type=\"button\" class=\"btn prev-step\">";
            // line 280
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.retour"), "html", null, true);
            echo "</button>
                                        <input type=\"submit\" class=\"btn next-step btn-grinta\" value=\"";
            // line 281
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("button.enregistrer"), "html", null, true);
            echo "\">
                                      ";
            // line 282
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
            echo "
                                    </div>
                                </div>
                            </div>
                          ";
            // line 286
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
            echo "
                        </form>
                      </div>

                  </div>
               </div>
               <!--Fin containt-->
           </div>
        </div>
  ";
        }
        // line 296
        echo "

<script type=\"text/javascript\">
    \$(document).ready(function () {
        var allOptions = \$('#selectprod option')
        \$('#selectcat').change(function () {
            \$('#selectprod option').remove()
            var classN = \$('#selectcat option:selected').prop('class');
            var opts = allOptions.filter('.' + classN);
            \$.each(opts, function (i, j) {
                \$(j).appendTo('#selectprod');
            });
        });
    });


    var ident = '0';
    \$('#comparator_bundle_eventbundle_event_startTime_hour option[value=\"' + ident +'\"]').prop(\"selected\", true);
    \$('#comparator_bundle_eventbundle_event_startTime_minute option[value=\"' + ident +'\"]').prop(\"selected\", true);
    \$('#comparator_bundle_eventbundle_event_endTime_hour option[value=\"' + ident +'\"]').prop(\"selected\", true);
    \$('#comparator_bundle_eventbundle_event_endTime_minute option[value=\"' + ident +'\"]').prop(\"selected\", true);

    \$(\"#comparator_bundle_eventbundle_event_startTime_hour\").find(\"option\").eq(0).remove();
    \$(\"#comparator_bundle_eventbundle_event_startTime_minute\").find(\"option\").eq(0).remove();
    \$(\"#comparator_bundle_eventbundle_event_endTime_hour\").find(\"option\").eq(0).remove();
    \$(\"#comparator_bundle_eventbundle_event_endTime_minute\").find(\"option\").eq(0).remove();

    \$(\"#comparator_bundle_eventbundle_event_startDate\").datepicker({
        dateFormat: 'dd/mm/yy',
        beforeShowDay: NotBeforeToday-1,
        minDate: 0,
        onSelect: function (dateStr) {
            var today = new Date();
            var todays = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!

            var yyyy = today.getFullYear();
            if(dd<10){
                dd='0'+dd
            }
            if(mm<10){
                mm='0'+mm
            }
            var today = dd+'/'+mm+'/'+yyyy;
            if(today==\$(\"#comparator_bundle_eventbundle_event_startDate\").val())
            {
                // \$(\"#comparator_bundle_eventbundle_event_startDate[value=\" + title + \"]\").hide();
                //\$('comparator_bundle_eventbundle_event_startDate option[value=17]').attr('disabled', 'disabled').hide();

                for(m=0;m<todays.getHours()+2;m++)
                {
                    //alert(i);
                    jQuery('#comparator_bundle_eventbundle_event_startTime_hour').children('option[value='+m+']').css('display','none');
                }
                //\$('#comparator_bundle_eventbundle_event_startTime_hour option[value=\"' + todays.getHours()+2 +'\"]').prop(\"selected\", true);


                for(n=0;n<todays.getMinutes()+1;n++)
                {
                    //alert(i);
                    jQuery('#comparator_bundle_eventbundle_event_startTime_minute').children('option[value='+n+']').css('display','none');
                }


                \$('#comparator_bundle_eventbundle_event_startTime_hour option[value=\"' + m +'\"]').prop(\"selected\", true);
                \$('#comparator_bundle_eventbundle_event_startTime_minute option[value=\"' + n +'\"]').prop(\"selected\", true);
                //alert(todays.getHours());
                //alert(todays.getMinutes());

                \$('#comparator_bundle_eventbundle_event_startTime_hour').on('change', function() {
                    if(this.value<=todays.getHours()+2)
                    {
                        for(bb=0;bb<todays.getMinutes()+1;bb++)
                        {
                            //alert(i);
                            jQuery('#comparator_bundle_eventbundle_event_startTime_minute').children('option[value='+bb+']').css('display','none');
                        }
                        \$('#comparator_bundle_eventbundle_event_startTime_minute option[value=\"' + bb +'\"]').prop(\"selected\", true);

                    }else{
                        for(h=0;h<60;h++)
                        {
                            //alert(i);
                            jQuery('#comparator_bundle_eventbundle_event_startTime_minute').children('option[value='+h+']').css('display','block');
                        }
                        \$('#comparator_bundle_eventbundle_event_startTime_minute option[value=\"0\"]').prop(\"selected\", true);
                    }



                });
            }

        }
    });
    function NotBeforeToday(date) {
        var now = new Date();//this gets the current date and time
        if (date.getFullYear() == now.getFullYear() && date.getMonth() == now.getMonth() && date.getDate() > now.getDate())
            return [true];
        if (date.getFullYear() >= now.getFullYear() && date.getMonth() > now.getMonth())
            return [true];
        if (date.getFullYear() > now.getFullYear())
            return [true];
        return [false];
    }
    function oncheck() {
        var x = document.getElementById(\"nbre_illimite\").checked;
        if (x == true) {
            \$(\".nbrej\").addClass(\"desactivej\");
            \$(\".nbrej\").removeClass(\"activej\");
            //alert(\$(\"#comparator_bundle_eventbundle_event_nbPlayer\").val());
            \$(\"#comparator_bundle_eventbundle_event_nbPlayer\").val(\"\");
        } else {
            \$(\".nbrej\").removeClass(\"desactivej\");
            \$(\".nbrej\").addClass(\"activej\");
        }
    }
    function ajout_option() {

    }

    var c, c2, ch1, ch2;

    // ajouter un champ avec son \"name\" propre;
    function plus() {
        c = document.getElementById('cadre');
        c2 = c.getElementsByTagName('input');
        ch2 = document.createElement('input');

        ch2.setAttribute('type', 'text');
        ch2.setAttribute('placeholder', 'Option');
        ch2.setAttribute('class', 'form-control');
        ch2.setAttribute('name', 'option[]');
        ch2.setAttribute('id', 'option' + c2.length);
        c.appendChild(ch2);

        document.getElementById('sup').style.display = 'inline';
    }

    // supprimer le dernier champ;
    function moins() {
        if (c2.length > 0) {
            c.removeChild(c2[c2.length - 1]);
        }
        if (c2.length == 0) {
            document.getElementById('sup').style.display = 'none'
        }
        ;
    }



    \$(document).ready(function () {


        \$('#hit').click(function () {

            var zero ='0';
            var adresse = \$(\"#comparator_bundle_eventbundle_event_address_address\").val();

            var title = \$(\"#comparator_bundle_eventbundle_event_title\").val();
            var date = \$(\"#comparator_bundle_eventbundle_event_startDate\").val();
            if(\$(\"#comparator_bundle_eventbundle_event_startTime_hour\").val().length==1)
            {
                var heured = zero.concat(\$(\"#comparator_bundle_eventbundle_event_startTime_hour\").val());
            }else{
                var heured = \$(\"#comparator_bundle_eventbundle_event_startTime_hour\").val();
            }

            if(\$(\"#comparator_bundle_eventbundle_event_startTime_minute\").val().length==1)
            {
                var mnd = zero.concat(\$(\"#comparator_bundle_eventbundle_event_startTime_minute\").val());
            }else{
                var mnd = \$(\"#comparator_bundle_eventbundle_event_startTime_minute\").val();
            }




            if(\$(\"#comparator_bundle_eventbundle_event_endTime_hour\").val().length==1)
            {
                var heuref = zero.concat(\$(\"#comparator_bundle_eventbundle_event_endTime_hour\").val());
            }else{
                var heuref = \$(\"#comparator_bundle_eventbundle_event_endTime_hour\").val();
            }

            if(\$(\"#comparator_bundle_eventbundle_event_endTime_minute\").val().length==1)
            {
                var mnf = zero.concat(\$(\"#comparator_bundle_eventbundle_event_endTime_minute\").val());
            }else{
                var mnf = \$(\"#comparator_bundle_eventbundle_event_endTime_minute\").val();
            }
            var nbplayer = \$(\"#comparator_bundle_eventbundle_event_nbPlayer\").val();
            var desc = \$(\"#comparator_bundle_eventbundle_event_description\").val();
            var h = \"h\";
            var deb = heured.concat(h);

            var resdeb = deb.concat(mnd);

            var fin = heuref.concat(h);

            var resfin = fin.concat(mnf);

            var handi = \$(\"#comparator_bundle_eventbundle_event_handi\").prop( \"checked\" )

            document.getElementById(\"p1\").innerHTML = adresse;

            document.getElementById(\"p3\").innerHTML = title;
            document.getElementById(\"p4\").innerHTML = date;
            document.getElementById(\"p5\").innerHTML = resdeb;
            document.getElementById(\"p6\").innerHTML = resfin;
            if (nbplayer > 0) {
                document.getElementById(\"p7\").innerHTML = nbplayer;

            } else {
                document.getElementById(\"p7\").innerHTML = \"";
        // line 512
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("joueur.illimite"), "html", null, true);
        echo "\";

            }

            if(handi==false)
            {
                document.getElementById(\"ph\").innerHTML = \"";
        // line 518
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.no"), "html", null, true);
        echo "\";
            }else{
                document.getElementById(\"ph\").innerHTML = \"";
        // line 520
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.yes"), "html", null, true);
        echo "\";
            }

            document.getElementById(\"p8\").innerHTML = desc;
            for (var i = 0; i < c2.length; i++) {
                document.getElementById(\"l\" + i).innerHTML = \$(\"#option\" + i).val();

            }
        });
    });
    \$('#selectprod').change(function (event) {
        var category = \$('#selectprod option:selected').text();
        document.getElementById(\"p2\").innerHTML = category;
    });

</script>

";
    }

    // line 1
    public function block_javascripts($context, array $blocks = array())
    {
        // line 2
        echo "    <script>
        \$(function () {
            var addresspicker = \$(\"#addresspicker\").addresspicker();
            var addresspickerMap = \$(\"#comparator_bundle_eventbundle_event_address_address\").addresspicker({
                regionBias: \"fr\",
                elements: {
                    map: \"#map\",
                    lat: \"#comparator_bundle_eventbundle_event_lat_address\",
                    lng: \"#comparator_bundle_eventbundle_event_lng_address\",
                    locality: '#comparator_bundle_eventbundle_event_locality_address',
                    administrative_area_level_2: '#administrative_area_level_2',
                    administrative_area_level_1: '#administrative_area_level_1',
                    country: '#comparator_bundle_eventbundle_event_country_address'
                }
            });
            var gmarker = addresspickerMap.addresspicker(\"marker\");
            gmarker.setVisible(true);
            addresspickerMap.addresspicker(\"updatePosition\");

            \$('#reverseGeocode').change(function () {
                \$(\"#comparator_bundle_eventbundle_event_address_address\").addresspicker(\"option\", \"reverseGeocode\", (\$(this).val() === 'true'));
            });

        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Event:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  771 => 2,  768 => 1,  746 => 520,  741 => 518,  732 => 512,  514 => 296,  501 => 286,  494 => 282,  490 => 281,  486 => 280,  480 => 276,  471 => 274,  467 => 273,  461 => 270,  453 => 265,  446 => 261,  439 => 257,  432 => 253,  425 => 249,  418 => 245,  411 => 241,  404 => 237,  397 => 233,  386 => 225,  382 => 224,  374 => 219,  369 => 217,  360 => 211,  352 => 206,  344 => 201,  340 => 200,  332 => 195,  325 => 191,  317 => 186,  311 => 183,  305 => 180,  297 => 175,  288 => 169,  284 => 168,  272 => 159,  266 => 156,  258 => 151,  253 => 149,  248 => 147,  241 => 143,  236 => 141,  227 => 137,  218 => 131,  214 => 130,  206 => 124,  193 => 122,  189 => 121,  182 => 116,  169 => 114,  165 => 113,  161 => 112,  150 => 104,  141 => 98,  137 => 97,  128 => 91,  124 => 90,  120 => 89,  116 => 88,  111 => 86,  103 => 81,  93 => 76,  83 => 69,  74 => 63,  65 => 57,  56 => 51,  47 => 45,  36 => 37,  28 => 31,  26 => 30,  22 => 28,  20 => 1,);
    }
}
