<?php

/* SonataUserBundle:Profile:action.html.twig */
class __TwigTemplate_47424f14ee2ae520d5bef98b56cf64cef91fb9a2b96dc93c54942c31fe7ab9d6 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt-->
<div class=\"container containt-dash\">
    <div class=\"formulaire-search col-md-10\">
        <div class=\"img-search\">
            <img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/parametres.png"), "html", null, true);
        echo "\">
        </div>
        <div class=\"title-search\">
            <h4 class=\"green\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("param.compte"), "html", null, true);
        echo "</h4>

        </div>
    </div>
\t<div>
\t
\t ";
        // line 14
        if (((isset($context["percentages"]) ? $context["percentages"] : null) < 100)) {
            // line 15
            echo "      <div class=\"col-md-12 pourcentage\"> 
\t   ";
            // line 16
            echo twig_escape_filter($this->env, (isset($context["percentages"]) ? $context["percentages"] : null), "html", null, true);
            echo " %
          ";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("etat.compte"), "html", null, true);
            echo " <span class=\"green\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.profil"), "html", null, true);
            echo "</span>
\t  </div>

\t ";
        } else {
            // line 21
            echo "      <div class=\"col-md-12 pourcentage\">
          ";
            // line 22
            echo twig_escape_filter($this->env, (isset($context["percentages"]) ? $context["percentages"] : null), "html", null, true);
            echo " %
          ";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("felic.compte"), "html", null, true);
            echo " <span class=\"green\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.profil"), "html", null, true);
            echo "</span> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profil.complet"), "html", null, true);
            echo "
\t  </div>
\t ";
        }
        // line 26
        echo "\t</div>
    <div class=\"list-games list-notif\">
        <div class=\"block-param\">
            <div class=\"col-sm-2 col-sm-offset-1\" align=\"center\">
                <a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("sonata_user_profile_edit_authentication");
        echo "\">
\t\t\t\t  <i class=\"fa fa-user size-ico-compte\"></i>    
\t\t\t\t  <br>
                    ";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.compte"), "html", null, true);
        echo "
\t\t\t\t  <p> ";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.cor"), "html", null, true);
        echo " </p>
                </a>
                ";
        // line 36
        if (((isset($context["compte"]) ? $context["compte"] : null) == false)) {
            // line 37
            echo "\t\t\t\t\t<p><a href=\"#\" class=\"tooltip3\">!<span>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.infobull.compte"), "html", null, true);
            echo "</span></a></p>
                ";
        }
        // line 39
        echo "
            </div>
            <div class=\"col-sm-2\" align=\"center\">
                <a href=\"";
        // line 42
        echo $this->env->getExtension('routing')->getPath("grintaaa_interest");
        echo "\">
\t\t\t\t  <i class=\"fa fa-star size-ico-compte\"></i>    
\t\t\t\t  <br>
                    ";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
        echo "
\t\t\t\t  <p>";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.center.interest"), "html", null, true);
        echo "</p>
                    ";
        // line 47
        if ((twig_length_filter($this->env, (isset($context["logo"]) ? $context["logo"] : null)) == 0)) {
            // line 48
            echo "\t\t\t\t\t\t<p><a href=\"#\" class=\"tooltip3\">!<span>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.infobull.interest"), "html", null, true);
            echo "</span></a></p>
                    ";
        }
        // line 50
        echo "
                </a>
            </div>
            ";
        // line 53
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "coach", array()) == 1)) {
            // line 54
            echo "            <div class=\"col-sm-2\" align=\"center\">
                <a href=\"";
            // line 55
            echo $this->env->getExtension('routing')->getPath("grintaaa_coaching");
            echo "\">
                    <i class=\"fa fa-futbol-o size-ico-compte\"></i>
                    <br>
                    ";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.coaching"), "html", null, true);
            echo "
                    <p>";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.center.coaching"), "html", null, true);
            echo "</p>


                </a>
            </div>
            ";
        }
        // line 65
        echo "            <div class=\"col-sm-2\" align=\"center\">
                <a href=\"";
        // line 66
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\">
\t\t\t\t  <i class=\"fa fa-picture-o size-ico-compte\"></i>    
\t\t\t\t  <br>
                    ";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.avatar"), "html", null, true);
        echo "
\t\t\t\t  <p>";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.my.avatar"), "html", null, true);
        echo "</p>

                    ";
        // line 72
        if ((twig_length_filter($this->env, (isset($context["logo"]) ? $context["logo"] : null)) == 0)) {
            // line 73
            echo "\t\t\t\t\t\t<p><a href=\"#\" class=\"tooltip3\">!<span>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.infobull.avatar"), "html", null, true);
            echo "</span></a></p>
                    ";
        }
        // line 75
        echo "                </a>
            </div>

            <div class=\"col-sm-2\" align=\"center\">
                <a href=\"";
        // line 79
        echo $this->env->getExtension('routing')->getPath("grintaaa_configuration_email");
        echo "\">
\t\t\t\t  <i class=\"fa fa-envelope-o size-ico-compte\"></i>    
\t\t\t\t  <br>
                    ";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.email"), "html", null, true);
        echo "
\t\t\t\t  <p>";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.notif.email"), "html", null, true);
        echo "</p>
                </a>
            </div>\t\t
\t\t\t
        </div>

    </div>
</div>
<!--Debut containt-->";
    }

    public function getTemplateName()
    {
        return "SonataUserBundle:Profile:action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 83,  198 => 82,  192 => 79,  186 => 75,  180 => 73,  178 => 72,  173 => 70,  169 => 69,  163 => 66,  160 => 65,  151 => 59,  147 => 58,  141 => 55,  138 => 54,  136 => 53,  131 => 50,  125 => 48,  123 => 47,  119 => 46,  115 => 45,  109 => 42,  104 => 39,  98 => 37,  96 => 36,  91 => 34,  87 => 33,  81 => 30,  75 => 26,  65 => 23,  61 => 22,  58 => 21,  49 => 17,  45 => 16,  42 => 15,  40 => 14,  31 => 8,  25 => 5,  19 => 1,);
    }
}
