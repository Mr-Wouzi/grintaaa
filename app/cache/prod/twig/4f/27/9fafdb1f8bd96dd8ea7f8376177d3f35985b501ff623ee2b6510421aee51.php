<?php

/* ComparatorEventBundle:Event:listEvent.html.twig */
class __TwigTemplate_4f279fafdb1f8bd96dd8ea7f8376177d3f35985b501ff623ee2b6510421aee51 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt-->
<div class=\"container containt-dash\">
    <!--Debut left-block-->
    <div class=\"col-md-2 no-padding-right\">
        <div class=\"block profil-block\">
            <div class=\"profil-dash\">
                <div class=\"col-md-12 no-padding\">
                    <div class=\"grid\">
                        <figure class=\"effect-winston\">
                            ";
        // line 10
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
                            <figcaption>
                                <p>
                                    <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\"><i class=\"fa fa-fw\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class=\"col-md-12 no-padding\">
                    <p class=\"titre\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>
                    <p class=\"titre-user\">      
\t\t\t\t\t\t\t\t<a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</p> 
                    <!--p class=\"pro\">Profil</p-->
                </div>

            </div>
            <ul class=\"list-unstyled\">
                <li>
                    <a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\">
                        <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\">
                        ";
        // line 38
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                    </a>
                </li>

                <li>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\">
                        ";
        // line 44
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                    </a>
                </li>
                <li>
                    ";
        // line 49
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                </li>
                <li>
                    <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\">
                        ";
        // line 53
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\">
                        ";
        // line 58
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                    </a>
                </li>
            </ul>
        </div>

        ";
        // line 64
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
        ";
        // line 65
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

    <br><br>
    </div>

    <div class=\"col-md-10\">
    <div class=\"formulaire-search col-md-12\">
        <div class=\"img-search\">
            <img src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/recherche.png"), "html", null, true);
        echo "\">
        </div>
        <div class=\"title-search\">
            <h2 class=\"green\">";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("recherche.sport"), "html", null, true);
        echo "</h2>
        </div>
        <div class=\"formulair\">
            <form id=\"form-search\" class=\"form-search\" action=\"";
        // line 79
        echo $this->env->getExtension('routing')->getPath("event_search");
        echo "\" method=\"post\">

                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"\">";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.pays"), "html", null, true);
        echo ":</div>
                        <select class=\"form-control\" name=\"genre\" id=\"pays\">
                            <option value=\"Afghanistan\" ";
        // line 85
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Afghanistan")) {
            echo " selected ";
        }
        echo ">Afghanistan</option>
                            <option value=\"Afrique du Sud\" ";
        // line 86
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Afrique du Sud")) {
            echo " selected ";
        }
        echo ">Afrique du Sud</option>
                            <option value=\"Albanie\" ";
        // line 87
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Albanie")) {
            echo " selected ";
        }
        echo ">Albanie</option>
                            <option value=\"Algérie\" ";
        // line 88
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Algérie")) {
            echo " selected ";
        }
        echo ">Algérie</option>
                            <option value=\"Allemagne\" ";
        // line 89
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Allemagne")) {
            echo " selected ";
        }
        echo ">Allemagne</option>
                            <option value=\"Andorre\" ";
        // line 90
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Andorre")) {
            echo " selected ";
        }
        echo ">Andorre</option>
                            <option value=\"Angola\" ";
        // line 91
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Angola")) {
            echo " selected ";
        }
        echo ">Angola</option>
                            <option value=\"Anguilla\" ";
        // line 92
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Anguilla")) {
            echo " selected ";
        }
        echo ">Anguilla</option>
                            <option value=\"Antarctique\" ";
        // line 93
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Antarctique")) {
            echo " selected ";
        }
        echo ">Antarctique</option>
                            <option value=\"Antigua et Barbuda\" ";
        // line 94
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Antigua et Barbuda")) {
            echo " selected ";
        }
        echo ">Antigua-et-Barbuda</option>
                            <option value=\"Arabie saoudite\" ";
        // line 95
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Arabie saoudite")) {
            echo " selected ";
        }
        echo ">Arabie saoudite</option>
                            <option value=\"Argentine\" ";
        // line 96
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Argentine")) {
            echo " selected ";
        }
        echo ">Argentine</option>
                            <option value=\"Arménie\" ";
        // line 97
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Arménie")) {
            echo " selected ";
        }
        echo ">Arménie</option>
                            <option value=\"Aruba\" ";
        // line 98
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Aruba")) {
            echo " selected ";
        }
        echo ">Aruba</option>
                            <option value=\"Australie\" ";
        // line 99
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Australie")) {
            echo " selected ";
        }
        echo ">Australie</option>
                            <option value=\"Autriche\" ";
        // line 100
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Autriche")) {
            echo " selected ";
        }
        echo ">Autriche</option>
                            <option value=\"Azerbaïdjan\" ";
        // line 101
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Azerbaïdjan")) {
            echo " selected ";
        }
        echo ">Azerbaïdjan</option>
                            <option value=\"Bahamas\" ";
        // line 102
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Bahamas")) {
            echo " selected ";
        }
        echo ">Bahamas</option>
                            <option value=\"Bahreïn\" ";
        // line 103
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Bahreïn")) {
            echo " selected ";
        }
        echo ">Bahreïn</option>
                            <option value=\"Bangladesh\" ";
        // line 104
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Bangladesh")) {
            echo " selected ";
        }
        echo ">Bangladesh</option>
                            <option value=\"Barbade\" ";
        // line 105
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Barbade")) {
            echo " selected ";
        }
        echo ">Barbade</option>
                            <option value=\"Belgique\" ";
        // line 106
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Belgique")) {
            echo " selected ";
        }
        echo ">Belgique</option>
                            <option value=\"Belize\" ";
        // line 107
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Belize")) {
            echo " selected ";
        }
        echo ">Belize</option>
                            <option value=\"Bénin\" ";
        // line 108
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Bénin")) {
            echo " selected ";
        }
        echo ">Bénin</option>
                            <option value=\"Bermudes\" ";
        // line 109
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Bermudes")) {
            echo " selected ";
        }
        echo ">Bermudes</option>
                            <option value=\"Bhoutan\" ";
        // line 110
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Bhoutan")) {
            echo " selected ";
        }
        echo ">Bhoutan</option>
                            <option value=\"Biélorussie\" ";
        // line 111
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Biélorussie")) {
            echo " selected ";
        }
        echo ">Biélorussie</option>
                            <option value=\"Bolivie\" ";
        // line 112
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Bolivie")) {
            echo " selected ";
        }
        echo ">Bolivie</option>
                            <option value=\"Bosnie Herzégovine\" ";
        // line 113
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Bosnie Herzégovine")) {
            echo " selected ";
        }
        echo ">Bosnie-Herzégovine</option>
                            <option value=\"Botswana\" ";
        // line 114
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Botswana")) {
            echo " selected ";
        }
        echo ">Botswana</option>
                            <option value=\"Brésil\" ";
        // line 115
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Brésil")) {
            echo " selected ";
        }
        echo ">Brésil</option>
                            <option value=\"Brunéi Darussalam\" ";
        // line 116
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Brunéi Darussalam")) {
            echo " selected ";
        }
        echo ">Brunéi Darussalam</option>
                            <option value=\"Bulgarie\" ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Bulgarie")) {
            echo " selected ";
        }
        echo ">Bulgarie</option>
                            <option value=\"Burkina Faso\" ";
        // line 118
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Burkina Faso")) {
            echo " selected ";
        }
        echo ">Burkina Faso</option>
                            <option value=\"Burundi\" ";
        // line 119
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Burundi")) {
            echo " selected ";
        }
        echo ">Burundi</option>
                            <option value=\"Cambodge\" ";
        // line 120
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Cambodge")) {
            echo " selected ";
        }
        echo ">Cambodge</option>
                            <option value=\"Cameroun\" ";
        // line 121
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Cameroun")) {
            echo " selected ";
        }
        echo ">Cameroun</option>
                            <option value=\"Canada\" ";
        // line 122
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Canada")) {
            echo " selected ";
        }
        echo ">Canada</option>
                            <option value=\"Cap Vert\" ";
        // line 123
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Cap Vert")) {
            echo " selected ";
        }
        echo ">Cap-Vert</option>
                            <option value=\"Ceuta et Melilla\" ";
        // line 124
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Ceuta et Melilla")) {
            echo " selected ";
        }
        echo ">Ceuta et Melilla</option>
                            <option value=\"Chili\" ";
        // line 125
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Chili")) {
            echo " selected ";
        }
        echo ">Chili</option>
                            <option value=\"Chine\" ";
        // line 126
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Chine")) {
            echo " selected ";
        }
        echo ">Chine</option>
                            <option value=\"Chypre\" ";
        // line 127
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Chypre")) {
            echo " selected ";
        }
        echo ">Chypre</option>
                            <option value=\"Colombie\" ";
        // line 128
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Colombie")) {
            echo " selected ";
        }
        echo ">Colombie</option>
                            <option value=\"Comores\" ";
        // line 129
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Comores")) {
            echo " selected ";
        }
        echo ">Comores</option>
                            <option value=\"Congo Brazzaville\" ";
        // line 130
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Congo Brazzaville")) {
            echo " selected ";
        }
        echo ">Congo-Brazzaville</option>
                            <option value=\"Congo Kinshasa\" ";
        // line 131
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Congo Kinshasa")) {
            echo " selected ";
        }
        echo ">Congo-Kinshasa</option>
                            <option value=\"Corée du Nord\" ";
        // line 132
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Corée du Nord")) {
            echo " selected ";
        }
        echo ">Corée du Nord</option>
                            <option value=\"Corée du Sud\" ";
        // line 133
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Corée du Sud")) {
            echo " selected ";
        }
        echo ">Corée du Sud</option>
                            <option value=\"Costa Rica\" ";
        // line 134
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Costa Rica")) {
            echo " selected ";
        }
        echo ">Costa Rica</option>
                            <option value=\"Côte d’Ivoire\" ";
        // line 135
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Côte d’Ivoire")) {
            echo " selected ";
        }
        echo ">Côte d’Ivoire</option>
                            <option value=\"Croatie\" ";
        // line 136
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Croatie")) {
            echo " selected ";
        }
        echo ">Croatie</option>
                            <option value=\"Cuba\" ";
        // line 137
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Cuba")) {
            echo " selected ";
        }
        echo ">Cuba</option>
                            <option value=\"Curaçao\" ";
        // line 138
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Curaçao")) {
            echo " selected ";
        }
        echo ">Curaçao</option>
                            <option value=\"Danemark\" ";
        // line 139
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Danemark")) {
            echo " selected ";
        }
        echo ">Danemark</option>
                            <option value=\"Diego Garcia\" ";
        // line 140
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Diego Garcia")) {
            echo " selected ";
        }
        echo ">Diego Garcia</option>
                            <option value=\"Djibouti\" ";
        // line 141
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Djibouti")) {
            echo " selected ";
        }
        echo ">Djibouti</option>
                            <option value=\"Dominique\" ";
        // line 142
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Dominique")) {
            echo " selected ";
        }
        echo ">Dominique</option>
                            <option value=\"Égypte\" ";
        // line 143
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Égypte")) {
            echo " selected ";
        }
        echo ">Égypte</option>
                            <option value=\"El Salvador\" ";
        // line 144
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "El Salvador")) {
            echo " selected ";
        }
        echo ">El Salvador</option>
                            <option value=\"Émirats arabes unis\" ";
        // line 145
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Émirats arabes unis")) {
            echo " selected ";
        }
        echo ">Émirats arabes unis</option>
                            <option value=\"Équateur\" ";
        // line 146
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Équateur")) {
            echo " selected ";
        }
        echo ">Équateur</option>
                            <option value=\"Érythrée\" ";
        // line 147
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Érythrée")) {
            echo " selected ";
        }
        echo ">Érythrée</option>
                            <option value=\"Espagne\" ";
        // line 148
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Espagne")) {
            echo " selected ";
        }
        echo ">Espagne</option>
                            <option value=\"Estonie\" ";
        // line 149
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Estonie")) {
            echo " selected ";
        }
        echo ">Estonie</option>
                            <option value=\"État de la Cité du Vatican\" ";
        // line 150
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "État de la Cité du Vatican")) {
            echo " selected ";
        }
        echo ">État de la Cité du Vatican</option>
                            <option value=\"États fédérés de Micronésie\" ";
        // line 151
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "États fédérés de Micronésie")) {
            echo " selected ";
        }
        echo ">États fédérés de Micronésie</option>
                            <option value=\"États Unis\" ";
        // line 152
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "États Unis")) {
            echo " selected ";
        }
        echo ">États-Unis</option>
                            <option value=\"Éthiopie\" ";
        // line 153
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Éthiopie")) {
            echo " selected ";
        }
        echo ">Éthiopie</option>
                            <option value=\"Fidji\" ";
        // line 154
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Fidji")) {
            echo " selected ";
        }
        echo ">Fidji</option>
                            <option value=\"Finlande\" ";
        // line 155
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Finlande")) {
            echo " selected ";
        }
        echo ">Finlande</option>
                            <option value=\"France\" ";
        // line 156
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "France")) {
            echo " selected ";
        }
        echo ">France</option>
                            <option value=\"Gabon\" ";
        // line 157
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Gabon")) {
            echo " selected ";
        }
        echo ">Gabon</option>
                            <option value=\"Gambie\" ";
        // line 158
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Gambie")) {
            echo " selected ";
        }
        echo ">Gambie</option>
                            <option value=\"Géorgie\" ";
        // line 159
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Géorgie")) {
            echo " selected ";
        }
        echo ">Géorgie</option>
                            <option value=\"Ghana\" ";
        // line 160
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Ghana")) {
            echo " selected ";
        }
        echo ">Ghana</option>
                            <option value=\"Gibraltar\" ";
        // line 161
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Gibraltar")) {
            echo " selected ";
        }
        echo ">Gibraltar</option>
                            <option value=\"Grèce\" ";
        // line 162
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Grèce")) {
            echo " selected ";
        }
        echo ">Grèce</option>
                            <option value=\"Grenade\" ";
        // line 163
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Grenade")) {
            echo " selected ";
        }
        echo ">Grenade</option>
                            <option value=\"Groenland\" ";
        // line 164
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Groenland")) {
            echo " selected ";
        }
        echo ">Groenland</option>
                            <option value=\"Guadeloupe\" ";
        // line 165
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Guadeloupe")) {
            echo " selected ";
        }
        echo ">Guadeloupe</option>
                            <option value=\"Guam\" ";
        // line 166
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Guam")) {
            echo " selected ";
        }
        echo ">Guam</option>
                            <option value=\"Guatemala\" ";
        // line 167
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Guatemala")) {
            echo " selected ";
        }
        echo ">Guatemala</option>
                            <option value=\"Guernesey\" ";
        // line 168
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Guernesey")) {
            echo " selected ";
        }
        echo ">Guernesey</option>
                            <option value=\"Guinée\" ";
        // line 169
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Guinée")) {
            echo " selected ";
        }
        echo ">Guinée</option>
                            <option value=\"Guinée équatoriale\" ";
        // line 170
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Guinée équatoriale")) {
            echo " selected ";
        }
        echo ">Guinée équatoriale</option>
                            <option value=\"Guinée Bissau\" ";
        // line 171
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Guinée Bissau")) {
            echo " selected ";
        }
        echo ">Guinée-Bissau</option>
                            <option value=\"Guyana\" ";
        // line 172
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Guyana")) {
            echo " selected ";
        }
        echo ">Guyana</option>
                            <option value=\"Guyane française\" ";
        // line 173
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Guyane française")) {
            echo " selected ";
        }
        echo ">Guyane française</option>
                            <option value=\"Haïti\" ";
        // line 174
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Haïti")) {
            echo " selected ";
        }
        echo ">Haïti</option>
                            <option value=\"Honduras\" ";
        // line 175
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Honduras")) {
            echo " selected ";
        }
        echo ">Honduras</option>
                            <option value=\"Hongrie\" ";
        // line 176
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Hongrie")) {
            echo " selected ";
        }
        echo ">Hongrie</option>
                            <option value=\"Île Christmas\" ";
        // line 177
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Île Christmas")) {
            echo " selected ";
        }
        echo ">Île Christmas</option>
                            <option value=\"Île de l’Ascension\" ";
        // line 178
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Île de l’Ascension")) {
            echo " selected ";
        }
        echo ">Île de l’Ascension</option>
                            <option value=\"Île de Man\" ";
        // line 179
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Île de Man")) {
            echo " selected ";
        }
        echo ">Île de Man</option>
                            <option value=\"Île Norfolk\" ";
        // line 180
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Île Norfolk")) {
            echo " selected ";
        }
        echo ">Île Norfolk</option>
                            <option value=\"Îles Åland\" ";
        // line 181
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Åland")) {
            echo " selected ";
        }
        echo ">Îles Åland</option>
                            <option value=\"Îles Caïmans\" ";
        // line 182
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Caïmans")) {
            echo " selected ";
        }
        echo ">Îles Caïmans</option>
                            <option value=\"Îles Canaries\" ";
        // line 183
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Canaries")) {
            echo " selected ";
        }
        echo ">Îles Canaries</option>
                            <option value=\"Îles Cocos\" ";
        // line 184
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Cocos")) {
            echo " selected ";
        }
        echo ">Îles Cocos</option>
                            <option value=\"Îles Cook\" ";
        // line 185
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Cook")) {
            echo " selected ";
        }
        echo ">Îles Cook</option>
                            <option value=\"Îles Féroé\" ";
        // line 186
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Féroé")) {
            echo " selected ";
        }
        echo ">Îles Féroé</option>
                            <option value=\"Îles Géorgie du Sud et Sandwich du Sud\" ";
        // line 187
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Géorgie du Sud et Sandwich du Sud")) {
            echo " selected ";
        }
        echo ">Îles Géorgie du Sud et Sandwich du Sud</option>
                            <option value=\"Îles Malouines\" ";
        // line 188
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Malouines")) {
            echo " selected ";
        }
        echo ">Îles Malouines</option>
                            <option value=\"Îles Mariannes du Nord\" ";
        // line 189
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Mariannes du Nord")) {
            echo " selected ";
        }
        echo ">Îles Mariannes du Nord</option>
                            <option value=\"Îles Marshall\" ";
        // line 190
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Marshall")) {
            echo " selected ";
        }
        echo ">Îles Marshall</option>
                            <option value=\"Îles mineures éloignées des États Unis\" ";
        // line 191
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles mineures éloignées des États Unis")) {
            echo " selected ";
        }
        echo ">Îles mineures éloignées des États-Unis</option>
                            <option value=\"Îles Salomon\" ";
        // line 192
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Salomon")) {
            echo " selected ";
        }
        echo ">Îles Salomon</option>
                            <option value=\"Îles Turques et Caïques\" ";
        // line 193
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Turques et Caïques")) {
            echo " selected ";
        }
        echo ">Îles Turques-et-Caïques</option>
                            <option value=\"Îles Vierges britanniques\" ";
        // line 194
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Vierges britanniques")) {
            echo " selected ";
        }
        echo ">Îles Vierges britanniques</option>
                            <option value=\"Îles Vierges des États-Unis\" ";
        // line 195
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Îles Vierges des États-Unis")) {
            echo " selected ";
        }
        echo ">Îles Vierges des États-Unis</option>
                            <option value=\"Inde\" ";
        // line 196
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Inde")) {
            echo " selected ";
        }
        echo ">Inde</option>
                            <option value=\"Indonésie\" ";
        // line 197
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Indonésie")) {
            echo " selected ";
        }
        echo ">Indonésie</option>
                            <option value=\"Irak\" ";
        // line 198
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Irak")) {
            echo " selected ";
        }
        echo ">Irak</option>
                            <option value=\"Iran\" ";
        // line 199
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Iran")) {
            echo " selected ";
        }
        echo ">Iran</option>
                            <option value=\"Irlande\" ";
        // line 200
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Irlande")) {
            echo " selected ";
        }
        echo ">Irlande</option>
                            <option value=\"Islande\" ";
        // line 201
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Islande")) {
            echo " selected ";
        }
        echo ">Islande</option>
                            <option value=\"Italie\" ";
        // line 202
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Italie")) {
            echo " selected ";
        }
        echo ">Italie</option>
                            <option value=\"Jamaïque\" ";
        // line 203
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Jamaïque")) {
            echo " selected ";
        }
        echo ">Jamaïque</option>
                            <option value=\"Japon\" ";
        // line 204
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Japon")) {
            echo " selected ";
        }
        echo ">Japon</option>
                            <option value=\"Jersey\" ";
        // line 205
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Jersey")) {
            echo " selected ";
        }
        echo ">Jersey</option>
                            <option value=\"Jordanie\" ";
        // line 206
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Jordanie")) {
            echo " selected ";
        }
        echo ">Jordanie</option>
                            <option value=\"Kazakhstan\" ";
        // line 207
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Kazakhstan")) {
            echo " selected ";
        }
        echo ">Kazakhstan</option>
                            <option value=\"Kenya\" ";
        // line 208
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Kenya")) {
            echo " selected ";
        }
        echo ">Kenya</option>
                            <option value=\"Kirghizistan\" ";
        // line 209
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Kirghizistan")) {
            echo " selected ";
        }
        echo ">Kirghizistan</option>
                            <option value=\"Kiribati\" ";
        // line 210
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Kiribati")) {
            echo " selected ";
        }
        echo ">Kiribati</option>
                            <option value=\"Kosovo\" ";
        // line 211
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Kosovo")) {
            echo " selected ";
        }
        echo ">Kosovo</option>
                            <option value=\"Koweït\" ";
        // line 212
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Koweït")) {
            echo " selected ";
        }
        echo ">Koweït</option>
                            <option value=\"La Réunion\" ";
        // line 213
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "La Réunion")) {
            echo " selected ";
        }
        echo ">La Réunion</option>
                            <option value=\"Laos\" ";
        // line 214
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Laos")) {
            echo " selected ";
        }
        echo ">Laos</option>
                            <option value=\"Lesotho\" ";
        // line 215
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Lesotho")) {
            echo " selected ";
        }
        echo ">Lesotho</option>
                            <option value=\"Lettonie\" ";
        // line 216
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Lettonie")) {
            echo " selected ";
        }
        echo ">Lettonie</option>
                            <option value=\"Liban\" ";
        // line 217
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Liban")) {
            echo " selected ";
        }
        echo ">Liban</option>
                            <option value=\"Libéria\" ";
        // line 218
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Libéria")) {
            echo " selected ";
        }
        echo ">Libéria</option>
                            <option value=\"Libye\" ";
        // line 219
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Libye")) {
            echo " selected ";
        }
        echo ">Libye</option>
                            <option value=\"Liechtenstein\" ";
        // line 220
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Liechtenstein")) {
            echo " selected ";
        }
        echo ">Liechtenstein</option>
                            <option value=\"Lituanie\" ";
        // line 221
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Lituanie")) {
            echo " selected ";
        }
        echo ">Lituanie</option>
                            <option value=\"Luxembourg\" ";
        // line 222
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Luxembourg")) {
            echo " selected ";
        }
        echo ">Luxembourg</option>
                            <option value=\"Macédoine\" ";
        // line 223
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Macédoine")) {
            echo " selected ";
        }
        echo ">Macédoine</option>
                            <option value=\"Madagascar\" ";
        // line 224
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Madagascar")) {
            echo " selected ";
        }
        echo ">Madagascar</option>
                            <option value=\"Malaisie\" ";
        // line 225
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Malaisie")) {
            echo " selected ";
        }
        echo ">Malaisie</option>
                            <option value=\"Malawi\" ";
        // line 226
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Malawi")) {
            echo " selected ";
        }
        echo ">Malawi</option>
                            <option value=\"Maldives\" ";
        // line 227
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Maldives")) {
            echo " selected ";
        }
        echo ">Maldives</option>
                            <option value=\"Mali\" ";
        // line 228
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Mali")) {
            echo " selected ";
        }
        echo ">Mali</option>
                            <option value=\"Malte\" ";
        // line 229
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Malte")) {
            echo " selected ";
        }
        echo ">Malte</option>
                            <option value=\"Maroc\" ";
        // line 230
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Maroc")) {
            echo " selected ";
        }
        echo ">Maroc</option>
                            <option value=\"Martinique\" ";
        // line 231
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Martinique")) {
            echo " selected ";
        }
        echo ">Martinique</option>
                            <option value=\"Maurice\" ";
        // line 232
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Maurice")) {
            echo " selected ";
        }
        echo ">Maurice</option>
                            <option value=\"Mauritanie\" ";
        // line 233
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Mauritanie")) {
            echo " selected ";
        }
        echo ">Mauritanie</option>
                            <option value=\"Mayotte\" ";
        // line 234
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Mayotte")) {
            echo " selected ";
        }
        echo ">Mayotte</option>
                            <option value=\"Mexique\" ";
        // line 235
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Mexique")) {
            echo " selected ";
        }
        echo ">Mexique</option>
                            <option value=\"Moldavie\" ";
        // line 236
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Moldavie")) {
            echo " selected ";
        }
        echo ">Moldavie</option>
                            <option value=\"Monaco\" ";
        // line 237
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Monaco")) {
            echo " selected ";
        }
        echo ">Monaco</option>
                            <option value=\"Mongolie\" ";
        // line 238
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Mongolie")) {
            echo " selected ";
        }
        echo ">Mongolie</option>
                            <option value=\"Monténégro\" ";
        // line 239
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Monténégro")) {
            echo " selected ";
        }
        echo ">Monténégro</option>
                            <option value=\"Montserrat\" ";
        // line 240
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Montserrat")) {
            echo " selected ";
        }
        echo ">Montserrat</option>
                            <option value=\"Mozambique\" ";
        // line 241
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Mozambique")) {
            echo " selected ";
        }
        echo ">Mozambique</option>
                            <option value=\"Myanmar\" ";
        // line 242
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Myanmar")) {
            echo " selected ";
        }
        echo ">Myanmar</option>
                            <option value=\"Namibie\" ";
        // line 243
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Namibie")) {
            echo " selected ";
        }
        echo ">Namibie</option>
                            <option value=\"Nauru\" ";
        // line 244
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Nauru")) {
            echo " selected ";
        }
        echo ">Nauru</option>
                            <option value=\"Népal\" ";
        // line 245
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Népal")) {
            echo " selected ";
        }
        echo ">Népal</option>
                            <option value=\"Nicaragua\" ";
        // line 246
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Nicaragua")) {
            echo " selected ";
        }
        echo ">Nicaragua</option>
                            <option value=\"Niger\" ";
        // line 247
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Niger")) {
            echo " selected ";
        }
        echo ">Niger</option>
                            <option value=\"Nigéria\" ";
        // line 248
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Nigéria")) {
            echo " selected ";
        }
        echo ">Nigéria</option>
                            <option value=\"Niue\" ";
        // line 249
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Niue")) {
            echo " selected ";
        }
        echo ">Niue</option>
                            <option value=\"Norvège\" ";
        // line 250
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Norvège")) {
            echo " selected ";
        }
        echo ">Norvège</option>
                            <option value=\"Nouvelle Calédonie\" ";
        // line 251
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Nouvelle Calédonie")) {
            echo " selected ";
        }
        echo ">Nouvelle-Calédonie</option>
                            <option value=\"Nouvelle Zélande\" ";
        // line 252
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Nouvelle Zélande")) {
            echo " selected ";
        }
        echo ">Nouvelle-Zélande</option>
                            <option value=\"Oman\" ";
        // line 253
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Oman")) {
            echo " selected ";
        }
        echo ">Oman</option>
                            <option value=\"Ouganda\" ";
        // line 254
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Ouganda")) {
            echo " selected ";
        }
        echo ">Ouganda</option>
                            <option value=\"Ouzbékistan\" ";
        // line 255
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Ouzbékistan")) {
            echo " selected ";
        }
        echo ">Ouzbékistan</option>
                            <option value=\"Pakistan\" ";
        // line 256
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Pakistan")) {
            echo " selected ";
        }
        echo ">Pakistan</option>
                            <option value=\"Palaos\" ";
        // line 257
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Palaos")) {
            echo " selected ";
        }
        echo ">Palaos</option>
                            <option value=\"Panama\" ";
        // line 258
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Panama")) {
            echo " selected ";
        }
        echo ">Panama</option>
                            <option value=\"Papouasie Nouvelle Guinée\" ";
        // line 259
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Papouasie Nouvelle Guinée")) {
            echo " selected ";
        }
        echo ">Papouasie-Nouvelle-Guinée</option>
                            <option value=\"Paraguay\" ";
        // line 260
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Paraguay")) {
            echo " selected ";
        }
        echo ">Paraguay</option>
                            <option value=\"Pays Bas\" ";
        // line 261
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Pays Bas")) {
            echo " selected ";
        }
        echo ">Pays-Bas</option>
                            <option value=\"Pays Bas caribéens\" ";
        // line 262
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Pays Bas caribéens")) {
            echo " selected ";
        }
        echo ">Pays-Bas caribéens</option>
                            <option value=\"Pérou\" ";
        // line 263
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Pérou")) {
            echo " selected ";
        }
        echo ">Pérou</option>
                            <option value=\"Philippines\" ";
        // line 264
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Philippines")) {
            echo " selected ";
        }
        echo ">Philippines</option>
                            <option value=\"Pitcairn\" ";
        // line 265
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Pitcairn")) {
            echo " selected ";
        }
        echo ">Pitcairn</option>
                            <option value=\"Pologne\" ";
        // line 266
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Pologne")) {
            echo " selected ";
        }
        echo ">Pologne</option>
                            <option value=\"Polynésie française\" ";
        // line 267
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Polynésie française")) {
            echo " selected ";
        }
        echo ">Polynésie française</option>
                            <option value=\"Porto Rico\" ";
        // line 268
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Porto Rico")) {
            echo " selected ";
        }
        echo ">Porto Rico</option>
                            <option value=\"Portugal\" ";
        // line 269
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Portugal")) {
            echo " selected ";
        }
        echo ">Portugal</option>
                            <option value=\"Qatar\" ";
        // line 270
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Qatar")) {
            echo " selected ";
        }
        echo ">Qatar</option>
                            <option value=\"R.A.S. chinoise de Hong Kong\" ";
        // line 271
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "R.A.S. chinoise de Hong Kong")) {
            echo " selected ";
        }
        echo ">R.A.S. chinoise de Hong Kong</option>
                            <option value=\"R.A.S. chinoise de Macao\" ";
        // line 272
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "R.A.S. chinoise de Macao")) {
            echo " selected ";
        }
        echo ">R.A.S. chinoise de Macao</option>
                            <option value=\"République centrafricaine\" ";
        // line 273
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "République centrafricaine")) {
            echo " selected ";
        }
        echo ">République centrafricaine</option>
                            <option value=\"République dominicaine\" ";
        // line 274
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "République dominicaine")) {
            echo " selected ";
        }
        echo ">République dominicaine</option>
                            <option value=\"République tchèque\" ";
        // line 275
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "République tchèque")) {
            echo " selected ";
        }
        echo ">République tchèque</option>
                            <option value=\"Roumanie\" ";
        // line 276
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Roumanie")) {
            echo " selected ";
        }
        echo ">Roumanie</option>
                            <option value=\"Royaume-Uni\" ";
        // line 277
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Royaume-Uni")) {
            echo " selected ";
        }
        echo ">Royaume-Uni</option>
                            <option value=\"Russie\" ";
        // line 278
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Russie")) {
            echo " selected ";
        }
        echo ">Russie</option>
                            <option value=\"Rwanda\" ";
        // line 279
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Rwanda")) {
            echo " selected ";
        }
        echo ">Rwanda</option>
                            <option value=\"Sahara occidental\" ";
        // line 280
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Sahara occidental")) {
            echo " selected ";
        }
        echo ">Sahara occidental</option>
                            <option value=\"Saint Barthélemy\" ";
        // line 281
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Saint-Barthélemy")) {
            echo " selected ";
        }
        echo ">Saint-Barthélemy</option>
                            <option value=\"Saint Christophe et Niévès\" ";
        // line 282
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Saint Christophe et Niévès")) {
            echo " selected ";
        }
        echo ">Saint-Christophe-et-Niévès</option>
                            <option value=\"Saint-Marin\" ";
        // line 283
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Saint-Marin")) {
            echo " selected ";
        }
        echo ">Saint-Marin</option>
                            <option value=\"Saint-Martin (partie française)\" ";
        // line 284
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Saint Martin (partie française)")) {
            echo " selected ";
        }
        echo ">Saint-Martin (partie française)</option>
                            <option value=\"Saint-Martin (partie néerlandaise)\" ";
        // line 285
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Saint Martin (partie néerlandaise)")) {
            echo " selected ";
        }
        echo ">Saint-Martin (partie néerlandaise)</option>
                            <option value=\"Saint Pierre et Miquelon\" ";
        // line 286
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Saint Pierre et Miquelon")) {
            echo " selected ";
        }
        echo ">Saint-Pierre-et-Miquelon</option>
                            <option value=\"Saint Vincent et les Grenadines\" ";
        // line 287
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Saint Vincent et les Grenadines")) {
            echo " selected ";
        }
        echo ">Saint-Vincent-et-les-Grenadines</option>
                            <option value=\"Sainte Hélène\" ";
        // line 288
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Sainte Hélène")) {
            echo " selected ";
        }
        echo ">Sainte-Hélène</option>
                            <option value=\"Sainte Lucie\" ";
        // line 289
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Sainte Lucie")) {
            echo " selected ";
        }
        echo ">Sainte-Lucie</option>
                            <option value=\"Samoa\" ";
        // line 290
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Samoa")) {
            echo " selected ";
        }
        echo ">Samoa</option>
                            <option value=\"Samoa américaines\" ";
        // line 291
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Samoa américaines")) {
            echo " selected ";
        }
        echo ">Samoa américaines</option>
                            <option value=\"Sao Tomé et Principe\" ";
        // line 292
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Sao Tomé et Principe")) {
            echo " selected ";
        }
        echo ">Sao Tomé-et-Principe</option>
                            <option value=\"Sénégal\" ";
        // line 293
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Sénégal")) {
            echo " selected ";
        }
        echo ">Sénégal</option>
                            <option value=\"Serbie\" ";
        // line 294
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Serbie")) {
            echo " selected ";
        }
        echo ">Serbie</option>
                            <option value=\"Seychelles\" ";
        // line 295
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Seychelles")) {
            echo " selected ";
        }
        echo ">Seychelles</option>
                            <option value=\"Sierra Leone\" ";
        // line 296
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Sierra Leone")) {
            echo " selected ";
        }
        echo ">Sierra Leone</option>
                            <option value=\"Singapour\" ";
        // line 297
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Singapour")) {
            echo " selected ";
        }
        echo ">Singapour</option>
                            <option value=\"Slovaquie\" ";
        // line 298
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Slovaquie")) {
            echo " selected ";
        }
        echo ">Slovaquie</option>
                            <option value=\"Slovénie\" ";
        // line 299
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Slovénie")) {
            echo " selected ";
        }
        echo ">Slovénie</option>
                            <option value=\"Somalie\" ";
        // line 300
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Somalie")) {
            echo " selected ";
        }
        echo ">Somalie</option>
                            <option value=\"Soudan\" ";
        // line 301
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Soudan")) {
            echo " selected ";
        }
        echo ">Soudan</option>
                            <option value=\"Soudan du Sud\" ";
        // line 302
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Soudan du Sud")) {
            echo " selected ";
        }
        echo ">Soudan du Sud</option>
                            <option value=\"Sri Lanka\" ";
        // line 303
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Sri Lanka")) {
            echo " selected ";
        }
        echo ">Sri Lanka</option>
                            <option value=\"Suède\" ";
        // line 304
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Suède")) {
            echo " selected ";
        }
        echo ">Suède</option>
                            <option value=\"Suisse\" ";
        // line 305
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Suisse")) {
            echo " selected ";
        }
        echo ">Suisse</option>
                            <option value=\"Suriname\" ";
        // line 306
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Suriname")) {
            echo " selected ";
        }
        echo ">Suriname</option>
                            <option value=\"Svalbard et Jan Mayen\" ";
        // line 307
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Svalbard et Jan Mayen")) {
            echo " selected ";
        }
        echo ">Svalbard et Jan Mayen</option>
                            <option value=\"Swaziland\" ";
        // line 308
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Swaziland")) {
            echo " selected ";
        }
        echo ">Swaziland</option>
                            <option value=\"Syrie\" ";
        // line 309
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Syrie")) {
            echo " selected ";
        }
        echo ">Syrie</option>
                            <option value=\"Tadjikistan\" ";
        // line 310
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Tadjikistan")) {
            echo " selected ";
        }
        echo ">Tadjikistan</option>
                            <option value=\"Taïwan\" ";
        // line 311
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Taïwan")) {
            echo " selected ";
        }
        echo ">Taïwan</option>
                            <option value=\"Tanzanie\" ";
        // line 312
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Tanzanie")) {
            echo " selected ";
        }
        echo ">Tanzanie</option>
                            <option value=\"Tchad\" ";
        // line 313
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Tchad")) {
            echo " selected ";
        }
        echo ">Tchad</option>
                            <option value=\"Terres australes françaises\" ";
        // line 314
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Terres australes françaises")) {
            echo " selected ";
        }
        echo ">Terres australes françaises</option>
                            <option value=\"Territoire britannique de l’océan Indien\" ";
        // line 315
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Territoire britannique de l’océan Indien")) {
            echo " selected ";
        }
        echo ">Territoire britannique de l’océan Indien</option>
                            <option value=\"Territoires palestiniens\" ";
        // line 316
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "PS")) {
            echo " selected ";
        }
        echo ">Territoires palestiniens</option>
                            <option value=\"Thaïlande\" ";
        // line 317
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Thaïlande")) {
            echo " selected ";
        }
        echo ">Thaïlande</option>
                            <option value=\"Timor oriental\" ";
        // line 318
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Timor oriental")) {
            echo " selected ";
        }
        echo ">Timor oriental</option>
                            <option value=\"Togo\" ";
        // line 319
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Togo")) {
            echo " selected ";
        }
        echo ">Togo</option>
                            <option value=\"Tokelau\" ";
        // line 320
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Tokelau")) {
            echo " selected ";
        }
        echo ">Tokelau</option>
                            <option value=\"Tonga\" ";
        // line 321
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Tonga")) {
            echo " selected ";
        }
        echo ">Tonga</option>
                            <option value=\"Trinité et Tobago\" ";
        // line 322
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Trinité et Tobago")) {
            echo " selected ";
        }
        echo ">Trinité-et-Tobago</option>
                            <option value=\"Trinité et Tobago\" ";
        // line 323
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Trinité et Tobago")) {
            echo " selected ";
        }
        echo ">Tristan da Cunha</option>
                            <option value=\"Tunisie\" ";
        // line 324
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Tunisie")) {
            echo " selected ";
        }
        echo ">Tunisie</option>
                            <option value=\"Turkménistan\" ";
        // line 325
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Turkménistan")) {
            echo " selected ";
        }
        echo ">Turkménistan</option>
                            <option value=\"Turquie\" ";
        // line 326
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Turquie")) {
            echo " selected ";
        }
        echo ">Turquie</option>
                            <option value=\"Tuvalu\" ";
        // line 327
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Tuvalu")) {
            echo " selected ";
        }
        echo ">Tuvalu</option>
                            <option value=\"Ukraine\" ";
        // line 328
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Ukraine")) {
            echo " selected ";
        }
        echo ">Ukraine</option>
                            <option value=\"Uruguay\" ";
        // line 329
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Uruguay")) {
            echo " selected ";
        }
        echo ">Uruguay</option>
                            <option value=\"Vanuatu\" ";
        // line 330
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Vanuatu")) {
            echo " selected ";
        }
        echo ">Vanuatu</option>
                            <option value=\"Venezuela\" ";
        // line 331
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Venezuela")) {
            echo " selected ";
        }
        echo ">Venezuela</option>
                            <option value=\"Vietnam\" ";
        // line 332
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Vietnam")) {
            echo " selected ";
        }
        echo ">Vietnam</option>
                            <option value=\"Wallisn et Futuna\" ";
        // line 333
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Wallis et Futuna")) {
            echo " selected ";
        }
        echo ">Wallis-et-Futuna</option>
                            <option value=\"Yémen\" ";
        // line 334
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Yémen")) {
            echo " selected ";
        }
        echo ">Yémen</option>
                            <option value=\"Zambie\" ";
        // line 335
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Zambie")) {
            echo " selected ";
        }
        echo ">Zambie</option>
                            <option value=\"Zimbabwe\" ";
        // line 336
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == "Zimbabwe")) {
            echo " selected ";
        }
        echo ">Zimbabwe</option>
                        </select>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"\">";
        // line 340
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.adress"), "html", null, true);
        echo ":</div>
                        <input type=\"text\" required=\"false\" class=\"form-control\" name=\"ville\" id=\"ville\" placeholder=\"";
        // line 341
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.adress"), "html", null, true);
        echo "\" />
                    </div>

                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"\">Sport:</div>  
                        <select class=\"form-control\" name=\"interest\">
                            <option value=\"\">Toutes</option>
                            ";
        // line 348
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["userInterest"]) ? $context["userInterest"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
            // line 349
            echo "                                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["interest"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["interest"], "title", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 351
        echo "                        </select>
                    </div>  
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"\">";
        // line 354
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("search.rayon"), "html", null, true);
        echo ":</div>
                        <div class=\"rayon\">
                            <label>";
        // line 356
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("search.rayon"), "html", null, true);
        echo "</label>
                            <input type=\"number\" name=\"rayon\" id=\"default\">
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <input type=\"checkbox\" name=\"handi\">
                        ";
        // line 362
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("handi.sport"), "html", null, true);
        echo "
                    </div>
                    <div class=\"col-md-10 col-sm-6\"></div>
                    <div class=\"col-md-2 col-sm-6\">
                        <input type=\"submit\" class=\"form-control btn btn-grinta\" value=\"";
        // line 366
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("button.search"), "html", null, true);
        echo "\" />
                    </div>
                </div>
            </form>
        </div>
        <div class=\"result-search\">
            <p class=\"result\">";
        // line 372
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "city", array()), "html", null, true);
        echo " ( ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "city", array()), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()), "html", null, true);
        echo ") ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("search.rayon"), "html", null, true);
        echo " 20 Km ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("rayon.autour"), "html", null, true);
        echo "</p>
            ";
        // line 373
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 374
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
                // line 375
                echo "                <div class=\"block item-act\">
                    <div class=\"row\">
                        <div class=\"col-sm-3 col-md-2\">
                            <a href=\"";
                // line 378
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()))), "html", null, true);
                echo "\">
                                <div class=\"img-avatar\">
                                    ";
                // line 380
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 381
                    echo "                                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"\">
                                    ";
                } else {
                    // line 383
                    echo "                                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"\">
                                    ";
                }
                // line 385
                echo "                                </div>
                                <div class=\"name-avatar\">
                                    <h5 class=\"text-center\">";
                // line 387
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                </div>
                            </a>   
                        </div>
                        <div class=\"col-sm-6 text-center\">
                            <h3 class=\"name-match green\">";
                // line 392
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "title", array()), "html", null, true);
                echo "</h3>
                            <div class=\"nb-participant\">
                                ";
                // line 394
                if ((null === $this->getAttribute($context["entity"], "nbPlayer", array()))) {
                    // line 395
                    echo "                                    <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo " participant</p>
                                ";
                } else {
                    // line 397
                    echo "                                    <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nbPlayer", array()), "html", null, true);
                    echo "</p>
                                    ";
                    // line 398
                    if (($this->getAttribute($context["entity"], "nbPlayer", array()) == $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"))) {
                        // line 399
                        echo "                                        <p class=\"etat complete\">";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.complet"), "html", null, true);
                        echo "</p>
                                    ";
                    } else {
                        // line 401
                        echo "                                        <p class=\"etat\">";
                        echo twig_escape_filter($this->env, ($this->getAttribute($context["entity"], "nbPlayer", array()) - $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array")), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.dispo"), "html", null, true);
                        echo "</p>
                                    ";
                    }
                    // line 403
                    echo "                                ";
                }
                // line 404
                echo "                            </div>
                        </div>
                        <div class=\"col-sm-3 info-date\">
                            <ul class=\"list-unstyled\">
                                <li><img src=\"";
                // line 408
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/date.png"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "startdate", array()), "d-m-Y"), "html", null, true);
                echo "</li>
                                <li class=\"lieu\"><img src=\"";
                // line 409
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "locality", array()), "html", null, true);
                echo " ( ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "country", array()), "html", null, true);
                echo ")</li>
                                <li><img src=\"";
                // line 410
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/heure.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "starttime", array()), "H:i"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "endtime", array()), "H:i"), "html", null, true);
                echo "</li>
                            </ul>
                            <a href=\"";
                // line 412
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($context["entity"], "slug", array()))), "html", null, true);
                echo "\" class=\"btn btn-grinta\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fiche.match"), "html", null, true);
                echo "</a>
                        </div>
                    </div>
                </div> 
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 416
            echo " 
            ";
        } else {
            // line 417
            echo " 
                <div class=\"msg-aucun\">";
            // line 418
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.sport.sug"), "html", null, true);
            echo "</div>
            ";
        }
        // line 420
        echo "        </div>
    </div>
  </div>
</div>
<!--end containt-->
";
        // line 425
        $this->displayBlock('javascripts', $context, $blocks);
    }

    public function block_javascripts($context, array $blocks = array())
    {
        // line 426
        echo "


    <script>
        \$(document).ready(function () {
            \$('#default').val(20);
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Event:listEvent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1925 => 426,  1919 => 425,  1912 => 420,  1907 => 418,  1904 => 417,  1900 => 416,  1887 => 412,  1876 => 410,  1868 => 409,  1862 => 408,  1856 => 404,  1853 => 403,  1845 => 401,  1839 => 399,  1837 => 398,  1830 => 397,  1824 => 395,  1822 => 394,  1817 => 392,  1807 => 387,  1803 => 385,  1797 => 383,  1790 => 381,  1788 => 380,  1783 => 378,  1778 => 375,  1773 => 374,  1771 => 373,  1759 => 372,  1750 => 366,  1743 => 362,  1734 => 356,  1729 => 354,  1724 => 351,  1713 => 349,  1709 => 348,  1699 => 341,  1695 => 340,  1686 => 336,  1680 => 335,  1674 => 334,  1668 => 333,  1662 => 332,  1656 => 331,  1650 => 330,  1644 => 329,  1638 => 328,  1632 => 327,  1626 => 326,  1620 => 325,  1614 => 324,  1608 => 323,  1602 => 322,  1596 => 321,  1590 => 320,  1584 => 319,  1578 => 318,  1572 => 317,  1566 => 316,  1560 => 315,  1554 => 314,  1548 => 313,  1542 => 312,  1536 => 311,  1530 => 310,  1524 => 309,  1518 => 308,  1512 => 307,  1506 => 306,  1500 => 305,  1494 => 304,  1488 => 303,  1482 => 302,  1476 => 301,  1470 => 300,  1464 => 299,  1458 => 298,  1452 => 297,  1446 => 296,  1440 => 295,  1434 => 294,  1428 => 293,  1422 => 292,  1416 => 291,  1410 => 290,  1404 => 289,  1398 => 288,  1392 => 287,  1386 => 286,  1380 => 285,  1374 => 284,  1368 => 283,  1362 => 282,  1356 => 281,  1350 => 280,  1344 => 279,  1338 => 278,  1332 => 277,  1326 => 276,  1320 => 275,  1314 => 274,  1308 => 273,  1302 => 272,  1296 => 271,  1290 => 270,  1284 => 269,  1278 => 268,  1272 => 267,  1266 => 266,  1260 => 265,  1254 => 264,  1248 => 263,  1242 => 262,  1236 => 261,  1230 => 260,  1224 => 259,  1218 => 258,  1212 => 257,  1206 => 256,  1200 => 255,  1194 => 254,  1188 => 253,  1182 => 252,  1176 => 251,  1170 => 250,  1164 => 249,  1158 => 248,  1152 => 247,  1146 => 246,  1140 => 245,  1134 => 244,  1128 => 243,  1122 => 242,  1116 => 241,  1110 => 240,  1104 => 239,  1098 => 238,  1092 => 237,  1086 => 236,  1080 => 235,  1074 => 234,  1068 => 233,  1062 => 232,  1056 => 231,  1050 => 230,  1044 => 229,  1038 => 228,  1032 => 227,  1026 => 226,  1020 => 225,  1014 => 224,  1008 => 223,  1002 => 222,  996 => 221,  990 => 220,  984 => 219,  978 => 218,  972 => 217,  966 => 216,  960 => 215,  954 => 214,  948 => 213,  942 => 212,  936 => 211,  930 => 210,  924 => 209,  918 => 208,  912 => 207,  906 => 206,  900 => 205,  894 => 204,  888 => 203,  882 => 202,  876 => 201,  870 => 200,  864 => 199,  858 => 198,  852 => 197,  846 => 196,  840 => 195,  834 => 194,  828 => 193,  822 => 192,  816 => 191,  810 => 190,  804 => 189,  798 => 188,  792 => 187,  786 => 186,  780 => 185,  774 => 184,  768 => 183,  762 => 182,  756 => 181,  750 => 180,  744 => 179,  738 => 178,  732 => 177,  726 => 176,  720 => 175,  714 => 174,  708 => 173,  702 => 172,  696 => 171,  690 => 170,  684 => 169,  678 => 168,  672 => 167,  666 => 166,  660 => 165,  654 => 164,  648 => 163,  642 => 162,  636 => 161,  630 => 160,  624 => 159,  618 => 158,  612 => 157,  606 => 156,  600 => 155,  594 => 154,  588 => 153,  582 => 152,  576 => 151,  570 => 150,  564 => 149,  558 => 148,  552 => 147,  546 => 146,  540 => 145,  534 => 144,  528 => 143,  522 => 142,  516 => 141,  510 => 140,  504 => 139,  498 => 138,  492 => 137,  486 => 136,  480 => 135,  474 => 134,  468 => 133,  462 => 132,  456 => 131,  450 => 130,  444 => 129,  438 => 128,  432 => 127,  426 => 126,  420 => 125,  414 => 124,  408 => 123,  402 => 122,  396 => 121,  390 => 120,  384 => 119,  378 => 118,  372 => 117,  366 => 116,  360 => 115,  354 => 114,  348 => 113,  342 => 112,  336 => 111,  330 => 110,  324 => 109,  318 => 108,  312 => 107,  306 => 106,  300 => 105,  294 => 104,  288 => 103,  282 => 102,  276 => 101,  270 => 100,  264 => 99,  258 => 98,  252 => 97,  246 => 96,  240 => 95,  234 => 94,  228 => 93,  222 => 92,  216 => 91,  210 => 90,  204 => 89,  198 => 88,  192 => 87,  186 => 86,  180 => 85,  175 => 83,  168 => 79,  162 => 76,  156 => 73,  145 => 65,  141 => 64,  132 => 58,  128 => 57,  121 => 53,  117 => 52,  111 => 49,  103 => 44,  99 => 43,  91 => 38,  87 => 37,  78 => 33,  74 => 32,  60 => 23,  56 => 22,  49 => 20,  37 => 13,  31 => 10,  20 => 1,);
    }
}
