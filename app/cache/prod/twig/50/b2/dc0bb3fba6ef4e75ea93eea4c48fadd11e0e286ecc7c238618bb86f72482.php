<?php

/* ComparatorEventBundle:Friends:countListFriend.html.twig */
class __TwigTemplate_50b2dc0bb3fba6ef4e75ea93eea4c48fadd11e0e286ecc7c238618bb86f72482 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a href=\"";
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\">
    <img src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/friends.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.amis"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, (isset($context["nbrFriends"]) ? $context["nbrFriends"] : null), "html", null, true);
        echo "</span>
</a>

";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Friends:countListFriend.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 2,  19 => 1,);
    }
}
