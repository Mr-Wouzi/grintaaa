<?php

/* ComparatorBaseBundle:Base:presentation.html.twig */
class __TwigTemplate_524bfb3062e8397a221cc3abfc289ec903a90459270af4e5d4d3441cdcae2d63 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"page-presentation\">
 <img src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/presentation.png"), "html", null, true);
        echo "\" />
 <div class=\"footer-pres\">
\t<div class=\"col-md-3 col-md-offset-1\"><img src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/cont-pres.png"), "html", null, true);
        echo "\" /></div>
\t<div class=\"col-md-4\"><img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/venir-pres.png"), "html", null, true);
        echo "\" /></div>
\t<div class=\"col-md-3\">
\t\t<img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/ref-pres.png"), "html", null, true);
        echo "\" /><br>
\t\t<a href=\"https://www.facebook.com/Grintaaa-944054715632395/?fref=ts\" target=\"_blanc\"><img src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fb-pres.png"), "html", null, true);
        echo "\" /></a><br>
\t\t<a href=\"https://twitter.com/@Grintaaa_\" target=\"_blanc\"><img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/tw-pres.png"), "html", null, true);
        echo "\" /></a>
\t</div>
 </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "ComparatorBaseBundle:Base:presentation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 9,  40 => 8,  36 => 7,  31 => 5,  27 => 4,  22 => 2,  19 => 1,);
    }
}
