<?php

/* ComparatorEventBundle:Friends:index.html.twig */
class __TwigTemplate_533051418ee45b5c4c881c46df7f2b604e01488c5ab0c89c80173080b824582a extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt-->
<div class=\"container containt-dash\">
    <!--Debut left-block-->
    <div class=\"col-md-2 no-padding-right\">
        <div class=\"block profil-block\">
            <div class=\"profil-dash\">
                <div class=\"col-md-12 no-padding\">
                    <div class=\"grid\">
                        <figure class=\"effect-winston\">
                            ";
        // line 10
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
                            <figcaption>
                                <p>
                                    <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\" title=\"defi sportif entre amis\"><i class=\"fa fa-fw\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
                                </p>
                            </figcaption>
                        </figure>
                    </div>  
                </div>
                <div class=\"col-md-12 no-padding\">
                    <p class=\"titre\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>

                    <p class=\"titre-user\">      
\t\t\t\t\t\t<a href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t<img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t</a>
\t\t\t\t\t</p>  
                    <!--p class=\"pro\">Profil</p-->
                </div>

            </div>  
            <ul class=\"list-unstyled\">
                <li>
                    <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" title=\"defi sportif entre amis\">
                        <img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\" alt=\"defi sportif entre amis\" /> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                    </a>
                </li>
                <li>
\t\t\t\t<a href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\" title=\"defi sportif entre amis\">
                        ";
        // line 39
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                    </a>
                </li>

                <li>
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\">
                        ";
        // line 45
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                    </a>
                </li>
                <li>
                    ";
        // line 50
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                </li>
                <li>
                    <a href=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\">
                        ";
        // line 54
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                    </a>
                </li>   
                <li>
                    <a href=\"";
        // line 58
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\">
                        ";
        // line 59
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                    </a>
                </li>
            </ul>
        </div>

        ";
        // line 65
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
        ";
        // line 66
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

    <br><br>
    </div>

    <div class=\"col-md-10\">
    <div class=\" block-list-friends\">
        <div class=\"formulaire-search col-md-10\">
            <div class=\"img-search\">
                <img src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/users.png"), "html", null, true);
        echo "\" alt=\"defi sportif entre amis\" />
            </div>
            <div class=\"title-search\">
                <h4 class=\"green\">";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("liste.of.invi.act"), "html", null, true);
        echo "</h4>
            </div>
        </div>
        <div class=\"result-search\">

            ";
        // line 83
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
            // line 84
            echo "                <div class=\"one-search col-md-6\">
                    <div class=\"col-md-12 cadre-ami no-padding\">
\t\t\t\t\t  <a href=\"";
            // line 86
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
            echo "\" title=\"defi sportif entre amis\">
                        <div class=\"img-search2\">
                            ";
            // line 88
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                // line 89
                echo "                                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                echo "\" alt=\"defi sportif entre amis\">
                            ";
            } else {
                // line 91
                echo "                                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                echo "\" alt=\"defi sportif entre amis\">
                            ";
            }
            // line 93
            echo "                        </div>    
\t\t\t\t\t\t </a>
                        <div class=\"info-date2\">
                            <h2 class=\"green\">";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
            echo " ";
            if (($this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "coach", array()) == 1)) {
                echo "(";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.u.coach"), "html", null, true);
                echo ")";
            }
            echo "</h2>
                            <ul class=\"list-unstyled\">
                                <li class=\"lieu\"><img
                                            src=\"";
            // line 99
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
            echo "\" alt=\"defi sportif entre amis\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "city", array()), "html", null, true);
            echo "
                                    ( ";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "country", array()), "html", null, true);
            echo ")
                                </li>   
                                <li class=\"star\"><img src=\"";
            // line 102
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/star.png"), "html", null, true);
            echo "\" alt=\"defi sportif entre amis\">
                                    Centre d’interêt :
\t\t\t\t\t\t\t\t\t";
            // line 104
            $context["xxxx"] = 0;
            // line 105
            echo "                                    ";
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["interest"]) ? $context["interest"] : null), $context["key"], array(), "array")) > 0)) {
                // line 106
                echo "                                        ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["interest"]) ? $context["interest"] : null), $context["key"], array(), "array"));
                foreach ($context['_seq'] as $context["keys"] => $context["interested"]) {
                    // line 107
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    if (((isset($context["xxxx"]) ? $context["xxxx"] : null) < 6)) {
                        // line 108
                        echo "                                            * ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["interested"], "category", array()), "title", array()), "html", null, true);
                        echo "
\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 110
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    $context["xxxx"] = ((isset($context["xxxx"]) ? $context["xxxx"] : null) + 1);
                    // line 111
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['keys'], $context['interested'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 112
                echo "
                                    ";
            }
            // line 114
            echo "                                </li>
                                ";
            // line 115
            if (($this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "coach", array()) == 1)) {
                // line 116
                echo "                                    <li class=\"star\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/star.png"), "html", null, true);
                echo "\" alt=\"defi sportif entre amis\">
                                        ";
                // line 117
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.coaching"), "html", null, true);
                echo " :
                                        ";
                // line 118
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["coaching"]) ? $context["coaching"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 119
                    echo "                                            ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["coaching"]) ? $context["coaching"] : null), $context["key"], array(), "array"));
                    foreach ($context['_seq'] as $context["keys"] => $context["coachinged"]) {
                        // line 120
                        echo "                                                * ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["coachinged"], "category", array()), "title", array()), "html", null, true);
                        echo "
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['keys'], $context['coachinged'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 122
                    echo "
                                        ";
                }
                // line 124
                echo "                                    </li>
                                ";
            }
            // line 126
            echo "
                            </ul>
                        </div>
                        <div class=\"info-date3\">
                            <div class=\" action-search\">

                                <form id=\"validate_field_types\" method=\"POST\"
                                      action=\"";
            // line 133
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_update", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">
                                    <input type=\"submit\" class=\"btn btn-grinta\" value=\"";
            // line 134
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("button.accept"), "html", null, true);
            echo "\"/>

                                </form>
                                <form id=\"validate_field_types\" method=\"POST\"
                                      action=\"";
            // line 138
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">
                                    <input type=\"submit\" class=\"btn btn-refus btn-refus-invit\" value=\"";
            // line 139
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("button.refus"), "html", null, true);
            echo "\"/>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "
        </div>
    </div>
 </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Friends:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  342 => 147,  328 => 139,  324 => 138,  317 => 134,  313 => 133,  304 => 126,  300 => 124,  296 => 122,  287 => 120,  282 => 119,  280 => 118,  276 => 117,  271 => 116,  269 => 115,  266 => 114,  262 => 112,  256 => 111,  253 => 110,  247 => 108,  244 => 107,  239 => 106,  236 => 105,  234 => 104,  229 => 102,  224 => 100,  218 => 99,  204 => 96,  199 => 93,  193 => 91,  186 => 89,  184 => 88,  179 => 86,  175 => 84,  171 => 83,  163 => 78,  157 => 75,  145 => 66,  141 => 65,  132 => 59,  128 => 58,  121 => 54,  117 => 53,  111 => 50,  103 => 45,  99 => 44,  91 => 39,  87 => 38,  78 => 34,  74 => 33,  60 => 24,  56 => 23,  48 => 20,  36 => 13,  30 => 10,  19 => 1,);
    }
}
