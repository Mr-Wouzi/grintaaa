<?php

/* ComparatorEventBundle:Mur:murEventFriends.html.twig */
class __TwigTemplate_5e1e9ceaf3b07b732ea64c6647b766e84956dcc39efaad4c2e514be9e6723b9d extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
            // line 3
            echo "    <div id=\"test\">
        <ul>

            ";
            // line 6
            if (($this->getAttribute($context["entity"], "type", array()) == "newevent")) {
                // line 7
                echo "
                <li id=\"nbr\">
                    <div class=\"block item-act\" id=\"";
                // line 9
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\">

                        <div class=\"row\">
                            <div class=\"col-sm-1\">
                                <a href=\"";
                // line 13
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 15
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 16
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                } else {
                    // line 18
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                }
                // line 20
                echo "                                    </div>
                                </a>
                            </div>
                            <div class=\"col-sm-11\">
                                <a href=\"";
                // line 24
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"name-avatar-mur\">
                                        <h5>";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                    </div>
                                </a>
                                <div class=\"date-mur size-date\">
                                    ";
                // line 30
                if ($this->getAttribute($context["entity"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "d-m-Y H:i:s"), "html", null, true);
                }
                // line 31
                echo "                                </div>
                            </div>
                            <div class=\"col-sm-12\">
                                <div class=\"notif-mur\">
                                    <h5>";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mur.create.event"), "html", null, true);
                echo ":</h5>
                                </div>
                            </div>

                            <div class=\"col-sm-12 no-padding notif-mur\">
                                <div class=\"col-sm-4\">
                                    <h5><strong class=\"green\">";
                // line 41
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "title", array()), "html", null, true);
                echo "</strong></h5>
                                    <img src=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\"> ";
                if (($this->getAttribute($this->getAttribute($context["entity"], "event", array()), "locality", array()) != "false")) {
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "locality", array()), "html", null, true);
                    echo " (";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "country", array()), "html", null, true);
                    echo ") ";
                } else {
                    echo "  ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "country", array()), "html", null, true);
                    echo " ";
                }
                // line 43
                echo "                                </div>
                                <div class=\"col-sm-4\">
                                    <div class=\"nb-participant\">
                                        ";
                // line 46
                if ((null === $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "nbPlayer", array()))) {
                    // line 47
                    echo "                                            <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo " participant</p>
                                        ";
                } else {
                    // line 49
                    echo "                                            <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "nbPlayer", array()), "html", null, true);
                    echo "</p>
                                            ";
                    // line 50
                    if (($this->getAttribute($this->getAttribute($context["entity"], "event", array()), "nbPlayer", array()) == $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"))) {
                        // line 51
                        echo "                                                <p class=\"etat complete\">";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profil.complet"), "html", null, true);
                        echo "</p>
                                            ";
                    } else {
                        // line 53
                        echo "                                                <p class=\"etat\">";
                        echo twig_escape_filter($this->env, ($this->getAttribute($this->getAttribute($context["entity"], "event", array()), "nbPlayer", array()) - $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array")), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.dispo"), "html", null, true);
                        echo "</p>
                                            ";
                    }
                    // line 55
                    echo "                                        ";
                }
                // line 56
                echo "                                    </div>
                                </div>
                                <div class=\"col-sm-4\" align=\"center\">
                                    <div class=\"time-participant size-date\">
                                        <img src=\"";
                // line 60
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/heure.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\"> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "starttime", array()), "H:i"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "endtime", array()), "H:i"), "html", null, true);
                echo " / ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "startdate", array()), "d-m-Y"), "html", null, true);
                echo "
                                    </div>
                                    <div>
                                        <a href=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "slug", array()))), "html", null, true);
                echo "\" class=\"btn btn-grinta\" title=\"reseau social sport\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fiche.match"), "html", null, true);
                echo "</a>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class=\"desc-info\">

                            <div class=\"nbre-peronne-aime\">
                                <a id=\"newevent";
                // line 73
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aime\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                <a id=\"disnewevent";
                // line 74
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aimepas\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                - <span id=\"newevent1";
                // line 75
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"> </span> personne(s) aime ça
                            </div>

                        </div>


                    </div>
                </li>
                <script>
                    \$(document).ready(function () {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
                // line 87
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                            data: \"\",
                            success: function(data){
                                if(data<1)
                                {
                                    jQuery('#disnewevent";
                // line 92
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');

                                }

                                document.getElementById(\"newevent1";
                // line 96
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;





                            }
                        });

                        \$(\"#disnewevent";
                // line 105
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 109
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){
                                    \$.ajax({
                                        type: \"POST\",
                                        url: \"";
                // line 114
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                        data: \"\",
                                        success: function(data){


                                            document.getElementById(\"newevent1";
                // line 119
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                            jQuery('#disnewevent";
                // line 121
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');



                                        }
                                    });


                                }
                            });



                        });



                        \$(\"#newevent";
                // line 138
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 142
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){

                                    if(data<1)
                                    {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: \"";
                // line 150
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                            data: \"\",
                                            success: function(data){
                                                \$.ajax({
                                                    type: \"POST\",
                                                    url: \"";
                // line 155
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                                    data: \"\",
                                                    success: function(data){


                                                        document.getElementById(\"newevent1";
                // line 160
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                                        jQuery('#disnewevent";
                // line 162
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','initial');



                                                    }
                                                });

                                            }
                                        });


                                    }

                                }
                            });


                        });


                    });
                </script>
                ";
            } elseif (($this->getAttribute(            // line 184
$context["entity"], "type", array()) == "statutmur")) {
                // line 185
                echo "                <li id=\"nbr\">
                    <div class=\"block item-act\" id=\"";
                // line 186
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\">

                        <div class=\"row\">
                            <div class=\"col-sm-1\">
                                <a href=\"";
                // line 190
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 192
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 193
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                } else {
                    // line 195
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                }
                // line 197
                echo "                                    </div>
                                </a>
                            </div>
                            <div class=\"col-sm-11\">
                                <a href=\"";
                // line 201
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"name-avatar-mur\">
                                        <h5>";
                // line 203
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                    </div>
                                </a>
                                <div class=\"date-mur size-date\">
                                    ";
                // line 207
                if ($this->getAttribute($context["entity"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "d-m-Y H:i:s"), "html", null, true);
                }
                // line 208
                echo "                                </div>
                            </div>
                            <div class=\"col-sm-12\">
                                <div class=\"desc-mur notif-mur\">
                                    ";
                // line 212
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "statut", array()), "descreption", array()), "html", null, true);
                echo "
                                </div>
                            </div>
                        </div>
                        <div class=\"desc-info\">

                            <div class=\"nbre-peronne-aime\">
                                <a id=\"statutmur";
                // line 219
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aime\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                <a id=\"disstatutmur";
                // line 220
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                - <span id=\"statutmur1";
                // line 221
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"> </span> personne(s) aime ça
                            </div>


                        </div>
                    </div>
                </li>
                <script>
                    \$(document).ready(function () {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
                // line 232
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                            data: \"\",
                            success: function(data){
                                if(data<1)
                                {
                                    jQuery('#disstatutmur";
                // line 237
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');

                                }

                                document.getElementById(\"statutmur1";
                // line 241
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;





                            }
                        });

                        \$(\"#disstatutmur";
                // line 250
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 254
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){
                                    \$.ajax({
                                        type: \"POST\",
                                        url: \"";
                // line 259
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                        data: \"\",
                                        success: function(data){


                                            document.getElementById(\"statutmur1";
                // line 264
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                            jQuery('#disstatutmur";
                // line 266
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');



                                        }
                                    });


                                }
                            });



                        });



                        \$(\"#statutmur";
                // line 283
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 287
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){

                                    if(data<1)
                                    {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: \"";
                // line 295
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                            data: \"\",
                                            success: function(data){
                                                \$.ajax({
                                                    type: \"POST\",
                                                    url: \"";
                // line 300
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                                    data: \"\",
                                                    success: function(data){


                                                        document.getElementById(\"statutmur1";
                // line 305
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                                        jQuery('#disstatutmur";
                // line 307
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','initial');



                                                    }
                                                });

                                            }
                                        });


                                    }

                                }
                            });


                        });


                    });
                </script>
                ";
            } elseif (($this->getAttribute(            // line 329
$context["entity"], "type", array()) == "imagemur")) {
                // line 330
                echo "                <li id=\"nbr\">
                    <div class=\"block item-act\" id=\"";
                // line 331
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\">

                        <div class=\"row\">
                            <div class=\"col-sm-1\">
                                <a href=\"";
                // line 335
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 337
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 338
                    echo "
                                            <img  src=\"";
                    // line 339
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                } else {
                    // line 341
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                }
                // line 343
                echo "                                    </div>
                                </a>
                            </div>
                            <div class=\"col-sm-11\">
                                <a href=\"";
                // line 347
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"name-avatar-mur\">
                                        <h5>";
                // line 349
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                    </div>
                                </a>
                                <div class=\"date-mur size-date\">
                                    ";
                // line 353
                if ($this->getAttribute($context["entity"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "d-m-Y H:i:s"), "html", null, true);
                }
                // line 354
                echo "                                </div>
                            </div>
                            <div class=\"col-sm-12\">
                                <div class=\"img-mur\">
                                    <img  src=\"";
                // line 358
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/murs/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "statut", array()), "logo", array()), "html", null, true);
                echo "\" alt=\"reseau social sport\">
                                </div>
                            </div>


                        </div>
                        <div class=\"desc-info\">

                            <div class=\"nbre-peronne-aime\">
                                <a id=\"imagemur";
                // line 367
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aime\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                <a id=\"disimagemur";
                // line 368
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                - <span id=\"imagemur1";
                // line 369
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"> </span> personne(s) aime ça
                            </div>


                        </div>

                    </div>
                </li>
                <script>
                    \$(document).ready(function () {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
                // line 381
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                            data: \"\",
                            success: function(data){
                                if(data<1)
                                {
                                    jQuery('#disimagemur";
                // line 386
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');

                                }

                                document.getElementById(\"imagemur1";
                // line 390
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;





                            }
                        });

                        \$(\"#disimagemur";
                // line 399
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 403
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){
                                    \$.ajax({
                                        type: \"POST\",
                                        url: \"";
                // line 408
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                        data: \"\",
                                        success: function(data){


                                            document.getElementById(\"imagemur1";
                // line 413
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                            jQuery('#disimagemur";
                // line 415
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');



                                        }
                                    });


                                }
                            });



                        });



                        \$(\"#imagemur";
                // line 432
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 436
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){

                                    if(data<1)
                                    {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: \"";
                // line 444
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                            data: \"\",
                                            success: function(data){
                                                \$.ajax({
                                                    type: \"POST\",
                                                    url: \"";
                // line 449
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                                    data: \"\",
                                                    success: function(data){


                                                        document.getElementById(\"imagemur1";
                // line 454
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                                        jQuery('#disimagemur";
                // line 456
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','initial');



                                                    }
                                                });

                                            }
                                        });


                                    }

                                }
                            });


                        });


                    });
                </script>
                ";
            } elseif (($this->getAttribute(            // line 478
$context["entity"], "type", array()) == "videomur")) {
                // line 479
                echo "                <li id=\"nbr\">
                    <div class=\"block item-act\" id=\"";
                // line 480
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\">

                        <div class=\"row\">
                            <div class=\"col-sm-1\">
                                <a href=\"";
                // line 484
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 486
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 487
                    echo "
                                            <img  src=\"";
                    // line 488
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                } else {
                    // line 490
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                }
                // line 492
                echo "                                    </div>
                                </a>
                            </div>
                            <div class=\"col-sm-11\">
                                <a href=\"";
                // line 496
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"name-avatar-mur\">
                                        <h5>";
                // line 498
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                    </div>
                                </a>
                                <div class=\"date-mur size-date\">
                                    ";
                // line 502
                if ($this->getAttribute($context["entity"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "d-m-Y H:i:s"), "html", null, true);
                }
                // line 503
                echo "                                </div>
                            </div>
                            <div class=\"col-sm-12\">
                                <div class=\"img-mur\">
                                    <iframe width=\"470\" height=\"265\"
                                            src=\"http://www.youtube.com/embed/";
                // line 508
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "statut", array()), "url", array()), "html", null, true);
                echo "\">
                                    </iframe>
                                </div>
                            </div>

                        </div>
                        <div class=\"desc-info\">

                            <div class=\"nbre-peronne-aime\">
                                <a id=\"videomur";
                // line 517
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aime\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                <a id=\"disvideomur";
                // line 518
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                - <span id=\"videomur1";
                // line 519
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"> </span> personne(s) aime ça
                            </div>

                        </div>

                    </div>
                </li>
                <script>
                    \$(document).ready(function () {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
                // line 530
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                            data: \"\",
                            success: function(data){
                                if(data<1)
                                {
                                    jQuery('#disvideomur";
                // line 535
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');

                                }

                                document.getElementById(\"videomur1";
                // line 539
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;





                            }
                        });

                        \$(\"#disvideomur";
                // line 548
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 552
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){
                                    \$.ajax({
                                        type: \"POST\",
                                        url: \"";
                // line 557
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                        data: \"\",
                                        success: function(data){


                                            document.getElementById(\"videomur1";
                // line 562
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                            jQuery('#disvideomur";
                // line 564
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');



                                        }
                                    });


                                }
                            });



                        });



                        \$(\"#videomur";
                // line 581
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 585
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){

                                    if(data<1)
                                    {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: \"";
                // line 593
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                            data: \"\",
                                            success: function(data){
                                                \$.ajax({
                                                    type: \"POST\",
                                                    url: \"";
                // line 598
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                                    data: \"\",
                                                    success: function(data){


                                                        document.getElementById(\"videomur1";
                // line 603
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                                        jQuery('#disvideomur";
                // line 605
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','initial');



                                                    }
                                                });

                                            }
                                        });


                                    }

                                }
                            });


                        });


                    });
                </script>
                ";
            } elseif (($this->getAttribute(            // line 627
$context["entity"], "type", array()) == "statutevent")) {
                // line 628
                echo "                <li id=\"nbr\">
                    <div class=\"block item-act\" id=\"";
                // line 629
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\">


                        <div class=\"row\">
                            <div class=\"col-sm-1\">
                                <a href=\"";
                // line 634
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 636
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 637
                    echo "
                                            <img  src=\"";
                    // line 638
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                } else {
                    // line 640
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                }
                // line 642
                echo "                                    </div>
                                </a>
                            </div>
                            <div class=\"col-sm-11\">
                                <div class=\"name-avatar-mur\">
                                    <h5>";
                // line 647
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                </div>
                                <div class=\"date-mur size-date\">
                                    ";
                // line 650
                if ($this->getAttribute($context["entity"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "d-m-Y H:i:s"), "html", null, true);
                }
                // line 651
                echo "                                </div>
                            </div>
                            <div class=\"col-sm-12\">
                                <div class=\"notif-mur\">
                                    <h5>";
                // line 655
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pub.stat.event"), "html", null, true);
                echo " <strong class=\"green\"><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "slug", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "title", array()), "html", null, true);
                echo "</a></strong></h5>
                                </div>
                                <div class=\"desc-mur\">
                                    <h5> ";
                // line 658
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "file", array()), "descreption", array()), "html", null, true);
                echo "</h5>
                                </div>
                            </div>

                        </div>
                        <div class=\"desc-info\">

                            <div class=\"nbre-peronne-aime\">
                                <a id=\"statutevent";
                // line 666
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aime\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                <a id=\"disstatutevent";
                // line 667
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                - <span id=\"statutevent1";
                // line 668
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"> </span> personne(s) aime ça
                            </div>

                        </div>
                    </div>
                </li>
                <script>
                    \$(document).ready(function () {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
                // line 678
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                            data: \"\",
                            success: function(data){
                                if(data<1)
                                {
                                    jQuery('#disstatutevent";
                // line 683
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');

                                }

                                document.getElementById(\"statutevent1";
                // line 687
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;





                            }
                        });

                        \$(\"#disstatutevent";
                // line 696
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 700
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){
                                    \$.ajax({
                                        type: \"POST\",
                                        url: \"";
                // line 705
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                        data: \"\",
                                        success: function(data){


                                            document.getElementById(\"statutevent1";
                // line 710
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                            jQuery('#disstatutevent";
                // line 712
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');



                                        }
                                    });


                                }
                            });



                        });



                        \$(\"#statutevent";
                // line 729
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 733
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){

                                    if(data<1)
                                    {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: \"";
                // line 741
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                            data: \"\",
                                            success: function(data){
                                                \$.ajax({
                                                    type: \"POST\",
                                                    url: \"";
                // line 746
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                                    data: \"\",
                                                    success: function(data){


                                                        document.getElementById(\"statutevent1";
                // line 751
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                                        jQuery('#disstatutevent";
                // line 753
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','initial');



                                                    }
                                                });

                                            }
                                        });


                                    }

                                }
                            });


                        });


                    });
                </script>
                ";
            } elseif (($this->getAttribute(            // line 775
$context["entity"], "type", array()) == "imageevent")) {
                // line 776
                echo "                <li id=\"nbr\">
                    <div class=\"block item-act\" id=\"";
                // line 777
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\">

                        <div class=\"row\">
                            <div class=\"col-sm-1\">
                                <a href=\"";
                // line 781
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 783
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 784
                    echo "
                                            <img  src=\"";
                    // line 785
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                } else {
                    // line 787
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                }
                // line 789
                echo "                                    </div>
                                </a>
                            </div>
                            <div class=\"col-sm-11\">
                                <a href=\"";
                // line 793
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"name-avatar-mur\">
                                        <h5>";
                // line 795
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                    </div>

                                </a>
                                <div class=\"date-mur size-date\">
                                    ";
                // line 800
                if ($this->getAttribute($context["entity"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "d-m-Y H:i:s"), "html", null, true);
                }
                // line 801
                echo "                                </div>
                            </div>
                            <div class=\"col-sm-12\">
                                <div class=\"notif-mur\">
                                    <h5>";
                // line 805
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pub.img.event"), "html", null, true);
                echo " <strong class=\"green\"><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "slug", array()))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "title", array()), "html", null, true);
                echo "</a></strong></h5>
                                </div>
                                <div class=\"imgevent-mur\">
                                    <img  src=\"";
                // line 808
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/events/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "file", array()), "logo", array()), "html", null, true);
                echo "\" alt=\"reseau social sport\">
                                </div>

                            </div>

                        </div>
                        <div class=\"desc-info\">

                            <div class=\"nbre-peronne-aime\">
                                <a id=\"imageevent";
                // line 817
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aime\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                <a id=\"disimageevent";
                // line 818
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                - <span id=\"imageevent1";
                // line 819
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"> </span> personne(s) aime ça
                            </div>

                        </div>

                    </div>
                </li>
                <script>
                    \$(document).ready(function () {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
                // line 830
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                            data: \"\",
                            success: function(data){
                                if(data<1)
                                {
                                    jQuery('#disimageevent";
                // line 835
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');

                                }

                                document.getElementById(\"imageevent1";
                // line 839
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;





                            }
                        });

                        \$(\"#disimageevent";
                // line 848
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 852
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){
                                    \$.ajax({
                                        type: \"POST\",
                                        url: \"";
                // line 857
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                        data: \"\",
                                        success: function(data){


                                            document.getElementById(\"imageevent1";
                // line 862
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                            jQuery('#disimageevent";
                // line 864
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');



                                        }
                                    });


                                }
                            });



                        });



                        \$(\"#imageevent";
                // line 881
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 885
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){

                                    if(data<1)
                                    {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: \"";
                // line 893
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                            data: \"\",
                                            success: function(data){
                                                \$.ajax({
                                                    type: \"POST\",
                                                    url: \"";
                // line 898
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                                    data: \"\",
                                                    success: function(data){


                                                        document.getElementById(\"imageevent1";
                // line 903
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                                        jQuery('#disimageevent";
                // line 905
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','initial');



                                                    }
                                                });

                                            }
                                        });


                                    }

                                }
                            });


                        });


                    });
                </script>
                ";
            } elseif (($this->getAttribute(            // line 927
$context["entity"], "type", array()) == "videoevent")) {
                // line 928
                echo "                <li id=\"nbr\">
                    <div class=\"block item-act\" id=\"";
                // line 929
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\">

                        <div class=\"row\">
                            <div class=\"col-sm-1\">
                                <a href=\"";
                // line 933
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 935
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 936
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                } else {
                    // line 938
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                }
                // line 940
                echo "                                    </div>
                                </a>
                            </div>
                            <div class=\"col-sm-11\">
                                <a href=\"";
                // line 944
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"name-avatar-mur\">
                                        <h5>";
                // line 946
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                    </div>
                                </a>
                                <div class=\"date-mur size-date\">
                                    ";
                // line 950
                if ($this->getAttribute($context["entity"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "d-m-Y H:i:s"), "html", null, true);
                }
                // line 951
                echo "                                </div>
                            </div>
                            <div class=\"col-sm-12\">
                                <div class=\"notif-mur\">
                                    <h5>";
                // line 955
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pub.video.event"), "html", null, true);
                echo " <strong class=\"green\"><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "slug", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "title", array()), "html", null, true);
                echo "</a></strong></h5>
                                </div>
                                <div class=\"video-mur\">
                                    <iframe width=\"470\" height=\"265\"
                                            src=\"http://www.youtube.com/embed/";
                // line 959
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "file", array()), "url", array()), "html", null, true);
                echo "\">
                                    </iframe>
                                </div>
                            </div>

                        </div>
                        <div class=\"desc-info\">

                            <div class=\"nbre-peronne-aime\">
                                <a id=\"videoevent";
                // line 968
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aime\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                <a id=\"disvideoevent";
                // line 969
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                - <span id=\"videoevent1";
                // line 970
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"> </span> personne(s) aime ça
                            </div>

                        </div>

                    </div>
                </li>
                <script>
                    \$(document).ready(function () {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
                // line 981
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                            data: \"\",
                            success: function(data){
                                if(data<1)
                                {
                                    jQuery('#disvideoevent";
                // line 986
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');

                                }

                                document.getElementById(\"videoevent1";
                // line 990
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;





                            }
                        });

                        \$(\"#disvideoevent";
                // line 999
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 1003
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){
                                    \$.ajax({
                                        type: \"POST\",
                                        url: \"";
                // line 1008
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                        data: \"\",
                                        success: function(data){


                                            document.getElementById(\"videoevent1";
                // line 1013
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                            jQuery('#disvideoevent";
                // line 1015
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');



                                        }
                                    });


                                }
                            });



                        });



                        \$(\"#videoevent";
                // line 1032
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 1036
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){

                                    if(data<1)
                                    {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: \"";
                // line 1044
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                            data: \"\",
                                            success: function(data){
                                                \$.ajax({
                                                    type: \"POST\",
                                                    url: \"";
                // line 1049
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                                    data: \"\",
                                                    success: function(data){


                                                        document.getElementById(\"videoevent1";
                // line 1054
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                                        jQuery('#disvideoevent";
                // line 1056
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','initial');



                                                    }
                                                });

                                            }
                                        });


                                    }

                                }
                            });


                        });


                    });
                </script>
                ";
            } elseif (($this->getAttribute(            // line 1078
$context["entity"], "type", array()) == "commentevent")) {
                // line 1079
                echo "                <li id=\"nbr\">
                    <div class=\"block item-act\" id=\"";
                // line 1080
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\">

                        <div class=\"row\">
                            <div class=\"col-sm-1\">
                                <a href=\"";
                // line 1084
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 1086
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 1087
                    echo "
                                            <img  src=\"";
                    // line 1088
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                } else {
                    // line 1090
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                }
                // line 1092
                echo "                                    </div>
                                </a>
                            </div>
                            <div class=\"col-sm-11\">
                                <a href=\"";
                // line 1096
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"name-avatar-mur\">
                                        <h5>";
                // line 1098
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                    </div>
                                </a>
                                <div class=\"date-mur size-date\">
                                    ";
                // line 1102
                if ($this->getAttribute($context["entity"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "d-m-Y H:i:s"), "html", null, true);
                }
                // line 1103
                echo "                                </div>
                            </div>
                            <div class=\"col-sm-12\">
                                <div class=\"notif-mur\">
                                    <h5>";
                // line 1107
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("comment.comments.event"), "html", null, true);
                echo " <strong class=\"green\"><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "slug", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\" >";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "title", array()), "html", null, true);
                echo "</a></strong></h5>
                                </div>
                                <div class=\"desc-mur\">
                                    <h5>";
                // line 1110
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "comment", array()), "description", array()), "html", null, true);
                echo "</h5>
                                </div>
                            </div>

                        </div>
                        <div class=\"desc-info\">

                            <div class=\"nbre-peronne-aime\">
                                <a id=\"commentevent";
                // line 1118
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aime\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                <a id=\"discommentevent";
                // line 1119
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                - <span id=\"commentevent1";
                // line 1120
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"> </span> personne(s) aime ça
                            </div>

                        </div>

                    </div>
                </li>
                <script>
                    \$(document).ready(function () {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
                // line 1131
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                            data: \"\",
                            success: function(data){
                                if(data<1)
                                {
                                    jQuery('#discommentevent";
                // line 1136
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');

                                }

                                document.getElementById(\"commentevent1";
                // line 1140
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;





                            }
                        });

                        \$(\"#discommentevent";
                // line 1149
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 1153
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){
                                    \$.ajax({
                                        type: \"POST\",
                                        url: \"";
                // line 1158
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                        data: \"\",
                                        success: function(data){


                                            document.getElementById(\"commentevent1";
                // line 1163
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                            jQuery('#discommentevent";
                // line 1165
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');



                                        }
                                    });


                                }
                            });



                        });



                        \$(\"#commentevent";
                // line 1182
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 1186
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){

                                    if(data<1)
                                    {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: \"";
                // line 1194
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                            data: \"\",
                                            success: function(data){
                                                \$.ajax({
                                                    type: \"POST\",
                                                    url: \"";
                // line 1199
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                                    data: \"\",
                                                    success: function(data){


                                                        document.getElementById(\"commentevent1";
                // line 1204
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                                        jQuery('#discommentevent";
                // line 1206
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','initial');



                                                    }
                                                });

                                            }
                                        });


                                    }

                                }
                            });


                        });


                    });
                </script>
                ";
            } elseif (($this->getAttribute(            // line 1228
$context["entity"], "type", array()) == "participeevent")) {
                // line 1229
                echo "                <li id=\"nbr\">
                    <div class=\"block item-act\" id=\"";
                // line 1230
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\">


                        <div class=\"row\">
                            <div class=\"col-sm-1\">
                                <a href=\"";
                // line 1235
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 1237
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 1238
                    echo "
                                            <img  src=\"";
                    // line 1239
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                } else {
                    // line 1241
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"reseau social sport\">
                                        ";
                }
                // line 1243
                echo "                                    </div>
                                </a>
                            </div>
                            <div class=\"col-sm-11\">
                                <a href=\"";
                // line 1247
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">
                                    <div class=\"name-avatar-mur\">
                                        <h5>";
                // line 1249
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                    </div>
                                </a>
                                <div class=\"date-mur size-date\">
                                    ";
                // line 1253
                if ($this->getAttribute($context["entity"], "createdAt", array())) {
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "d-m-Y H:i:s"), "html", null, true);
                }
                // line 1254
                echo "                                </div>
                            </div>
                            <div class=\"col-sm-12\">
                                <div class=\"notif-mur\">
                                    <h5>";
                // line 1258
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("participe.participe.event"), "html", null, true);
                echo " <strong class=\"green\"><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "slug", array()))), "html", null, true);
                echo "\" title=\"reseau social sport\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "title", array()), "html", null, true);
                echo "</a></strong></h5>
                                </div>
                            </div>

                        </div>
                        <div class=\"desc-info\">

                            <div class=\"nbre-peronne-aime\">
                                <a id=\"participeevent";
                // line 1266
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\" class=\"aime\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                <a id=\"disparticipeevent";
                // line 1267
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
                echo "\" alt=\"reseau social sport\" /> J'aime</a>
                                - <span id=\"participeevent1";
                // line 1268
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\"> </span> personne(s) aime ça
                            </div>

                        </div>

                    </div>
                </li>
                <script>
                    \$(document).ready(function () {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
                // line 1279
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                            data: \"\",
                            success: function(data){
                                if(data<1)
                                {
                                    jQuery('#disparticipeevent";
                // line 1284
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');

                                }

                                document.getElementById(\"participeevent1";
                // line 1288
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;





                            }
                        });

                        \$(\"#disparticipeevent";
                // line 1297
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 1301
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){
                                    \$.ajax({
                                        type: \"POST\",
                                        url: \"";
                // line 1306
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                        data: \"\",
                                        success: function(data){


                                            document.getElementById(\"participeevent1";
                // line 1311
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                            jQuery('#disparticipeevent";
                // line 1313
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','none');



                                        }
                                    });


                                }
                            });



                        });



                        \$(\"#participeevent";
                // line 1330
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").click(function () {

                            \$.ajax({
                                type: \"POST\",
                                url: \"";
                // line 1334
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                data: \"\",
                                success: function(data){

                                    if(data<1)
                                    {
                                        \$.ajax({
                                            type: \"POST\",
                                            url: \"";
                // line 1342
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                            data: \"\",
                                            success: function(data){
                                                \$.ajax({
                                                    type: \"POST\",
                                                    url: \"";
                // line 1347
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("likem", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\",
                                                    data: \"\",
                                                    success: function(data){


                                                        document.getElementById(\"participeevent1";
                // line 1352
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "\").innerHTML = data;

                                                        jQuery('#disparticipeevent";
                // line 1354
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
                echo "').css('display','initial');



                                                    }
                                                });

                                            }
                                        });


                                    }

                                }
                            });


                        });


                    });
                </script>
            ";
            }
            // line 1377
            echo "        </ul>
    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1381
        echo "
<script type=\"text/javascript\">







    (function() {
        var results = \$('[id^=nbr]').filter(function () {
            return this.id.match(/nbr/) //regex for the pattern \"Q followed by a number\"
        }).length;



        var occ = ";
        // line 1397
        echo twig_escape_filter($this->env, (isset($context["offset"]) ? $context["offset"] : null), "html", null, true);
        echo "+1;
        \$(window).scroll(function () {


            if ((\$(window).scrollTop() == \$(document).height() - \$(window).height())) {



                //var ol =\$(\"[id^='nbr']\").eq(x);
                // alert(ol);

                //  var starts =ol.children().length;
                var result = \$('[id^=nbr]').filter(function () {
                    return this.id.match(/nbr/) //regex for the pattern \"Q followed by a number\"
                }).length;

                // console.log(starts);
                // \$.get('Bundle:Controller:Action ',{'start': starts})
                // alert(starts);



                if (result <= ";
        // line 1419
        echo twig_escape_filter($this->env, (isset($context["derocc"]) ? $context["derocc"] : null), "html", null, true);
        echo ")
                {
                    \$.ajax({
                        type: \"POST\",
                        url: \"";
        // line 1423
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("mures", array("page" => ((isset($context["offset"]) ? $context["offset"] : null) + 1))), "html", null, true);
        echo "\",
                        data: \"offset=\" + occ,
                        success: function (data) {

                            var x = document.getElementById(\"";
        // line 1427
        echo twig_escape_filter($this->env, (isset($context["length"]) ? $context["length"] : null), "html", null, true);
        echo "\").id;
                            var ol = \$('#' + x);

                            //\$(\"[id^='nbr']\").eq(x).append(data);


                            ol.append(data);



                        }
                    });
                }else{

                    
                }

                // alert(console.log(start));
                //https://www.youtube.com/watch?v=HBBQs0O0sAY
            }

            //fin
        })

    })();
</script>
";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Mur:murEventFriends.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2416 => 1427,  2409 => 1423,  2402 => 1419,  2377 => 1397,  2359 => 1381,  2350 => 1377,  2324 => 1354,  2319 => 1352,  2311 => 1347,  2303 => 1342,  2292 => 1334,  2285 => 1330,  2265 => 1313,  2260 => 1311,  2252 => 1306,  2244 => 1301,  2237 => 1297,  2225 => 1288,  2218 => 1284,  2210 => 1279,  2196 => 1268,  2190 => 1267,  2184 => 1266,  2165 => 1258,  2159 => 1254,  2155 => 1253,  2146 => 1249,  2141 => 1247,  2135 => 1243,  2129 => 1241,  2123 => 1239,  2120 => 1238,  2118 => 1237,  2113 => 1235,  2105 => 1230,  2102 => 1229,  2100 => 1228,  2075 => 1206,  2070 => 1204,  2062 => 1199,  2054 => 1194,  2043 => 1186,  2036 => 1182,  2016 => 1165,  2011 => 1163,  2003 => 1158,  1995 => 1153,  1988 => 1149,  1976 => 1140,  1969 => 1136,  1961 => 1131,  1947 => 1120,  1941 => 1119,  1935 => 1118,  1924 => 1110,  1910 => 1107,  1904 => 1103,  1900 => 1102,  1891 => 1098,  1886 => 1096,  1880 => 1092,  1874 => 1090,  1868 => 1088,  1865 => 1087,  1863 => 1086,  1858 => 1084,  1851 => 1080,  1848 => 1079,  1846 => 1078,  1821 => 1056,  1816 => 1054,  1808 => 1049,  1800 => 1044,  1789 => 1036,  1782 => 1032,  1762 => 1015,  1757 => 1013,  1749 => 1008,  1741 => 1003,  1734 => 999,  1722 => 990,  1715 => 986,  1707 => 981,  1693 => 970,  1687 => 969,  1681 => 968,  1669 => 959,  1654 => 955,  1648 => 951,  1644 => 950,  1635 => 946,  1630 => 944,  1624 => 940,  1618 => 938,  1611 => 936,  1609 => 935,  1604 => 933,  1597 => 929,  1594 => 928,  1592 => 927,  1567 => 905,  1562 => 903,  1554 => 898,  1546 => 893,  1535 => 885,  1528 => 881,  1508 => 864,  1503 => 862,  1495 => 857,  1487 => 852,  1480 => 848,  1468 => 839,  1461 => 835,  1453 => 830,  1439 => 819,  1433 => 818,  1427 => 817,  1414 => 808,  1400 => 805,  1394 => 801,  1390 => 800,  1380 => 795,  1375 => 793,  1369 => 789,  1363 => 787,  1357 => 785,  1354 => 784,  1352 => 783,  1347 => 781,  1340 => 777,  1337 => 776,  1335 => 775,  1310 => 753,  1305 => 751,  1297 => 746,  1289 => 741,  1278 => 733,  1271 => 729,  1251 => 712,  1246 => 710,  1238 => 705,  1230 => 700,  1223 => 696,  1211 => 687,  1204 => 683,  1196 => 678,  1183 => 668,  1177 => 667,  1171 => 666,  1160 => 658,  1146 => 655,  1140 => 651,  1136 => 650,  1128 => 647,  1121 => 642,  1115 => 640,  1109 => 638,  1106 => 637,  1104 => 636,  1099 => 634,  1091 => 629,  1088 => 628,  1086 => 627,  1061 => 605,  1056 => 603,  1048 => 598,  1040 => 593,  1029 => 585,  1022 => 581,  1002 => 564,  997 => 562,  989 => 557,  981 => 552,  974 => 548,  962 => 539,  955 => 535,  947 => 530,  933 => 519,  927 => 518,  921 => 517,  909 => 508,  902 => 503,  898 => 502,  889 => 498,  884 => 496,  878 => 492,  872 => 490,  866 => 488,  863 => 487,  861 => 486,  856 => 484,  849 => 480,  846 => 479,  844 => 478,  819 => 456,  814 => 454,  806 => 449,  798 => 444,  787 => 436,  780 => 432,  760 => 415,  755 => 413,  747 => 408,  739 => 403,  732 => 399,  720 => 390,  713 => 386,  705 => 381,  690 => 369,  684 => 368,  678 => 367,  665 => 358,  659 => 354,  655 => 353,  646 => 349,  641 => 347,  635 => 343,  629 => 341,  623 => 339,  620 => 338,  618 => 337,  613 => 335,  606 => 331,  603 => 330,  601 => 329,  576 => 307,  571 => 305,  563 => 300,  555 => 295,  544 => 287,  537 => 283,  517 => 266,  512 => 264,  504 => 259,  496 => 254,  489 => 250,  477 => 241,  470 => 237,  462 => 232,  448 => 221,  442 => 220,  436 => 219,  426 => 212,  420 => 208,  416 => 207,  407 => 203,  402 => 201,  396 => 197,  390 => 195,  383 => 193,  381 => 192,  376 => 190,  369 => 186,  366 => 185,  364 => 184,  339 => 162,  334 => 160,  326 => 155,  318 => 150,  307 => 142,  300 => 138,  280 => 121,  275 => 119,  267 => 114,  259 => 109,  252 => 105,  240 => 96,  233 => 92,  225 => 87,  210 => 75,  204 => 74,  198 => 73,  183 => 63,  169 => 60,  163 => 56,  160 => 55,  152 => 53,  146 => 51,  144 => 50,  137 => 49,  131 => 47,  129 => 46,  124 => 43,  111 => 42,  107 => 41,  94 => 35,  88 => 31,  84 => 30,  75 => 26,  70 => 24,  64 => 20,  58 => 18,  51 => 16,  49 => 15,  44 => 13,  37 => 9,  33 => 7,  31 => 6,  26 => 3,  22 => 2,  19 => 1,);
    }
}
