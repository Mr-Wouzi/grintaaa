<?php

/* ComparatorEventBundle:Notification:index.html.twig */
class __TwigTemplate_61501e52a183cbb21ca575ed32f2005bd5d8c803dd397e6fd43d69766345ff98 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container containt-dash\">
    <!--Debut left-block-->
    <div class=\"col-md-2 no-padding-right\">
        <div class=\"block profil-block\">
            <div class=\"profil-dash\">
                <div class=\"col-md-12 no-padding\">
                    <div class=\"grid\">
                        <figure class=\"effect-winston\">
                            ";
        // line 9
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
                            <figcaption>
                                <p>
                                    <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\" title=\"réseau sportif en france\"><i
                                                class=\"fa fa-fw\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
                                </p>
                            </figcaption>
                        </figure>
                    </div>    
                </div>     
                <div class=\"col-md-12 no-padding\">
                    <p class=\"titre\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>

                    <p class=\"titre-user\">      
\t\t\t\t\t\t<a href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t<img src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t</a>
\t\t\t\t\t</p>  
                    <!--p class=\"pro\">Profil</p-->
                </div>

            </div>
            <ul class=\"list-unstyled\">
                <li>
                    <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" title=\"réseau sportif en france\">
                        <img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\"
                             alt=\"réseau sportif en france\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\" class=\"active\" title=\"réseau sportif en france\">
                        ";
        // line 40
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                    </a>
                </li>

                <li>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\" title=\"réseau sportif en france\">
                        ";
        // line 46
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                    </a>
                </li>
                <li>
                    ";
        // line 51
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                </li>
                <li>
                    <a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\" title=\"réseau sportif en france\">
                        ";
        // line 55
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\" title=\"réseau sportif en france\">
                        ";
        // line 60
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                    </a>
                </li>
            </ul>
        </div>

        ";
        // line 66
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
        ";
        // line 67
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

        <br><br>
    </div>

    <div class=\"col-md-10\">
        <div class=\"formulaire-search col-md-10\">
            <div class=\"img-search\">
                <img src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/notifications.png"), "html", null, true);
        echo "\"
                     alt=\"réseau sportif en france\">
            </div>
            <div class=\"title-search\">
                <h4 class=\"green\">";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.notifs"), "html", null, true);
        echo "</h4>
            </div>
        </div>
        <div class=\"list-games list-notif\">

            ";
        // line 84
        $context["x"] = 0;
        // line 85
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["notif"]) {
            echo "  
                ";
            // line 86
            $context["x"] = ((isset($context["x"]) ? $context["x"] : null) + 1);
            // line 87
            echo "                <div class=\"block notif\">
                    <div class=\"col-sm-2 info-date no-padding-left\">
                        <ul class=\"list-unstyled\">
                            <li class=\"size-date\"><!--img src=\"";
            // line 90
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/date.png"), "html", null, true);
            echo "\"-->
                                <i class=\"fa fa-calendar cal-event\"></i>
                                ";
            // line 92
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["notif"], "createdAt", array()), "d-m-Y"), "html", null, true);
            echo "
                            </li>
                        </ul>
                    </div>
                    <div class=\"col-sm-8 notif-avatar\">

                        ";
            // line 98
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["imagesUsers"]) ? $context["imagesUsers"] : null), $context["key"], array(), "array")) > 0)) {
                // line 99
                echo "                            <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["imagesUsers"]) ? $context["imagesUsers"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                echo "\"
                                 alt=\"réseau sportif en france\"/>
                        ";
            } else {
                // line 102
                echo "                            <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                echo "\"
                                 alt=\"réseau sportif en france\"/>
                        ";
            }
            // line 105
            echo "                        ";
            if (($this->getAttribute($context["notif"], "title", array()) == "Participe")) {
                // line 106
                echo "                            ";
                if (($this->getAttribute($this->getAttribute($this->getAttribute($context["notif"], "event", array()), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "id", array()))) {
                    // line 107
                    echo "                                ";
                    if ($this->getAttribute($context["notif"], "enabled", array())) {
                        // line 108
                        echo "                                    <p class=\"vue\">  ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.participe.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    } else {
                        // line 110
                        echo "                                    <p class=\"non-vue\">  ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.participe.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    }
                    // line 112
                    echo "
                            ";
                } else {
                    // line 114
                    echo "
                                ";
                    // line 115
                    if ($this->getAttribute($context["notif"], "enabled", array())) {
                        // line 116
                        echo "                                    <p>";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.participe.event"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</a></p>
                                ";
                    } else {
                        // line 118
                        echo "                                    <p class=\"non-vue\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.participe.event"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</a></p>
                                ";
                    }
                    // line 120
                    echo "

                            ";
                }
                // line 123
                echo "
                        ";
            }
            // line 125
            echo "                        ";
            if (($this->getAttribute($context["notif"], "title", array()) == "Commente")) {
                // line 126
                echo "                            ";
                if (($this->getAttribute($this->getAttribute($this->getAttribute($context["notif"], "event", array()), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "id", array()))) {
                    // line 127
                    echo "
                                ";
                    // line 128
                    if ($this->getAttribute($context["notif"], "enabled", array())) {
                        // line 129
                        echo "                                    <p> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.comment.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    } else {
                        // line 131
                        echo "                                    <p class=\"non-vue\"> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.comment.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    }
                    // line 133
                    echo "
                            ";
                } else {
                    // line 135
                    echo "



                            ";
                }
                // line 140
                echo "                        ";
            }
            // line 141
            echo "

                        ";
            // line 143
            if (($this->getAttribute($context["notif"], "title", array()) == "Suppression Participe")) {
                // line 144
                echo "                            ";
                if (($this->getAttribute($this->getAttribute($this->getAttribute($context["notif"], "event", array()), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "id", array()))) {
                    // line 145
                    echo "
                                ";
                    // line 146
                    if ($this->getAttribute($context["notif"], "enabled", array())) {
                        // line 147
                        echo "                                    <p>  ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.supprime.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    } else {
                        // line 149
                        echo "                                    <p class=\"non-vue\">  ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.supprime.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    }
                    // line 151
                    echo "

                            ";
                } else {
                    // line 154
                    echo "
                                ";
                    // line 155
                    if ($this->getAttribute($context["notif"], "enabled", array())) {
                        // line 156
                        echo "                                    <p>";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.supprime.event"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.supprime.cuse"), "html", null, true);
                        echo "
                                        \"";
                        // line 157
                        echo twig_escape_filter($this->env, $this->getAttribute($context["notif"], "description", array()), "html", null, true);
                        echo "\"</p>
                                ";
                    } else {
                        // line 159
                        echo "                                    <p class=\"non-vue\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.supprime.event"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.supprime.cuse"), "html", null, true);
                        echo "
                                        \"";
                        // line 160
                        echo twig_escape_filter($this->env, $this->getAttribute($context["notif"], "description", array()), "html", null, true);
                        echo "\"</p>
                                ";
                    }
                    // line 162
                    echo "

                            ";
                }
                // line 165
                echo "                        ";
            }
            // line 166
            echo "                        ";
            if (($this->getAttribute($context["notif"], "title", array()) == "Confirme")) {
                // line 167
                echo "
                            ";
                // line 168
                if (($this->getAttribute($this->getAttribute($this->getAttribute($context["notif"], "event", array()), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "id", array()))) {
                    // line 169
                    echo "
                                ";
                    // line 170
                    if ($this->getAttribute($context["notif"], "enabled", array())) {
                        // line 171
                        echo "                                    <p>  ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.confirme.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    } else {
                        // line 173
                        echo "                                    <p class=\"non-vue\">  ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.confirme.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    }
                    // line 175
                    echo "


                            ";
                } else {
                    // line 179
                    echo "
                                ";
                    // line 180
                    if ($this->getAttribute($context["notif"], "enabled", array())) {
                        // line 181
                        echo "                                    <p>";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.confirme.event"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo " </p>
                                ";
                    } else {
                        // line 183
                        echo "                                    <p class=\"non-vue\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.confirme.event"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo " </p>
                                ";
                    }
                    // line 185
                    echo "
                            ";
                }
                // line 187
                echo "                        ";
            }
            // line 188
            echo "

                        ";
            // line 190
            if (($this->getAttribute($context["notif"], "title", array()) == "Confirme Attend")) {
                // line 191
                echo "
                            ";
                // line 192
                if ($this->getAttribute($context["notif"], "enabled", array())) {
                    // line 193
                    echo "                                <p> ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.confirmed.msg1"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.confirmed.events"), "html", null, true);
                    echo "  </p>
                            ";
                } else {
                    // line 195
                    echo "                                <p class=\"non-vue\"> ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.confirmed.msg1"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.confirmed.events"), "html", null, true);
                    echo "  </p>
                            ";
                }
                // line 197
                echo "

                        ";
            }
            // line 200
            echo "

                        ";
            // line 202
            if (($this->getAttribute($context["notif"], "title", array()) == "Fin Evenement")) {
                // line 203
                echo "                            ";
                if (($this->getAttribute($this->getAttribute($this->getAttribute($context["notif"], "event", array()), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "id", array()))) {
                    // line 204
                    echo "
                                ";
                    // line 205
                    if ($this->getAttribute($context["notif"], "enabled", array())) {
                        // line 206
                        echo "                                    <p> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.fin.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    } else {
                        // line 208
                        echo "                                    <p class=\"non-vue\"> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.fin.event.vous"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "</p>
                                ";
                    }
                    // line 210
                    echo "

                            ";
                } else {
                    // line 213
                    echo "
                                ";
                    // line 214
                    if ($this->getAttribute($context["notif"], "enabled", array())) {
                        // line 215
                        echo "                                    <p>";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.fin.event"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "
                                        \"</p>
                                ";
                    } else {
                        // line 218
                        echo "                                    <p class=\"non-vue\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.fin.event"), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "title", array()), "html", null, true);
                        echo "
                                        \"</p>
                                ";
                    }
                    // line 221
                    echo "

                            ";
                }
                // line 224
                echo "                        ";
            }
            // line 225
            echo "
                        ";
            // line 226
            if (($this->getAttribute($context["notif"], "title", array()) == "Register")) {
                // line 227
                echo "
                            ";
                // line 228
                if ($this->getAttribute($context["notif"], "enabled", array())) {
                    // line 229
                    echo "                                <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.register.profile"), "html", null, true);
                    echo " </p>
                            ";
                } else {
                    // line 231
                    echo "                                <p class=\"non-vue\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.register.profile"), "html", null, true);
                    echo " </p>
                            ";
                }
                // line 233
                echo "



                        ";
            }
            // line 238
            echo "                        ";
            if (($this->getAttribute($context["notif"], "title", array()) == "Commente Profil")) {
                // line 239
                echo "
                            ";
                // line 240
                if ($this->getAttribute($context["notif"], "enabled", array())) {
                    // line 241
                    echo "                                <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.comment.my.profile"), "html", null, true);
                    echo " </p>
                            ";
                } else {
                    // line 243
                    echo "                                <p class=\"non-vue\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["notif"], "user", array()), "lastname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.comment.my.profile"), "html", null, true);
                    echo " </p>
                            ";
                }
                // line 245
                echo "

                        ";
            }
            // line 248
            echo "                    </div>
                    ";
            // line 249
            if (($this->getAttribute($context["notif"], "title", array()) == "Register")) {
                // line 250
                echo "                        <div class=\"col-sm-2 info-date no-padding-right\" align=\"right\">
                            <a href=\"";
                // line 251
                echo $this->env->getExtension('routing')->getPath("fos_user_profile_show");
                echo "\"
                               title=\"réseau sportif en france\">";
                // line 252
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.voir"), "html", null, true);
                echo "</a>
                        </div>
                    ";
            } elseif (($this->getAttribute(            // line 254
$context["notif"], "title", array()) == "Commente Profil")) {
                // line 255
                echo "                        <div class=\"col-sm-2 info-date no-padding-right\" align=\"right\">
                            <a href=\"";
                // line 256
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
                echo "\"
                               title=\"réseau sportif en france\">";
                // line 257
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.voir"), "html", null, true);
                echo "</a>
                        </div>
                    ";
            } else {
                // line 260
                echo "                        <div class=\"col-sm-2 info-date no-padding-right\" align=\"right\">
                            <a href=\"";
                // line 261
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["notif"], "event", array()), "slug", array()))), "html", null, true);
                echo "\"
                               title=\"réseau sportif en france\">";
                // line 262
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.voir"), "html", null, true);
                echo "</a>
                        </div>
                    ";
            }
            // line 265
            echo "                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['notif'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 267
        echo "            <div class=\"pagination pagination-centered\">
                <ul>
                    ";
        // line 269
        echo $this->env->getExtension('knp_pagination')->render((isset($context["entities"]) ? $context["entities"] : null));
        echo "
                </ul>
            </div>
        </div>
        <br><br><br>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Notification:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  720 => 269,  716 => 267,  709 => 265,  703 => 262,  699 => 261,  696 => 260,  690 => 257,  686 => 256,  683 => 255,  681 => 254,  676 => 252,  672 => 251,  669 => 250,  667 => 249,  664 => 248,  659 => 245,  649 => 243,  639 => 241,  637 => 240,  634 => 239,  631 => 238,  624 => 233,  614 => 231,  604 => 229,  602 => 228,  599 => 227,  597 => 226,  594 => 225,  591 => 224,  586 => 221,  573 => 218,  560 => 215,  558 => 214,  555 => 213,  550 => 210,  542 => 208,  534 => 206,  532 => 205,  529 => 204,  526 => 203,  524 => 202,  520 => 200,  515 => 197,  505 => 195,  495 => 193,  493 => 192,  490 => 191,  488 => 190,  484 => 188,  481 => 187,  477 => 185,  465 => 183,  453 => 181,  451 => 180,  448 => 179,  442 => 175,  434 => 173,  426 => 171,  424 => 170,  421 => 169,  419 => 168,  416 => 167,  413 => 166,  410 => 165,  405 => 162,  400 => 160,  387 => 159,  382 => 157,  369 => 156,  367 => 155,  364 => 154,  359 => 151,  351 => 149,  343 => 147,  341 => 146,  338 => 145,  335 => 144,  333 => 143,  329 => 141,  326 => 140,  319 => 135,  315 => 133,  307 => 131,  299 => 129,  297 => 128,  294 => 127,  291 => 126,  288 => 125,  284 => 123,  279 => 120,  267 => 118,  255 => 116,  253 => 115,  250 => 114,  246 => 112,  238 => 110,  230 => 108,  227 => 107,  224 => 106,  221 => 105,  214 => 102,  206 => 99,  204 => 98,  195 => 92,  190 => 90,  185 => 87,  183 => 86,  176 => 85,  174 => 84,  166 => 79,  159 => 75,  148 => 67,  144 => 66,  135 => 60,  131 => 59,  124 => 55,  120 => 54,  114 => 51,  106 => 46,  102 => 45,  94 => 40,  90 => 39,  83 => 35,  79 => 34,  75 => 33,  61 => 24,  57 => 23,  49 => 20,  39 => 13,  35 => 12,  29 => 9,  19 => 1,);
    }
}
