<?php

/* FOSUserBundle:Registration:checkEmail.html.twig */
class __TwigTemplate_644a0724e79bb03d0143518069f6c442b6f8848f4ba293c6473f8e73643d703d extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "


<!--Debut containt-->
<div class=\"containt\">
    <div class=\"container\">

        <div class=\"row col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 login-error\">
            <div class=\"login-error-form\">

                ";
        // line 11
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 14
        echo "
            </div>
        </div>
    </div>
</div>
<!--Fin containt-->
</div>
</div>
";
    }

    // line 11
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 12
        echo "                    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("regeister.user.confirmed"), "html", null, true);
        echo "</p>
                ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:checkEmail.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  49 => 12,  46 => 11,  34 => 14,  32 => 11,  20 => 1,);
    }
}
