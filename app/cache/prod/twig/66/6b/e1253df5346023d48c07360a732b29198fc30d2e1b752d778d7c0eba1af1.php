<?php

/* ComparatorEventBundle:Interest:index.html.twig */
class __TwigTemplate_666be1253df5346023d48c07360a732b29198fc30d2e1b752d778d7c0eba1af1 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array())) {
            // line 2
            echo " <!--Debut containt-->
<div class=\"container containt-dash\">
           <!--Debut left-block-->
           <div class=\"col-md-2 no-padding-right\"> 
               <div class=\"block profil-block\">
                   <div class=\"profil-dash\"> 
                         <div class=\"col-md-12 no-padding\">
\t\t\t\t\t\t    <div class=\"grid\">
\t\t\t\t\t         <figure class=\"effect-winston\">
\t\t\t\t\t\t      ";
            // line 11
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
            echo "
\t\t\t\t\t\t      <figcaption>  
\t\t\t\t\t\t\t   <p> 
\t\t\t\t\t\t\t\t<a href=\"";
            // line 14
            echo $this->env->getExtension('routing')->getPath("user_file_new");
            echo "\" title=\"reseau des sports\"><i class=\"fa fa-fw\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
            echo "</i></a>
\t\t\t\t\t\t\t   </p>
\t\t\t\t\t\t      </figcaption>\t\t\t
\t\t\t\t\t         </figure>
\t\t\t\t            </div>       
                         </div>
                         <div class=\"col-md-12 no-padding\">  
                             <p class=\"titre\">";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
            echo "</p>
                             

                    <p class=\"titre-user\">      
\t\t\t\t\t\t<a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
            echo "\"> 
\t\t\t\t\t\t\t<img src=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
            echo "\" class=\"img-loginnn\" />";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
            echo " 
\t\t\t\t\t\t</a>
\t\t\t\t\t</p>  
                             <!--p class=\"pro\">Profil</p-->
                         </div>
                      
                   </div>
                   <ul class=\"list-unstyled\">
                       <li>
                           <a href=\"";
            // line 35
            echo $this->env->getExtension('routing')->getPath("home");
            echo "\" title=\"reseau des sports\" class=\"active\">
                              <img src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
            echo "\" alt=\"reseau des sports\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
            echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
            // line 40
            echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
            echo "\" title=\"reseau des sports\">
                            ";
            // line 41
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
            echo "
                           </a>
                       </li>
                       
                       <li>
                           <a href=\"";
            // line 46
            echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
            echo "\" title=\"reseau des sports\">
                             ";
            // line 47
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
            echo "

                           </a>
                       </li>
                       <li>
                            ";
            // line 52
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
            echo "
                       </li>
                       <li>
                           <a href=\"";
            // line 55
            echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
            echo "\" title=\"reseau des sports\">
                              ";
            // line 56
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
            echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
            // line 60
            echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
            echo "\" title=\"reseau des sports\">
                               ";
            // line 61
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
            echo "
                           </a>
                       </li>
                   </ul>
               </div>
 
               ";
            // line 67
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
            echo "
               ";
            // line 68
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
            echo "

            <br><br>  
           </div>
           <!--Fin left-block-->
           <!--Debut center-block-->
           <div class=\"col-md-10\"> 
    <div class=\"head-compte\">
        <div class=\"\">
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
            // line 78
            echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit_authentication");
            echo "\" class=\"btn btn-compte\" title=\"reseau des sports\">
\t\t\t \t <i class=\"fa fa-user\"></i>  <br>
                    ";
            // line 80
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.compte"), "html", null, true);
            echo "
\t\t\t\t</a>
            </div>
            <div class=\"col-md-2 col-sm-3 no-padding\">
                <a href=\"";
            // line 84
            echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
            echo "\" class=\"btn btn-compte\" title=\"reseau des sports\">
\t\t\t \t <i class=\"fa fa-pencil-square-o\"></i>  <br>
                    ";
            // line 86
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change.password"), "html", null, true);
            echo "
\t\t\t\t</a>
            </div>
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
            // line 90
            echo $this->env->getExtension('routing')->getPath("grintaaa_interest");
            echo "\" class=\"btn btn-compte active\" title=\"reseau des sports\">
\t\t\t \t <i class=\"fa fa-star\"></i>  
\t\t\t\t <br>";
            // line 92
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
            echo "
\t\t\t\t</a>       
            </div>
            ";
            // line 95
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "coach", array()) == 1)) {
                echo " 
            <div class=\"col-md-1 col-sm-3 no-padding\">
                <a href=\"";
                // line 97
                echo $this->env->getExtension('routing')->getPath("grintaaa_coaching");
                echo "\" class=\"btn btn-compte\" title=\"reseau des sports\">
                    <i class=\"fa fa-futbol-o\"></i>
                    <br>";
                // line 99
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.coaching"), "html", null, true);
                echo "
                </a>
            </div>
            ";
            }
            // line 103
            echo "            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
            // line 104
            echo $this->env->getExtension('routing')->getPath("user_file_new");
            echo "\" title=\"reseau des sports\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-picture-o\"></i>  
\t\t\t\t <br>";
            // line 106
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.avatar"), "html", null, true);
            echo "
\t\t\t\t</a>
            </div>


            <div class=\"col-md-2 col-sm-3 no-padding\">
                <a href=\"";
            // line 112
            echo $this->env->getExtension('routing')->getPath("grintaaa_configuration_email");
            echo "\" title=\"reseau des sports\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-envelope-o\"></i>
\t\t\t\t <br>";
            // line 114
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.email"), "html", null, true);
            echo "
\t\t        </a>
            </div>

        </div> 
    </div>
 
\t<div class=\" inscrit\">
  \t  <div class=\"cadre-blanc\">   
        <form class=\"form-inscription\" action=\"";
            // line 123
            echo $this->env->getExtension('routing')->getPath("grintaaa_interest_show");
            echo "\" method=\"post\">
    
                <div class=\"row\">
                    <div class=\"col-md-12 interet222\"><br>
                     <div class=\"col-md-12 titre-gris\" align=\"center\">
                         ";
            // line 128
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.precise.interest"), "html", null, true);
            echo "
\t\t\t\t     </div>
\t\t\t\t\t <br><br>
                        ";
            // line 131
            $context["x"] = 0;
            // line 132
            echo "\t\t\t\t\t\t";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["disciplines"]) ? $context["disciplines"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 133
                echo "\t\t\t\t\t\t";
                $context["x"] = ((isset($context["x"]) ? $context["x"] : null) + 1);
                // line 134
                echo "\t\t\t\t\t\t 
\t\t\t\t\t\t  <div class=\"col-md-3\">
                            <h3 class=\"titre-centreint\">";
                // line 136
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "title", array()), "html", null, true);
                echo "</h3>
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t  <div class=\"col-md-9\">
                            <ul> 
                                ";
                // line 140
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["entity"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                    // line 141
                    echo "                                    <li class=\"col-md-4\"><input type=\"checkbox\" name=\"discipline[]\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["children"], "id", array()), "html", null, true);
                    echo "\" ";
                    if (twig_in_filter($this->getAttribute($context["children"], "id", array()), (isset($context["interestUser"]) ? $context["interestUser"] : null))) {
                        echo "checked ";
                    }
                    echo "> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["children"], "title", array()), "html", null, true);
                    echo " </li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 143
                echo "
                            </ul>  
\t\t\t\t\t\t  </div> 
\t\t                  <div class=\"col-md-12\" align=\"center\">  
\t\t\t\t\t\t    <hr class=\"style-one\" >
\t\t\t\t\t\t  </div> 
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 150
            echo "                    </div> 
                </div>     
            <div class=\"row\" align=\"center\">
\t\t      <div class=\"col-md-12\" align=\"center\">
                <div class=\"col-md-4\"></div>
                <div class=\"col-md-4\">
                    <input type=\"submit\" class=\"btn btn-grinta\" value=\" ";
            // line 156
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("button.enregistrer"), "html", null, true);
            echo "\">
                </div>   
              </div>\t\t\t\t
            </div>


        </form>
      </div>
    </div>
  </div>
</div>
               <!--Fin containt-->
  ";
        }
        // line 169
        echo "\t<div class=\"block-footer\"></div>





";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Interest:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  337 => 169,  321 => 156,  313 => 150,  301 => 143,  286 => 141,  282 => 140,  275 => 136,  271 => 134,  268 => 133,  263 => 132,  261 => 131,  255 => 128,  247 => 123,  235 => 114,  230 => 112,  221 => 106,  216 => 104,  213 => 103,  206 => 99,  201 => 97,  196 => 95,  190 => 92,  185 => 90,  178 => 86,  173 => 84,  166 => 80,  161 => 78,  148 => 68,  144 => 67,  135 => 61,  131 => 60,  124 => 56,  120 => 55,  114 => 52,  106 => 47,  102 => 46,  94 => 41,  90 => 40,  81 => 36,  77 => 35,  63 => 26,  59 => 25,  50 => 21,  38 => 14,  32 => 11,  21 => 2,  19 => 1,);
    }
}
