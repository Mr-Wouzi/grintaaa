<?php

/* ComparatorEventBundle:Notification:countNotification.html.twig */
class __TwigTemplate_68207dce8875449f3f76732f93392d4239d8a3d57d41dea2cec35f2ef3e0355c extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<img src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/notificat.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.notifs"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, (isset($context["notification"]) ? $context["notification"] : null), "html", null, true);
        echo "</span>";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Notification:countNotification.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }
}
