<?php

/* ComparatorEventBundle:Friends:searchFriend.html.twig */
class __TwigTemplate_6ae5b71328978633c73e4fc8fa2b1c7113a61cd4a10b53610aa4cfb3035b9ae1 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt-->
<div class=\"container containt-dash\">
    <!--Debut left-block-->
    <div class=\"col-md-2 no-padding-right\">
        <div class=\"block profil-block\">
            <div class=\"profil-dash\">
                <div class=\"col-md-12 no-padding\">
                    <div class=\"grid\">
                        <figure class=\"effect-winston\">
                            ";
        // line 10
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
                            <figcaption>
                                <p>
                                    <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\" title=\"site de rencontre football\"><i class=\"fa fa-fw\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class=\"col-md-12 no-padding\">
                    <p class=\"titre\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>
                    <p class=\"titre-user\">      
\t\t\t\t\t\t\t\t<a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</p> 
                    <!--p class=\"pro\">Profil</p-->
                </div>

            </div>
            <ul class=\"list-unstyled\">
                <li>
                    <a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" title=\"site de rencontre football\">
                        <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\" alt=\"site de rencontre football\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\" title=\"site de rencontre football\">
                        ";
        // line 38
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                    </a>
                </li>

                <li>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\" title=\"site de rencontre football\">
                        ";
        // line 44
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                    </a>
                </li>
                <li>
                    ";
        // line 49
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                </li>
                <li>
                    <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\" title=\"site de rencontre football\">
                        ";
        // line 53
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\" title=\"site de rencontre football\">
                        ";
        // line 58
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                    </a>
                </li>
            </ul>
        </div>

        ";
        // line 64
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
        ";
        // line 65
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

    <br><br>
    </div>

    <div class=\"col-md-10\">
\t<div class=\"formulaire-search col-md-10\">
        <div class=\"img-search\">
            <i class=\"fa fa-filter icon-puce green\"></i>
        </div>   
        <div class=\"title-search\">   
            <h4 class=\"green friends\">";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.filter.par"), "html", null, true);
        echo "</h4>
        </div>   
    </div>
    <div class=\"block-list-friends\">  
        <div class=\"formulaire-search\">
            <div class=\"formulair\">
                <form id=\"form-search\" class=\"form-search\" action=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend_searchs");
        echo "\" method=\"post\">
                    <div class=\"row\">
\t\t\t\t\t    <!--div class=\"col-md-2 col-sm-12\">
\t\t\t\t\t\t <div class=\"titre_filtre\">
\t\t\t\t\t\t  FILTRER PAR
\t\t\t\t\t\t </div>
\t\t\t\t\t\t</div-->

                        <div class=\"col-md-3 col-sm-6\">
                            <select class=\"form-control\" name=\"loca\">
                                <option value=\"\">";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.pays"), "html", null, true);
        echo "</option>
                                <option value=\"Afghanistan\">Afghanistan</option>
                                <option value=\"Afrique du Sud\">Afrique du Sud</option>
                                <option value=\"Albanie\">Albanie</option>
                                <option value=\"Algérie\">Algérie</option>
                                <option value=\"Allemagne\">Allemagne</option>
                                <option value=\"Andorre\">Andorre</option>
                                <option value=\"Angola\">Angola</option>
                                <option value=\"Anguilla\">Anguilla</option>
                                <option value=\"Antarctique\">Antarctique</option>
                                <option value=\"Antigua et Barbuda\">Antigua-et-Barbuda</option>
                                <option value=\"Arabie saoudite\">Arabie saoudite</option>
                                <option value=\"Argentine\">Argentine</option>
                                <option value=\"Arménie\">Arménie</option>
                                <option value=\"Aruba\">Aruba</option>
                                <option value=\"Australie\">Australie</option>
                                <option value=\"Autriche\">Autriche</option>
                                <option value=\"Azerbaïdjan\">Azerbaïdjan</option>
                                <option value=\"Bahamas\">Bahamas</option>
                                <option value=\"Bahreïn\">Bahreïn</option>
                                <option value=\"Bangladesh\">Bangladesh</option>
                                <option value=\"Barbade\">Barbade</option>
                                <option value=\"Belgique\">Belgique</option>
                                <option value=\"Belize\">Belize</option>
                                <option value=\"Bénin\">Bénin</option>
                                <option value=\"Bermudes\">Bermudes</option>
                                <option value=\"Bhoutan\">Bhoutan</option>
                                <option value=\"Biélorussie\">Biélorussie</option>
                                <option value=\"Bolivie\">Bolivie</option>
                                <option value=\"Bosnie Herzégovine\">Bosnie-Herzégovine</option>
                                <option value=\"Botswana\">Botswana</option>
                                <option value=\"Brésil\">Brésil</option>
                                <option value=\"Brunéi Darussalam\">Brunéi Darussalam</option>
                                <option value=\"Bulgarie\">Bulgarie</option>
                                <option value=\"Burkina Faso\">Burkina Faso</option>
                                <option value=\"Burundi\">Burundi</option>
                                <option value=\"Cambodge\">Cambodge</option>
                                <option value=\"Cameroun\">Cameroun</option>
                                <option value=\"Canada\">Canada</option>
                                <option value=\"Cap Vert\">Cap-Vert</option>
                                <option value=\"Ceuta et Melilla\">Ceuta et Melilla</option>
                                <option value=\"Chili\">Chili</option>
                                <option value=\"Chine\">Chine</option>
                                <option value=\"Chypre\">Chypre</option>
                                <option value=\"Colombie\">Colombie</option>
                                <option value=\"Comores\">Comores</option>
                                <option value=\"Congo Brazzaville\">Congo-Brazzaville</option>
                                <option value=\"Congo Kinshasa\">Congo-Kinshasa</option>
                                <option value=\"Corée du Nord\">Corée du Nord</option>
                                <option value=\"Corée du Sud\">Corée du Sud</option>
                                <option value=\"Costa Rica\">Costa Rica</option>
                                <option value=\"Côte d’Ivoire\">Côte d’Ivoire</option>
                                <option value=\"Croatie\">Croatie</option>
                                <option value=\"Cuba\">Cuba</option>
                                <option value=\"Curaçao\">Curaçao</option>
                                <option value=\"Danemark\">Danemark</option>
                                <option value=\"Diego Garcia\">Diego Garcia</option>
                                <option value=\"Djibouti\">Djibouti</option>
                                <option value=\"Dominique\">Dominique</option>
                                <option value=\"Égypte\">Égypte</option>
                                <option value=\"El Salvador\">El Salvador</option>
                                <option value=\"Émirats arabes unis\">Émirats arabes unis</option>
                                <option value=\"Équateur\">Équateur</option>
                                <option value=\"Érythrée\">Érythrée</option>
                                <option value=\"Espagne\">Espagne</option>
                                <option value=\"Estonie\">Estonie</option>
                                <option value=\"État de la Cité du Vatican\">État de la Cité du Vatican</option>
                                <option value=\"États fédérés de Micronésie\">États fédérés de Micronésie</option>
                                <option value=\"États-Unis\">États-Unis</option>
                                <option value=\"Éthiopie\">Éthiopie</option>
                                <option value=\"Fidji\">Fidji</option>
                                <option value=\"Finlande\">Finlande</option>
                                <option value=\"France\">France</option>
                                <option value=\"Gabon\">Gabon</option>
                                <option value=\"Gambie\">Gambie</option>
                                <option value=\"Géorgie\">Géorgie</option>
                                <option value=\"Ghana\">Ghana</option>
                                <option value=\"Gibraltar\">Gibraltar</option>
                                <option value=\"Grèce\">Grèce</option>
                                <option value=\"Grenade\">Grenade</option>
                                <option value=\"Groenland\">Groenland</option>
                                <option value=\"Guadeloupe\">Guadeloupe</option>
                                <option value=\"Guam\">Guam</option>
                                <option value=\"Guatemala\">Guatemala</option>
                                <option value=\"Guernesey\">Guernesey</option>
                                <option value=\"Guinée\">Guinée</option>
                                <option value=\"Guinée équatoriale\">Guinée équatoriale</option>
                                <option value=\"Guinée Bissau\">Guinée-Bissau</option>
                                <option value=\"Guyana\">Guyana</option>
                                <option value=\"Guyane française\">Guyane française</option>
                                <option value=\"Haïti\">Haïti</option>
                                <option value=\"Honduras\">Honduras</option>
                                <option value=\"Hongrie\">Hongrie</option>
                                <option value=\"Île Christmas\">Île Christmas</option>
                                <option value=\"Île de l’Ascension\">Île de l’Ascension</option>
                                <option value=\"Île de Man\">Île de Man</option>
                                <option value=\"Île Norfolk\">Île Norfolk</option>
                                <option value=\"Îles Åland\">Îles Åland</option>
                                <option value=\"Îles Caïmans\">Îles Caïmans</option>
                                <option value=\"Îles Canaries\">Îles Canaries</option>
                                <option value=\"Îles Cocos\">Îles Cocos</option>
                                <option value=\"Îles Cook\">Îles Cook</option>
                                <option value=\"Îles Féroé\">Îles Féroé</option>
                                <option value=\"Îles Géorgie du Sud et Sandwich du Sud\">Îles Géorgie du Sud et Sandwich du Sud</option>
                                <option value=\"Îles Malouines\">Îles Malouines</option>
                                <option value=\"Îles Mariannes du Nord\">Îles Mariannes du Nord</option>
                                <option value=\"Îles Marshall\">Îles Marshall</option>
                                <option value=\"Îles mineures éloignées des États-Unis\">Îles mineures éloignées des États-Unis</option>
                                <option value=\"Îles Salomon\">Îles Salomon</option>
                                <option value=\"Îles Turques et Caïques\">Îles Turques-et-Caïques</option>
                                <option value=\"Îles Vierges britanniques\">Îles Vierges britanniques</option>
                                <option value=\"Îles Vierges des États Unis\">Îles Vierges des États-Unis</option>
                                <option value=\"Inde\">Inde</option>
                                <option value=\"Indonésie\">Indonésie</option>
                                <option value=\"Irak\">Irak</option>
                                <option value=\"Iran\">Iran</option>
                                <option value=\"Irlande\">Irlande</option>
                                <option value=\"Islande\">Islande</option>
                                <option value=\"Italie\">Italie</option>
                                <option value=\"Jamaïque\">Jamaïque</option>
                                <option value=\"Japon\">Japon</option>
                                <option value=\"Jersey\">Jersey</option>
                                <option value=\"Jordanie\">Jordanie</option>
                                <option value=\"Kazakhstan\">Kazakhstan</option>
                                <option value=\"Kenya\">Kenya</option>
                                <option value=\"Kirghizistan\">Kirghizistan</option>
                                <option value=\"Kiribati\">Kiribati</option>
                                <option value=\"Kosovo\">Kosovo</option>
                                <option value=\"Koweït\">Koweït</option>
                                <option value=\"La Réunion\">La Réunion</option>
                                <option value=\"Laos\">Laos</option>
                                <option value=\"Lesotho\">Lesotho</option>
                                <option value=\"Lettonie\">Lettonie</option>
                                <option value=\"Liban\">Liban</option>
                                <option value=\"Libéria\">Libéria</option>
                                <option value=\"Libye\">Libye</option>
                                <option value=\"Liechtenstein\">Liechtenstein</option>
                                <option value=\"Lituanie\">Lituanie</option>
                                <option value=\"Luxembourg\">Luxembourg</option>
                                <option value=\"Macédoine\">Macédoine</option>
                                <option value=\"Madagascar\">Madagascar</option>
                                <option value=\"Malaisie\">Malaisie</option>
                                <option value=\"Malawi\">Malawi</option>
                                <option value=\"Maldives\">Maldives</option>
                                <option value=\"Mali\">Mali</option>
                                <option value=\"Malte\">Malte</option>
                                <option value=\"Maroc\">Maroc</option>
                                <option value=\"Martinique\">Martinique</option>
                                <option value=\"Maurice\">Maurice</option>
                                <option value=\"Mauritanie\">Mauritanie</option>
                                <option value=\"Mayotte\">Mayotte</option>
                                <option value=\"Mexique\">Mexique</option>
                                <option value=\"Moldavie\">Moldavie</option>
                                <option value=\"Monaco\">Monaco</option>
                                <option value=\"Mongolie\">Mongolie</option>
                                <option value=\"Monténégro\">Monténégro</option>
                                <option value=\"Montserrat\">Montserrat</option>
                                <option value=\"Mozambique\">Mozambique</option>
                                <option value=\"Myanmar\">Myanmar</option>
                                <option value=\"Namibie\">Namibie</option>
                                <option value=\"Nauru\">Nauru</option>
                                <option value=\"Népal\">Népal</option>
                                <option value=\"Nicaragua\">Nicaragua</option>
                                <option value=\"Niger\">Niger</option>
                                <option value=\"Nigéria\">Nigéria</option>
                                <option value=\"Niue\">Niue</option>
                                <option value=\"Norvège\">Norvège</option>
                                <option value=\"Nouvelle Calédonie\">Nouvelle-Calédonie</option>
                                <option value=\"Nouvelle Zélande\">Nouvelle-Zélande</option>
                                <option value=\"Oman\">Oman</option>
                                <option value=\"Ouganda\">Ouganda</option>
                                <option value=\"Ouzbékistan\">Ouzbékistan</option>
                                <option value=\"Pakistan\">Pakistan</option>
                                <option value=\"Palaos\">Palaos</option>
                                <option value=\"Panama\">Panama</option>
                                <option value=\"Papouasie Nouvelle Guinée\">Papouasie-Nouvelle-Guinée</option>
                                <option value=\"Paraguay\">Paraguay</option>
                                <option value=\"Pays Bas\">Pays-Bas</option>
                                <option value=\"Pays Bas caribéens\">Pays-Bas caribéens</option>
                                <option value=\"Pérou\">Pérou</option>
                                <option value=\"Philippines\">Philippines</option>
                                <option value=\"Pitcairn\">Pitcairn</option>
                                <option value=\"Pologne\">Pologne</option>
                                <option value=\"Polynésie française\">Polynésie française</option>
                                <option value=\"Porto Rico\">Porto Rico</option>
                                <option value=\"Portugal\">Portugal</option>
                                <option value=\"Qatar\">Qatar</option>
                                <option value=\"R.A.S. chinoise de Hong Kong\">R.A.S. chinoise de Hong Kong</option>
                                <option value=\"R.A.S. chinoise de Macao\">R.A.S. chinoise de Macao</option>
                                <option value=\"République centrafricaine\">République centrafricaine</option>
                                <option value=\"République dominicaine\">République dominicaine</option>
                                <option value=\"République tchèque\">République tchèque</option>
                                <option value=\"Roumanie\">Roumanie</option>
                                <option value=\"Royaume Uni\">Royaume-Uni</option>
                                <option value=\"Russie\">Russie</option>
                                <option value=\"Rwanda\">Rwanda</option>
                                <option value=\"Sahara occidental\">Sahara occidental</option>
                                <option value=\"Saint Barthélemy\">Saint-Barthélemy</option>
                                <option value=\"Saint Christophe et Niévès\">Saint-Christophe-et-Niévès</option>
                                <option value=\"Saint Marin\">Saint-Marin</option>
                                <option value=\"Saint Martin (partie française)\">Saint-Martin (partie française)</option>
                                <option value=\"Saint Martin (partie néerlandaise)\">Saint-Martin (partie néerlandaise)</option>
                                <option value=\"Saint Pierre et Miquelon\">Saint-Pierre-et-Miquelon</option>
                                <option value=\"Saint Vincent et les Grenadines\">Saint-Vincent-et-les-Grenadines</option>
                                <option value=\"Sainte Hélène\">Sainte-Hélène</option>
                                <option value=\"Sainte Lucie\">Sainte-Lucie</option>
                                <option value=\"Samoa\">Samoa</option>
                                <option value=\"Samoa américaines\">Samoa américaines</option>
                                <option value=\"Sao Tomé et Principe\">Sao Tomé-et-Principe</option>
                                <option value=\"Sénégal\">Sénégal</option>
                                <option value=\"Serbie\">Serbie</option>
                                <option value=\"Seychelles\">Seychelles</option>
                                <option value=\"Sierra Leone\">Sierra Leone</option>
                                <option value=\"Singapour\">Singapour</option>
                                <option value=\"Slovaquie\">Slovaquie</option>
                                <option value=\"Slovénie\">Slovénie</option>
                                <option value=\"Somalie\">Somalie</option>
                                <option value=\"Soudan\">Soudan</option>
                                <option value=\"Soudan du Sud\">Soudan du Sud</option>
                                <option value=\"Sri Lanka\">Sri Lanka</option>
                                <option value=\"Suède\">Suède</option>
                                <option value=\"Suisse\">Suisse</option>
                                <option value=\"Suriname\">Suriname</option>
                                <option value=\"Svalbard et Jan Mayen\">Svalbard et Jan Mayen</option>
                                <option value=\"Swaziland\">Swaziland</option>
                                <option value=\"Syrie\">Syrie</option>
                                <option value=\"Tadjikistan\">Tadjikistan</option>
                                <option value=\"Taïwan\">Taïwan</option>
                                <option value=\"Tanzanie\">Tanzanie</option>
                                <option value=\"Tchad\">Tchad</option>
                                <option value=\"Terres australes françaises\">Terres australes françaises</option>
                                <option value=\"Territoire britannique de l’océan Indien\">Territoire britannique de l’océan Indien</option>
                                <option value=\"Territoires palestiniens\">Territoires palestiniens</option>
                                <option value=\"Thaïlande\">Thaïlande</option>
                                <option value=\"Timor oriental\">Timor oriental</option>
                                <option value=\"Togo\">Togo</option>
                                <option value=\"Tokelau\">Tokelau</option>
                                <option value=\"Tonga\">Tonga</option>
                                <option value=\"Trinité et Tobago\">Trinité-et-Tobago</option>
                                <option value=\"Tristan da Cunha\">Tristan da Cunha</option>
                                <option value=\"Tunisie\">Tunisie</option>
                                <option value=\"Turkménistan\">Turkménistan</option>
                                <option value=\"Turquie\">Turquie</option>
                                <option value=\"Tuvalu\">Tuvalu</option>
                                <option value=\"Ukraine\">Ukraine</option>
                                <option value=\"Uruguay\">Uruguay</option>
                                <option value=\"Vanuatu\">Vanuatu</option>
                                <option value=\"Venezuela\">Venezuela</option>
                                <option value=\"Vietnam\">Vietnam</option>
                                <option value=\"Wallis et Futuna\">Wallis-et-Futuna</option>
                                <option value=\"Yémen\">Yémen</option>
                                <option value=\"Zambie\">Zambie</option>
                                <option value=\"Zimbabwe\">Zimbabwe</option>
                            </select>
                        </div>
     
                        <div class=\"col-md-3 col-sm-6\"> 
                            <select class=\"form-control\" name=\"interest\">
                                <option value=\"\">";
        // line 350
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.discipline"), "html", null, true);
        echo "</option>
                                ";
        // line 351
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["disciplines"]) ? $context["disciplines"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["discipline"]) {
            // line 352
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["discipline"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["discipline"], "title", array()), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discipline'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 354
        echo "                            </select>
                        </div>

                        <div class=\"col-md-3 col-sm-6\">
                            <input type=\"number\"  name=\"sizemin\"  class=\"form-control\" placeholder=\"";
        // line 358
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("taille.min"), "html", null, true);
        echo "\" min=\"0\">
                        </div>

                        <div class=\"col-md-3 col-sm-6\">
                            <input type=\"number\"  name=\"sizemax\"  class=\"form-control\" placeholder=\"";
        // line 362
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("taille.max"), "html", null, true);
        echo "\" min=\"0\">
                        </div>

                        <div class=\"col-md-3 col-sm-6\">
                            <input type=\"number\"  name=\"weightmin\"  class=\"form-control\" placeholder=\"";
        // line 366
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("poid.min"), "html", null, true);
        echo "\" min=\"0\">
   
                        </div>
   
                        <div class=\"col-md-3 col-sm-6\">
                            <input type=\"number\"  name=\"weightmax\"  class=\"form-control\" placeholder=\"";
        // line 371
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("poid.max"), "html", null, true);
        echo "\" min=\"0\">

                        </div>   

                        <div class=\"col-md-3 col-sm-6\">
                            <input type=\"number\"  name=\"agemin\"  class=\"form-control\" placeholder=\"";
        // line 376
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("age.min"), "html", null, true);
        echo "\" min=\"0\">
                        </div>

                        <div class=\"col-md-3 col-sm-6\">
                            <input type=\"number\"  name=\"agemax\"  class=\"form-control\" placeholder=\"";
        // line 380
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("age.max"), "html", null, true);
        echo "\" min=\"0\">
                        </div> 

                        <div class=\"col-md-3 col-sm-12\"></div>

                        <div class=\"col-md-3 col-sm-6\">
                            <input type=\"checkbox\" name=\"coach\">
                            ";
        // line 387
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.u.coach"), "html", null, true);
        echo "
                        </div>

                        <div class=\"col-md-3 col-sm-6\">
                            <input type=\"checkbox\" name=\"handi\">
                            ";
        // line 392
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("handi.sport"), "html", null, true);
        echo "
                        </div>



                        <div class=\"col-md-3 col-sm-6\">
                            <input type=\"submit\" class=\"form-control btn btn-grinta\" value=\"";
        // line 398
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("button.search"), "html", null, true);
        echo "\" />
                        </div>
                    </div>
                </form>
            </div>




            <div class=\"img-search\">
                <img src=\"";
        // line 408
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/users.png"), "html", null, true);
        echo "\" alt=\"site de rencontre football\">
            </div>
            <div class=\"title-search\">
                ";
        // line 411
        if (((isset($context["mot"]) ? $context["mot"] : null) == "")) {
            // line 412
            echo "                    <h4 class=\"green\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("liste.of.amis.sug"), "html", null, true);
            echo "</h4>
                ";
        } else {
            // line 414
            echo "
                    <h4 class=\"green\">";
            // line 415
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("liste.of.amis.trouv"), "html", null, true);
            echo "</h4>
                ";
        }
        // line 417
        echo "

            </div>
        </div>  
        <div class=\"result-search\">
            ";
        // line 422
        if (((isset($context["mot"]) ? $context["mot"] : null) == "")) {
            // line 423
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
                // line 424
                echo "                ";
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()) != $this->getAttribute($context["entity"], "id", array()))) {
                    // line 425
                    echo "                    ";
                    if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "country", array()) == $this->getAttribute($context["entity"], "country", array()))) {
                        // line 426
                        echo "                        <div class=\"one-search col-md-6 \">
                            <div class=\"col-md-12 cadre-ami no-padding\">
                                <div class=\"img-search2\">
                                    <a href=\"";
                        // line 429
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($context["entity"], "username", array()), "id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                        echo "\" title=\"site de rencontre football\">
                                        ";
                        // line 430
                        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                            // line 431
                            echo "                                            <img src=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                            echo "\" alt=\"site de rencontre football\">
                                        ";
                        } else {
                            // line 433
                            echo "                                            <img src=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                            echo "\" alt=\"site de rencontre football\">
                                        ";
                        }
                        // line 435
                        echo "                                    </a>
                                </div>    

                                <div class=\"info-date2\">
                                    <a href=\"";
                        // line 439
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($context["entity"], "username", array()), "id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                        echo "\" title=\"site de rencontre football\">
\t\t\t\t\t\t\t\t\t<h2 class=\"green\">";
                        // line 440
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "lastname", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "firstname", array()), "html", null, true);
                        echo " ";
                        if (($this->getAttribute($context["entity"], "coach", array()) == 1)) {
                            echo " (";
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.u.coach"), "html", null, true);
                            echo ") ";
                        }
                        echo "</h2>
\t\t\t\t\t\t\t\t\t</a>
                                    <ul class=\"list-unstyled\">
                                        <li>
                                            <img src=\"";
                        // line 444
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/home.png"), "html", null, true);
                        echo "\" alt=\"site de rencontre football\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "city", array()), "html", null, true);
                        echo "
                                            ( ";
                        // line 445
                        echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "country", array()), "html", null, true);
                        echo ")
                                        </li>
                                        <li class=\"star\"><img
                                                    src=\"";
                        // line 448
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/star.png"), "html", null, true);
                        echo "\" alt=\"site de rencontre football\"> ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
                        echo ":
\t\t\t\t\t\t\t\t\t\t\t";
                        // line 449
                        $context["x"] = 0;
                        echo " 
                                            ";
                        // line 450
                        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["interest"]) ? $context["interest"] : null), $context["key"], array(), "array")) > 0)) {
                            // line 451
                            echo "                                                ";
                            $context['_parent'] = (array) $context;
                            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["interest"]) ? $context["interest"] : null), $context["key"], array(), "array"));
                            foreach ($context['_seq'] as $context["keys"] => $context["interested"]) {
                                // line 452
                                echo "\t\t\t\t\t\t\t\t\t\t\t      ";
                                $context["x"] = ((isset($context["x"]) ? $context["x"] : null) + 1);
                                // line 453
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t  ";
                                if (((isset($context["x"]) ? $context["x"] : null) < 8)) {
                                    // line 454
                                    echo "                                                    *";
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["interested"], "category", array()), "title", array()), "html", null, true);
                                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t  ";
                                }
                                // line 455
                                echo " 
                                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['keys'], $context['interested'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 456
                            echo "...

                                            ";
                        }
                        // line 458
                        echo " 
                                        </li>
                                    </ul>
                                </div>

                                <div class=\"info-date3\">
                                    <div class=\"action-search\">


                                        ";
                        // line 467
                        if (!twig_in_filter($this->getAttribute($context["entity"], "id", array()), (isset($context["idAlls"]) ? $context["idAlls"] : null))) {
                            // line 468
                            echo "
                                            <a href=\"";
                            // line 469
                            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_friend", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                            echo "\"
                                               class=\"btn btn-grinta\" title=\"site de rencontre football\">";
                            // line 470
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.inviter"), "html", null, true);
                            echo " </a>

                                        ";
                        }
                        // line 473
                        echo "
                                        ";
                        // line 474
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["allFreinds"]) ? $context["allFreinds"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["friend"]) {
                            // line 475
                            echo "                                            ";
                            if ((($this->getAttribute($context["entity"], "id", array()) == $this->getAttribute($this->getAttribute($context["friend"], "user1", array()), "id", array())) || ($this->getAttribute($context["entity"], "id", array()) == $this->getAttribute($this->getAttribute($context["friend"], "user2", array()), "id", array())))) {
                                // line 476
                                echo "                                                <form id=\"validate_field_types\" method=\"POST\"
                                                      action=\"";
                                // line 477
                                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["friend"], "id", array()))), "html", null, true);
                                echo "\">
                                                    <input type=\"submit\" class=\"btn btn-refus\" value=\"";
                                // line 478
                                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.delete"), "html", null, true);
                                echo "\"/>

                                                </form>
                                            ";
                            }
                            // line 482
                            echo "
                                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friend'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 484
                        echo "

                                        ";
                        // line 486
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["friendssattente"]) ? $context["friendssattente"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["friendattente"]) {
                            // line 487
                            echo "
                                            ";
                            // line 488
                            if (($this->getAttribute($context["entity"], "id", array()) == $this->getAttribute($this->getAttribute($context["friendattente"], "user2", array()), "id", array()))) {
                                // line 489
                                echo "
                                                <form id=\"validate_field_types\" method=\"POST\"
                                                      action=\"";
                                // line 491
                                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["friendattente"], "id", array()))), "html", null, true);
                                echo "\">
                                                    <input type=\"submit\" class=\"btn btn-refus\" value=\"";
                                // line 492
                                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.annuler"), "html", null, true);
                                echo "\"/>

                                                </form>
                                            ";
                            }
                            // line 496
                            echo "                                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friendattente'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 497
                        echo "
                                        ";
                        // line 498
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["friendsinviattente"]) ? $context["friendsinviattente"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["friendinviattente"]) {
                            // line 499
                            echo "

                                            ";
                            // line 501
                            if (($this->getAttribute($context["entity"], "id", array()) == $this->getAttribute($this->getAttribute($context["friendinviattente"], "user1", array()), "id", array()))) {
                                // line 502
                                echo "                                                <form id=\"validate_field_types\" method=\"POST\"
                                                      action=\"";
                                // line 503
                                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_update", array("id" => $this->getAttribute($context["friendinviattente"], "id", array()))), "html", null, true);
                                echo "\">
                                                    <input type=\"submit\" class=\"btn btn-grinta\" value=\"";
                                // line 504
                                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.accepte"), "html", null, true);
                                echo "\"/>

                                                </form>
                                                <form id=\"validate_field_types\" method=\"POST\"
                                                      action=\"";
                                // line 508
                                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["friendinviattente"], "id", array()))), "html", null, true);
                                echo "\">
                                                    <input type=\"submit\" class=\"btn btn-refus\" value=\"";
                                // line 509
                                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.refuse"), "html", null, true);
                                echo "\"/>

                                                </form>
                                            ";
                            }
                            // line 513
                            echo "                                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friendinviattente'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 514
                        echo " 
                                    </div>
\t\t\t\t\t\t\t\t</div>
                            </div>

                        </div>
                        ";
                    }
                    // line 521
                    echo "                ";
                }
                // line 522
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 523
            echo "            ";
        } else {
            // line 524
            echo "
                ";
            // line 525
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
                // line 526
                echo "                    ";
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()) != $this->getAttribute($context["entity"], "id", array()))) {
                    // line 527
                    echo "                        <div class=\"one-search col-md-6 \">
                            <div class=\"col-md-12 cadre-ami no-padding\">
                                <div class=\"img-search2\">
                                    <a href=\"";
                    // line 530
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($context["entity"], "username", array()), "id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                    echo "\" title=\"site de rencontre football\">
                                        ";
                    // line 531
                    if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                        // line 532
                        echo "                                            <img src=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                        echo "\" alt=\"site de rencontre football\">
                                        ";
                    } else {
                        // line 534
                        echo "                                            <img src=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                        echo "\" alt=\"site de rencontre football\">
                                        ";
                    }
                    // line 536
                    echo "                                    </a>
                                </div>

                                <div class=\"info-date2\">
                                    <a href=\"";
                    // line 540
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($context["entity"], "username", array()), "id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                    echo "\" title=\"site de rencontre football\">
                                        <h2 class=\"green\">";
                    // line 541
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "lastname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "firstname", array()), "html", null, true);
                    echo " ";
                    if (($this->getAttribute($context["entity"], "coach", array()) == 1)) {
                        echo " (";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.u.coach"), "html", null, true);
                        echo ") ";
                    }
                    echo "</h2>
                                    </a>
                                    <ul class=\"list-unstyled\">
                                        <li>
                                            <img src=\"";
                    // line 545
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/home.png"), "html", null, true);
                    echo "\" alt=\"site de rencontre football\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "city", array()), "html", null, true);
                    echo "
                                            ( ";
                    // line 546
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "country", array()), "html", null, true);
                    echo ")
                                        </li>
                                        <li class=\"star\"><img
                                                    src=\"";
                    // line 549
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/star.png"), "html", null, true);
                    echo "\" alt=\"site de rencontre football\"> ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
                    echo ":
                                            ";
                    // line 550
                    $context["x"] = 0;
                    // line 551
                    echo "                                            ";
                    if ((twig_length_filter($this->env, $this->getAttribute((isset($context["interest"]) ? $context["interest"] : null), $context["key"], array(), "array")) > 0)) {
                        // line 552
                        echo "                                                ";
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["interest"]) ? $context["interest"] : null), $context["key"], array(), "array"));
                        foreach ($context['_seq'] as $context["keys"] => $context["interested"]) {
                            // line 553
                            echo "                                                    ";
                            $context["x"] = ((isset($context["x"]) ? $context["x"] : null) + 1);
                            // line 554
                            echo "                                                    ";
                            if (((isset($context["x"]) ? $context["x"] : null) < 8)) {
                                // line 555
                                echo "                                                        *";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["interested"], "category", array()), "title", array()), "html", null, true);
                                echo "
                                                    ";
                            }
                            // line 557
                            echo "                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['keys'], $context['interested'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        echo "...

                                            ";
                    }
                    // line 560
                    echo "                                        </li>
                                    </ul>
                                </div>

                                <div class=\"info-date3\">
                                    <div class=\"action-search\">


                                        ";
                    // line 568
                    if (!twig_in_filter($this->getAttribute($context["entity"], "id", array()), (isset($context["idAlls"]) ? $context["idAlls"] : null))) {
                        // line 569
                        echo "
                                            <a href=\"";
                        // line 570
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_friend", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                        echo "\"
                                               class=\"btn btn-grinta\" title=\"site de rencontre football\">";
                        // line 571
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.inviter"), "html", null, true);
                        echo " </a>

                                        ";
                    }
                    // line 574
                    echo "
                                        ";
                    // line 575
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["allFreinds"]) ? $context["allFreinds"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["friend"]) {
                        // line 576
                        echo "                                            ";
                        if ((($this->getAttribute($context["entity"], "id", array()) == $this->getAttribute($this->getAttribute($context["friend"], "user1", array()), "id", array())) || ($this->getAttribute($context["entity"], "id", array()) == $this->getAttribute($this->getAttribute($context["friend"], "user2", array()), "id", array())))) {
                            // line 577
                            echo "                                                <form id=\"validate_field_types\" method=\"POST\"
                                                      action=\"";
                            // line 578
                            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["friend"], "id", array()))), "html", null, true);
                            echo "\">
                                                    <input type=\"submit\" class=\"btn btn-refus\" value=\"";
                            // line 579
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.delete"), "html", null, true);
                            echo "\"/>

                                                </form>
                                            ";
                        }
                        // line 583
                        echo "
                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friend'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 585
                    echo "

                                        ";
                    // line 587
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["friendssattente"]) ? $context["friendssattente"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["friendattente"]) {
                        // line 588
                        echo "
                                            ";
                        // line 589
                        if (($this->getAttribute($context["entity"], "id", array()) == $this->getAttribute($this->getAttribute($context["friendattente"], "user2", array()), "id", array()))) {
                            // line 590
                            echo "
                                                <form id=\"validate_field_types\" method=\"POST\"
                                                      action=\"";
                            // line 592
                            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["friendattente"], "id", array()))), "html", null, true);
                            echo "\">
                                                    <input type=\"submit\" class=\"btn btn-refus\" value=\"";
                            // line 593
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.annuler"), "html", null, true);
                            echo "\"/>

                                                </form>
                                            ";
                        }
                        // line 597
                        echo "                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friendattente'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 598
                    echo "
                                        ";
                    // line 599
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["friendsinviattente"]) ? $context["friendsinviattente"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["friendinviattente"]) {
                        // line 600
                        echo "

                                            ";
                        // line 602
                        if (($this->getAttribute($context["entity"], "id", array()) == $this->getAttribute($this->getAttribute($context["friendinviattente"], "user1", array()), "id", array()))) {
                            // line 603
                            echo "                                                <form id=\"validate_field_types\" method=\"POST\"
                                                      action=\"";
                            // line 604
                            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_update", array("id" => $this->getAttribute($context["friendinviattente"], "id", array()))), "html", null, true);
                            echo "\">
                                                    <input type=\"submit\" class=\"btn btn-grinta\" value=\"";
                            // line 605
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.accepte"), "html", null, true);
                            echo "\"/>

                                                </form>
                                                <form id=\"validate_field_types\" method=\"POST\"
                                                      action=\"";
                            // line 609
                            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["friendinviattente"], "id", array()))), "html", null, true);
                            echo "\">
                                                    <input type=\"submit\" class=\"btn btn-refus\" value=\"";
                            // line 610
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.refuse"), "html", null, true);
                            echo "\"/>

                                                </form>
                                            ";
                        }
                        // line 614
                        echo "                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friendinviattente'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 615
                    echo "
                                    </div>
                                </div>
                            </div>

                        </div>
                    ";
                }
                // line 622
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 623
            echo "            ";
        }
        // line 624
        echo "        </div>
    </div>
  </div>
</div>    
<br><br>
<!--Debut containt-->";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Friends:searchFriend.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1104 => 624,  1101 => 623,  1095 => 622,  1086 => 615,  1080 => 614,  1073 => 610,  1069 => 609,  1062 => 605,  1058 => 604,  1055 => 603,  1053 => 602,  1049 => 600,  1045 => 599,  1042 => 598,  1036 => 597,  1029 => 593,  1025 => 592,  1021 => 590,  1019 => 589,  1016 => 588,  1012 => 587,  1008 => 585,  1001 => 583,  994 => 579,  990 => 578,  987 => 577,  984 => 576,  980 => 575,  977 => 574,  971 => 571,  967 => 570,  964 => 569,  962 => 568,  952 => 560,  942 => 557,  936 => 555,  933 => 554,  930 => 553,  925 => 552,  922 => 551,  920 => 550,  914 => 549,  908 => 546,  902 => 545,  887 => 541,  883 => 540,  877 => 536,  871 => 534,  864 => 532,  862 => 531,  858 => 530,  853 => 527,  850 => 526,  846 => 525,  843 => 524,  840 => 523,  834 => 522,  831 => 521,  822 => 514,  816 => 513,  809 => 509,  805 => 508,  798 => 504,  794 => 503,  791 => 502,  789 => 501,  785 => 499,  781 => 498,  778 => 497,  772 => 496,  765 => 492,  761 => 491,  757 => 489,  755 => 488,  752 => 487,  748 => 486,  744 => 484,  737 => 482,  730 => 478,  726 => 477,  723 => 476,  720 => 475,  716 => 474,  713 => 473,  707 => 470,  703 => 469,  700 => 468,  698 => 467,  687 => 458,  682 => 456,  675 => 455,  669 => 454,  666 => 453,  663 => 452,  658 => 451,  656 => 450,  652 => 449,  646 => 448,  640 => 445,  634 => 444,  619 => 440,  615 => 439,  609 => 435,  603 => 433,  596 => 431,  594 => 430,  590 => 429,  585 => 426,  582 => 425,  579 => 424,  574 => 423,  572 => 422,  565 => 417,  560 => 415,  557 => 414,  551 => 412,  549 => 411,  543 => 408,  530 => 398,  521 => 392,  513 => 387,  503 => 380,  496 => 376,  488 => 371,  480 => 366,  473 => 362,  466 => 358,  460 => 354,  449 => 352,  445 => 351,  441 => 350,  180 => 92,  167 => 82,  158 => 76,  144 => 65,  140 => 64,  131 => 58,  127 => 57,  120 => 53,  116 => 52,  110 => 49,  102 => 44,  98 => 43,  90 => 38,  86 => 37,  77 => 33,  73 => 32,  59 => 23,  55 => 22,  48 => 20,  36 => 13,  30 => 10,  19 => 1,);
    }
}
