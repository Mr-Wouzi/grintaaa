<?php

/* ComparatorEventBundle:Mur:countVideos.html.twig */
class __TwigTemplate_72d2c9528f28e8fce9b0dc24cf68be1087748a354bf279620e11a78b76085c37 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/video.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.video"), "html", null, true);
        echo " <span> ";
        echo twig_escape_filter($this->env, (isset($context["nbvideos"]) ? $context["nbvideos"] : null), "html", null, true);
        echo "</span>
";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Mur:countVideos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
