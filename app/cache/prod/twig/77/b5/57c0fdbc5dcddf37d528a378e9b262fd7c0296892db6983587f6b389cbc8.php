<?php

/* ComparatorBaseBundle:Block:block_footer.html.twig */
class __TwigTemplate_77b557c0fdbc5dcddf37d528a378e9b262fd7c0296892db6983587f6b389cbc8 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array())) {
            // line 2
            if (((((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "event_new") && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "event_confirmed")) && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "hwi_oauth_connect_registration")) && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "fos_user_registration_confirmed")) && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "grintaaa_friend"))) {
                // line 3
                echo "
    <!--Debut Footer-->
    <div class=\"footer\">
        <div class=\"bg-footer\">
            <div class=\"container\">
                <div class=\"top-footer\">
                    <!--div class=\"col-md-3 col-sm-6 links\">
                        <ul>
                            <li><a href=\"";
                // line 11
                echo $this->env->getExtension('routing')->getPath("event_new");
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/circle.png"), "html", null, true);
                echo "\">Créer un sport</a></li>
                            <li><a href=\"";
                // line 12
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/circle.png"), "html", null, true);
                echo "\">Partagez vos photos et vidéos</a></li>
                        </ul>
                    </div--> 
                    <div class=\"col-md-5 col-sm-6 links hidd-tel hidd-tablette\"> 
                        <ul> 
                            <li><a href=\"";
                // line 17
                echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend_search");
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/circle.png"), "html", null, true);
                echo "\">Rencontrer de nouveaux joueurs de votre région</a></li>
                            <li><a href=\"";
                // line 18
                echo $this->env->getExtension('routing')->getPath("event");
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/circle.png"), "html", null, true);
                echo "\">Trouver des sports près de chez vous</a></li>
                        </ul>
                    </div>
                    <div class=\"col-md-7 col-sm-12 bottons text-center\">
                        <ul class=\"list-inline\">
                            <li class=\"col-xs-4\">
                                <a href=\"https://www.facebook.com/Grintaaa-944054715632395/?fref=ts\" target=\"_new\">
\t\t\t\t\t\t\t\t  <div class=\"col-xs-1\" align=\"right\">
                                    <img src=\"";
                // line 26
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/fb.png"), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t  </div>   
\t\t\t\t\t\t\t\t  <div class=\"col-xs-7 hidd-tel\">
                                    Suivez-nous <br><span>sur facebook</span>
\t\t\t\t\t\t\t\t  </div>
                                </a>  
                            </li>  
                            <li class=\"col-xs-4\">
                                <a href=\"mailto:grintaaa1@gmail.com\">
\t\t\t\t\t\t\t\t  <div class=\"col-xs-1\" align=\"right\">
                                    <img src=\"";
                // line 36
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/mail.png"), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t  <div class=\"col-xs-7 hidd-tel\">
                                    Nous contacter <br><span>grintaaa1@gmail.com</span>
\t\t\t\t\t\t\t\t  </div>
                                </a>
                            </li>  
                            <li class=\"col-xs-4\">
                                <a href=\"#\">
\t\t\t\t\t\t\t\t  <div class=\"col-xs-1\" align=\"right\">
                                    <img src=\"";
                // line 46
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/map.png"), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t  <div class=\"col-xs-9 hidd-tel\">
                                    Adresse<br><span>Paris, 1028 France</span>
\t\t\t\t\t\t\t\t  </div>
                                </a>  
                            </li> 
                        </ul> 
                    </div> 
                </div>

            </div>
        </div>
    </div> 
    <!--Fin Footer-->
   </div>

";
            }
            // line 64
            echo "

";
        }
    }

    public function getTemplateName()
    {
        return "ComparatorBaseBundle:Block:block_footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 64,  94 => 46,  81 => 36,  68 => 26,  55 => 18,  49 => 17,  39 => 12,  33 => 11,  23 => 3,  21 => 2,  19 => 1,);
    }
}
