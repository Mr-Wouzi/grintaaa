<?php

/* ComparatorEventBundle:Event:search.html.twig */
class __TwigTemplate_7e00441fbdef9c284a8bce2be4daa11118020ee6ccf6e96b35c8b0780f1e0abd extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--containt-->
<!--Debut containt-->
<div class=\"container containt-dash\">
    <!--Debut left-block-->
    <div class=\"col-md-2 no-padding-right\">
        <div class=\"block profil-block\">
            <div class=\"profil-dash\">
                <div class=\"col-md-12 no-padding\">
                    <div class=\"grid\">
                        <figure class=\"effect-winston\">
                            ";
        // line 11
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
                            <figcaption>
                                <p>
                                    <a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\"><i class=\"fa fa-fw\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class=\"col-md-12 no-padding\">
                    <p class=\"titre\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>
                    
                    <p class=\"titre-user\">      
\t\t\t\t\t\t\t\t<a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</p> 
                    <!--p class=\"pro\">Profil</p-->
                </div>

            </div>
            <ul class=\"list-unstyled\">
                <li>
                    <a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\">
                        <img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\" class=\"active\">
                        ";
        // line 40
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                    </a>
                </li>

                <li>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\">
                        ";
        // line 46
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                    </a>
                </li>
                <li>
                    ";
        // line 51
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                </li>
                <li>
                    <a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\">
                        ";
        // line 55
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\">
                        ";
        // line 60
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                    </a>
                </li>
            </ul>
        </div>

        ";
        // line 66
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
        ";
        // line 67
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

    <br><br>
    </div>

    <div class=\"col-md-10\">
    <div class=\"formulaire-search col-md-12\">
        <div class=\"img-search\">  
            <img src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/recherche.png"), "html", null, true);
        echo "\">
        </div> 
        <div class=\"title-search\">  
            <h2 class=\"green\">";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("recherche.sport"), "html", null, true);
        echo "</h2>
        </div>
        <div class=\"formulair\">
            <form id=\"form-search\" class=\"form-search\" action=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("event_search");
        echo "\" method=\"post\">
                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"\">";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.pays"), "html", null, true);
        echo ":</div>
                        <select class=\"form-control\" name=\"genre\" id=\"pays\">
                            <option value=\"Afghanistan\" ";
        // line 86
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Afghanistan")) {
            echo " selected ";
        }
        echo ">Afghanistan</option>
                            <option value=\"Afrique du Sud\" ";
        // line 87
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Afrique du Sud")) {
            echo " selected ";
        }
        echo ">Afrique du Sud</option>
                            <option value=\"Albanie\" ";
        // line 88
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Albanie")) {
            echo " selected ";
        }
        echo ">Albanie</option>
                            <option value=\"Algérie\" ";
        // line 89
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Algérie")) {
            echo " selected ";
        }
        echo ">Algérie</option>
                            <option value=\"Allemagne\" ";
        // line 90
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Allemagne")) {
            echo " selected ";
        }
        echo ">Allemagne</option>
                            <option value=\"Andorre\" ";
        // line 91
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Andorre")) {
            echo " selected ";
        }
        echo ">Andorre</option>
                            <option value=\"Angola\" ";
        // line 92
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Angola")) {
            echo " selected ";
        }
        echo ">Angola</option>
                            <option value=\"Anguilla\" ";
        // line 93
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Anguilla")) {
            echo " selected ";
        }
        echo ">Anguilla</option>
                            <option value=\"Antarctique\" ";
        // line 94
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Antarctique")) {
            echo " selected ";
        }
        echo ">Antarctique</option>
                            <option value=\"Antigua et Barbuda\" ";
        // line 95
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Antigua et Barbuda")) {
            echo " selected ";
        }
        echo ">Antigua-et-Barbuda</option>
                            <option value=\"Arabie saoudite\" ";
        // line 96
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Arabie saoudite")) {
            echo " selected ";
        }
        echo ">Arabie saoudite</option>
                            <option value=\"Argentine\" ";
        // line 97
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Argentine")) {
            echo " selected ";
        }
        echo ">Argentine</option>
                            <option value=\"Arménie\" ";
        // line 98
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Arménie")) {
            echo " selected ";
        }
        echo ">Arménie</option>
                            <option value=\"Aruba\" ";
        // line 99
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Aruba")) {
            echo " selected ";
        }
        echo ">Aruba</option>
                            <option value=\"Australie\" ";
        // line 100
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Australie")) {
            echo " selected ";
        }
        echo ">Australie</option>
                            <option value=\"Autriche\" ";
        // line 101
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Autriche")) {
            echo " selected ";
        }
        echo ">Autriche</option>
                            <option value=\"Azerbaïdjan\" ";
        // line 102
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Azerbaïdjan")) {
            echo " selected ";
        }
        echo ">Azerbaïdjan</option>
                            <option value=\"Bahamas\" ";
        // line 103
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Bahamas")) {
            echo " selected ";
        }
        echo ">Bahamas</option>
                            <option value=\"Bahreïn\" ";
        // line 104
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Bahreïn")) {
            echo " selected ";
        }
        echo ">Bahreïn</option>
                            <option value=\"Bangladesh\" ";
        // line 105
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Bangladesh")) {
            echo " selected ";
        }
        echo ">Bangladesh</option>
                            <option value=\"Barbade\" ";
        // line 106
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Barbade")) {
            echo " selected ";
        }
        echo ">Barbade</option>
                            <option value=\"Belgique\" ";
        // line 107
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Belgique")) {
            echo " selected ";
        }
        echo ">Belgique</option>
                            <option value=\"Belize\" ";
        // line 108
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Belize")) {
            echo " selected ";
        }
        echo ">Belize</option>
                            <option value=\"Bénin\" ";
        // line 109
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Bénin")) {
            echo " selected ";
        }
        echo ">Bénin</option>
                            <option value=\"Bermudes\" ";
        // line 110
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Bermudes")) {
            echo " selected ";
        }
        echo ">Bermudes</option>
                            <option value=\"Bhoutan\" ";
        // line 111
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Bhoutan")) {
            echo " selected ";
        }
        echo ">Bhoutan</option>
                            <option value=\"Biélorussie\" ";
        // line 112
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Biélorussie")) {
            echo " selected ";
        }
        echo ">Biélorussie</option>
                            <option value=\"Bolivie\" ";
        // line 113
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Bolivie")) {
            echo " selected ";
        }
        echo ">Bolivie</option>
                            <option value=\"Bosnie Herzégovine\" ";
        // line 114
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Bosnie Herzégovine")) {
            echo " selected ";
        }
        echo ">Bosnie-Herzégovine</option>
                            <option value=\"Botswana\" ";
        // line 115
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Botswana")) {
            echo " selected ";
        }
        echo ">Botswana</option>
                            <option value=\"Brésil\" ";
        // line 116
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Brésil")) {
            echo " selected ";
        }
        echo ">Brésil</option>
                            <option value=\"Brunéi Darussalam\" ";
        // line 117
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Brunéi Darussalam")) {
            echo " selected ";
        }
        echo ">Brunéi Darussalam</option>
                            <option value=\"Bulgarie\" ";
        // line 118
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Bulgarie")) {
            echo " selected ";
        }
        echo ">Bulgarie</option>
                            <option value=\"Burkina Faso\" ";
        // line 119
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Burkina Faso")) {
            echo " selected ";
        }
        echo ">Burkina Faso</option>
                            <option value=\"Burundi\" ";
        // line 120
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Burundi")) {
            echo " selected ";
        }
        echo ">Burundi</option>
                            <option value=\"Cambodge\" ";
        // line 121
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Cambodge")) {
            echo " selected ";
        }
        echo ">Cambodge</option>
                            <option value=\"Cameroun\" ";
        // line 122
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Cameroun")) {
            echo " selected ";
        }
        echo ">Cameroun</option>
                            <option value=\"Canada\" ";
        // line 123
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Canada")) {
            echo " selected ";
        }
        echo ">Canada</option>
                            <option value=\"Cap Vert\" ";
        // line 124
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Cap Vert")) {
            echo " selected ";
        }
        echo ">Cap-Vert</option>
                            <option value=\"Ceuta et Melilla\" ";
        // line 125
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Ceuta et Melilla")) {
            echo " selected ";
        }
        echo ">Ceuta et Melilla</option>
                            <option value=\"Chili\" ";
        // line 126
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Chili")) {
            echo " selected ";
        }
        echo ">Chili</option>
                            <option value=\"Chine\" ";
        // line 127
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Chine")) {
            echo " selected ";
        }
        echo ">Chine</option>
                            <option value=\"Chypre\" ";
        // line 128
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Chypre")) {
            echo " selected ";
        }
        echo ">Chypre</option>
                            <option value=\"Colombie\" ";
        // line 129
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Colombie")) {
            echo " selected ";
        }
        echo ">Colombie</option>
                            <option value=\"Comores\" ";
        // line 130
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Comores")) {
            echo " selected ";
        }
        echo ">Comores</option>
                            <option value=\"Congo Brazzaville\" ";
        // line 131
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Congo Brazzaville")) {
            echo " selected ";
        }
        echo ">Congo-Brazzaville</option>
                            <option value=\"Congo Kinshasa\" ";
        // line 132
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Congo Kinshasa")) {
            echo " selected ";
        }
        echo ">Congo-Kinshasa</option>
                            <option value=\"Corée du Nord\" ";
        // line 133
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Corée du Nord")) {
            echo " selected ";
        }
        echo ">Corée du Nord</option>
                            <option value=\"Corée du Sud\" ";
        // line 134
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Corée du Sud")) {
            echo " selected ";
        }
        echo ">Corée du Sud</option>
                            <option value=\"Costa Rica\" ";
        // line 135
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Costa Rica")) {
            echo " selected ";
        }
        echo ">Costa Rica</option>
                            <option value=\"Côte d’Ivoire\" ";
        // line 136
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Côte d’Ivoire")) {
            echo " selected ";
        }
        echo ">Côte d’Ivoire</option>
                            <option value=\"Croatie\" ";
        // line 137
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Croatie")) {
            echo " selected ";
        }
        echo ">Croatie</option>
                            <option value=\"Cuba\" ";
        // line 138
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Cuba")) {
            echo " selected ";
        }
        echo ">Cuba</option>
                            <option value=\"Curaçao\" ";
        // line 139
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Curaçao")) {
            echo " selected ";
        }
        echo ">Curaçao</option>
                            <option value=\"Danemark\" ";
        // line 140
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Danemark")) {
            echo " selected ";
        }
        echo ">Danemark</option>
                            <option value=\"Diego Garcia\" ";
        // line 141
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Diego Garcia")) {
            echo " selected ";
        }
        echo ">Diego Garcia</option>
                            <option value=\"Djibouti\" ";
        // line 142
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Djibouti")) {
            echo " selected ";
        }
        echo ">Djibouti</option>
                            <option value=\"Dominique\" ";
        // line 143
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Dominique")) {
            echo " selected ";
        }
        echo ">Dominique</option>
                            <option value=\"Égypte\" ";
        // line 144
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Égypte")) {
            echo " selected ";
        }
        echo ">Égypte</option>
                            <option value=\"El Salvador\" ";
        // line 145
        if (((isset($context["loca"]) ? $context["loca"] : null) == "El Salvador")) {
            echo " selected ";
        }
        echo ">El Salvador</option>
                            <option value=\"Émirats arabes unis\" ";
        // line 146
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Émirats arabes unis")) {
            echo " selected ";
        }
        echo ">Émirats arabes unis</option>
                            <option value=\"Équateur\" ";
        // line 147
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Équateur")) {
            echo " selected ";
        }
        echo ">Équateur</option>
                            <option value=\"Érythrée\" ";
        // line 148
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Érythrée")) {
            echo " selected ";
        }
        echo ">Érythrée</option>
                            <option value=\"Espagne\" ";
        // line 149
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Espagne")) {
            echo " selected ";
        }
        echo ">Espagne</option>
                            <option value=\"Estonie\" ";
        // line 150
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Estonie")) {
            echo " selected ";
        }
        echo ">Estonie</option>
                            <option value=\"État de la Cité du Vatican\" ";
        // line 151
        if (((isset($context["loca"]) ? $context["loca"] : null) == "État de la Cité du Vatican")) {
            echo " selected ";
        }
        echo ">État de la Cité du Vatican</option>
                            <option value=\"États fédérés de Micronésie\" ";
        // line 152
        if (((isset($context["loca"]) ? $context["loca"] : null) == "États fédérés de Micronésie")) {
            echo " selected ";
        }
        echo ">États fédérés de Micronésie</option>
                            <option value=\"États-Unis\" ";
        // line 153
        if (((isset($context["loca"]) ? $context["loca"] : null) == "États-Unis")) {
            echo " selected ";
        }
        echo ">États-Unis</option>
                            <option value=\"Éthiopie\" ";
        // line 154
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Éthiopie")) {
            echo " selected ";
        }
        echo ">Éthiopie</option>
                            <option value=\"Fidji\" ";
        // line 155
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Fidji")) {
            echo " selected ";
        }
        echo ">Fidji</option>
                            <option value=\"Finlande\" ";
        // line 156
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Finlande")) {
            echo " selected ";
        }
        echo ">Finlande</option>
                            <option value=\"France\" ";
        // line 157
        if (((isset($context["loca"]) ? $context["loca"] : null) == "France")) {
            echo " selected ";
        }
        echo ">France</option>
                            <option value=\"Gabon\" ";
        // line 158
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Gabon")) {
            echo " selected ";
        }
        echo ">Gabon</option>
                            <option value=\"Gambie\" ";
        // line 159
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Gambie")) {
            echo " selected ";
        }
        echo ">Gambie</option>
                            <option value=\"Géorgie\" ";
        // line 160
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Géorgie")) {
            echo " selected ";
        }
        echo ">Géorgie</option>
                            <option value=\"Ghana\" ";
        // line 161
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Ghana")) {
            echo " selected ";
        }
        echo ">Ghana</option>
                            <option value=\"Gibraltar\" ";
        // line 162
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Gibraltar")) {
            echo " selected ";
        }
        echo ">Gibraltar</option>
                            <option value=\"Grèce\" ";
        // line 163
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Grèce")) {
            echo " selected ";
        }
        echo ">Grèce</option>
                            <option value=\"Grenade\" ";
        // line 164
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Grenade")) {
            echo " selected ";
        }
        echo ">Grenade</option>
                            <option value=\"Groenland\" ";
        // line 165
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Groenland")) {
            echo " selected ";
        }
        echo ">Groenland</option>
                            <option value=\"Guadeloupe\" ";
        // line 166
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Guadeloupe")) {
            echo " selected ";
        }
        echo ">Guadeloupe</option>
                            <option value=\"Guam\" ";
        // line 167
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Guam")) {
            echo " selected ";
        }
        echo ">Guam</option>
                            <option value=\"Guatemala\" ";
        // line 168
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Guatemala")) {
            echo " selected ";
        }
        echo ">Guatemala</option>
                            <option value=\"Guernesey\" ";
        // line 169
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Guernesey")) {
            echo " selected ";
        }
        echo ">Guernesey</option>
                            <option value=\"Guinée\" ";
        // line 170
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Guinée")) {
            echo " selected ";
        }
        echo ">Guinée</option>
                            <option value=\"Guinée équatoriale\" ";
        // line 171
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Guinée équatoriale")) {
            echo " selected ";
        }
        echo ">Guinée équatoriale</option>
                            <option value=\"Guinée Bissau\" ";
        // line 172
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Guinée Bissau")) {
            echo " selected ";
        }
        echo ">Guinée-Bissau</option>
                            <option value=\"Guyana\" ";
        // line 173
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Guyana")) {
            echo " selected ";
        }
        echo ">Guyana</option>
                            <option value=\"Guyane française\" ";
        // line 174
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Guyane française")) {
            echo " selected ";
        }
        echo ">Guyane française</option>
                            <option value=\"Haïti\" ";
        // line 175
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Haïti")) {
            echo " selected ";
        }
        echo ">Haïti</option>
                            <option value=\"Honduras\" ";
        // line 176
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Honduras")) {
            echo " selected ";
        }
        echo ">Honduras</option>
                            <option value=\"Hongrie\" ";
        // line 177
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Hongrie")) {
            echo " selected ";
        }
        echo ">Hongrie</option>
                            <option value=\"Île Christmas\" ";
        // line 178
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Île Christmas")) {
            echo " selected ";
        }
        echo ">Île Christmas</option>
                            <option value=\"Île de l’Ascension\" ";
        // line 179
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Île de l’Ascension")) {
            echo " selected ";
        }
        echo ">Île de l’Ascension</option>
                            <option value=\"Île de Man\" ";
        // line 180
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Île de Man")) {
            echo " selected ";
        }
        echo ">Île de Man</option>
                            <option value=\"Île Norfolk\" ";
        // line 181
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Île Norfolk")) {
            echo " selected ";
        }
        echo ">Île Norfolk</option>
                            <option value=\"Îles Åland\" ";
        // line 182
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Åland")) {
            echo " selected ";
        }
        echo ">Îles Åland</option>
                            <option value=\"Îles Caïmans\" ";
        // line 183
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Caïmans")) {
            echo " selected ";
        }
        echo ">Îles Caïmans</option>
                            <option value=\"Îles Canaries\" ";
        // line 184
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Canaries")) {
            echo " selected ";
        }
        echo ">Îles Canaries</option>
                            <option value=\"Îles Cocos\" ";
        // line 185
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Cocos")) {
            echo " selected ";
        }
        echo ">Îles Cocos</option>
                            <option value=\"Îles Cook\" ";
        // line 186
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Cook")) {
            echo " selected ";
        }
        echo ">Îles Cook</option>
                            <option value=\"Îles Féroé\" ";
        // line 187
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Féroé")) {
            echo " selected ";
        }
        echo ">Îles Féroé</option>
                            <option value=\"Îles Géorgie du Sud et Sandwich du Sud\" ";
        // line 188
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Géorgie du Sud et Sandwich du Sud")) {
            echo " selected ";
        }
        echo ">Îles Géorgie du Sud et Sandwich du Sud</option>
                            <option value=\"Îles Malouines\" ";
        // line 189
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Malouines")) {
            echo " selected ";
        }
        echo ">Îles Malouines</option>
                            <option value=\"Îles Mariannes du Nord\" ";
        // line 190
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Mariannes du Nord")) {
            echo " selected ";
        }
        echo ">Îles Mariannes du Nord</option>
                            <option value=\"Îles Marshall\" ";
        // line 191
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Marshall")) {
            echo " selected ";
        }
        echo ">Îles Marshall</option>
                            <option value=\"Îles mineures éloignées des États-Unis\" ";
        // line 192
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles mineures éloignées des États-Unis")) {
            echo " selected ";
        }
        echo ">Îles mineures éloignées des États-Unis</option>
                            <option value=\"Îles Salomon\" ";
        // line 193
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Salomon")) {
            echo " selected ";
        }
        echo ">Îles Salomon</option>
                            <option value=\"Îles Turques et Caïques\" ";
        // line 194
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Turques et Caïques")) {
            echo " selected ";
        }
        echo ">Îles Turques-et-Caïques</option>
                            <option value=\"Îles Vierges britanniques\" ";
        // line 195
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Vierges britanniques")) {
            echo " selected ";
        }
        echo ">Îles Vierges britanniques</option>
                            <option value=\"Îles Vierges des États Unis\" ";
        // line 196
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Îles Vierges des États Unis")) {
            echo " selected ";
        }
        echo ">Îles Vierges des États-Unis</option>
                            <option value=\"Inde\" ";
        // line 197
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Inde")) {
            echo " selected ";
        }
        echo ">Inde</option>
                            <option value=\"Indonésie\" ";
        // line 198
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Indonésie")) {
            echo " selected ";
        }
        echo ">Indonésie</option>
                            <option value=\"Irak\" ";
        // line 199
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Irak")) {
            echo " selected ";
        }
        echo ">Irak</option>
                            <option value=\"Iran\" ";
        // line 200
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Iran")) {
            echo " selected ";
        }
        echo ">Iran</option>
                            <option value=\"Irlande\" ";
        // line 201
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Irlande")) {
            echo " selected ";
        }
        echo ">Irlande</option>
                            <option value=\"Islande\" ";
        // line 202
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Islande")) {
            echo " selected ";
        }
        echo ">Islande</option>
                            <option value=\"Italie\" ";
        // line 203
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Italie")) {
            echo " selected ";
        }
        echo ">Italie</option>
                            <option value=\"Jamaïque\" ";
        // line 204
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Jamaïque")) {
            echo " selected ";
        }
        echo ">Jamaïque</option>
                            <option value=\"Japon\" ";
        // line 205
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Japon")) {
            echo " selected ";
        }
        echo ">Japon</option>
                            <option value=\"Jersey\" ";
        // line 206
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Jersey")) {
            echo " selected ";
        }
        echo ">Jersey</option>
                            <option value=\"Jordanie\" ";
        // line 207
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Jordanie")) {
            echo " selected ";
        }
        echo ">Jordanie</option>
                            <option value=\"Kazakhstan\" ";
        // line 208
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Kazakhstan")) {
            echo " selected ";
        }
        echo ">Kazakhstan</option>
                            <option value=\"Kenya\" ";
        // line 209
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Kenya")) {
            echo " selected ";
        }
        echo ">Kenya</option>
                            <option value=\"Kirghizistan\" ";
        // line 210
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Kirghizistan")) {
            echo " selected ";
        }
        echo ">Kirghizistan</option>
                            <option value=\"Kiribati\" ";
        // line 211
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Kiribati")) {
            echo " selected ";
        }
        echo ">Kiribati</option>
                            <option value=\"Kosovo\" ";
        // line 212
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Kosovo")) {
            echo " selected ";
        }
        echo ">Kosovo</option>
                            <option value=\"Koweït\" ";
        // line 213
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Koweït")) {
            echo " selected ";
        }
        echo ">Koweït</option>
                            <option value=\"La Réunion\" ";
        // line 214
        if (((isset($context["loca"]) ? $context["loca"] : null) == "La Réunion")) {
            echo " selected ";
        }
        echo ">La Réunion</option>
                            <option value=\"Laos\" ";
        // line 215
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Laos")) {
            echo " selected ";
        }
        echo ">Laos</option>
                            <option value=\"Lesotho\" ";
        // line 216
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Lesotho")) {
            echo " selected ";
        }
        echo ">Lesotho</option>
                            <option value=\"Lettonie\" ";
        // line 217
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Lettonie")) {
            echo " selected ";
        }
        echo ">Lettonie</option>
                            <option value=\"Liban\" ";
        // line 218
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Liban")) {
            echo " selected ";
        }
        echo ">Liban</option>
                            <option value=\"Libéria\" ";
        // line 219
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Libéria")) {
            echo " selected ";
        }
        echo ">Libéria</option>
                            <option value=\"Libye\" ";
        // line 220
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Libye")) {
            echo " selected ";
        }
        echo ">Libye</option>
                            <option value=\"Liechtenstein\" ";
        // line 221
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Liechtenstein")) {
            echo " selected ";
        }
        echo ">Liechtenstein</option>
                            <option value=\"Lituanie\" ";
        // line 222
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Lituanie")) {
            echo " selected ";
        }
        echo ">Lituanie</option>
                            <option value=\"Luxembourg\" ";
        // line 223
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Luxembourg")) {
            echo " selected ";
        }
        echo ">Luxembourg</option>
                            <option value=\"Macédoine\" ";
        // line 224
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Macédoine")) {
            echo " selected ";
        }
        echo ">Macédoine</option>
                            <option value=\"Madagascar\" ";
        // line 225
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Madagascar")) {
            echo " selected ";
        }
        echo ">Madagascar</option>
                            <option value=\"Malaisie\" ";
        // line 226
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Malaisie")) {
            echo " selected ";
        }
        echo ">Malaisie</option>
                            <option value=\"Malawi\" ";
        // line 227
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Malawi")) {
            echo " selected ";
        }
        echo ">Malawi</option>
                            <option value=\"Maldives\" ";
        // line 228
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Maldives")) {
            echo " selected ";
        }
        echo ">Maldives</option>
                            <option value=\"Mali\" ";
        // line 229
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Mali")) {
            echo " selected ";
        }
        echo ">Mali</option>
                            <option value=\"Malte\" ";
        // line 230
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Malte")) {
            echo " selected ";
        }
        echo ">Malte</option>
                            <option value=\"Maroc\" ";
        // line 231
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Maroc")) {
            echo " selected ";
        }
        echo ">Maroc</option>
                            <option value=\"Martinique\" ";
        // line 232
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Martinique")) {
            echo " selected ";
        }
        echo ">Martinique</option>
                            <option value=\"Maurice\" ";
        // line 233
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Maurice")) {
            echo " selected ";
        }
        echo ">Maurice</option>
                            <option value=\"Mauritanie\" ";
        // line 234
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Mauritanie")) {
            echo " selected ";
        }
        echo ">Mauritanie</option>
                            <option value=\"Mayotte\" ";
        // line 235
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Mayotte")) {
            echo " selected ";
        }
        echo ">Mayotte</option>
                            <option value=\"Mexique\" ";
        // line 236
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Mexique")) {
            echo " selected ";
        }
        echo ">Mexique</option>
                            <option value=\"Moldavie\" ";
        // line 237
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Moldavie")) {
            echo " selected ";
        }
        echo ">Moldavie</option>
                            <option value=\"Monaco\" ";
        // line 238
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Monaco")) {
            echo " selected ";
        }
        echo ">Monaco</option>
                            <option value=\"Mongolie\" ";
        // line 239
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Mongolie")) {
            echo " selected ";
        }
        echo ">Mongolie</option>
                            <option value=\"Monténégro\" ";
        // line 240
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Monténégro")) {
            echo " selected ";
        }
        echo ">Monténégro</option>
                            <option value=\"Montserrat\" ";
        // line 241
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Montserrat")) {
            echo " selected ";
        }
        echo ">Montserrat</option>
                            <option value=\"Mozambique\" ";
        // line 242
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Mozambique")) {
            echo " selected ";
        }
        echo ">Mozambique</option>
                            <option value=\"Myanmar\" ";
        // line 243
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Myanmar")) {
            echo " selected ";
        }
        echo ">Myanmar</option>
                            <option value=\"Namibie\" ";
        // line 244
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Namibie")) {
            echo " selected ";
        }
        echo ">Namibie</option>
                            <option value=\"Nauru\" ";
        // line 245
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Nauru")) {
            echo " selected ";
        }
        echo ">Nauru</option>
                            <option value=\"Népal\" ";
        // line 246
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Népal")) {
            echo " selected ";
        }
        echo ">Népal</option>
                            <option value=\"Nicaragua\" ";
        // line 247
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Nicaragua")) {
            echo " selected ";
        }
        echo ">Nicaragua</option>
                            <option value=\"Niger\" ";
        // line 248
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Niger")) {
            echo " selected ";
        }
        echo ">Niger</option>
                            <option value=\"Nigéria\" ";
        // line 249
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Nigéria")) {
            echo " selected ";
        }
        echo ">Nigéria</option>
                            <option value=\"Niue\" ";
        // line 250
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Niue")) {
            echo " selected ";
        }
        echo ">Niue</option>
                            <option value=\"Norvège\" ";
        // line 251
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Norvège")) {
            echo " selected ";
        }
        echo ">Norvège</option>
                            <option value=\"Nouvelle Calédonie\" ";
        // line 252
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Nouvelle Calédonie")) {
            echo " selected ";
        }
        echo ">Nouvelle-Calédonie</option>
                            <option value=\"Nouvelle Zélande\" ";
        // line 253
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Nouvelle Zélande")) {
            echo " selected ";
        }
        echo ">Nouvelle-Zélande</option>
                            <option value=\"Oman\" ";
        // line 254
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Oman")) {
            echo " selected ";
        }
        echo ">Oman</option>
                            <option value=\"Ouganda\" ";
        // line 255
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Ouganda")) {
            echo " selected ";
        }
        echo ">Ouganda</option>
                            <option value=\"Ouzbékistan\" ";
        // line 256
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Ouzbékistan")) {
            echo " selected ";
        }
        echo ">Ouzbékistan</option>
                            <option value=\"Pakistan\" ";
        // line 257
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Pakistan")) {
            echo " selected ";
        }
        echo ">Pakistan</option>
                            <option value=\"Palaos\" ";
        // line 258
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Palaos")) {
            echo " selected ";
        }
        echo ">Palaos</option>
                            <option value=\"Panama\" ";
        // line 259
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Panama")) {
            echo " selected ";
        }
        echo ">Panama</option>
                            <option value=\"Papouasie Nouvelle Guinée\" ";
        // line 260
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Papouasie Nouvelle Guinée")) {
            echo " selected ";
        }
        echo ">Papouasie-Nouvelle-Guinée</option>
                            <option value=\"Paraguay\" ";
        // line 261
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Paraguay")) {
            echo " selected ";
        }
        echo ">Paraguay</option>
                            <option value=\"Pays Bas\" ";
        // line 262
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Pays Bas")) {
            echo " selected ";
        }
        echo ">Pays-Bas</option>
                            <option value=\"Pays Bas caribéens\" ";
        // line 263
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Pays Bas caribéens")) {
            echo " selected ";
        }
        echo ">Pays-Bas caribéens</option>
                            <option value=\"Pérou\" ";
        // line 264
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Pérou")) {
            echo " selected ";
        }
        echo ">Pérou</option>
                            <option value=\"Philippines\" ";
        // line 265
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Philippines")) {
            echo " selected ";
        }
        echo ">Philippines</option>
                            <option value=\"Pitcairn\" ";
        // line 266
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Pitcairn")) {
            echo " selected ";
        }
        echo ">Pitcairn</option>
                            <option value=\"Pologne\" ";
        // line 267
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Pologne")) {
            echo " selected ";
        }
        echo ">Pologne</option>
                            <option value=\"Polynésie française\" ";
        // line 268
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Polynésie française")) {
            echo " selected ";
        }
        echo ">Polynésie française</option>
                            <option value=\"Porto Rico\" ";
        // line 269
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Porto Rico")) {
            echo " selected ";
        }
        echo ">Porto Rico</option>
                            <option value=\"Portugal\" ";
        // line 270
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Portugal")) {
            echo " selected ";
        }
        echo ">Portugal</option>
                            <option value=\"Qatar\" ";
        // line 271
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Qatar")) {
            echo " selected ";
        }
        echo ">Qatar</option>
                            <option value=\"R.A.S. chinoise de Hong Kong\" ";
        // line 272
        if (((isset($context["loca"]) ? $context["loca"] : null) == "R.A.S. chinoise de Hong Kong")) {
            echo " selected ";
        }
        echo ">R.A.S. chinoise de Hong Kong</option>
                            <option value=\"R.A.S. chinoise de Macao\" ";
        // line 273
        if (((isset($context["loca"]) ? $context["loca"] : null) == "R.A.S. chinoise de Macao")) {
            echo " selected ";
        }
        echo ">R.A.S. chinoise de Macao</option>
                            <option value=\"République centrafricaine\" ";
        // line 274
        if (((isset($context["loca"]) ? $context["loca"] : null) == "République centrafricaine")) {
            echo " selected ";
        }
        echo ">République centrafricaine</option>
                            <option value=\"République dominicaine\" ";
        // line 275
        if (((isset($context["loca"]) ? $context["loca"] : null) == "République dominicaine")) {
            echo " selected ";
        }
        echo ">République dominicaine</option>
                            <option value=\"République tchèque\" ";
        // line 276
        if (((isset($context["loca"]) ? $context["loca"] : null) == "République tchèque")) {
            echo " selected ";
        }
        echo ">République tchèque</option>
                            <option value=\"Roumanie\" ";
        // line 277
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Roumanie")) {
            echo " selected ";
        }
        echo ">Roumanie</option>
                            <option value=\"Royaume Uni\" ";
        // line 278
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Royaume Uni")) {
            echo " selected ";
        }
        echo ">Royaume-Uni</option>
                            <option value=\"Russie\" ";
        // line 279
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Russie")) {
            echo " selected ";
        }
        echo ">Russie</option>
                            <option value=\"Rwanda\" ";
        // line 280
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Rwanda")) {
            echo " selected ";
        }
        echo ">Rwanda</option>
                            <option value=\"Sahara occidental\" ";
        // line 281
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Sahara occidental")) {
            echo " selected ";
        }
        echo ">Sahara occidental</option>
                            <option value=\"Saint Barthélemy\" ";
        // line 282
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Saint Barthélemy")) {
            echo " selected ";
        }
        echo ">Saint-Barthélemy</option>
                            <option value=\"Saint Christophe et Niévès\" ";
        // line 283
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Saint Christophe et Niévès")) {
            echo " selected ";
        }
        echo ">Saint-Christophe-et-Niévès</option>
                            <option value=\"Saint Marin\" ";
        // line 284
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Saint-Marin")) {
            echo " selected ";
        }
        echo ">Saint-Marin</option>
                            <option value=\"Saint Martin (partie française)\" ";
        // line 285
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Saint Martin (partie française)")) {
            echo " selected ";
        }
        echo ">Saint-Martin (partie française)</option>
                            <option value=\"Saint Martin (partie néerlandaise)\" ";
        // line 286
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Saint Martin (partie néerlandaise)")) {
            echo " selected ";
        }
        echo ">Saint-Martin (partie néerlandaise)</option>
                            <option value=\"Saint Pierre et Miquelon\" ";
        // line 287
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Saint Pierre et Miquelon")) {
            echo " selected ";
        }
        echo ">Saint-Pierre-et-Miquelon</option>
                            <option value=\"Saint Vincent et les Grenadines\" ";
        // line 288
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Saint Vincent et les Grenadines")) {
            echo " selected ";
        }
        echo ">Saint-Vincent-et-les-Grenadines</option>
                            <option value=\"Sainte Hélène\" ";
        // line 289
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Sainte Hélène")) {
            echo " selected ";
        }
        echo ">Sainte-Hélène</option>
                            <option value=\"Sainte Lucie\" ";
        // line 290
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Sainte Lucie")) {
            echo " selected ";
        }
        echo ">Sainte-Lucie</option>
                            <option value=\"Samoa\" ";
        // line 291
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Samoa")) {
            echo " selected ";
        }
        echo ">Samoa</option>
                            <option value=\"Samoa américaines\" ";
        // line 292
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Samoa américaines")) {
            echo " selected ";
        }
        echo ">Samoa américaines</option>
                            <option value=\"Sao Tomé et Principe\" ";
        // line 293
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Sao Tomé et Principe")) {
            echo " selected ";
        }
        echo ">Sao Tomé-et-Principe</option>
                            <option value=\"Sénégal\" ";
        // line 294
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Sénégal")) {
            echo " selected ";
        }
        echo ">Sénégal</option>
                            <option value=\"Serbie\" ";
        // line 295
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Serbie")) {
            echo " selected ";
        }
        echo ">Serbie</option>
                            <option value=\"Seychelles\" ";
        // line 296
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Seychelles")) {
            echo " selected ";
        }
        echo ">Seychelles</option>
                            <option value=\"Sierra Leone\" ";
        // line 297
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Sierra Leone")) {
            echo " selected ";
        }
        echo ">Sierra Leone</option>
                            <option value=\"Singapour\" ";
        // line 298
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Singapour")) {
            echo " selected ";
        }
        echo ">Singapour</option>
                            <option value=\"Slovaquie\" ";
        // line 299
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Slovaquie")) {
            echo " selected ";
        }
        echo ">Slovaquie</option>
                            <option value=\"Slovénie\" ";
        // line 300
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Slovénie")) {
            echo " selected ";
        }
        echo ">Slovénie</option>
                            <option value=\"Somalie\" ";
        // line 301
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Somalie")) {
            echo " selected ";
        }
        echo ">Somalie</option>
                            <option value=\"Soudan\" ";
        // line 302
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Soudan")) {
            echo " selected ";
        }
        echo ">Soudan</option>
                            <option value=\"Soudan du Sud\" ";
        // line 303
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Soudan du Sud")) {
            echo " selected ";
        }
        echo ">Soudan du Sud</option>
                            <option value=\"Sri Lanka\" ";
        // line 304
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Sri Lanka")) {
            echo " selected ";
        }
        echo ">Sri Lanka</option>
                            <option value=\"Suède\" ";
        // line 305
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Suède")) {
            echo " selected ";
        }
        echo ">Suède</option>
                            <option value=\"Suisse\" ";
        // line 306
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Suisse")) {
            echo " selected ";
        }
        echo ">Suisse</option>
                            <option value=\"Suriname\" ";
        // line 307
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Suriname")) {
            echo " selected ";
        }
        echo ">Suriname</option>
                            <option value=\"Svalbard et Jan Mayen\" ";
        // line 308
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Svalbard et Jan Mayen")) {
            echo " selected ";
        }
        echo ">Svalbard et Jan Mayen</option>
                            <option value=\"Swaziland\" ";
        // line 309
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Swaziland")) {
            echo " selected ";
        }
        echo ">Swaziland</option>
                            <option value=\"Syrie\" ";
        // line 310
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Syrie")) {
            echo " selected ";
        }
        echo ">Syrie</option>
                            <option value=\"Tadjikistan\" ";
        // line 311
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Tadjikistan")) {
            echo " selected ";
        }
        echo ">Tadjikistan</option>
                            <option value=\"Taïwan\" ";
        // line 312
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Taïwan")) {
            echo " selected ";
        }
        echo ">Taïwan</option>
                            <option value=\"Tanzanie\" ";
        // line 313
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Tanzanie")) {
            echo " selected ";
        }
        echo ">Tanzanie</option>
                            <option value=\"Tchad\" ";
        // line 314
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Tchad")) {
            echo " selected ";
        }
        echo ">Tchad</option>
                            <option value=\"Terres australes françaises\" ";
        // line 315
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Terres australes françaises")) {
            echo " selected ";
        }
        echo ">Terres australes françaises</option>
                            <option value=\"Territoire britannique de l’océan Indien\" ";
        // line 316
        if (((isset($context["loca"]) ? $context["loca"] : null) == "IO")) {
            echo " selected ";
        }
        echo ">Territoire britannique de l’océan Indien</option>
                            <option value=\"Territoires palestiniens\" ";
        // line 317
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Territoire britannique de l’océan Indien")) {
            echo " selected ";
        }
        echo ">Territoires palestiniens</option>
                            <option value=\"Thaïlande\" ";
        // line 318
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Thaïlande")) {
            echo " selected ";
        }
        echo ">Thaïlande</option>
                            <option value=\"Timor oriental\" ";
        // line 319
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Timor oriental")) {
            echo " selected ";
        }
        echo ">Timor oriental</option>
                            <option value=\"Togo\" ";
        // line 320
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Togo")) {
            echo " selected ";
        }
        echo ">Togo</option>
                            <option value=\"Tokelau\" ";
        // line 321
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Tokelau")) {
            echo " selected ";
        }
        echo ">Tokelau</option>
                            <option value=\"Tonga\" ";
        // line 322
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Tonga")) {
            echo " selected ";
        }
        echo ">Tonga</option>
                            <option value=\"Trinité et Tobago\" ";
        // line 323
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Trinité et Tobago")) {
            echo " selected ";
        }
        echo ">Trinité-et-Tobago</option>
                            <option value=\"Tristan da Cunha\" ";
        // line 324
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Tristan da Cunha")) {
            echo " selected ";
        }
        echo ">Tristan da Cunha</option>
                            <option value=\"Tunisie\" ";
        // line 325
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Tunisie")) {
            echo " selected ";
        }
        echo ">Tunisie</option>
                            <option value=\"Turkménistan\" ";
        // line 326
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Turkménistan")) {
            echo " selected ";
        }
        echo ">Turkménistan</option>
                            <option value=\"Turquie\" ";
        // line 327
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Turquie")) {
            echo " selected ";
        }
        echo ">Turquie</option>
                            <option value=\"Tuvalu\" ";
        // line 328
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Tuvalu")) {
            echo " selected ";
        }
        echo ">Tuvalu</option>
                            <option value=\"Ukraine\" ";
        // line 329
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Ukraine")) {
            echo " selected ";
        }
        echo ">Ukraine</option>
                            <option value=\"Uruguay\" ";
        // line 330
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Uruguay")) {
            echo " selected ";
        }
        echo ">Uruguay</option>
                            <option value=\"Vanuatu\" ";
        // line 331
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Vanuatu")) {
            echo " selected ";
        }
        echo ">Vanuatu</option>
                            <option value=\"Venezuela\" ";
        // line 332
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Venezuela")) {
            echo " selected ";
        }
        echo ">Venezuela</option>
                            <option value=\"Vietnam\" ";
        // line 333
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Vietnam")) {
            echo " selected ";
        }
        echo ">Vietnam</option>
                            <option value=\"Wallis et Futuna\" ";
        // line 334
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Wallis et Futuna")) {
            echo " selected ";
        }
        echo ">Wallis-et-Futuna</option>
                            <option value=\"Yémen\" ";
        // line 335
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Yémen")) {
            echo " selected ";
        }
        echo ">Yémen</option>
                            <option value=\"Zambie\" ";
        // line 336
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Zambie")) {
            echo " selected ";
        }
        echo ">Zambie</option>
                            <option value=\"Zimbabwe\" ";
        // line 337
        if (((isset($context["loca"]) ? $context["loca"] : null) == "Zimbabwe")) {
            echo " selected ";
        }
        echo ">Zimbabwe</option>
                        </select>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"\">";
        // line 341
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.adress"), "html", null, true);
        echo ":</div>
                        <input type=\"text\"  class=\"form-control\" name=\"ville\" ";
        // line 342
        if (((isset($context["ville"]) ? $context["ville"] : null) == "")) {
            echo " placeholder=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.adress"), "html", null, true);
            echo "\" ";
        } else {
            echo "value=\"";
            echo twig_escape_filter($this->env, (isset($context["ville"]) ? $context["ville"] : null), "html", null, true);
            echo " ";
        }
        echo "\" id=\"ville\"  />
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"\">Sport:</div>  
                        <select class=\"form-control\" name=\"interest\">
                            <option value=\"\"";
        // line 347
        if (twig_test_empty((isset($context["interestSearch"]) ? $context["interestSearch"] : null))) {
            echo " selected='selected'";
        }
        echo "></option>
                            ";
        // line 348
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["userInterests"]) ? $context["userInterests"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["interest"]) {
            // line 349
            echo "                                <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["interest"], "id", array()), "html", null, true);
            echo "\"";
            if (($this->getAttribute($context["interest"], "id", array()) == (isset($context["interestSearch"]) ? $context["interestSearch"] : null))) {
                echo " selected='selected'";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["interest"], "title", array()), "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['interest'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 351
        echo "                        </select>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"\">";
        // line 354
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("search.rayon"), "html", null, true);
        echo ":</div>
                        <div class=\"rayon\">
                            <label>";
        // line 356
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("search.rayon"), "html", null, true);
        echo "</label>
                            <input type=\"number\" name=\"rayon\" id=\"default\" value=\"";
        // line 357
        echo twig_escape_filter($this->env, (isset($context["rayon"]) ? $context["rayon"] : null), "html", null, true);
        echo "\" >
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6\">
                        <input type=\"checkbox\" ";
        // line 361
        if (((isset($context["handi"]) ? $context["handi"] : null) == 1)) {
            echo "checked";
        }
        echo " name=\"handi\">
                        ";
        // line 362
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("handi.sport"), "html", null, true);
        echo "
                    </div>
                    <div class=\"col-md-10 col-sm-6\"></div>
                    <div class=\"col-md-2 col-sm-6\">
                        <input type=\"submit\" class=\"form-control btn btn-grinta\" value=\"";
        // line 366
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("button.search"), "html", null, true);
        echo "\" />
                    </div>
                </div>
            </form>
        </div>
        <div class=\"result-search\">
            <p class=\"result\">";
        // line 372
        echo twig_escape_filter($this->env, (isset($context["ville"]) ? $context["ville"] : null), "html", null, true);
        echo " ( ";
        echo twig_escape_filter($this->env, (isset($context["ville"]) ? $context["ville"] : null), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, (isset($context["loca"]) ? $context["loca"] : null), "html", null, true);
        echo ") ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("search.rayon"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["rayon"]) ? $context["rayon"] : null), "html", null, true);
        echo " Km ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("rayon.autour"), "html", null, true);
        echo "</p>
            ";
        // line 373
        if ((twig_length_filter($this->env, (isset($context["entities"]) ? $context["entities"] : null)) > 0)) {
            // line 374
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
                // line 375
                echo "                <div class=\"block item-act\">
                    <div class=\"row\">   
                        <div class=\"col-sm-3 col-md-2\">
                            <a href=\"";
                // line 378
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()))), "html", null, true);
                echo "\">
                                <div class=\"img-avatar\"> 
                                    ";
                // line 380
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 381
                    echo "                                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"\">
                                    ";
                } else {
                    // line 383
                    echo "                                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"\">
                                    ";
                }
                // line 385
                echo "                                </div> 
                                <div class=\"name-avatar\">  
                                    <h5 class=\"text-center\">";
                // line 387
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                </div>
                            </a>
                        </div>
                        <div class=\"col-sm-6 text-center\">
                            <h3 class=\"name-match green\">";
                // line 392
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "title", array()), "html", null, true);
                echo "</h3>
                            <div class=\"nb-participant\">
                                ";
                // line 394
                if ((null === $this->getAttribute($context["entity"], "nbPlayer", array()))) {
                    // line 395
                    echo "                                    <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.participants"), "html", null, true);
                    echo "</p>
                                ";
                } else {
                    // line 397
                    echo "                                    <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nbPlayer", array()), "html", null, true);
                    echo "</p>
                                    ";
                    // line 398
                    if (($this->getAttribute($context["entity"], "nbPlayer", array()) == $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"))) {
                        // line 399
                        echo "                                        <p class=\"etat complete\">";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.complet"), "html", null, true);
                        echo "</p>
                                    ";
                    } else {
                        // line 401
                        echo "                                        <p class=\"etat\">";
                        echo twig_escape_filter($this->env, ($this->getAttribute($context["entity"], "nbPlayer", array()) - $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array")), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.dispo"), "html", null, true);
                        echo "</p>
                                    ";
                    }
                    // line 403
                    echo "                                ";
                }
                // line 404
                echo "                            </div>   
                        </div>
                        <div class=\"col-sm-3 info-date\">
                            <ul class=\"list-unstyled\">
                                <li><img src=\"";
                // line 408
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/date.png"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "startdate", array()), "d-m-Y"), "html", null, true);
                echo "</li>
                                <li class=\"lieu\"><img src=\"";
                // line 409
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "locality", array()), "html", null, true);
                echo " ( ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "country", array()), "html", null, true);
                echo ")</li>
                                <li><img src=\"";
                // line 410
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/heure.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "starttime", array()), "H:i"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "endtime", array()), "H:i"), "html", null, true);
                echo "</li>
                            </ul>
                            <a href=\"";
                // line 412
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($context["entity"], "slug", array()))), "html", null, true);
                echo "\" class=\"btn btn-grinta\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fiche.match"), "html", null, true);
                echo "</a>
                        </div>
                    </div>
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 417
            echo "            ";
        } else {
            // line 418
            echo "                <div class=\"msg-aucun\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.aucun.sport"), "html", null, true);
            echo "</div>
            ";
        }
        // line 420
        echo "        </div>
    </div>
  </div>
</div>
<!--Debut containt-->
";
        // line 425
        $this->displayBlock('javascripts', $context, $blocks);
    }

    public function block_javascripts($context, array $blocks = array())
    {
        // line 426
        echo "    <script type=\"text/javascript\">
        \$(\"#ville\").focus(function () {
            var pays = \$('#pays').find(\":selected\").text();

            if(pays==\"France\")
            {
                \$(function() {
                    var availableTags = [
                        ";
        // line 434
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["villes"]) ? $context["villes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ville"]) {
            // line 435
            echo "                        \"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ville"], "name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ville"], "cp", array()), "html", null, true);
            echo "\",
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ville'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 437
        echo "
                    ];
                    \$( \"#ville\" ).autocomplete({
                        source: availableTags
                    });
                });
            }
            else{
                \$(function() {
                    var availableTags = [
                        ";
        // line 447
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["villes"]) ? $context["villes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ville"]) {
            // line 448
            echo "                        \"\",
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ville'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 450
        echo "
                    ];
                    \$( \"#ville\" ).autocomplete({
                        source: availableTags
                    });
                });

            }

        });
    </script>


";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Event:search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1999 => 450,  1992 => 448,  1988 => 447,  1976 => 437,  1965 => 435,  1961 => 434,  1951 => 426,  1945 => 425,  1938 => 420,  1932 => 418,  1929 => 417,  1916 => 412,  1905 => 410,  1897 => 409,  1891 => 408,  1885 => 404,  1882 => 403,  1874 => 401,  1868 => 399,  1866 => 398,  1859 => 397,  1851 => 395,  1849 => 394,  1844 => 392,  1834 => 387,  1830 => 385,  1824 => 383,  1817 => 381,  1815 => 380,  1810 => 378,  1805 => 375,  1800 => 374,  1798 => 373,  1784 => 372,  1775 => 366,  1768 => 362,  1762 => 361,  1755 => 357,  1751 => 356,  1746 => 354,  1741 => 351,  1726 => 349,  1722 => 348,  1716 => 347,  1700 => 342,  1696 => 341,  1687 => 337,  1681 => 336,  1675 => 335,  1669 => 334,  1663 => 333,  1657 => 332,  1651 => 331,  1645 => 330,  1639 => 329,  1633 => 328,  1627 => 327,  1621 => 326,  1615 => 325,  1609 => 324,  1603 => 323,  1597 => 322,  1591 => 321,  1585 => 320,  1579 => 319,  1573 => 318,  1567 => 317,  1561 => 316,  1555 => 315,  1549 => 314,  1543 => 313,  1537 => 312,  1531 => 311,  1525 => 310,  1519 => 309,  1513 => 308,  1507 => 307,  1501 => 306,  1495 => 305,  1489 => 304,  1483 => 303,  1477 => 302,  1471 => 301,  1465 => 300,  1459 => 299,  1453 => 298,  1447 => 297,  1441 => 296,  1435 => 295,  1429 => 294,  1423 => 293,  1417 => 292,  1411 => 291,  1405 => 290,  1399 => 289,  1393 => 288,  1387 => 287,  1381 => 286,  1375 => 285,  1369 => 284,  1363 => 283,  1357 => 282,  1351 => 281,  1345 => 280,  1339 => 279,  1333 => 278,  1327 => 277,  1321 => 276,  1315 => 275,  1309 => 274,  1303 => 273,  1297 => 272,  1291 => 271,  1285 => 270,  1279 => 269,  1273 => 268,  1267 => 267,  1261 => 266,  1255 => 265,  1249 => 264,  1243 => 263,  1237 => 262,  1231 => 261,  1225 => 260,  1219 => 259,  1213 => 258,  1207 => 257,  1201 => 256,  1195 => 255,  1189 => 254,  1183 => 253,  1177 => 252,  1171 => 251,  1165 => 250,  1159 => 249,  1153 => 248,  1147 => 247,  1141 => 246,  1135 => 245,  1129 => 244,  1123 => 243,  1117 => 242,  1111 => 241,  1105 => 240,  1099 => 239,  1093 => 238,  1087 => 237,  1081 => 236,  1075 => 235,  1069 => 234,  1063 => 233,  1057 => 232,  1051 => 231,  1045 => 230,  1039 => 229,  1033 => 228,  1027 => 227,  1021 => 226,  1015 => 225,  1009 => 224,  1003 => 223,  997 => 222,  991 => 221,  985 => 220,  979 => 219,  973 => 218,  967 => 217,  961 => 216,  955 => 215,  949 => 214,  943 => 213,  937 => 212,  931 => 211,  925 => 210,  919 => 209,  913 => 208,  907 => 207,  901 => 206,  895 => 205,  889 => 204,  883 => 203,  877 => 202,  871 => 201,  865 => 200,  859 => 199,  853 => 198,  847 => 197,  841 => 196,  835 => 195,  829 => 194,  823 => 193,  817 => 192,  811 => 191,  805 => 190,  799 => 189,  793 => 188,  787 => 187,  781 => 186,  775 => 185,  769 => 184,  763 => 183,  757 => 182,  751 => 181,  745 => 180,  739 => 179,  733 => 178,  727 => 177,  721 => 176,  715 => 175,  709 => 174,  703 => 173,  697 => 172,  691 => 171,  685 => 170,  679 => 169,  673 => 168,  667 => 167,  661 => 166,  655 => 165,  649 => 164,  643 => 163,  637 => 162,  631 => 161,  625 => 160,  619 => 159,  613 => 158,  607 => 157,  601 => 156,  595 => 155,  589 => 154,  583 => 153,  577 => 152,  571 => 151,  565 => 150,  559 => 149,  553 => 148,  547 => 147,  541 => 146,  535 => 145,  529 => 144,  523 => 143,  517 => 142,  511 => 141,  505 => 140,  499 => 139,  493 => 138,  487 => 137,  481 => 136,  475 => 135,  469 => 134,  463 => 133,  457 => 132,  451 => 131,  445 => 130,  439 => 129,  433 => 128,  427 => 127,  421 => 126,  415 => 125,  409 => 124,  403 => 123,  397 => 122,  391 => 121,  385 => 120,  379 => 119,  373 => 118,  367 => 117,  361 => 116,  355 => 115,  349 => 114,  343 => 113,  337 => 112,  331 => 111,  325 => 110,  319 => 109,  313 => 108,  307 => 107,  301 => 106,  295 => 105,  289 => 104,  283 => 103,  277 => 102,  271 => 101,  265 => 100,  259 => 99,  253 => 98,  247 => 97,  241 => 96,  235 => 95,  229 => 94,  223 => 93,  217 => 92,  211 => 91,  205 => 90,  199 => 89,  193 => 88,  187 => 87,  181 => 86,  176 => 84,  170 => 81,  164 => 78,  158 => 75,  147 => 67,  143 => 66,  134 => 60,  130 => 59,  123 => 55,  119 => 54,  113 => 51,  105 => 46,  101 => 45,  93 => 40,  89 => 39,  80 => 35,  76 => 34,  62 => 25,  58 => 24,  50 => 21,  38 => 14,  32 => 11,  20 => 1,);
    }
}
