<?php

/* ComparatorEventBundle:Event:listEventFriendsHome.html.twig */
class __TwigTemplate_83cf6601fc2e19073fe8a264286f8dff001997f0a34d27ffa553ce22d01bf56f extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"block event\">
    <div class=\"title-block\">
        <h4 class=\"green\">";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("list.events"), "html", null, true);
        echo "</h4>
    </div>
    <div class=\"body-event\">

        ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 8
            echo "        ";
            if ((twig_date_format_filter($this->env, "now", "Y-m-d") < twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "startdate", array()), "Y-m-d"))) {
                // line 9
                echo "        <div class=\"item-event\">
            <a href=\"";
                // line 10
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "slug", array()))), "html", null, true);
                echo "\">
                <div class=\"info-event\">
                    <h4 class=\"green\">";
                // line 12
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "title", array()), "html", null, true);
                echo "</h4>
                    <p>";
                // line 13
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "event", array()), "startdate", array()), "d-m-Y"), "html", null, true);
                echo "</p>
                </div>
            </a>
        </div>
        ";
            }
            // line 18
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Event:listEventFriendsHome.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 19,  57 => 18,  49 => 13,  45 => 12,  40 => 10,  37 => 9,  34 => 8,  30 => 7,  23 => 3,  19 => 1,);
    }
}
