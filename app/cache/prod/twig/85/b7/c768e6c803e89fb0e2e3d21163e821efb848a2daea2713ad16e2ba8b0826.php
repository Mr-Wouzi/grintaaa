<?php

/* ComparatorEventBundle:Friends:countIndex.html.twig */
class __TwigTemplate_85b7c768e6c803e89fb0e2e3d21163e821efb848a2daea2713ad16e2ba8b0826 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["invitation"]) ? $context["invitation"] : null) > 0)) {
            // line 2
            echo "<span class=\"nbr-notif\">";
            echo twig_escape_filter($this->env, (isset($context["invitation"]) ? $context["invitation"] : null), "html", null, true);
            echo "</span>
";
        }
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Friends:countIndex.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  21 => 2,  19 => 1,);
    }
}
