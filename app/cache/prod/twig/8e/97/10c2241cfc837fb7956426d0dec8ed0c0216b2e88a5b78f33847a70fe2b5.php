<?php

/* ComparatorEventBundle:Event:show.html.twig */
class __TwigTemplate_8e9710c2241cfc837fb7956426d0dec8ed0c0216b2e88a5b78f33847a70fe2b5 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["currentPath"] = $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), $this->getAttribute($this->getAttribute($this->getAttribute(        // line 2
(isset($context["app"]) ? $context["app"] : null), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method"));
        // line 3
        echo "<!--Debut containt-->
<div class=\"container containt-dash\">
    <!--Debut left-block-->
    <div class=\"col-sm-2 no-padding-right\">
        <div id=\"fb-root\"></div>

        <div class=\"block profil-block\">
            <div class=\"profil-dash\">
                <div class=\"col-md-12 no-padding\">
                    <div class=\"grid\">
                        <figure class=\"effect-winston\">
                            ";
        // line 14
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
                            <figcaption>
                                <p>
                                    <a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\"
                                       title=\"organisation evenements sportifs en france\"><i
                                                class=\"fa fa-fw\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class=\"col-md-12 no-padding\">
                    <p class=\"titre\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>
                    <p class=\"titre-user\">
                        <a href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\">
                            <img src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\"
                                 class=\"img-loginnn\"/>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo "
                        </a>
                    </p>
                    <!--p class=\"pro\">Profil</p-->
                </div>

            </div>
            <ul class=\"list-unstyled\">
                <li>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" title=\"organisation evenements sportifs en france\">
                        <img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\"
                             alt=\"organisation evenements sportifs en france\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\" title=\"organisation evenements sportifs en france\">
                        ";
        // line 46
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                    </a>
                </li>

                <li>
                    <a href=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\" title=\"organisation evenements sportifs en france\">
                        ";
        // line 52
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                    </a>
                </li>
                <li>
                    ";
        // line 57
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                </li>
                <li>
                    <a href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\"
                       title=\"organisation evenements sportifs en france\">
                        ";
        // line 62
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 66
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\"
                       title=\"organisation evenements sportifs en france\">
                        ";
        // line 68
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                    </a>
                </li>
            </ul>
        </div>


        ";
        // line 75
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "


        ";
        // line 78
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "


        <div class=\"block participant\">
            <div class=\"title-block\">
                <h4 class=\"green\">";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("liste.participants"), "html", null, true);
        echo "</h4>
            </div>
            <div class=\"body-participant\">

                ";
        // line 87
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["participesConfirm"]) ? $context["participesConfirm"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["key"] => $context["participeConfirm"]) {
            // line 88
            echo "                    <div class=\"item-participant\">
                        <a href=\"";
            // line 89
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "id", array()))), "html", null, true);
            echo "\"
                           title=\"organisation evenements sportifs en france\">
                            <div class=\"img-participant\">

                                ";
            // line 93
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["imagesParticipesConfirm"]) ? $context["imagesParticipesConfirm"] : null), $context["key"], array(), "array")) > 0)) {
                // line 94
                echo "                                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["imagesParticipesConfirm"]) ? $context["imagesParticipesConfirm"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                echo "\"
                                         alt=\"organisation evenements sportifs en france\">
                                ";
            } else {
                // line 97
                echo "                                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                echo "\"
                                         alt=\"organisation evenements sportifs en france\">
                                ";
            }
            // line 100
            echo "                            </div>
                            <div class=\"info-participant\">
                                <h4>";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "firstname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "lastname", array()), "html", null, true);
            echo "</h4>
                                ";
            // line 103
            $this->env->loadTemplate("DCSRatingBundle:Rating:control.html.twig")->display(array_merge($context, array("id" => $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "id", array()), "role" => "ROLE_USER")));
            // line 104
            echo "                            </div>
                        </a>
                    </div>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['participeConfirm'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 108
        echo "

            </div>
        </div>
    </div>
    <!--Fin left-block-->
    <!--Debut center-block-->
    <div class=\"col-sm-10\">
        <div class=\"block exprimer\">
            <div class=\"titre green\">
                <img src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/pencil.png"), "html", null, true);
        echo "\"
                     alt=\"organisation evenements sportifs en france\">
                <h4>";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous"), "html", null, true);
        echo "</h4>
            </div>
            <div class=\"add-act\">
                <!-- Nav tabs -->
                <ul class=\"nav nav-tabs\" role=\"tablist\">
                    <li role=\"presentation\" class=\"active\"><a href=\"#statut\" aria-controls=\"statut\" role=\"tab\"
                                                              data-toggle=\"tab\"
                                                              title=\"organisation evenements sportifs en france\"><i
                                    class=\"fa fa-pencil-square\"></i> ";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous.staut"), "html", null, true);
        echo "</a></li>
                    <li role=\"presentation\"><a href=\"#photo\" aria-controls=\"photo\" role=\"tab\" data-toggle=\"tab\"
                                               title=\"organisation evenements sportifs en france\"><i
                                    class=\"fa fa-picture-o\"></i> ";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous.photo"), "html", null, true);
        echo "</a></li>
                    <li role=\"presentation\"><a href=\"#video\" aria-controls=\"video\" role=\"tab\" data-toggle=\"tab\"
                                               title=\"organisation evenements sportifs en france\" class=\"video\"><i
                                    class=\"fa fa-caret-square-o-right\"></i> ";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous.video"), "html", null, true);
        echo "</a></li>
                </ul>

                <!-- Tab panes -->
                <div class=\"tab-content\">
                    <div role=\"tabpanel\" class=\"tab-pane active\" id=\"statut\">
                        <form method=\"post\" action=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("events_statut_create", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\">
                            <textarea placeholder=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("exprime.vous"), "html", null, true);
        echo "\" name=\"textarea\"></textarea>
                            <div class=\"send\">
                                <button type=\"submit\" class=\"btn btn-grinta\">";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.publier"), "html", null, true);
        echo "</button>
                            </div>
                        </form>
                    </div>
                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"photo\">
                        <form action=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("events_file_create", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\"
                              method=\"post\" ";
        // line 149
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo ">
                            <div class=\"row\">
                                <div class=\"col-md-12\">
                                    <div class=\"input-file-container\">
                                        ";
        // line 153
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "file", array()), 'errors');
        echo "
                                        ";
        // line 154
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "file", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                        <label for=\"my-file\" class=\"input-file-trigger\"
                                               tabindex=\"0\">";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.select.image"), "html", null, true);
        echo "</label>
                                    </div>
                                    <p class=\"file-return\"></p>
                                </div>
                            </div>

                            <div class=\"send\">
                                <button type=\"submit\" class=\"btn btn-grinta\">";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.publier"), "html", null, true);
        echo " </button>
                            </div>
                            ";
        // line 165
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
                        </form>
                    </div>
                    <div role=\"tabpanel\" class=\"tab-pane\" id=\"video\">
                        <form method=\"post\" action=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("events_video_create", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\">
                            <textarea placeholder=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.video.youtube"), "html", null, true);
        echo "\" name=\"video\"></textarea>
                            <div class=\"send\">
                                <button type=\"submit\" class=\"btn btn-grinta\">";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.publier"), "html", null, true);
        echo "</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <div class=\"block item-act\">
            <div class=\"row\">
                <div class=\"title-event-show\">
                    ";
        // line 183
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "title", array()), "html", null, true);
        echo "
                </div>
                <div class=\"clerfix\"></div>
                <div class=\"col-sm-1\">
                    <a href=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"
                       title=\"organisation evenements sportifs en france\">
                        <div class=\"img-avatar\">
                            ";
        // line 190
        if ((twig_length_filter($this->env, (isset($context["entityImageUser"]) ? $context["entityImageUser"] : null)) > 0)) {
            // line 191
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entityImageUser"]) ? $context["entityImageUser"] : null), 0, array(), "array"), "logo", array()), "html", null, true);
            echo "\"
                                     alt=\"organisation evenements sportifs en france\">
                            ";
        } else {
            // line 194
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
            echo "\"
                                     alt=\"organisation evenements sportifs en france\">
                            ";
        }
        // line 197
        echo "                        </div>
                    </a>
                </div>
                <div class=\"col-sm-2\">
                    <div class=\"col-sm-12 no-padding-right\">
                        <a href=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"
                           title=\"organisation evenements sportifs en france\">
                            <div class=\"name-avatar\">
                                <h5>";
        // line 205
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</h5>
                                ";
        // line 206
        if (((twig_date_format_filter($this->env, "now", "Y-m-d") > twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "startdate", array()), "Y-m-d")) && ((isset($context["participeUser"]) ? $context["participeUser"] : null) == true))) {
            // line 207
            echo "
                                    ";
            // line 208
            $this->env->loadTemplate("DCSRatingBundle:Rating:control.html.twig")->display(array_merge($context, array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()), "id", array()), "role" => "ROLE_USER")));
            // line 209
            echo "
                                ";
        }
        // line 211
        echo "
                            </div>
                        </a>
                    </div>
                </div>
                <div class=\"col-sm-7\">
                    <div class=\"info-act\">
                        <div class=\"col-md-4 text-center\">
                            <div class=\"image-info\">
                                <img src=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/large-lieu.png"), "html", null, true);
        echo "\"
                                     alt=\"organisation evenements sportifs en france\">
                            </div>
                            <div class=\"desc-info\">
                                <p>";
        // line 224
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "address", array()), "html", null, true);
        echo "</p>
                            </div>
                        </div>
                        <div class=\"col-md-4 text-center\">
                            <div class=\"image-date\">
                                <img src=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/large-date.png"), "html", null, true);
        echo "\"
                                     alt=\"organisation evenements sportifs en france\">
                            </div>
                            <div class=\"desc-info size-date\">
                                <p>";
        // line 233
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "startdate", array()), "d-m-Y"), "html", null, true);
        echo "</p>
                            </div>
                        </div>
                        <div class=\"col-md-4 text-center\">
                            <div class=\"image-info\">
                                <img src=\"";
        // line 238
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/large-time.png"), "html", null, true);
        echo "\"
                                     alt=\"organisation evenements sportifs en france\">
                            </div>
                            <div class=\"desc-info size-date\">
                                <p>";
        // line 242
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "starttime", array()), "H:i"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "endtime", array()), "H:i"), "html", null, true);
        echo "
                                    <!-- ";
        // line 243
        echo twig_escape_filter($this->env, (isset($context["nbHeure"]) ? $context["nbHeure"] : null), "html", null, true);
        echo " heures --> </p>
                            </div>

                            <div class=\"desc-info\">

                                <div class=\"nbre-peronne-aime\">
                                    <a id=\"likes\" class=\"aime\"><img
                                                src=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aimepas.png"), "html", null, true);
        echo "\"
                                                alt=\"organisation evenements sportifs en france\"/> J'aime</a>
                                    <a id=\"dislikes\"><img src=\"";
        // line 252
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/aime.png"), "html", null, true);
        echo "\"
                                                          alt=\"organisation evenements sportifs en france\"/> J'aime</a>
                                    <br>
                                    <span id=\"p1\"> </span> personne(s) aime ça
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"col-sm-2 pull-right share\">
                    <h4><i class=\"fa fa-share-alt green icon-puce\"></i> ";
        // line 263
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.partager"), "html", null, true);
        echo "</h4>
                    <ul class=\"list-inline margin-rs\">
                        <li class=\"fb-match\">

                            <div class=\"fb-share-button\"
                                 data-href=\"";
        // line 268
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "schemeAndHttpHost", array()), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "getBaseURL", array(), "method"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["currentPath"]) ? $context["currentPath"] : null), "html", null, true);
        echo "\"
                                 data-layout=\"button_count\" data-size=\"small\" data-mobile-iframe=\"false\"><a
                                        class=\"fb-xfbml-parse-ignore\" target=\"_blank\"
                                        href=\"https://www.facebook.com/sharer/sharer.php?u='";
        // line 271
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "schemeAndHttpHost", array()), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "getBaseURL", array(), "method"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["currentPath"]) ? $context["currentPath"] : null), "html", null, true);
        echo "';src=sdkpreparse\">Partager</a>
                            </div>
                        </li>
                    </ul>

                    <ul class=\"list-inline margin-rs\">
                        <li>
                            <a href=\"https://plus.google.com/share?url=";
        // line 278
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "schemeAndHttpHost", array()), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "getBaseURL", array(), "method"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["currentPath"]) ? $context["currentPath"] : null), "html", null, true);
        echo "\"
                               onclick=\"javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;\">
                                <img src=\"";
        // line 280
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/goo.png"), "html", null, true);
        echo "\" alt=\"Share on Google+\"/>
                            </a>
                        </li>
                        <li>
                            <a href=\"http://twitter.com/share\" data-url=\"";
        // line 284
        echo twig_escape_filter($this->env, (isset($context["currentPath"]) ? $context["currentPath"] : null), "html", null, true);
        echo "\"
                               onclick=\"javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;\">
                                <img src=\"";
        // line 286
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/tww.png"), "html", null, true);
        echo "\"
                                     alt=\"Share on twitter+\"/>
                            </a>
                        </li>


                    </ul>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-sm-8\">

                    <div class=\"titre2 green\">
                        <img src=\"";
        // line 300
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/description.png"), "html", null, true);
        echo "\"
                             alt=\"organisation evenements sportifs en france\">
                        <h4>";
        // line 302
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.description"), "html", null, true);
        echo "</h4>
                    </div>
                    <div class=\"content-description-event\">
                        ";
        // line 305
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "category", array()), "title", array()), "html", null, true);
        echo "
                        ";
        // line 306
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "description", array()), "html", null, true);
        echo "
                    </div>


                    <div class=\"titre2 green\">
                        <img src=\"";
        // line 311
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/description.png"), "html", null, true);
        echo "\"
                             alt=\"organisation evenements sportifs en france\">
                        <h4>";
        // line 313
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.options"), "html", null, true);
        echo "</h4>
                    </div>
                    <div class=\"content-description\">
                        <ul class=\"option-ul\">
                            ";
        // line 317
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["optionEvent"]) ? $context["optionEvent"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
            // line 318
            echo "                                <li>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["option"], "title", array()), "html", null, true);
            echo "</li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 320
        echo "                        </ul>
                    </div>
                </div>
                ";
        // line 323
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()) == $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()))) {
            // line 324
            echo "                    ";
            if ((twig_date_format_filter($this->env, "now", "Y-m-d") > twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "startdate", array()), "Y-m-d"))) {
                // line 325
                echo "                        <div class=\"col-sm-4\">
                            <form method=\"POST\" action=\"";
                // line 326
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_send_notif_show", array("slug" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "slug", array()))), "html", null, true);
                echo "\">
                                <input type=\"submit\" value=\"";
                // line 327
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.envoie.notif"), "html", null, true);
                echo "\" class=\"btn btn-grinta\"/>
                            </form>
                        </div>
                    ";
            }
            // line 331
            echo "                ";
        }
        // line 332
        echo "                <span class=\"likebtn-wrapper\" data-theme=\"large\" data-identifier=\"item_1\"></span>
            </div>

        </div>

        <div class=\"block regoindre\">

            <div class=\"title-block\">
                <h4>";
        // line 340
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("rejoindre.ce.jeu"), "html", null, true);
        echo "</h4>
            </div>
            <br>

            ";
        // line 344
        if ((isset($context["testparticipe"]) ? $context["testparticipe"] : null)) {
            // line 345
            echo "                ";
            if ((($this->getAttribute((isset($context["testparticipe"]) ? $context["testparticipe"] : null), "enabled", array()) == 1) || ($this->getAttribute((isset($context["testparticipe"]) ? $context["testparticipe"] : null), "enabled", array()) == 3))) {
                // line 346
                echo "                    <div class=\"col-md-4 col-md-offset-4\">
                        <form id=\"validate_field_types\" method=\"POST\"
                              action=\"";
                // line 348
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_delete", array("id" => $this->getAttribute((isset($context["testparticipe"]) ? $context["testparticipe"] : null), "id", array()))), "html", null, true);
                echo "\">
                            <input type=\"submit\" class=\"btn btn-delete\" value=\"";
                // line 349
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.annuler"), "html", null, true);
                echo "\"/>

                        </form>
                    </div>
                ";
            } else {
                // line 354
                echo "
                    ";
                // line 355
                if ((null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()))) {
                    // line 356
                    echo "                        <div class=\"col-md-4 col-md-offset-4\">
                            <form id=\"validate_field_types\" method=\"POST\"
                                  action=\"";
                    // line 358
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_update", array("id" => $this->getAttribute((isset($context["testparticipe"]) ? $context["testparticipe"] : null), "id", array()))), "html", null, true);
                    echo "\">
                                <input type=\"submit\" class=\"btn btn-grinta\" value=\"";
                    // line 359
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.confirme"), "html", null, true);
                    echo "\"/>

                            </form>
                        </div>
                    ";
                } else {
                    // line 364
                    echo "
                        ";
                    // line 365
                    if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()) > (isset($context["nbparticipe"]) ? $context["nbparticipe"] : null))) {
                        // line 366
                        echo "                            <div class=\"col-md-4 col-md-offset-4\">
                                <form id=\"validate_field_types\" method=\"POST\"
                                      action=\"";
                        // line 368
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_update", array("id" => $this->getAttribute((isset($context["testparticipe"]) ? $context["testparticipe"] : null), "id", array()))), "html", null, true);
                        echo "\">
                                    <input type=\"submit\" class=\"btn btn-grinta\" value=\"";
                        // line 369
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.confirme"), "html", null, true);
                        echo "\"/>

                                </form>
                            </div>
                        ";
                    }
                    // line 374
                    echo "                    ";
                }
                // line 375
                echo "

                    <div class=\"col-md-4 col-md-offset-4\">
                        <form id=\"validate_field_types\" method=\"POST\"
                              action=\"";
                // line 379
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_delete", array("id" => $this->getAttribute((isset($context["testparticipe"]) ? $context["testparticipe"] : null), "id", array()))), "html", null, true);
                echo "\">
                            <input type=\"submit\" class=\"btn btn-delete\" value=\"";
                // line 380
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.annuler"), "html", null, true);
                echo "\"/>

                        </form>
                    </div>
                ";
            }
            // line 385
            echo "            ";
        } else {
            // line 386
            echo "                ";
            if ((null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()))) {
                // line 387
                echo "                    <div class=\"col-md-3\">
                        <a href=\"";
                // line 388
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_favorite", array("enabled" => "true", "id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
                echo "\"
                           class=\"btn btn-grinta\">";
                // line 389
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.rejoindre"), "html", null, true);
                echo "</a>
                    </div>
                    <div class=\"col-md-6\"></div>
                    <div class=\"col-md-3\">
                        <a href=\"";
                // line 393
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_favorite", array("enabled" => "false", "id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
                echo "\"
                           class=\"btn btn-red\">";
                // line 394
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.peut.etre"), "html", null, true);
                echo "</a>
                    </div>
                ";
            } else {
                // line 397
                echo "                    ";
                if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()) > (isset($context["nbparticipe"]) ? $context["nbparticipe"] : null))) {
                    // line 398
                    echo "                        <div class=\"col-md-3\">
                            <a href=\"";
                    // line 399
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_favorite", array("enabled" => "true", "id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
                    echo "\"
                               class=\"btn btn-grinta\">";
                    // line 400
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.rejoindre"), "html", null, true);
                    echo "</a>
                        </div>
                        <div class=\"col-md-6\"></div>
                        <div class=\"col-md-3\">
                            <a href=\"";
                    // line 404
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_favorite", array("enabled" => "false", "id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
                    echo "\"
                               class=\"btn btn-red\">";
                    // line 405
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.peut.etre"), "html", null, true);
                    echo "</a>
                        </div>
                    ";
                }
                // line 408
                echo "                ";
            }
            // line 409
            echo "
            ";
        }
        // line 411
        echo "
            <div class=\"clearfix\"></div>
        </div>

        <div class=\"block invit\">
            <div class=\"col-md-3 botton-invit\">
                <a class=\"btn btn-grinta ajouter-ami\">";
        // line 417
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("inviter.amis"), "html", null, true);
        echo "</a>

                <div class=\"invit-ami-form\">
                    <form method=\"POST\" action=\"";
        // line 420
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_send_show", array("slug" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "slug", array()))), "html", null, true);
        echo "\">
                        <table border=\"0\" id=\"catg\" width=\"100%\" cellpadding=\"0\">
                            <tbody>
                            <tr id=\"ligne0\">
                                <td height=\"37\">
                                    <input type=\"text\" placeholder=\"Nom\" name=\"nom[]\" class=\"form-control form-control\"
                                           id=\"friends0\" required=\"required\"/>
                                    <a id=\"todelete\" href=\"javascript:deleterow('ligne0')\">
                                        <i class=\"fa fa-times delete-ajout\"></i>
                                    </a>
                                    <script type=\"text/javascript\">
                                        \$(function () {
                                            var availableTags = [
                                                ";
        // line 433
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["completes"]) ? $context["completes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 434
            echo "                                                ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["status"], "user1", array()), "id", array()))) {
                // line 435
                echo "                                                \"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["status"], "user2", array()), "firstname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["status"], "user2", array()), "lastname", array()), "html", null, true);
                echo "\",
                                                ";
            } else {
                // line 437
                echo "                                                \"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["status"], "user1", array()), "firstname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["status"], "user1", array()), "lastname", array()), "html", null, true);
                echo "\",
                                                ";
            }
            // line 439
            echo "                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 440
        echo "
                                            ];
                                            \$(\"#friends0\").autocomplete({
                                                source: availableTags
                                            });
                                        });
                                    </script>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <a href=\"javascript:myFunction()\" class=\"lien-ami-form\">+ ";
        // line 451
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.ajouter"), "html", null, true);
        echo "</a>
                        <input type=\"submit\" value=\"";
        // line 452
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.envoyer"), "html", null, true);
        echo "\" class=\"btn btn-grinta\"/>
                    </form>
                </div>
                <p>";
        // line 455
        if ((null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()))) {
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("text.dispo.event"), "html", null, true);
            echo " ";
        } else {
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("text.il.a"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()) - (isset($context["nbparticipeConfirm"]) ? $context["nbparticipeConfirm"] : null)), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("text.place.dispo"), "html", null, true);
        }
        echo "</p>
            </div>
            <div class=\"col-md-6 pull-right botton-invit\">
                <a class=\"btn btn-grinta ajouter-joueur\">";
        // line 458
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("add.player.manu"), "html", null, true);
        echo "</a>
                <div class=\"invit-joueur-form\">
                    <form method=\"POST\" action=\"";
        // line 460
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_send_mail_show", array("slug" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "slug", array()))), "html", null, true);
        echo "\">
                        <table border=\"0\" id=\"catg2\" width=\"100%\" cellpadding=\"0\">
                            <tbody>
                            <tr id=\"ligne0\">
                                <td height=\"37\">
                                    <input type=\"email\" placeholder=\"Email\" name=\"mail[]\"
                                           class=\"form-control form-control\" required=\"required\"/>
                                    <a id=\"todelete2\" href=\"javascript:deleterow2('ligne0')\">
                                        <i class=\"fa fa-times delete-ajout2\"></i>
                                    </a>
                                </td>

                            </tr>
                            </tbody>
                        </table>
                        <a href=\"javascript:myFunction2()\" class=\"lien-joueur-form\">+ ";
        // line 475
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.ajouter"), "html", null, true);
        echo "</a>
                        <input type=\"submit\" value=\"";
        // line 476
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.envoyer"), "html", null, true);
        echo "\" class=\"btn btn-grinta\"/>
                    </form>
                </div>
                <p>";
        // line 479
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.add.player.manu"), "html", null, true);
        echo "</p>
            </div>
        </div>
        <div class=\"block exprimer description\">
            <div class=\"titre green\">
                <img src=\"";
        // line 484
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/gamers.png"), "html", null, true);
        echo "\"
                     alt=\"organisation evenements sportifs en france\">
                <h4>";
        // line 486
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("player.player"), "html", null, true);
        echo "</h4>
            </div>

            <div class=\"clerfix\"></div>
            <div class=\"top-gamers\">
                <div class=\"col-md-9 no-padding-left\">

                    ";
        // line 493
        if ((null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()))) {
            // line 494
            echo "                        <span class=\"green\">•</span> ";
            echo twig_escape_filter($this->env, (isset($context["nbparticipeConfirm"]) ? $context["nbparticipeConfirm"] : null), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.participants"), "html", null, true);
            echo "
                        <span class=\"green\">•</span> ";
            // line 495
            echo twig_escape_filter($this->env, (isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.attente"), "html", null, true);
            echo "
                    ";
        } else {
            // line 497
            echo "                        ";
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()) > (isset($context["nbparticipeConfirm"]) ? $context["nbparticipeConfirm"] : null))) {
                // line 498
                echo "                            ";
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()) - (isset($context["nbparticipeConfirm"]) ? $context["nbparticipeConfirm"] : null)), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.dispo"), "html", null, true);
                echo " :
                        ";
            }
            // line 500
            echo "                        <span class=\"green\">•</span> ";
            echo twig_escape_filter($this->env, (isset($context["nbparticipeConfirm"]) ? $context["nbparticipeConfirm"] : null), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.participants"), "html", null, true);
            echo "
                        <span class=\"green\">•</span> ";
            // line 501
            echo twig_escape_filter($this->env, (isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.attente"), "html", null, true);
            echo "
                    ";
        }
        // line 503
        echo "
                </div>
                <div class=\"col-md-3 text-center nombre\">

                    ";
        // line 507
        if ((null === $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()))) {
            // line 508
            echo "                        <p class=\"nbr-match\">";
            echo twig_escape_filter($this->env, (isset($context["nbparticipeConfirm"]) ? $context["nbparticipeConfirm"] : null), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.participants"), "html", null, true);
            echo "</p>
                    ";
        } else {
            // line 510
            echo "                        <p class=\"nbr-match\">";
            echo twig_escape_filter($this->env, (isset($context["nbparticipeConfirm"]) ? $context["nbparticipeConfirm"] : null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()), "html", null, true);
            echo "</p>
                        <p class=\"postes\">";
            // line 511
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()) > (isset($context["nbparticipeConfirm"]) ? $context["nbparticipeConfirm"] : null))) {
                // line 512
                echo "                                ";
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nbPlayer", array()) - (isset($context["nbparticipeConfirm"]) ? $context["nbparticipeConfirm"] : null)), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("event.dispo"), "html", null, true);
            }
            echo "</p>

                    ";
        }
        // line 515
        echo "                </div>
                <div class=\"clerfix\"></div>
            </div>

            <div class=\"clerfix\"></div>
            <!--List gamers-->
            <div class=\"list-gamers\">

                <div class=\"row one-gamer\">
                    ";
        // line 524
        $context["nbreee"] = 0;
        // line 525
        echo "                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["participes"]) ? $context["participes"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["key"] => $context["participe"]) {
            // line 526
            echo "                        ";
            $context["nbreee"] = ((isset($context["nbreee"]) ? $context["nbreee"] : null) + 1);
            // line 527
            echo "                        <div class=\"col-sm-3 no-padding-left gamer-par\">
                            <div class=\"col-sm-4 img-avatar\">
                                <a href=\"";
            // line 529
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["participe"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["participe"], "user", array()), "id", array()))), "html", null, true);
            echo "\">
                                    ";
            // line 530
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["imagesParticipes"]) ? $context["imagesParticipes"] : null), $context["key"], array(), "array")) > 0)) {
                // line 531
                echo "                                        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["imagesParticipes"]) ? $context["imagesParticipes"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                echo "\"
                                             alt=\"organisation evenements sportifs en france\">
                                    ";
            } else {
                // line 534
                echo "                                        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                echo "\"
                                             alt=\"organisation evenements sportifs en france\">
                                    ";
            }
            // line 537
            echo "                                </a>
                            </div>
                            <div class=\"col-sm-8 name-avatar\">
                                <h5>";
            // line 540
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participe"], "user", array()), "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participe"], "user", array()), "firstname", array()), "html", null, true);
            echo "</h5>
                                ";
            // line 541
            $this->env->loadTemplate("DCSRatingBundle:Rating:control.html.twig")->display(array_merge($context, array("id" => $this->getAttribute($this->getAttribute($context["participe"], "user", array()), "id", array()), "role" => "ROLE_USER")));
            // line 542
            echo "                                ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()), "id", array()))) {
                // line 543
                echo "                                    <div class=\"col-sm-12 no-padding-left\">

                                        <a class=\"btn btn-grinta sup-joueur1";
                // line 545
                echo twig_escape_filter($this->env, (isset($context["nbreee"]) ? $context["nbreee"] : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.delete"), "html", null, true);
                echo "</a>
                                        <div class=\"sup-jour supprimer-joueur1";
                // line 546
                echo twig_escape_filter($this->env, (isset($context["nbreee"]) ? $context["nbreee"] : null), "html", null, true);
                echo "\">
                                            <form id=\"validate_field_types\" method=\"POST\"
                                                  action=\"";
                // line 548
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_delete", array("id" => $this->getAttribute($context["participe"], "id", array()))), "html", null, true);
                echo "\">
                                                <textarea name=\"descsupp\"></textarea>
                                                <input type=\"submit\" class=\"btn btn-grinta sup_joueur\"
                                                       value=\"";
                // line 551
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.delete"), "html", null, true);
                echo "\"/>
                                                <script>
                                                    \$(document).ready(function () {
                                                        \$(\"a.sup-joueur1";
                // line 554
                echo twig_escape_filter($this->env, (isset($context["nbreee"]) ? $context["nbreee"] : null), "html", null, true);
                echo "\").click(function () {
                                                            \$(\".supprimer-joueur1";
                // line 555
                echo twig_escape_filter($this->env, (isset($context["nbreee"]) ? $context["nbreee"] : null), "html", null, true);
                echo "\").addClass(\"supprimer-joueur12-hidd\");
                                                        });
                                                    });
                                                </script>
                                            </form>
                                        </div>
                                        <form id=\"validate_field_typess\" method=\"POST\"
                                              action=\"";
                // line 562
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_confirm", array("id" => $this->getAttribute($context["participe"], "id", array()))), "html", null, true);
                echo "\">
                                            <input type=\"submit\" class=\"btn btn-grinta\" value=\"Confirmer\"/>
                                        </form>


                                    </div>
                                ";
            }
            // line 569
            echo "                            </div>
                        </div>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['participe'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 572
        echo "                </div>


            </div>
            <div class=\"clerfix\"></div>
            <!--Fin List gamers-->

            <!--List gamers-->
            <div class=\"list-gamers\">


                <div class=\"row one-gamer\">
                    ";
        // line 584
        $context["nbreeeee"] = 0;
        // line 585
        echo "                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["participesConfirm"]) ? $context["participesConfirm"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["keyc"] => $context["participeConfirm"]) {
            // line 586
            echo "                        ";
            $context["nbreeeee"] = ((isset($context["nbreeeee"]) ? $context["nbreeeee"] : null) + 1);
            // line 587
            echo "                        <div class=\"col-sm-3 no-padding-left gamer-par\">
                            <div class=\"col-sm-4 img-avatar\">
                                <a href=\"";
            // line 589
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "id", array()))), "html", null, true);
            echo "\">
                                    ";
            // line 590
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["imagesParticipesConfirm"]) ? $context["imagesParticipesConfirm"] : null), $context["keyc"], array(), "array")) > 0)) {
                // line 591
                echo "                                        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["imagesParticipesConfirm"]) ? $context["imagesParticipesConfirm"] : null), $context["keyc"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                echo "\"
                                             alt=\"organisation evenements sportifs en france\">
                                    ";
            } else {
                // line 594
                echo "                                        <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                echo "\"
                                             alt=\"organisation evenements sportifs en france\">
                                    ";
            }
            // line 597
            echo "                                </a>
                            </div>
                            <div class=\"col-sm-8 name-avatar\">
                                <h5>";
            // line 600
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "firstname", array()), "html", null, true);
            echo "</h5>
                                ";
            // line 601
            $this->env->loadTemplate("DCSRatingBundle:Rating:control.html.twig")->display(array_merge($context, array("id" => $this->getAttribute($this->getAttribute($context["participeConfirm"], "user", array()), "id", array()), "role" => "ROLE_USER")));
            // line 602
            echo "                                ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "user", array()), "id", array()))) {
                // line 603
                echo "                                    <div class=\"col-sm-12 no-padding-left\">
                                        <a class=\"btn btn-grinta sup-joueur2";
                // line 604
                echo twig_escape_filter($this->env, (isset($context["nbreeeee"]) ? $context["nbreeeee"] : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.delete"), "html", null, true);
                echo "</a>
                                        <div class=\"sup-jour supprimer-joueur2";
                // line 605
                echo twig_escape_filter($this->env, (isset($context["nbreeeee"]) ? $context["nbreeeee"] : null), "html", null, true);
                echo "\">
                                            <form id=\"validate_field_types\" method=\"POST\"
                                                  action=\"";
                // line 607
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("favoris_delete", array("id" => $this->getAttribute($context["participeConfirm"], "id", array()))), "html", null, true);
                echo "\">
                                                <textarea name=\"descsupp\"></textarea>
                                                <input type=\"submit\" class=\"btn btn-grinta sup_joueur\"
                                                       value=\"Confirmer\"/>
                                                <script>
                                                    \$(document).ready(function () {
                                                        \$(\"a.sup-joueur2";
                // line 613
                echo twig_escape_filter($this->env, (isset($context["nbreeeee"]) ? $context["nbreeeee"] : null), "html", null, true);
                echo "\").click(function () {
                                                            \$(\".supprimer-joueur2";
                // line 614
                echo twig_escape_filter($this->env, (isset($context["nbreeeee"]) ? $context["nbreeeee"] : null), "html", null, true);
                echo "\").addClass(\"supprimer-joueur12-hidd\");
                                                        });
                                                    });
                                                </script>

                                            </form>
                                        </div>
                                    </div>
                                ";
            }
            // line 623
            echo "                            </div>
                        </div>
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['keyc'], $context['participeConfirm'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 626
        echo "                </div>


            </div>
            <!--Fin List gamers-->

        </div>
        <!--Partage-->
        <div class=\"block exprimer description comment\">
            <div class=\"titre green\">
                <img src=\"";
        // line 636
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/comment.png"), "html", null, true);
        echo "\"
                     alt=\"organisation evenements sportifs en france\">
                <h4>";
        // line 638
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.partages"), "html", null, true);
        echo "</h4>
            </div>
            ";
        // line 640
        if ((twig_length_filter($this->env, (isset($context["listStatut"]) ? $context["listStatut"] : null)) <= 0)) {
            // line 641
            echo "                <div class=\"top-comment\">
                    <div class=\"col-sm-12 no-padding-left\">
                        <p>";
            // line 643
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.partage.text"), "html", null, true);
            echo "</p>
                    </div>
                </div>

            ";
        }
        // line 648
        echo "

            <div class=\"col-md-12 no-padding list-comment\">

                ";
        // line 652
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listStatut"]) ? $context["listStatut"] : null));
        foreach ($context['_seq'] as $context["keys"] => $context["statut"]) {
            // line 653
            echo "                    <div class=\"col-md-12 no-padding item-comment\">
                        <div class=\"col-md-1 img-comment no-padding\">
                            <a href=\"";
            // line 655
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["statut"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["statut"], "user", array()), "id", array()))), "html", null, true);
            echo "\">
                                ";
            // line 656
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["imagesStatut"]) ? $context["imagesStatut"] : null), $context["keys"], array(), "array")) > 0)) {
                // line 657
                echo "                                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["imagesStatut"]) ? $context["imagesStatut"] : null), $context["keys"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                echo "\"
                                         alt=\"organisation evenements sportifs en france\">
                                ";
            } else {
                // line 660
                echo "                                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                echo "\"
                                         alt=\"organisation evenements sportifs en france\">
                                ";
            }
            // line 663
            echo "                            </a>
                        </div>
                        <div class=\"col-md-11 desc-comment no-padding-right\">
                            <p><span class=\"green2\">";
            // line 666
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["statut"], "user", array()), "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["statut"], "user", array()), "firstname", array()), "html", null, true);
            echo "</span> <span
                                        class=\"color-gris size-date\">";
            // line 667
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["statut"], "createdAt", array()), "d-m-Y"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["statut"], "createdAt", array()), "H:i:s"), "html", null, true);
            echo "</span>
                            </p>

                            ";
            // line 670
            if (($this->getAttribute($context["statut"], "type", array()) == "statut")) {
                // line 671
                echo "                                <p>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["statut"], "descreption", array()), "html", null, true);
                echo " </p>
                            ";
            }
            // line 673
            echo "                            ";
            if (($this->getAttribute($context["statut"], "type", array()) == "video")) {
                // line 674
                echo "                                <p>
                                    <iframe width=\"360\" height=\"215\"
                                            src=\"https://www.youtube.com/embed/";
                // line 676
                echo twig_escape_filter($this->env, $this->getAttribute($context["statut"], "url", array()), "html", null, true);
                echo "\" frameborder=\"0\"
                                            allowfullscreen></iframe>
                                </p>
                            ";
            }
            // line 680
            echo "                            ";
            if (($this->getAttribute($context["statut"], "type", array()) == "image")) {
                // line 681
                echo "                                <img class=\"img-profil-match\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/events/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($context["statut"], "logo", array()), "html", null, true);
                echo "\"
                                     alt=\"organisation evenements sportifs en france\">
                            ";
            }
            // line 684
            echo "
                        </div>
                    </div>

                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['keys'], $context['statut'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 689
        echo "
            </div>

            <div class=\"clerfix\"></div>

        </div>

        <!--Partage-->


        <div class=\"block exprimer description comment\">
            <div class=\"titre green\">
                <img src=\"";
        // line 701
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/comment.png"), "html", null, true);
        echo "\"
                     alt=\"organisation evenements sportifs en france\">
                <h4>";
        // line 703
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("comment.comment"), "html", null, true);
        echo "</h4>
            </div>
            ";
        // line 705
        if ((twig_length_filter($this->env, (isset($context["listComment"]) ? $context["listComment"] : null)) <= 0)) {
            // line 706
            echo "                <div class=\"top-comment\">
                    <div class=\"col-sm-12 no-padding-left\">
                        <p>";
            // line 708
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.comment.comment"), "html", null, true);
            echo "</p>
                    </div>
                </div>

            ";
        }
        // line 713
        echo "

            <div class=\"col-md-12 no-padding list-comment\">

                ";
        // line 717
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listComment"]) ? $context["listComment"] : null));
        foreach ($context['_seq'] as $context["keys"] => $context["comment"]) {
            // line 718
            echo "                    <div class=\"col-md-12 no-padding item-comment\">
                        <div class=\"col-md-1 img-comment no-padding\">
                            <a href=\"";
            // line 720
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "id", array()))), "html", null, true);
            echo "\">
                                ";
            // line 721
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["imagesComment"]) ? $context["imagesComment"] : null), $context["keys"], array(), "array")) > 0)) {
                // line 722
                echo "                                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["imagesComment"]) ? $context["imagesComment"] : null), $context["keys"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                echo "\"
                                         alt=\"organisation evenements sportifs en france\">
                                ";
            } else {
                // line 725
                echo "                                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                echo "\"
                                         alt=\"organisation evenements sportifs en france\">
                                ";
            }
            // line 728
            echo "                            </a>
                        </div>
                        <div class=\"col-md-11 desc-comment no-padding-right\">
                            <p><span class=\"green2\">";
            // line 731
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "firstname", array()), "html", null, true);
            echo "</span>
                                <span class=\"color-gris size-date\">";
            // line 732
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "createdAt", array()), "d-m-Y"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "createdAt", array()), "H:i:s"), "html", null, true);
            echo "</span>
                            </p>
                            <p>";
            // line 734
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "description", array()), "html", null, true);
            echo " </p>


                        </div>
                    </div>

                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['keys'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 741
        echo "
            </div>

            <div class=\"clerfix\"></div>
            <div class=\"form-commentg\">
                <form action=\"";
        // line 746
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_comment", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\" method=\"post\">
                    <div class=\"comment-avatar col-sm-1 no-padding-left\">
                        <div class=\"img-comment\">
                            ";
        // line 749
        if ((twig_length_filter($this->env, (isset($context["logo"]) ? $context["logo"] : null)) > 0)) {
            // line 750
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["logo"]) ? $context["logo"] : null), 0, array(), "array"), "logo", array()), "html", null, true);
            echo "\"
                                     alt=\"organisation evenements sportifs en france\">

                            ";
        } else {
            // line 754
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
            echo "\"
                                     alt=\"organisation evenements sportifs en france\">
                            ";
        }
        // line 757
        echo "                        </div>
                    </div>
                    <div class=\"col-sm-11 no-padding form-comment\">
                        <div class=\"col-sm-11 no-padding\">
                            <textarea class=\"form-control\" name=\"textarea\"
                                      placeholder=\"";
        // line 762
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("vos.comment"), "html", null, true);
        echo "\"></textarea>
                        </div>
                        <div class=\"col-sm-1 no-padding\">
                            <button type=\"submit\" class=\"btn btn-grinta sup_joueur\"><i class=\"fa fa-comments\"></i>
                            </button>
                        </div>
                    </div>
                    <div class=\"clerfix\"></div>
                </form>
            </div>
        </div>


    </div>
    <div class=\"clerfix\"></div>
    <!--Fin center-block-->
    <!--Debut right-block-->
    <!--Fin right-block-->
    <br><br> <br>
</div>
<!--Debut containt-->
<script>
    \$(document).ready(function () {
        document.getElementById(\"p1\").innerHTML =";
        // line 785
        echo twig_escape_filter($this->env, (isset($context["nblikes"]) ? $context["nblikes"] : null), "html", null, true);
        echo ";
        \$.ajax({
            type: \"POST\",
            url: \"";
        // line 788
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_liked", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\",
            data: \"\",
            success: function (data) {
                if (data < 1) {
                    jQuery('#dislikes').css('display', 'none');


                } else {
                    jQuery('#likes').css('display', 'none');
                    jQuery('#dislikes').css('display', 'block');
                }

            }
        });

        \$(\"#dislikes\").click(function () {

            \$.ajax({
                type: \"POST\",
                url: \"";
        // line 807
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("del_liked", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\",
                data: \"\",
                success: function (data) {
                    \$.ajax({
                        type: \"POST\",
                        url: \"";
        // line 812
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("liked", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\",
                        data: \"\",
                        success: function (data) {


                            document.getElementById(\"p1\").innerHTML = ";
        // line 817
        echo twig_escape_filter($this->env, ((isset($context["nblikes"]) ? $context["nblikes"] : null) - 1), "html", null, true);
        echo ";

                            jQuery('#dislikes').css('display', 'none');


                        }
                    });


                }
            });


        });


        \$(\"#likes\").click(function () {

            \$.ajax({
                type: \"POST\",
                url: \"";
        // line 837
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("test_liked", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\",
                data: \"\",
                success: function (data) {
                    if (data < 1) {
                        \$.ajax({
                            type: \"POST\",
                            url: \"";
        // line 843
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_liked", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\",
                            data: \"\",
                            success: function (data) {
                                document.getElementById(\"p1\").innerHTML = ";
        // line 846
        echo twig_escape_filter($this->env, ((isset($context["nblikes"]) ? $context["nblikes"] : null) + 1), "html", null, true);
        echo ";
                                jQuery('#dislikes').css('display', 'block');
                            }
                        });


                    }

                }
            });


        });


    });

    var iii = 0;
    var row = \$(\"#catg tbody\").html();
    \$(document).ready(function () {
        \$(\"#todelete\").remove();
    });

    function myFunction() {
        iii++;


        var rowint = replaceAll('ligne0', 'ligne' + iii, row)
        var rowint = replaceAll('friends0', 'friends' + iii, rowint)

        \$('#catg tbody').append(rowint);

    }
    function deleterow(div) {
        \$(\"#catg #\" + div).remove();
    }
    function replaceAll(find, replace, str) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    \$(document).ready(function () {
        \$(\"a.ajouter-ami\").click(function () {
            \$(\".invit-ami-form\").addClass(\"invit-ami-form2\");
        });
    });


    var iiii = 0;
    var roww = \$(\"#catg2 tbody\").html();
    \$(document).ready(function () {
        \$(\"#todelete2\").remove();
    });

    function myFunction2() {
        iiii++;
        var rowintt = replaceAll('ligne0', 'ligne' + iiii, roww)
        \$('#catg2 tbody').append(rowintt);

    }
    function deleterow2(div) {
        \$(\"#catg2 #\" + div).remove();
    }

    \$(document).ready(function () {
        \$(\"a.ajouter-joueur\").click(function () {
            \$(\".invit-joueur-form\").addClass(\"invit-joueur-form2\");
        });
    });


    \$('textarea.form-control').autoResize();
</script>
<script>
    document.querySelector(\"html\").classList.add('js');

    var fileInput = document.querySelector(\".form-control\"),
            button = document.querySelector(\".input-file-trigger\"),
            the_return = document.querySelector(\".file-return\");

    button.addEventListener(\"keydown\", function (event) {
        if (event.keyCode == 13 || event.keyCode == 32) {
            fileInput.focus();
        }
    });
    button.addEventListener(\"click\", function (event) {
        fileInput.focus();
        return false;
    });
    fileInput.addEventListener(\"change\", function (event) {
        the_return.innerHTML = this.value;
    });
    \$(\".input-file-container .col-lg-9\").removeClass(\"col-lg-9\");


</script>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = \"//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.6\";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Event:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1720 => 846,  1714 => 843,  1705 => 837,  1682 => 817,  1674 => 812,  1666 => 807,  1644 => 788,  1638 => 785,  1612 => 762,  1605 => 757,  1598 => 754,  1589 => 750,  1587 => 749,  1581 => 746,  1574 => 741,  1561 => 734,  1552 => 732,  1546 => 731,  1541 => 728,  1534 => 725,  1526 => 722,  1524 => 721,  1520 => 720,  1516 => 718,  1512 => 717,  1506 => 713,  1498 => 708,  1494 => 706,  1492 => 705,  1487 => 703,  1482 => 701,  1468 => 689,  1458 => 684,  1450 => 681,  1447 => 680,  1440 => 676,  1436 => 674,  1433 => 673,  1427 => 671,  1425 => 670,  1415 => 667,  1409 => 666,  1404 => 663,  1397 => 660,  1389 => 657,  1387 => 656,  1383 => 655,  1379 => 653,  1375 => 652,  1369 => 648,  1361 => 643,  1357 => 641,  1355 => 640,  1350 => 638,  1345 => 636,  1333 => 626,  1317 => 623,  1305 => 614,  1301 => 613,  1292 => 607,  1287 => 605,  1281 => 604,  1278 => 603,  1275 => 602,  1273 => 601,  1267 => 600,  1262 => 597,  1255 => 594,  1247 => 591,  1245 => 590,  1241 => 589,  1237 => 587,  1234 => 586,  1216 => 585,  1214 => 584,  1200 => 572,  1184 => 569,  1174 => 562,  1164 => 555,  1160 => 554,  1154 => 551,  1148 => 548,  1143 => 546,  1137 => 545,  1133 => 543,  1130 => 542,  1128 => 541,  1122 => 540,  1117 => 537,  1110 => 534,  1102 => 531,  1100 => 530,  1096 => 529,  1092 => 527,  1089 => 526,  1071 => 525,  1069 => 524,  1058 => 515,  1048 => 512,  1046 => 511,  1039 => 510,  1031 => 508,  1029 => 507,  1023 => 503,  1016 => 501,  1009 => 500,  1001 => 498,  998 => 497,  991 => 495,  984 => 494,  982 => 493,  972 => 486,  967 => 484,  959 => 479,  953 => 476,  949 => 475,  931 => 460,  926 => 458,  911 => 455,  905 => 452,  901 => 451,  888 => 440,  882 => 439,  874 => 437,  866 => 435,  863 => 434,  859 => 433,  843 => 420,  837 => 417,  829 => 411,  825 => 409,  822 => 408,  816 => 405,  812 => 404,  805 => 400,  801 => 399,  798 => 398,  795 => 397,  789 => 394,  785 => 393,  778 => 389,  774 => 388,  771 => 387,  768 => 386,  765 => 385,  757 => 380,  753 => 379,  747 => 375,  744 => 374,  736 => 369,  732 => 368,  728 => 366,  726 => 365,  723 => 364,  715 => 359,  711 => 358,  707 => 356,  705 => 355,  702 => 354,  694 => 349,  690 => 348,  686 => 346,  683 => 345,  681 => 344,  674 => 340,  664 => 332,  661 => 331,  654 => 327,  650 => 326,  647 => 325,  644 => 324,  642 => 323,  637 => 320,  628 => 318,  624 => 317,  617 => 313,  612 => 311,  604 => 306,  600 => 305,  594 => 302,  589 => 300,  572 => 286,  567 => 284,  560 => 280,  553 => 278,  541 => 271,  533 => 268,  525 => 263,  511 => 252,  506 => 250,  496 => 243,  488 => 242,  481 => 238,  473 => 233,  466 => 229,  458 => 224,  451 => 220,  440 => 211,  436 => 209,  434 => 208,  431 => 207,  429 => 206,  423 => 205,  417 => 202,  410 => 197,  403 => 194,  395 => 191,  393 => 190,  387 => 187,  380 => 183,  366 => 172,  361 => 170,  357 => 169,  350 => 165,  345 => 163,  335 => 156,  330 => 154,  326 => 153,  319 => 149,  315 => 148,  307 => 143,  302 => 141,  298 => 140,  289 => 134,  283 => 131,  277 => 128,  266 => 120,  261 => 118,  249 => 108,  232 => 104,  230 => 103,  224 => 102,  220 => 100,  213 => 97,  205 => 94,  203 => 93,  196 => 89,  193 => 88,  176 => 87,  169 => 83,  161 => 78,  155 => 75,  145 => 68,  140 => 66,  133 => 62,  128 => 60,  122 => 57,  114 => 52,  110 => 51,  102 => 46,  98 => 45,  91 => 41,  87 => 40,  83 => 39,  71 => 30,  67 => 29,  63 => 28,  56 => 26,  46 => 19,  41 => 17,  35 => 14,  22 => 3,  20 => 2,  19 => 1,);
    }
}
