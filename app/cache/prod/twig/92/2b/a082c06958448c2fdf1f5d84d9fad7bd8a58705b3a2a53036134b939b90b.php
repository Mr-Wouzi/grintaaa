<?php

/* ComparatorEventBundle:Mur:countImages.html.twig */
class __TwigTemplate_922ba082c06958448c2fdf1f5d84d9fad7bd8a58705b3a2a53036134b939b90b extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/camera.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.tof"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, (isset($context["nbimages"]) ? $context["nbimages"] : null), "html", null, true);
        echo "</span>
";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Mur:countImages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
