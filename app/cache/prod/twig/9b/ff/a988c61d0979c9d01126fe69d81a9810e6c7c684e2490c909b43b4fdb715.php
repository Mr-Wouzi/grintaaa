<?php

/* ComparatorEventBundle:Event:countEventByUser.html.twig */
class __TwigTemplate_9bffa988c61d0979c9d01126fe69d81a9810e6c7c684e2490c909b43b4fdb715 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<a href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_user", array("username" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()))), "html", null, true);
        echo "\">
    <img src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/competition.png"), "html", null, true);
        echo "\" src=\"organiser une journée sportive\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.competition"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, (isset($context["nb"]) ? $context["nb"] : null), "html", null, true);
        echo "</span>
</a>";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Event:countEventByUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }
}
