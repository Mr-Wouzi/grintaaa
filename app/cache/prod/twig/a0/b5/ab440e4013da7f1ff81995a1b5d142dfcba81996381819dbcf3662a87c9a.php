<?php

/* ComparatorEventBundle:Notification:countNotificationss.html.twig */
class __TwigTemplate_a0b5ab440e4013da7f1ff81995a1b5d142dfcba81996381819dbcf3662a87c9a extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.notifs"), "html", null, true);
        echo " <span class=\"red\">";
        echo twig_escape_filter($this->env, (isset($context["notification"]) ? $context["notification"] : null), "html", null, true);
        echo "</span></a>

";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Notification:countNotificationss.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }
}
