<?php

/* ComparatorStatutBundle:Statut:new.html.twig */
class __TwigTemplate_abd0eff63be9c5ed754b6bedd4f1a05f5a01c487b742fc4aa1440ed58172afdf extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt -->



        <form class=\"\" action=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("user_statut_create");
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo ">
            <div class=\"row\">   
                <div class=\"col-md-12\">
                    <div class=\"input-file-container\">
                    ";
        // line 9
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "file", array()), 'errors');
        echo "     
                    ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "file", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                    <label for=\"my-file\" class=\"input-file-trigger\" tabindex=\"0\">";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.select.image"), "html", null, true);
        echo "</label>
                    </div>
                    <p class=\"file-return\"></p>
                </div>
            </div>       

            <div class=\"row\">
                <div class=\"col-md-12\">
                    <input type=\"submit\" class=\"btn btn-grinta\" name=\"\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.publier"), "html", null, true);
        echo "\">
                </div>
            </div>
            ";
        // line 22
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
        </form>


<!--Fin containt-->   ";
    }

    public function getTemplateName()
    {
        return "ComparatorStatutBundle:Statut:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 22,  53 => 19,  42 => 11,  38 => 10,  34 => 9,  25 => 5,  19 => 1,);
    }
}
