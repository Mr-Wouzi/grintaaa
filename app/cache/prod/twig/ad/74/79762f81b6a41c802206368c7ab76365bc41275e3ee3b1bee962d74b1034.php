<?php

/* ApplicationSonataUserBundle:User:show.html.twig */
class __TwigTemplate_ad7479762f81b6a41c802206368c7ab76365bc41275e3ee3b1bee962d74b1034 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt-->
<div class=\"container containt-dash\">
    <div class=\"\">
        <!--Debut left-block-->
        <div class=\"col-md-2 no-padding-right\">
            <div class=\"block profil-block\">
                <div class=\"profil-dash\">
                    <div class=\"col-md-12 no-padding\">
                        <div class=\"grid\">
                            <figure class=\"effect-winston\">
                                ";
        // line 11
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
                                <figcaption>
                                    <p>
                                        <a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\"><i
                                                    class=\"fa fa-fw\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
                                    </p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class=\"col-md-12 no-padding\">
                        <p class=\"titre\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>
                        <p class=\"titre-user\">      
\t\t\t\t\t\t\t\t<a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</p> 
                        <!--p class=\"pro\">Profil</p-->
                    </div>

                </div>
                <ul class=\"list-unstyled\">
                    <li>
                        <a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\">
                            <img src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                        </a>
                    </li>
                    <li>
                        <a href=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\">
                            ";
        // line 40
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                        </a>
                    </li>

                    <li>
                        <a href=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\">
                            ";
        // line 46
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                        </a>
                    </li>
                    <li>
                        ";
        // line 51
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                    </li>
                    <li>
                        <a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\">
                            ";
        // line 55
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                        </a>
                    </li>
                    <li>
                        <a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\">
                            ";
        // line 60
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                        </a>
                    </li>
                </ul>
            </div>


        </div>
        <!--Fin left-block-->
        <div class=\"formulaire-search col-md-10\">
            <div class=\"block item-act\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <div class=\"title-search padding-bottom-profil\">
                            <h4 class=\"green\">";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "firstname", array()), "html", null, true);
        echo " ";
        if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "coach", array()) == 1)) {
            echo "(";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.u.coach"), "html", null, true);
            echo ") ";
        }
        echo "</h4>
                        </div>
                    </div>
                    <div class=\"col-md-10 no-padding-left\">
                        <div class=\"col-md-4 titre-gris pad-top\">
                            ";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.info.general"), "html", null, true);
        echo "
                        </div>
                        <div class=\"col-md-8 color-gris2\">
                            <div class=\"etoile-profil\">
                                ";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.eval.moy"), "html", null, true);
        echo "

                               ";
        // line 85
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()) == $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))) {
            // line 86
            echo "                                <span class=\"etoile-profil2\">";
            $this->env->loadTemplate("DCSRatingBundle:Rating:rating.html.twig")->display(array_merge($context, array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()), "role" => "ROLE_USER")));
            echo "</span>
                               ";
        } else {
            // line 88
            echo "                                <span class=\"etoile-profil2\">";
            $this->env->loadTemplate("DCSRatingBundle:Rating:control.html.twig")->display(array_merge($context, array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()), "role" => "ROLE_USER")));
            echo "</span>
                               ";
        }
        // line 90
        echo "                            </div>
                        </div>
\t\t\t\t\t\t<div class=\"clearfix\"></div>
                        <div class=\"col-md-4\">
                            <div class=\"img-search img-profil\">
                                ";
        // line 95
        if ((twig_length_filter($this->env, (isset($context["logo"]) ? $context["logo"] : null)) > 0)) {
            // line 96
            echo "                                    <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["logo"]) ? $context["logo"] : null), 0, array(), "array"), "logo", array()), "html", null, true);
            echo "\">
                                ";
        } else {
            // line 98
            echo "                                    <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
            echo "\">
                                ";
        }
        // line 100
        echo "                            </div>
                        </div>
                        <div class=\"col-md-8\">
                            <div class=\"col-md-4 no-padding-left padding-bottom-profil color-gris2\">";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.date.birth"), "html", null, true);
        echo "
                                :
                            </div>
                            <div class=\"col-md-8 padding-bottom-profil color-gris2 size-date\">";
        // line 106
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "dateOfBirth", array()), "d-m-Y"), "html", null, true);
        echo "</div>
\t\t\t\t\t\t\t<div class=\"clearfix\"></div>
                            <div class=\"col-md-4 no-padding-left padding-bottom-profil color-gris2\">";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.pays"), "html", null, true);
        echo "
                                :
                            </div>
                            <div class=\"col-md-8 padding-bottom-profil color-gris2\">";
        // line 111
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "country", array()), "html", null, true);
        echo "</div>
                            <div class=\"col-md-4 no-padding-left padding-bottom-profil color-gris2\">";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.adress"), "html", null, true);
        echo "
                                :
                            </div>
                            <div class=\"col-md-8 padding-bottom-profil color-gris2\">";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "city", array()), "html", null, true);
        echo "</div>
                            <div class=\"col-md-4 no-padding-left padding-bottom-profil color-gris2\">";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.weight"), "html", null, true);
        echo "
                                :
                            </div>
                            <div class=\"col-md-8 padding-bottom-profil color-gris2\">";
        // line 119
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "weight", array()), "html", null, true);
        echo " Kg</div>
                            <div class=\"col-md-4 no-padding-left padding-bottom-profil color-gris2\">";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.size"), "html", null, true);
        echo "
                                :
                            </div>
                            <div class=\"col-md-8 padding-bottom-profil color-gris2\">";
        // line 123
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "size", array()), "html", null, true);
        echo " Cm</div>
                        </div>
                    </div>

                    <div class=\"col-md-2\">
                        ";
        // line 128
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()) != $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))) {
            // line 129
            echo "                            <div class=\" action-search\">


                                ";
            // line 132
            if (((twig_test_empty((isset($context["sends"]) ? $context["sends"] : null)) && twig_test_empty((isset($context["requestinvis"]) ? $context["requestinvis"] : null))) && twig_test_empty((isset($context["friends"]) ? $context["friends"] : null)))) {
                // line 133
                echo "
                                    <a href=\"";
                // line 134
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_friend", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
                echo "\"
                                       class=\"btn btn-grinta\">";
                // line 135
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.inviter"), "html", null, true);
                echo " </a>

                                ";
            }
            // line 138
            echo "
                                ";
            // line 139
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["friends"]) ? $context["friends"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["friend"]) {
                // line 140
                echo "
                                    <form id=\"validate_field_types\" method=\"POST\"
                                          action=\"";
                // line 142
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["friend"], "id", array()))), "html", null, true);
                echo "\">
                                        <input type=\"submit\" class=\"btn btn-refus\" value=\"";
                // line 143
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.delete"), "html", null, true);
                echo "\"/>

                                    </form>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friend'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 147
            echo "

                                ";
            // line 149
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["sends"]) ? $context["sends"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["send"]) {
                // line 150
                echo "
                                    <form id=\"validate_field_types\" method=\"POST\"
                                          action=\"";
                // line 152
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["send"], "id", array()))), "html", null, true);
                echo "\">
                                        <input type=\"submit\" class=\"btn btn-refus\" value=\"";
                // line 153
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.annuler"), "html", null, true);
                echo "\"/>

                                    </form>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['send'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 157
            echo "
                                ";
            // line 158
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["requestinvis"]) ? $context["requestinvis"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["requestinvi"]) {
                // line 159
                echo "
                                    <form id=\"validate_field_types\" method=\"POST\"
                                          action=\"";
                // line 161
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_update", array("id" => $this->getAttribute($context["requestinvi"], "id", array()))), "html", null, true);
                echo "\">
                                        <input type=\"submit\" class=\"btn btn-grinta\" value=\"";
                // line 162
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.accpte"), "html", null, true);
                echo "\"/>

                                    </form>
                                    <form id=\"validate_field_types\" method=\"POST\"
                                          action=\"";
                // line 166
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["requestinvi"], "id", array()))), "html", null, true);
                echo "\">
                                        <input type=\"submit\" class=\"btn btn-refus\" value=\"";
                // line 167
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.refuse"), "html", null, true);
                echo "\"/>

                                    </form>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['requestinvi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 171
            echo "
                            </div>
                        ";
        }
        // line 174
        echo "                    </div>
                </div>
            </div>

            <div class=\"block item-act\">
                <div class=\"row\">
                    <div class=\"col-md-10 no-padding-left\">
                        <div class=\"col-md-4 titre-gris\">
                            ";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
        echo "
                        </div>
                        <div class=\"col-md-8\">
                            ";
        // line 185
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["friendsInterest"]) ? $context["friendsInterest"] : null));
        foreach ($context['_seq'] as $context["keys"] => $context["interested"]) {
            // line 186
            echo "                                ";
            if ($this->getAttribute($this->getAttribute($context["interested"], "category", array()), "parent", array())) {
                // line 187
                echo "                                    <div class=\"profil-centres color-gris2 padding-bottom-profil\"><i
                                                class=\"fa fa-circle\"></i> ";
                // line 188
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["interested"], "category", array()), "title", array()), "html", null, true);
                echo "</div>
                                ";
            }
            // line 190
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['keys'], $context['interested'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 191
        echo "                        </div>
                    </div>
                </div>
            </div>


            ";
        // line 197
        if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "coach", array()) == 1)) {
            // line 198
            echo "                <div class=\"block item-act\">
                    <div class=\"row\">
                        <div class=\"col-md-10 no-padding-left\">
                            <div class=\"col-md-4 titre-gris\">
                                ";
            // line 202
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.u.coach"), "html", null, true);
            echo "
                            </div>
                            <div class=\"col-md-8\">
                                ";
            // line 205
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["friendsCoaching"]) ? $context["friendsCoaching"] : null));
            foreach ($context['_seq'] as $context["keys"] => $context["coaching"]) {
                // line 206
                echo "                                    ";
                if ($this->getAttribute($this->getAttribute($context["coaching"], "category", array()), "parent", array())) {
                    // line 207
                    echo "                                        <div class=\"profil-centres color-gris2 padding-bottom-profil\"><i
                                                    class=\"fa fa-circle\"></i> ";
                    // line 208
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["coaching"], "category", array()), "title", array()), "html", null, true);
                    echo "</div>
                                    ";
                }
                // line 210
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['keys'], $context['coaching'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 211
            echo "                            </div>
                        </div>
                    </div>
                </div>

            ";
        }
        // line 217
        echo "
            <div class=\"block item-act\">
                <div class=\"row\">
                    <div class=\"col-md-8 titre-gris\">
                        Amis
                    </div>
                    <div class=\"col-md-4\" align=\"right\">
                        <!--a href=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user", array("username" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "username", array()), "id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
        echo "\">Voir plus</a-->
                    </div>
                    <div class=\"item-friend\"></div>
                    ";
        // line 227
        $context["x"] = 0;
        // line 228
        echo "                    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listallFreinds"]) ? $context["listallFreinds"] : null));
        foreach ($context['_seq'] as $context["listkey"] => $context["listentity"]) {
            // line 229
            echo "                        ";
            if (((isset($context["x"]) ? $context["x"] : null) < 9)) {
                // line 230
                echo "                            <div class=\"list-ami-profil\">
                                ";
                // line 231
                if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()) == $this->getAttribute($this->getAttribute($context["listentity"], "user1", array()), "id", array()))) {
                    // line 232
                    echo "                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["listentity"], "user2", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["listentity"], "user2", array()), "id", array()))), "html", null, true);
                    echo "\">
                                        <div class=\"img-friend\">
                                            ";
                    // line 234
                    if ((twig_length_filter($this->env, $this->getAttribute((isset($context["listimages"]) ? $context["listimages"] : null), $context["listkey"], array(), "array")) > 0)) {
                        // line 235
                        echo "                                                <img src=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["listimages"]) ? $context["listimages"] : null), $context["listkey"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                        echo "\"
                                                     alt=\"\">
                                            ";
                    } else {
                        // line 238
                        echo "                                                <img src=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                        echo "\"
                                                     alt=\"\">
                                            ";
                    }
                    // line 241
                    echo "                                        </div>
                                        <div class=\"info-friend\">
                                            <h4>";
                    // line 243
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["listentity"], "user2", array()), "lastname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["listentity"], "user2", array()), "firstname", array()), "html", null, true);
                    echo "</h4>
                                        </div>
                                    </a>
                                    ";
                    // line 246
                    $context["x"] = ((isset($context["x"]) ? $context["x"] : null) + 1);
                    // line 247
                    echo "                                ";
                } else {
                    // line 248
                    echo "                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["listentity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["listentity"], "user1", array()), "id", array()))), "html", null, true);
                    echo "\">
                                        <div class=\"img-friend\">
                                            ";
                    // line 250
                    if ((twig_length_filter($this->env, $this->getAttribute((isset($context["listimages"]) ? $context["listimages"] : null), $context["listkey"], array(), "array")) > 0)) {
                        // line 251
                        echo "                                                <img src=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["listimages"]) ? $context["listimages"] : null), $context["listkey"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                        echo "\"
                                                     alt=\"\">
                                            ";
                    } else {
                        // line 254
                        echo "                                                <img src=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                        echo "\"
                                                     alt=\"\">
                                            ";
                    }
                    // line 257
                    echo "                                        </div>
                                        <div class=\"info-friend\">
                                            <h4>";
                    // line 259
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["listentity"], "user1", array()), "lastname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["listentity"], "user1", array()), "firstname", array()), "html", null, true);
                    echo "</h4>
                                        </div>
                                    </a>
                                    ";
                    // line 262
                    $context["x"] = ((isset($context["x"]) ? $context["x"] : null) + 1);
                    // line 263
                    echo "                                ";
                }
                // line 264
                echo "                            </div>
                        ";
            }
            // line 266
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['listkey'], $context['listentity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 267
        echo "                </div>
            </div>


            <div class=\"block item-act\">
                <div class=\"row\">
                    <div class=\"col-md-12 titre-gris\">
                        ";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("sport.org.cour"), "html", null, true);
        echo "
                    </div>
                    <div class=\"item-friend\"></div>
                    ";
        // line 277
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["events"]) ? $context["events"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["event"]) {
            // line 278
            echo "                        ";
            if ((twig_date_format_filter($this->env, "now", "Y-m-d") < twig_date_format_filter($this->env, $this->getAttribute($context["event"], "startdate", array()), "Y-m-d"))) {
                // line 279
                echo "                            <div class=\"col-md-4\">
                                <img src=\"/uploads/events/56e992100127d.jpeg\" alt=\"\" class=\"img-event-profil\"/>
                            </div>
                            <div class=\"col-md-4\">
                                <h3 class=\"name-match green\">";
                // line 283
                echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "title", array()), "html", null, true);
                echo "</h3>

                                <div class=\"col-md-8 no-padding-left\">
                                    <a href=\"";
                // line 286
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($context["event"], "slug", array()))), "html", null, true);
                echo "\"
                                       class=\"btn btn-grinta\">";
                // line 287
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fich.match"), "html", null, true);
                echo "</a>
                                </div>
                            </div>
                            <div class=\"col-md-4 info-date\">
                                <ul class=\"list-unstyled\">
                                    <li>
                                        <img src=\"";
                // line 293
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/date.png"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["event"], "startdate", array()), "d-m-Y"), "html", null, true);
                echo "
                                    </li>
                                    <li class=\"lieu\"><img
                                                src=\"";
                // line 296
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "locality", array()), "html", null, true);
                echo "
                                        ( ";
                // line 297
                echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "country", array()), "html", null, true);
                echo ")
                                    </li>
                                    <li>
                                        <img src=\"";
                // line 300
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/heure.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["event"], "starttime", array()), "H:i"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
                echo "  ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["event"], "endtime", array()), "H:i"), "html", null, true);
                echo "
                                    </li>
                                </ul>
                            </div>
                            <div class=\"col-md-12\"> &nbsp</div>
                        ";
            }
            // line 306
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 307
        echo "                </div>
            </div>

            <div class=\"block item-act\">
                <div class=\"row\">
                    <div class=\"col-md-12 titre-gris\">
                        ";
        // line 313
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("sport.org.arch"), "html", null, true);
        echo "
                    </div>
                    <div class=\"item-friend\"></div>
                    ";
        // line 316
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["events"]) ? $context["events"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["event"]) {
            // line 317
            echo "                        ";
            if ((twig_date_format_filter($this->env, "now", "Y-m-d") > twig_date_format_filter($this->env, $this->getAttribute($context["event"], "startdate", array()), "Y-m-d"))) {
                // line 318
                echo "                            <div class=\"col-md-4\">
                                <img src=\"/uploads/events/56e992100127d.jpeg\" alt=\"\" class=\"img-event-profil\"/>
                            </div>
                            <div class=\"col-md-4\">
                                <h3 class=\"name-match green\">";
                // line 322
                echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "title", array()), "html", null, true);
                echo "</h3>

                                <div class=\"col-md-8 no-padding-left\">
                                    <a href=\"";
                // line 325
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($context["event"], "slug", array()))), "html", null, true);
                echo "\"
                                       class=\"btn btn-grinta\">";
                // line 326
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fich.match"), "html", null, true);
                echo "</a>
                                </div>
                            </div>
                            <div class=\"col-md-4 info-date\">
                                <ul class=\"list-unstyled\">
                                    <li>
                                        <img src=\"";
                // line 332
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/date.png"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["event"], "startdate", array()), "d-m-Y"), "html", null, true);
                echo "
                                    </li>
                                    <li class=\"lieu\"><img
                                                src=\"";
                // line 335
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "locality", array()), "html", null, true);
                echo "
                                        ( ";
                // line 336
                echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "country", array()), "html", null, true);
                echo ")
                                    </li>
                                    <li>
                                        <img src=\"";
                // line 339
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/heure.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["event"], "starttime", array()), "H:i"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["event"], "endtime", array()), "H:i"), "html", null, true);
                echo "
                                    </li>
                                </ul>
                            </div>
                            <div class=\"col-md-12\"> &nbsp</div>
                        ";
            }
            // line 345
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 346
        echo "                </div>
            </div>

            <div class=\"block item-act\">
                <div class=\"row\">
                    <div class=\"col-md-12 titre-gris\">
                        ";
        // line 352
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("participation.cour"), "html", null, true);
        echo "
                    </div>
                    <div class=\"item-friend\"></div>
                    ";
        // line 355
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["participeFreinds"]) ? $context["participeFreinds"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["participeFreind"]) {
            // line 356
            echo "                        ";
            if ((twig_date_format_filter($this->env, "now", "Y-m-d") < twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "startdate", array()), "Y-m-d"))) {
                // line 357
                echo "                            <div class=\"col-md-4\">
                                <img src=\"/uploads/events/56e992100127d.jpeg\" alt=\"\" class=\"img-event-profil\"/>
                            </div>
                            <div class=\"col-md-4\">
                                <h3 class=\"name-match green\">";
                // line 361
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "title", array()), "html", null, true);
                echo "</h3>

                                <div class=\"col-md-8 no-padding-left\">
                                    <a href=\"";
                // line 364
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "slug", array()))), "html", null, true);
                echo "\"
                                       class=\"btn btn-grinta\">";
                // line 365
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fich.match"), "html", null, true);
                echo "</a>
                                </div>
                            </div>

                            <div class=\"col-md-4 info-date\">
                                <ul class=\"list-unstyled\">
                                    <li>
                                        <img src=\"";
                // line 372
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/date.png"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "startdate", array()), "d-m-Y"), "html", null, true);
                echo "
                                    </li>
                                    <li class=\"lieu\"><img
                                                src=\"";
                // line 375
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "locality", array()), "html", null, true);
                echo "
                                        ( ";
                // line 376
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "country", array()), "html", null, true);
                echo ")
                                    </li>
                                    <li>
                                        <img src=\"";
                // line 379
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/heure.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "starttime", array()), "H:i"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "endtime", array()), "H:i"), "html", null, true);
                echo "
                                    </li>
                                </ul>

                            </div>
                            <div class=\"col-md-12\"> &nbsp</div>
                        ";
            }
            // line 386
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participeFreind'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 387
        echo "                </div>
            </div>

            <div class=\"block item-act\">
                <div class=\"row\">
                    <div class=\"col-md-12 titre-gris\">
                        ";
        // line 393
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("participation.arch"), "html", null, true);
        echo "
                    </div>
                    <div class=\"item-friend\"></div>
                    ";
        // line 396
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["participeFreinds"]) ? $context["participeFreinds"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["participeFreind"]) {
            // line 397
            echo "                        ";
            if ((twig_date_format_filter($this->env, "now", "Y-m-d") > twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "startdate", array()), "Y-m-d"))) {
                // line 398
                echo "                            <div class=\"col-md-4\">
                                <img src=\"/uploads/events/56e992100127d.jpeg\" alt=\"\" class=\"img-event-profil\"/>
                            </div>
                            <div class=\"col-md-4\">
                                <h3 class=\"name-match green\">";
                // line 402
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "title", array()), "html", null, true);
                echo "</h3>

                                <div class=\"col-md-8 no-padding-left\">
                                    <a href=\"";
                // line 405
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "slug", array()))), "html", null, true);
                echo "\"
                                       class=\"btn btn-grinta\">";
                // line 406
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fich.match"), "html", null, true);
                echo "</a>
                                </div>
                            </div>

                            <div class=\"col-md-4 info-date\">
                                <ul class=\"list-unstyled\">
                                    <li>
                                        <img src=\"";
                // line 413
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/date.png"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "startdate", array()), "d-m-Y"), "html", null, true);
                echo "
                                    </li>
                                    <li class=\"lieu\"><img
                                                src=\"";
                // line 416
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "locality", array()), "html", null, true);
                echo "
                                        ( ";
                // line 417
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "country", array()), "html", null, true);
                echo ")
                                    </li>
                                    <li>
                                        <img src=\"";
                // line 420
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/heure.png"), "html", null, true);
                echo "\"> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "starttime", array()), "H:i"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["participeFreind"], "event", array()), "endtime", array()), "H:i"), "html", null, true);
                echo "
                                    </li>
                                </ul>

                            </div>
                            <div class=\"col-md-12\"> &nbsp</div>
                        ";
            }
            // line 427
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['participeFreind'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 428
        echo "                </div>
            </div>

            <div class=\"clerfix\"></div>

            
        ";
        // line 434
        if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()) != $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))) {
            // line 435
            echo "            ";
            if ((twig_length_filter($this->env, (isset($context["testComment"]) ? $context["testComment"] : null)) == 0)) {
                // line 436
                echo "            <div class=\"form-commentg\">

                <form action=\"";
                // line 438
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("add_comment_user", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()))), "html", null, true);
                echo "\" method=\"post\">
                    <div class=\"comment-avatar col-sm-1 no-padding-left\">
                        <div class=\"img-comment\">
                            ";
                // line 441
                if ((twig_length_filter($this->env, (isset($context["mylogo"]) ? $context["mylogo"] : null)) > 0)) {
                    // line 442
                    echo "                                <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["mylogo"]) ? $context["mylogo"] : null), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\">

                            ";
                } else {
                    // line 445
                    echo "                                <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\">
                            ";
                }
                // line 447
                echo "                        </div>
                    </div>
                    <div class=\"col-sm-11 no-padding form-comment\">
                        <div class=\"col-sm-11 no-padding\">
                            <textarea class=\"form-control\" name=\"textarea\"
                                      placeholder=\"";
                // line 452
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("vos.comment"), "html", null, true);
                echo "\"></textarea>
                        </div>
                        <div class=\"col-sm-1 no-padding\">
                            <button type=\"submit\" class=\"btn btn-grinta sup_joueur\"><i class=\"fa fa-comments\"></i>
                            </button>
                        </div>
                    </div>
                    <div class=\"clerfix\"></div>
                </form>


            </div>
              ";
            }
            // line 465
            echo "        ";
        }
        // line 466
        echo "

            <div class=\"block item-act\">
                <div class=\"row\">

                    ";
        // line 471
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["listComment"]) ? $context["listComment"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["keys"] => $context["comment"]) {
            // line 472
            echo "                        <div class=\"col-md-1 img-bas-profil\">
                            <a href=\"";
            // line 473
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "id", array()))), "html", null, true);
            echo "\">
                                ";
            // line 474
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["imagesComment"]) ? $context["imagesComment"] : null), $context["keys"], array(), "array")) > 0)) {
                // line 475
                echo "                                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["imagesComment"]) ? $context["imagesComment"] : null), $context["keys"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                echo "\"
                                         class=\"img-event-profil2\">
                                ";
            } else {
                // line 478
                echo "                                    <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                echo "\"
                                         class=\"img-event-profil2\">
                                ";
            }
            // line 481
            echo "                            </a>
                        </div>
                        <div class=\"col-md-2 green\" align=\"center\">
                            <div class=\"green titre-size\">";
            // line 484
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "firstname", array()), "html", null, true);
            echo "</div>
                        </div>
                        <div class=\"col-md-7\">

                            <div class=\"col-md-12 titre-grisR\">
                                ";
            // line 489
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "description", array()), "html", null, true);
            echo "<br>

                                ";
            // line 491
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "createdAt", array()), "d-m-Y"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["comment"], "createdAt", array()), "H:i"), "html", null, true);
            echo "
                            </div>
                        </div>
                        <div class=\"col-md-2\" align=\"center\">


                            ";
            // line 497
            if ($this->getAttribute((isset($context["votes"]) ? $context["votes"] : null), $context["keys"], array(), "array")) {
                // line 498
                echo "                                <span class=\"etoile-profil\">
\t\t\t       <div class=\"dcs-rating-container\">
                       ";
                // line 500
                if (($this->getAttribute($this->getAttribute((isset($context["votes"]) ? $context["votes"] : null), $context["keys"], array(), "array"), "value", array()) == 5)) {
                    // line 501
                    echo "                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>

                       ";
                } elseif (($this->getAttribute($this->getAttribute(                // line 507
(isset($context["votes"]) ? $context["votes"] : null), $context["keys"], array(), "array"), "value", array()) == 4)) {
                    // line 508
                    echo "                           <span class=\"star\">☆</span>
                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>

                       ";
                } elseif (($this->getAttribute($this->getAttribute(                // line 514
(isset($context["votes"]) ? $context["votes"] : null), $context["keys"], array(), "array"), "value", array()) == 3)) {
                    // line 515
                    echo "                           <span class=\"star\">☆</span>
                           <span class=\"star\">☆</span>
                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>

                       ";
                } elseif (($this->getAttribute($this->getAttribute(                // line 521
(isset($context["votes"]) ? $context["votes"] : null), $context["keys"], array(), "array"), "value", array()) == 2)) {
                    // line 522
                    echo "                           <span class=\"star\">☆</span>
                           <span class=\"star\">☆</span>
                           <span class=\"star\">☆</span>
                           <span class=\"star full\">☆</span>
                           <span class=\"star full\">☆</span>
                       ";
                } elseif (($this->getAttribute($this->getAttribute(                // line 527
(isset($context["votes"]) ? $context["votes"] : null), $context["keys"], array(), "array"), "value", array()) == 1)) {
                    // line 528
                    echo "                           <span class=\"star\">☆</span>
                           <span class=\"star\">☆</span>
                           <span class=\"star\">☆</span>
                           <span class=\"star\">☆</span>
                           <span class=\"star full\">☆</span>
                       ";
                }
                // line 534
                echo "
                   </div>
\t\t\t\t  </span>
                            ";
            } else {
                // line 538
                echo "                                <div class=\"col-md-10 color-gris2\">
                                    <div class=\"etoile-profil\">
                                        ";
                // line 540
                $this->env->loadTemplate("DCSRatingBundle:Rating:control.html.twig")->display(array_merge($context, array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id", array()), "role" => "ROLE_USER")));
                echo "</span>
                                    </div>
                                </div>
                            ";
            }
            // line 544
            echo "

                        </div>
\t\t\t\t\t\t";
            // line 547
            if ((($context["keys"] + 1) < twig_length_filter($this->env, (isset($context["listComment"]) ? $context["listComment"] : null)))) {
                // line 548
                echo "                        <div class=\"col-md-12\" align=\"center\">
                            <hr class=\"style-two\">
                        </div>
\t\t\t\t\t\t";
            }
            // line 552
            echo "                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['keys'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 553
        echo "

                </div>
            </div>

        </div>

    </div>
    <div class=\"block-footer\"></div>
</div>
<!--Debut containt-->";
    }

    public function getTemplateName()
    {
        return "ApplicationSonataUserBundle:User:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1195 => 553,  1181 => 552,  1175 => 548,  1173 => 547,  1168 => 544,  1161 => 540,  1157 => 538,  1151 => 534,  1143 => 528,  1141 => 527,  1134 => 522,  1132 => 521,  1124 => 515,  1122 => 514,  1114 => 508,  1112 => 507,  1104 => 501,  1102 => 500,  1098 => 498,  1096 => 497,  1083 => 491,  1078 => 489,  1068 => 484,  1063 => 481,  1056 => 478,  1048 => 475,  1046 => 474,  1042 => 473,  1039 => 472,  1022 => 471,  1015 => 466,  1012 => 465,  996 => 452,  989 => 447,  983 => 445,  975 => 442,  973 => 441,  967 => 438,  963 => 436,  960 => 435,  958 => 434,  950 => 428,  944 => 427,  928 => 420,  922 => 417,  916 => 416,  908 => 413,  898 => 406,  894 => 405,  888 => 402,  882 => 398,  879 => 397,  875 => 396,  869 => 393,  861 => 387,  855 => 386,  839 => 379,  833 => 376,  827 => 375,  819 => 372,  809 => 365,  805 => 364,  799 => 361,  793 => 357,  790 => 356,  786 => 355,  780 => 352,  772 => 346,  766 => 345,  751 => 339,  745 => 336,  739 => 335,  731 => 332,  722 => 326,  718 => 325,  712 => 322,  706 => 318,  703 => 317,  699 => 316,  693 => 313,  685 => 307,  679 => 306,  664 => 300,  658 => 297,  652 => 296,  644 => 293,  635 => 287,  631 => 286,  625 => 283,  619 => 279,  616 => 278,  612 => 277,  606 => 274,  597 => 267,  591 => 266,  587 => 264,  584 => 263,  582 => 262,  574 => 259,  570 => 257,  563 => 254,  555 => 251,  553 => 250,  547 => 248,  544 => 247,  542 => 246,  534 => 243,  530 => 241,  523 => 238,  515 => 235,  513 => 234,  507 => 232,  505 => 231,  502 => 230,  499 => 229,  494 => 228,  492 => 227,  486 => 224,  477 => 217,  469 => 211,  463 => 210,  458 => 208,  455 => 207,  452 => 206,  448 => 205,  442 => 202,  436 => 198,  434 => 197,  426 => 191,  420 => 190,  415 => 188,  412 => 187,  409 => 186,  405 => 185,  399 => 182,  389 => 174,  384 => 171,  374 => 167,  370 => 166,  363 => 162,  359 => 161,  355 => 159,  351 => 158,  348 => 157,  338 => 153,  334 => 152,  330 => 150,  326 => 149,  322 => 147,  312 => 143,  308 => 142,  304 => 140,  300 => 139,  297 => 138,  291 => 135,  287 => 134,  284 => 133,  282 => 132,  277 => 129,  275 => 128,  267 => 123,  261 => 120,  257 => 119,  251 => 116,  247 => 115,  241 => 112,  237 => 111,  231 => 108,  226 => 106,  220 => 103,  215 => 100,  209 => 98,  202 => 96,  200 => 95,  193 => 90,  187 => 88,  181 => 86,  179 => 85,  174 => 83,  167 => 79,  151 => 74,  134 => 60,  130 => 59,  123 => 55,  119 => 54,  113 => 51,  105 => 46,  101 => 45,  93 => 40,  89 => 39,  80 => 35,  76 => 34,  62 => 25,  58 => 24,  51 => 22,  41 => 15,  37 => 14,  31 => 11,  19 => 1,);
    }
}
