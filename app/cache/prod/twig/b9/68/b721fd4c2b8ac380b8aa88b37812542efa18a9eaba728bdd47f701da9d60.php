<?php

/* ApplicationSonataPageBundle::base_layout.html.twig */
class __TwigTemplate_b968b721fd4c2b8ac380b8aa88b37812542efa18a9eaba728bdd47f701da9d60 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonata_page_html_tag' => array($this, 'block_sonata_page_html_tag'),
            'sonata_page_head' => array($this, 'block_sonata_page_head'),
            'sonata_page_stylesheets' => array($this, 'block_sonata_page_stylesheets'),
            'page_stylesheets' => array($this, 'block_page_stylesheets'),
            'sonata_page_javascripts' => array($this, 'block_sonata_page_javascripts'),
            'page_javascripts' => array($this, 'block_page_javascripts'),
            'sonata_page_body_tag' => array($this, 'block_sonata_page_body_tag'),
            'sonata_page_top_bar' => array($this, 'block_sonata_page_top_bar'),
            'page_top_bar' => array($this, 'block_page_top_bar'),
            'sonata_page_container' => array($this, 'block_sonata_page_container'),
            'page_container' => array($this, 'block_page_container'),
            'sonata_page_asset_footer' => array($this, 'block_sonata_page_asset_footer'),
            'page_asset_footer' => array($this, 'block_page_asset_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('sonata_page_html_tag', $context, $blocks);
        // line 6
        $this->displayBlock('sonata_page_head', $context, $blocks);
        // line 46
        echo "
";
        // line 47
        $this->displayBlock('sonata_page_body_tag', $context, $blocks);
        // line 51
        $this->displayBlock('sonata_page_top_bar', $context, $blocks);
        // line 55
        $this->displayBlock('sonata_page_container', $context, $blocks);
        // line 58
        $this->displayBlock('sonata_page_asset_footer', $context, $blocks);
        // line 80
        echo "


</body>
</html>
";
    }

    // line 1
    public function block_sonata_page_html_tag($context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<link rel=\"alternate\" href=\"http://www.grintaaa.com\" hreflang=\"fr\" />
<html ";
        // line 4
        echo $this->env->getExtension('sonata_seo')->getHtmlAttributes();
        echo ">
";
    }

    // line 6
    public function block_sonata_page_head($context, array $blocks = array())
    {
        // line 7
        echo "    <head ";
        echo $this->env->getExtension('sonata_seo')->getHeadAttributes();
        echo ">

        ";
        // line 9
        echo $this->env->getExtension('sonata_seo')->getTitle();
        echo "
        ";
        // line 10
        echo $this->env->getExtension('sonata_seo')->getMetadatas();
        echo "
        ";
        // line 11
        $this->displayBlock('sonata_page_stylesheets', $context, $blocks);
        // line 19
        echo "
        ";
        // line 20
        $this->displayBlock('sonata_page_javascripts', $context, $blocks);
        // line 34
        echo "\t\t<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-79846234-1', 'auto');
  ga('send', 'pageview');

</script>
    </head>
";
    }

    // line 11
    public function block_sonata_page_stylesheets($context, array $blocks = array())
    {
        // line 12
        echo "
            ";
        // line 13
        $this->displayBlock('page_stylesheets', $context, $blocks);
        // line 18
        echo "        ";
    }

    // line 13
    public function block_page_stylesheets($context, array $blocks = array())
    {
        // line 14
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["sonata_page"]) ? $context["sonata_page"] : null), "assets", array()), "stylesheets", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["stylesheet"]) {
            // line 15
            echo "                    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($context["stylesheet"]), "html", null, true);
            echo "\" type=\"text/css\" media=\"all\"/>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stylesheet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "            ";
    }

    // line 20
    public function block_sonata_page_javascripts($context, array $blocks = array())
    {
        // line 21
        echo "            ";
        $this->displayBlock('page_javascripts', $context, $blocks);
        // line 32
        echo "
        ";
    }

    // line 21
    public function block_page_javascripts($context, array $blocks = array())
    {
        echo " ";
        // line 22
        echo "\t\t\t";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "presentation")) {
            // line 23
            echo "                <script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>
";
        }
        // line 25
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "presentation")) {
            // line 26
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["sonata_page"]) ? $context["sonata_page"] : null), "assets", array()), "javascripts", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["js"]) {
                // line 27
                echo "                    <script src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($context["js"]), "html", null, true);
                echo "\" type=\"text/javascript\"></script>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['js'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 30
        echo "
            ";
    }

    // line 47
    public function block_sonata_page_body_tag($context, array $blocks = array())
    {
        // line 48
        echo "

";
    }

    // line 51
    public function block_sonata_page_top_bar($context, array $blocks = array())
    {
        // line 52
        echo "    ";
        $this->displayBlock('page_top_bar', $context, $blocks);
    }

    public function block_page_top_bar($context, array $blocks = array())
    {
        echo " ";
        // line 53
        echo "    ";
    }

    // line 55
    public function block_sonata_page_container($context, array $blocks = array())
    {
        // line 56
        echo "    ";
        $this->displayBlock('page_container', $context, $blocks);
        echo " ";
    }

    public function block_page_container($context, array $blocks = array())
    {
    }

    // line 58
    public function block_sonata_page_asset_footer($context, array $blocks = array())
    {
        // line 59
        echo "    ";
        $this->displayBlock('page_asset_footer', $context, $blocks);
    }

    public function block_page_asset_footer($context, array $blocks = array())
    {
        echo " ";
        // line 60
        echo "        ";
        if (array_key_exists("page", $context)) {
            // line 61
            echo "            ";
            if ( !twig_test_empty($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "javascript", array()))) {
                // line 62
                echo "                <script>
                    ";
                // line 63
                echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "javascript", array());
                echo "
                </script>
            ";
            }
            // line 66
            echo "            ";
            if ( !twig_test_empty($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "stylesheet", array()))) {
                // line 67
                echo "                <style>
                    ";
                // line 68
                echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "stylesheet", array());
                echo "
                </style>
            ";
            }
            // line 71
            echo "        ";
        }
        // line 72
        echo "        ";
        // line 76
        echo "        ";
        echo call_user_func_array($this->env->getFunction('sonata_block_include_stylesheets')->getCallable(), array("screen", $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "basePath", array())));
        echo "
        ";
        // line 77
        echo call_user_func_array($this->env->getFunction('sonata_block_include_javascripts')->getCallable(), array("screen", $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "basePath", array())));
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "ApplicationSonataPageBundle::base_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  270 => 77,  265 => 76,  263 => 72,  260 => 71,  254 => 68,  251 => 67,  248 => 66,  242 => 63,  239 => 62,  236 => 61,  233 => 60,  225 => 59,  222 => 58,  212 => 56,  209 => 55,  205 => 53,  197 => 52,  194 => 51,  188 => 48,  185 => 47,  180 => 30,  170 => 27,  165 => 26,  163 => 25,  159 => 23,  156 => 22,  152 => 21,  147 => 32,  144 => 21,  141 => 20,  137 => 17,  128 => 15,  123 => 14,  120 => 13,  116 => 18,  114 => 13,  111 => 12,  108 => 11,  93 => 34,  91 => 20,  88 => 19,  86 => 11,  82 => 10,  78 => 9,  72 => 7,  69 => 6,  63 => 4,  59 => 2,  56 => 1,  47 => 80,  45 => 58,  43 => 55,  41 => 51,  39 => 47,  36 => 46,  34 => 6,  32 => 1,);
    }
}
