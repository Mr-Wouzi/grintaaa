<?php

/* ApplicationSonataUserBundle:Registration:register_content2.html.twig */
class __TwigTemplate_c701e556289a73733795b27cdd7bbe6ce8dd017bd707fcb04b8ee28f98dd884b extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
                <!--Debut containt-->
                <div class=\"containt-inscrit\">
                    <div class=\"container\"> 
                        <div class=\"col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 inscrit\">
\t\t\t\t\t\t  <div class=\"col-md-10 col-md-offset-1 cadre-blanc\">
                            <form class=\"form-inscription\"  id=\"inscriptionForm\" action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo " method=\"post\">
                                <div class=\"\">
                                    <div class=\"row\">
                                     <div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t  <h2 class=\"titre_connect\">";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("reg.inscription"), "html", null, true);
        echo "</h2>
\t\t\t\t\t\t\t\t\t  <!--h4 class=\"titre_connect\">Organiser vos sports entre amis</h4-->
\t\t\t\t\t\t\t\t\t </div>
\t\t\t\t\t\t\t\t\t</div>
                                    <div class=\"row\">
                                        <div class=\"col-md-6\">
                                            ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "civility", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                        </div>
\t\t\t\t\t\t\t\t\t\t
                                        <div class=\"col-md-6\">
                                            ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "username", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.pseudo"))));
        echo "
                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-md-6\">
                                            ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "firstname", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.name"))));
        echo "
                                        </div>
\t\t\t\t\t\t\t\t\t\t
                                        <div class=\"col-md-6\">
                                            ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "lastname", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.lastname"))));
        echo "

                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-md-6\">
                                            ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "country", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                                        </div>
                                        <div class=\"col-md-6\">
                                            ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.adress"))));
        echo "

                                        </div>
\t\t\t\t\t\t\t\t\t\t
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-md-6\">
                                            ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.phone"))));
        echo "

                                        </div>
\t\t\t\t\t\t\t\t\t\t
                                        <div class=\"col-md-6\">
                                            ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.adress.email"))));
        echo "

                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-md-12\">
                                            ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "plainPassword", array()), "first", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("pass.word"))));
        echo "

                                        </div>
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-md-12\">
                                            ";
        // line 63
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "plainPassword", array()), "second", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("verif.pass.word"))));
        echo "

                                        </div>
                                    </div>
                                    <div class=\"row\">\t
                                        <div class=\"col-md-12\"> 
\t\t\t\t\t\t\t\t\t\t <h4 class=\"titre-inscrit2\"><label>";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.date.birth"), "html", null, true);
        echo "</label></h4>
                                        </div>\t
\t\t\t\t\t\t\t\t\t\t<div id=\"date_birth\">
                                           ";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "date_of_birth", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => $this->env->getExtension('translator')->trans("user.date.birth"))));
        echo "
                                        </div>\t
                                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-md-6\">
                                            <h4 class=\"titre-inscrit\">";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "handi", array()), 'label');
        echo "</h4>
                                        </div>
                                        <div class=\"col-md-6\">
                                            ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "handi", array()), 'widget');
        echo "
                                        </div>
                                    </div>
                                    <div class=\"row\" style=\"display: none\">
                                        <div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t  <h4 class=\"titre-inscrit\">";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coach", array()), 'label');
        echo "</h4>
\t\t\t\t\t\t\t\t\t\t</div>
                                        <div class=\"col-md-6\">
                                            ";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "coach", array()), 'widget');
        echo "
\t\t\t\t\t\t\t\t\t\t</div>
                                    </div>

                                    <div class=\"row\">
                                        <div class=\"col-md-6 col-md-offset-3\">
                                            <input type=\"submit\" class=\"btn btn-grinta\" value=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("submit.inscription"), "html", null, true);
        echo "\" />
                                        </div>
                                    </div>
                                </div> 
                                ";
        // line 98
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
                            </form>
\t\t\t\t\t\t  </div>
                        </div>
                    </div>
                </div>
                <!--Fin containt-->
                </div>
                </div> 
<script type=\"text/javascript\">
\$(\"select#fos_user_registration_form_country\").val(\"France\");
\$('#fos_user_registration_form_coach_1').prop('checked', true);
\$('#fos_user_registration_form_handi_1').prop('checked', true);
\$(\"#date_birth .col-lg-9\").addClass(\"col-md-4\");
\$(\"#date_birth .col-md-4\").removeClass(\"col-lg-9\");
</script>";
    }

    public function getTemplateName()
    {
        return "ApplicationSonataUserBundle:Registration:register_content2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 98,  170 => 94,  161 => 88,  155 => 85,  147 => 80,  141 => 77,  133 => 72,  127 => 69,  118 => 63,  109 => 57,  100 => 51,  92 => 46,  82 => 39,  76 => 36,  67 => 30,  60 => 26,  52 => 21,  45 => 17,  36 => 11,  27 => 7,  19 => 1,);
    }
}
