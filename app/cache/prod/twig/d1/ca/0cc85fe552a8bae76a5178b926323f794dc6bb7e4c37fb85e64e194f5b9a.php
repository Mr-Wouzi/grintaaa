<?php

/* SonataUserBundle:Security:base_login.html.twig */
class __TwigTemplate_d1ca0cc85fe552a8bae76a5178b926323f794dc6bb7e4c37fb85e64e194f5b9a extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'sonata_user_login_form' => array($this, 'block_sonata_user_login_form'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") == "fos_user_security_login")) {
            // line 2
            echo "

                <!--Debut containt-->
               <div class=\"containt\">
                  <div class=\"container\">

                     <div class=\"row col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 login-error\">
                         <div class=\"login-error-form row\">
\t\t\t\t\t\t  <div class=\" col-md-offset-1 col-md-10\">
\t\t\t\t\t\t   <h3 class=\"titre_connect\">";
            // line 11
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("se.connect.grintaaa"), "html", null, true);
            echo "</h3>
                          ";
            // line 12
            if ((isset($context["error"]) ? $context["error"] : null)) {
                // line 13
                echo "                            <p class=\"red\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.connect.grintaaa"), "html", null, true);
                echo "</p>
                          ";
            }
            // line 15
            echo "                             <form class=\"form-login-error\" method=\"post\" action=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
            echo "\" method=\"post\">

                            <div class=\"form-group\">
                                   <input type=\"text\" class=\"form-control2\" id=\"username\" name=\"_username\" placeholder=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("e.mail"), "html", null, true);
            echo "\">
                                 </div>

                               <div class=\"form-group\">
                                  <input type=\"password\" class=\"form-control2\" id=\"password\" name=\"_password\" placeholder=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pass.word"), "html", null, true);
            echo "\">
                                 </div>

                        <button type=\"submit\" class=\"btn btn-grinta\"><img src=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/go.png"), "html", null, true);
            echo "\"></button>



                             </form>
                             <div class=\"not-accont text-center\">
                                <a href=\"";
            // line 31
            echo $this->env->getExtension('routing')->getPath("fos_user_resetting_request");
            echo "\" class=\"green\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pass.oublie"), "html", null, true);
            echo "</a>
                                <a href=\"";
            // line 32
            echo $this->env->getExtension('routing')->getPath("personne");
            echo "\" class=\"green\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("insc.grintaaa"), "html", null, true);
            echo "</a>
                             </div>
\t\t\t\t\t\t</div>
                         </div>
                     </div>
                  </div>
               </div>
               <!--Fin containt-->
           </div>
        </div>


";
        } else {
            // line 45
            echo "    ";
            $this->displayBlock('fos_user_content', $context, $blocks);
        }
    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 46
        echo "


        ";
        // line 49
        $this->displayBlock('sonata_user_login_form', $context, $blocks);
        // line 88
        echo "

    ";
    }

    // line 49
    public function block_sonata_user_login_form($context, array $blocks = array())
    {
        // line 50
        echo "


            <form action='";
        // line 53
        echo $this->env->getExtension('routing')->getPath("fos_user_security_check");
        echo "' class=\"navbar-form navbar-left form-login connexion-form-marg\" role=\"Email\"
                  method=\"post\">
\t\t\t\t<div class=\"col-md-9 no-padding\">
\t\t\t\t
                <div class=\"form-group col-md-6\">
                    <input type=\"text\" class=\"form-control\" id=\"username\" name=\"_username\"
                           required=\"required\"
                           placeholder=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("e.mail"), "html", null, true);
        echo "\">
                </div>

                <div class=\"form-group col-md-6 no-padding\">
                    <input type=\"password\" class=\"form-control\" id=\"password\"
                           name=\"_password\" required=\"required\"
                           placeholder=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pass.word"), "html", null, true);
        echo "\">
                </div>
\t\t
                <div class=\"form-group col-md-6\">
\t\t\t\t\t<div>
\t\t\t\t     <input type=\"checkbox\" name=\"session_active\" class=\"session_active\" />
\t\t\t\t\t <div class=\"titre-session-active\">";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("session.activ"), "html", null, true);
        echo "</div>
\t\t\t\t\t</div>
                </div>

                <div class=\"form-group col-md-6 no-padding\">
\t\t\t\t\t<div class=\"passe-oublie\">
\t\t\t\t\t <a href='";
        // line 78
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_request");
        echo "' class=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pass.oublie"), "html", null, true);
        echo "</a>
\t\t\t\t\t</div>
                </div>

                </div>
\t\t\t\t<div class=\"col-md-3\">
                 <button type=\"submit\" class=\"btn btn-grinta\"><img src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/go.png"), "html", null, true);
        echo "\"></button>
                </div>
            </form>
        ";
    }

    public function getTemplateName()
    {
        return "SonataUserBundle:Security:base_login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 84,  165 => 78,  156 => 72,  147 => 66,  138 => 60,  128 => 53,  123 => 50,  120 => 49,  114 => 88,  112 => 49,  107 => 46,  99 => 45,  81 => 32,  75 => 31,  66 => 25,  60 => 22,  53 => 18,  46 => 15,  40 => 13,  38 => 12,  34 => 11,  23 => 2,  21 => 1,);
    }
}
