<?php

/* ComparatorMultimediaBundle:File:new.html.twig */
class __TwigTemplate_d576ee8e7dc450bc852138ac90b983608d183638814353596d1299474cfac51b extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt-->
<div class=\"container containt-dash\">
           <!--Debut left-block-->
           <div class=\"col-md-2 no-padding-right\"> 
               <div class=\"block profil-block\">
                   <div class=\"profil-dash\"> 
                         <div class=\"col-md-12 no-padding\">
\t\t\t\t\t\t    <div class=\"grid\">
\t\t\t\t\t         <figure class=\"effect-winston\">
\t\t\t\t\t\t      ";
        // line 10
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
\t\t\t\t\t\t      <figcaption>  
\t\t\t\t\t\t\t   <p> 
\t\t\t\t\t\t\t\t<a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\"><i class=\"fa fa-fw\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
\t\t\t\t\t\t\t   </p>
\t\t\t\t\t\t      </figcaption>\t\t\t
\t\t\t\t\t         </figure>
\t\t\t\t            </div>       
                         </div>
                         <div class=\"col-md-12 no-padding\">  
                             <p class=\"titre\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>
                             

                    <p class=\"titre-user\">      
\t\t\t\t\t\t<a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t<img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t</a>
\t\t\t\t\t</p>                             
                         </div>
                                        
                   </div>
                   <ul class=\"list-unstyled\">
                       <li>
                           <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"active\">
                              <img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                           </a>
                       </li>     
                       <li>
                           <a href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\">
                            ";
        // line 39
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                           </a>
                       </li>
                       
                       <li>
                           <a href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\">
                             ";
        // line 45
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                           </a>
                       </li>
                       <li>
                            ";
        // line 50
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                       </li>
                       <li>
                           <a href=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\">
                              ";
        // line 54
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                           </a>
                       </li>
                       <li>
                           <a href=\"";
        // line 58
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\">
                               ";
        // line 59
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                           </a>
                       </li>
                   </ul>
               </div>
 
               ";
        // line 65
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
               ";
        // line 66
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

            <br><br>  
           </div>
           <!--Fin left-block-->
           <!--Debut center-block-->
           <div class=\"col-md-10\"> 
    <div class=\"head-compte\">
        <div class=\"\">
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
        // line 76
        echo $this->env->getExtension('routing')->getPath("fos_user_profile_edit_authentication");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-user\"></i>  <br>
                    ";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.compte"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>
            <div class=\"col-md-2 col-sm-3 no-padding\">
                <a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("fos_user_change_password");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-pencil-square-o\"></i>  <br>
                    ";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("change.password"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>
            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
        // line 88
        echo $this->env->getExtension('routing')->getPath("grintaaa_interest");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-star\"></i>  
\t\t\t\t <br>";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
        echo "
\t\t\t\t</a>       
            </div>
            ";
        // line 93
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "coach", array()) == 1)) {
            // line 94
            echo "            <div class=\"col-md-1 col-sm-3 no-padding\">
                <a href=\"";
            // line 95
            echo $this->env->getExtension('routing')->getPath("grintaaa_coaching");
            echo "\" class=\"btn btn-compte\">
                    <i class=\"fa fa-futbol-o\"></i>
                    <br>";
            // line 97
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.coaching"), "html", null, true);
            echo "
                </a>
            </div>
            ";
        }
        // line 101
        echo "            <div class=\"col-md-2 col-sm-3\">
                <a href=\"";
        // line 102
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\" class=\"btn btn-compte active\">
\t\t\t \t <i class=\"fa fa-picture-o\"></i>  
\t\t\t\t <br>";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("my.avatar"), "html", null, true);
        echo "
\t\t\t\t</a>
            </div>

            <div class=\"col-md-3 col-sm-3 no-padding\">
                <a href=\"";
        // line 109
        echo $this->env->getExtension('routing')->getPath("grintaaa_configuration_email");
        echo "\" class=\"btn btn-compte\">
\t\t\t \t <i class=\"fa fa-envelope-o\"></i>
\t\t\t\t <br>";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notif.email"), "html", null, true);
        echo "
\t\t        </a>
            </div>

        </div> 
    </div>       
\t<div class=\"body-compte\"><br> 
\t\t<div class=\"col-md-12 titre-gris\" align=\"center\">
\t\t\tAlbums
\t\t</div><br>
\t\t<div class=\"col-md-12 no-padding\">
\t\t\t<div class=\"col-md-12 header-photos\">
\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t<a href=\"";
        // line 124
        echo $this->env->getExtension('routing')->getPath("user_album_new");
        echo "\">
                        + ";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("creat.album"), "html", null, true);
        echo "
                    </a>
\t\t
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-6\" align=\"right\">
\t\t\t\t\t<form class=\"form-inscription\" action=\"";
        // line 130
        echo $this->env->getExtension('routing')->getPath("user_file_create");
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo ">
\t\t\t\t\t\t<div class=\"col-md-8 no-padding\" align=\"center\">
\t\t\t\t\t\t\t";
        // line 132
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "file", array()), 'errors');
        echo "    
\t\t\t\t\t\t\t";
        // line 133
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "file", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t\t\t<label for=\"my-file\" class=\"input-file-trigger\" tabindex=\"0\">";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("parcour.new.tof"), "html", null, true);
        echo "</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-4 no-padding\">
\t\t\t\t\t\t\t<input type=\"submit\" class=\"btn btn-grinta-album\" name=\"\" value=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.submit.jour"), "html", null, true);
        echo "\">
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t<p class=\"file-return\"></p>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 142
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
\t\t\t\t\t</form>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-md-12 container-photos\">
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<div class=\"cadre-album no-padding\">
\t\t\t\t\t\t<a href=\"";
        // line 149
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\">
\t\t\t\t\t\t\t<div class=\"cadre-image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/play-icon-2-512.png"), "html", null, true);
        echo "\" class=\"icon-video\" alt=\"\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"titre-album-image\">
\t\t\t\t\t\t\t\tMes Vidéos
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</a>
\t\t\t\t\t</div> 
\t\t\t\t</div>
                <div class=\"col-md-3\">
                    <div class=\"cadre-album no-padding\">
                        <a href=\"";
        // line 161
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\">
                            <div class=\"cadre-image\">
\t\t\t\t\t\t\t\t<img src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/photo-icon-2-512.png"), "html", null, true);
        echo "\" class=\"icon-video\" alt=\"\" />
                            </div>
                            <div class=\"titre-album-image\">
                                Mes Photos
                            </div>
                        </a>
                    </div>
                </div>
\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t<div class=\"cadre-album no-padding\">
\t\t\t\t\t<a href=\"";
        // line 173
        echo $this->env->getExtension('routing')->getPath("user_file_show");
        echo "\">
\t\t\t\t\t\t<div class=\"cadre-image\">
                                <img src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-loginnnn.png"), "html", null, true);
        echo "\" class=\"icon-video\" alt=\"\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"titre-album-image\">
\t\t\t\t\t\t\t";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("grintaa.album"), "html", null, true);
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</a>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t";
        // line 184
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["albums"]) ? $context["albums"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["album"]) {
            // line 185
            echo "\t\t\t\t\t<div class=\"col-md-3\">
\t\t\t\t\t\t<div class=\"cadre-album no-padding\">
\t\t\t\t\t\t";
            // line 187
            if ($this->getAttribute($context["album"], "logo", array())) {
                // line 188
                echo "\t\t\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_album_show", array("slug" => $this->getAttribute($context["album"], "slug", array()))), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t<div class=\"cadre-image\">
\t\t\t\t\t\t\t\t\t<img src=\"";
                // line 190
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "logo", array()), "html", null, true);
                echo "\" class=\"img-responsive\" alt=\"\" />
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"titre-album-image\">
\t\t\t\t\t\t\t\t\t";
                // line 193
                echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "title", array()), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t";
            } else {
                // line 197
                echo "\t\t\t\t\t\t\t<a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_album_show", array("slug" => $this->getAttribute($context["album"], "slug", array()))), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t<div class=\"cadre-image\"><br><br><br>
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-plus\"></i> <br>
\t\t\t\t\t\t\t\t\tAjouter photo
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"titre-album-image\">
\t\t\t\t\t\t\t\t\t";
                // line 203
                echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "title", array()), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t";
            }
            // line 207
            echo "\t\t\t\t\t     </div>
\t\t\t\t    </div>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 210
        echo "\t\t\t</div>
\t\t</div>
\t\t<div class=\"clearfix\"></div> 
\t\t<br><br>
\t\t<div class=\"col-md-12 titre-gris\" align=\"center\">
\t\t\t";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("choice.avatar.profil"), "html", null, true);
        echo "
\t\t</div>
\t

        <div class=\"col-md-11 col-md-offset-1 col-sm-6 inscrit\">
          ";
        // line 220
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "civility", array()) == "Femme")) {
            // line 221
            echo "\t\t   <div class=\"col-md-1 avatar-select\">  
            <form class=\"form-inscriptions\" action=\"";
            // line 222
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme1");
            echo "\" method=\"post\" id=\"AvatarFemme1\">
              <img onclick=\"getAvatarFemme1(this)\" src=\"";
            // line 223
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/1AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 224
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
            </form>
           </div>

\t\t   <div class=\"col-md-1 avatar-select\">  
            <form class=\"form-inscriptions\" action=\"";
            // line 229
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme2");
            echo "\" method=\"post\" id=\"AvatarFemme2\">
              <img onclick=\"getAvatarFemme2(this)\" src=\"";
            // line 230
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/2AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 231
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
            </form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\">  
            <form class=\"form-inscriptions\" action=\"";
            // line 237
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme3");
            echo "\" method=\"post\" id=\"AvatarFemme3\">
              <img onclick=\"getAvatarFemme3(this)\" src=\"";
            // line 238
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/3AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 239
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
\t\t\t</form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\">  
\t\t\t<form class=\"form-inscriptions\" action=\"";
            // line 245
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme4");
            echo "\" method=\"post\" id=\"AvatarFemme4\">
              <img onclick=\"getAvatarFemme4(this)\" src=\"";
            // line 246
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/4AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 247
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
\t\t\t</form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
\t\t\t<form class=\"form-inscriptions\" action=\"";
            // line 253
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme5");
            echo "\" method=\"post\" id=\"AvatarFemme5\">
              <img onclick=\"getAvatarFemme5(this)\" src=\"";
            // line 254
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/5AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 255
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
\t\t\t</form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
\t\t\t<form class=\"form-inscriptions\" action=\"";
            // line 261
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme6");
            echo "\" method=\"post\" id=\"AvatarFemme6\">
              <img onclick=\"getAvatarFemme6(this)\" src=\"";
            // line 262
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/6AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 263
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
\t\t\t</form>
           </div>


\t\t   <div class=\"col-md-1 col-md-offset-1 avatar-select\"> 
\t\t\t<form class=\"form-inscriptions\" action=\"";
            // line 269
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme7");
            echo "\" method=\"post\" id=\"AvatarFemme7\">
              <img onclick=\"getAvatarFemme7(this)\" src=\"";
            // line 270
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/7AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 271
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
\t\t\t</form>
           </div>

\t\t   <div class=\"col-md-1 avatar-select\"> 
\t\t\t<form class=\"form-inscriptions\" action=\"";
            // line 276
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme8");
            echo "\" method=\"post\" id=\"AvatarFemme8\">
              <img onclick=\"getAvatarFemme8(this)\" src=\"";
            // line 277
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/8AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 278
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
\t\t\t</form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
\t\t\t<form class=\"form-inscriptions\" action=\"";
            // line 284
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme9");
            echo "\" method=\"post\" id=\"AvatarFemme9\">
              <img onclick=\"getAvatarFemme9(this)\" src=\"";
            // line 285
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/9AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 286
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
\t\t\t</form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
\t\t\t<form class=\"form-inscriptions\" action=\"";
            // line 292
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme10");
            echo "\" method=\"post\" id=\"AvatarFemme10\">
              <img onclick=\"getAvatarFemme10(this)\" src=\"";
            // line 293
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/10AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 294
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
\t\t\t</form>
           </div>

\t\t   <div class=\"col-md-1 avatar-select\"> 
\t\t\t<form class=\"form-inscriptions\" action=\"";
            // line 299
            echo $this->env->getExtension('routing')->getPath("user_file_avatarFemme11");
            echo "\" method=\"post\" id=\"AvatarFemme11\">
              <img onclick=\"getAvatarFemme11(this)\" src=\"";
            // line 300
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/11AvatarFemme.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
              <input type=\"submit\"  name=\"\" value=\"";
            // line 301
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
\t\t\t</form>
           </div>
       ";
        }
        // line 305
        echo "
     ";
        // line 306
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "civility", array()) == "Homme")) {
            // line 307
            echo "\t\t   <div class=\"col-md-1 avatar-select\"> 
            <form class=\"form-inscriptions\" action=\"";
            // line 308
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes1");
            echo "\" method=\"post\" id=\"AvatarHommes1\">
                <img onclick=\"getAvatarHommes1(this)\" src=\"";
            // line 309
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/1AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                <input type=\"submit\"  name=\"\" value=\"";
            // line 310
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
            </form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
            <form class=\"form-inscriptions\" action=\"";
            // line 316
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes2");
            echo "\" method=\"post\" id=\"AvatarHommes2\">
                <img onclick=\"getAvatarHommes2(this)\" src=\"";
            // line 317
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/2AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                <input type=\"submit\"  name=\"\" value=\"";
            // line 318
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
            </form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
            <form class=\"form-inscriptions\" action=\"";
            // line 324
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes3");
            echo "\" method=\"post\" id=\"AvatarHommes3\">
                <img onclick=\"getAvatarHommes3(this)\" src=\"";
            // line 325
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/3AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                <input type=\"submit\"  name=\"\" value=\"";
            // line 326
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
            </form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
            <form class=\"form-inscriptions\" action=\"";
            // line 332
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes4");
            echo "\" method=\"post\" id=\"AvatarHommes4\">
                <img onclick=\"getAvatarHommes4(this)\" src=\"";
            // line 333
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/4AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                <input type=\"submit\"  name=\"\" value=\"";
            // line 334
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
            </form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
            <form class=\"form-inscriptions\" action=\"";
            // line 340
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes5");
            echo "\" method=\"post\" id=\"AvatarHommes5\">
                <img onclick=\"getAvatarHommes5(this)\" src=\"";
            // line 341
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/5AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                <input type=\"submit\"  name=\"\" value=\"";
            // line 342
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
            </form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
            <form class=\"form-inscriptions\" action=\"";
            // line 348
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes6");
            echo "\" method=\"post\" id=\"AvatarHommes6\">
                <img onclick=\"getAvatarHommes6(this)\" src=\"";
            // line 349
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/6AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                <input type=\"submit\"  name=\"\" value=\"";
            // line 350
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
            </form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
            <form class=\"form-inscriptions\" action=\"";
            // line 356
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes7");
            echo "\" method=\"post\" id=\"AvatarHommes7\">
                <img onclick=\"getAvatarHommes7(this)\" src=\"";
            // line 357
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/7AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                <input type=\"submit\"  name=\"\" value=\"";
            // line 358
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
            </form>
           </div>

\t\t   <div class=\"col-md-1 avatar-select\"> 
                <form class=\"form-inscriptions\" action=\"";
            // line 363
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes8");
            echo "\" method=\"post\" id=\"AvatarHommes8\">
                    <img onclick=\"getAvatarHommes8(this)\" src=\"";
            // line 364
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/8AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                    <input type=\"submit\"  name=\"\" value=\"";
            // line 365
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
                </form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
                <form class=\"form-inscriptions\" action=\"";
            // line 371
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes9");
            echo "\" method=\"post\" id=\"AvatarHommes9\">
                    <img onclick=\"getAvatarHommes9(this)\" src=\"";
            // line 372
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/9AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                    <input type=\"submit\"  name=\"\" value=\"";
            // line 373
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
                </form>
           </div>


\t\t   <div class=\"col-md-1 avatar-select\"> 
                <form class=\"form-inscriptions\" action=\"";
            // line 379
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes10");
            echo "\" method=\"post\" id=\"AvatarHommes10\">
                    <img onclick=\"getAvatarHommes10(this)\" src=\"";
            // line 380
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/10AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                    <input type=\"submit\"  name=\"\" value=\"";
            // line 381
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
                </form>
           </div>

\t\t   <div class=\"col-md-1 avatar-select\"> 
                <form class=\"form-inscriptions\" action=\"";
            // line 386
            echo $this->env->getExtension('routing')->getPath("user_file_avatarHommes11");
            echo "\" method=\"post\" id=\"AvatarHommes11\">
                    <img onclick=\"getAvatarHommes11(this)\" src=\"";
            // line 387
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/11AvatarHommes.jpg"), "html", null, true);
            echo "\" class=\"img-responsive\" alt=\"\" />
                    <input type=\"submit\"  name=\"\" value=\"";
            // line 388
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.ok"), "html", null, true);
            echo "\" style=\"display: none\">
                </form>
           </div>
                ";
        }
        // line 391
        echo "   
\t</div>
           </div>
           <!--Fin center-block-->
</div>
</div>
<br><br>
<script>

    function getAvatarFemme1(theForm) {
        \$(\"#AvatarFemme1\").submit();
    }
    function getAvatarFemme2(theForm) {
        \$(\"#AvatarFemme2\").submit();
    }
    function getAvatarFemme3(theForm) {
        \$(\"#AvatarFemme3\").submit();
    }
    function getAvatarFemme4(theForm) {
        \$(\"#AvatarFemme4\").submit();
    }
    function getAvatarFemme5(theForm) {
        \$(\"#AvatarFemme5\").submit();
    }
    function getAvatarFemme6(theForm) {
        \$(\"#AvatarFemme6\").submit();
    }
    function getAvatarFemme7(theForm) {
        \$(\"#AvatarFemme7\").submit();
    }
    function getAvatarFemme8(theForm) {
        \$(\"#AvatarFemme8\").submit();
    }
    function getAvatarFemme9(theForm) {
        \$(\"#AvatarFemme9\").submit();
    }
    function getAvatarFemme10(theForm) {
        \$(\"#AvatarFemme10\").submit();
    }
    function getAvatarFemme11(theForm) {
        \$(\"#AvatarFemme11\").submit();
    }

    function getAvatarHommes1(theForm) {
        \$(\"#AvatarHommes1\").submit();
    }

    function getAvatarHommes2(theForm) {
        \$(\"#AvatarHommes2\").submit();
    }
    function getAvatarHommes3(theForm) {
        \$(\"#AvatarHommes3\").submit();
    }
    function getAvatarHommes4(theForm) {
        \$(\"#AvatarHommes4\").submit();
    }
    function getAvatarHommes5(theForm) {
        \$(\"#AvatarHommes5\").submit();
    }
    function getAvatarHommes6(theForm) {
        \$(\"#AvatarHommes6\").submit();
    }
    function getAvatarHommes7(theForm) {
        \$(\"#AvatarHommes7\").submit();
    }
    function getAvatarHommes8(theForm) {
        \$(\"#AvatarHommes8\").submit();
    }
    function getAvatarHommes9(theForm) {
        \$(\"#AvatarHommes9\").submit();
    }
    function getAvatarHommes10(theForm) {
        \$(\"#AvatarHommes10\").submit();
    }
    function getAvatarHommes11(theForm) {
        \$(\"#AvatarHommes11\").submit();
    }

document.querySelector(\"html\").classList.add('js');

var fileInput  = document.querySelector( \".form-control\" ),  
    button     = document.querySelector( \".input-file-trigger\" ),
    the_return = document.querySelector(\".file-return\");
      
button.addEventListener( \"keydown\", function( event ) {  
    if ( event.keyCode == 13 || event.keyCode == 32 ) {  
        fileInput.focus();  
    }  
});
button.addEventListener( \"click\", function( event ) {
   fileInput.focus();
   return false;
});  
fileInput.addEventListener( \"change\", function( event ) {  
    the_return.innerHTML = this.value;  
});  
\$(\".input-file-container .col-lg-9\").removeClass(\"col-lg-9\");
</script>
<!--Fin containt-->";
    }

    public function getTemplateName()
    {
        return "ComparatorMultimediaBundle:File:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  802 => 391,  795 => 388,  791 => 387,  787 => 386,  779 => 381,  775 => 380,  771 => 379,  762 => 373,  758 => 372,  754 => 371,  745 => 365,  741 => 364,  737 => 363,  729 => 358,  725 => 357,  721 => 356,  712 => 350,  708 => 349,  704 => 348,  695 => 342,  691 => 341,  687 => 340,  678 => 334,  674 => 333,  670 => 332,  661 => 326,  657 => 325,  653 => 324,  644 => 318,  640 => 317,  636 => 316,  627 => 310,  623 => 309,  619 => 308,  616 => 307,  614 => 306,  611 => 305,  604 => 301,  600 => 300,  596 => 299,  588 => 294,  584 => 293,  580 => 292,  571 => 286,  567 => 285,  563 => 284,  554 => 278,  550 => 277,  546 => 276,  538 => 271,  534 => 270,  530 => 269,  521 => 263,  517 => 262,  513 => 261,  504 => 255,  500 => 254,  496 => 253,  487 => 247,  483 => 246,  479 => 245,  470 => 239,  466 => 238,  462 => 237,  453 => 231,  449 => 230,  445 => 229,  437 => 224,  433 => 223,  429 => 222,  426 => 221,  424 => 220,  416 => 215,  409 => 210,  401 => 207,  394 => 203,  384 => 197,  377 => 193,  370 => 190,  364 => 188,  362 => 187,  358 => 185,  354 => 184,  345 => 178,  339 => 175,  334 => 173,  321 => 163,  316 => 161,  303 => 151,  298 => 149,  288 => 142,  280 => 137,  274 => 134,  270 => 133,  266 => 132,  259 => 130,  251 => 125,  247 => 124,  231 => 111,  226 => 109,  218 => 104,  213 => 102,  210 => 101,  203 => 97,  198 => 95,  195 => 94,  193 => 93,  187 => 90,  182 => 88,  175 => 84,  170 => 82,  163 => 78,  158 => 76,  145 => 66,  141 => 65,  132 => 59,  128 => 58,  121 => 54,  117 => 53,  111 => 50,  103 => 45,  99 => 44,  91 => 39,  87 => 38,  78 => 34,  74 => 33,  61 => 25,  57 => 24,  48 => 20,  36 => 13,  30 => 10,  19 => 1,);
    }
}
