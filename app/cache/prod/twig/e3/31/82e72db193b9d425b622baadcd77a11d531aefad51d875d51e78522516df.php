<?php

/* ComparatorEventBundle:Friends:listFriend.html.twig */
class __TwigTemplate_e33182e72db193b9d425b622baadcd77a11d531aefad51d875d51e78522516df extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt-->
<div class=\"container containt-dash\">
    <!--Debut left-block-->
    <div class=\"col-md-2 no-padding-right\">
        <div class=\"block profil-block\">
            <div class=\"profil-dash\">
                <div class=\"col-md-12 no-padding\">
                    <div class=\"grid\">
                        <figure class=\"effect-winston\">
                            ";
        // line 10
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
                            <figcaption>
                                <p>
                                    <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\" title=\"defi sportif entre amis\"><i class=\"fa fa-fw\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
                                </p>
                            </figcaption>
                        </figure>
                    </div>   
                </div>
                <div class=\"col-md-12 no-padding\">
                    <p class=\"titre\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>
                    <p class=\"titre-user\">      
\t\t\t\t\t\t\t\t<a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</p> 
                    <!--p class=\"pro\">Profil</p-->
                </div>

            </div>
            <ul class=\"list-unstyled\">
                <li>
                    <a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" title=\"defi sportif entre amis\">
                        <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\" alt=\"defi sportif entre amis\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\" title=\"defi sportif entre amis\">
                        ";
        // line 38
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                    </a>
                </li>

                <li>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\" class=\"active\" title=\"defi sportif entre amis\">
                        ";
        // line 44
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "
                    </a>
                </li>
                <li>
                    ";
        // line 48
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                </li>
                <li>
                    <a href=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\" title=\"defi sportif entre amis\">
                        ";
        // line 52
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 56
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\" title=\"defi sportif entre amis\">
                        ";
        // line 57
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                    </a>
                </li>
            </ul>
        </div>

        ";
        // line 63
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
        ";
        // line 64
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

    <br><br>
    </div>

    <div class=\"col-md-10\">
    <div class=\"formulaire-search col-md-10\">
        <div class=\"img-search\">
            <img src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/my-friends.png"), "html", null, true);
        echo "\" alt=\"defi sportif entre amis\" class=\"img-friends\">
        </div>
        <div class=\"title-search\">
            <h4 class=\"green friends\">";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.friends"), "html", null, true);
        echo "</h4>
        </div>
    </div>
    <div class=\"list-games\">
        <div class=\"\">

            ";
        // line 81
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["allFreinds"]) ? $context["allFreinds"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
            // line 82
            echo "
                <div class=\"col-md-6\">
                  <div class=\"col-md-12 cadre-ami no-padding\">
                    
                      <div class=\"img-search2\">
                        <a href=\"";
            // line 87
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($context["entity"], "username", array()), "id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\" title=\"defi sportif entre amis\">
                        ";
            // line 88
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                // line 89
                echo "                            <img  src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                echo "\" alt=\"defi sportif entre amis\">
                        ";
            } else {
                // line 91
                echo "                            <img  src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                echo "\" alt=\"defi sportif entre amis\">
                        ";
            }
            // line 93
            echo "\t\t\t\t\t\t</a>
\t\t\t\t\t  </div>
                      <div class=\"info-date2\">
                        <a href=\"";
            // line 96
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($context["entity"], "username", array()), "id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\" title=\"defi sportif entre amis\">
\t\t\t\t\t\t\t<h2 class=\"green\">";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "lastname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "firstname", array()), "html", null, true);
            echo " ";
            if (($this->getAttribute($context["entity"], "coach", array()) == 1)) {
                echo "(";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("user.u.coachs"), "html", null, true);
                echo ")";
            }
            echo "</h2>
\t\t\t\t\t\t</a>
\t\t\t\t\t\t<ul class=\"list-unstyled\">
                                        <li class=\"star\"><img
                                                    src=\"";
            // line 101
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/star.png"), "html", null, true);
            echo "\" alt=\"defi sportif entre amis\">
                                            ";
            // line 102
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.interest"), "html", null, true);
            echo " :
\t\t\t\t\t\t\t\t\t\t\t";
            // line 103
            $context["x"] = 0;
            // line 104
            echo "                                            ";
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["interest"]) ? $context["interest"] : null), $context["key"], array(), "array")) > 0)) {
                // line 105
                echo "                                                ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["interest"]) ? $context["interest"] : null), $context["key"], array(), "array"));
                foreach ($context['_seq'] as $context["keys"] => $context["interested"]) {
                    // line 106
                    echo "\t\t\t\t\t\t\t\t\t\t\t      ";
                    $context["x"] = ((isset($context["x"]) ? $context["x"] : null) + 1);
                    // line 107
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t  ";
                    if (((isset($context["x"]) ? $context["x"] : null) < 8)) {
                        // line 108
                        echo "                                                    *";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["interested"], "category", array()), "title", array()), "html", null, true);
                        echo "
\t\t\t\t\t\t\t\t\t\t\t\t  ";
                    }
                    // line 110
                    echo "                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['keys'], $context['interested'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                echo "...

                                            ";
            }
            // line 113
            echo "                                        </li>
                            ";
            // line 114
            if (($this->getAttribute($context["entity"], "coach", array()) == 1)) {
                // line 115
                echo "                            <li class=\"star\"><img
                                        src=\"";
                // line 116
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/star.png"), "html", null, true);
                echo "\" alt=\"defi sportif entre amis\"> ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("center.coaching"), "html", null, true);
                echo " :
                                ";
                // line 117
                $context["x"] = 0;
                // line 118
                echo "                                ";
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["coaching"]) ? $context["coaching"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 119
                    echo "                                    ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["coaching"]) ? $context["coaching"] : null), $context["key"], array(), "array"));
                    foreach ($context['_seq'] as $context["keys"] => $context["coachinged"]) {
                        // line 120
                        echo "                                        ";
                        $context["x"] = ((isset($context["x"]) ? $context["x"] : null) + 1);
                        // line 121
                        echo "                                        ";
                        if (((isset($context["x"]) ? $context["x"] : null) < 8)) {
                            // line 122
                            echo "                                            *";
                            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["coachinged"], "category", array()), "title", array()), "html", null, true);
                            echo "
                                        ";
                        }
                        // line 124
                        echo "                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['keys'], $context['coachinged'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    echo "...

                                ";
                }
                // line 127
                echo "                            </li>
                            ";
            }
            // line 129
            echo "                                    </ul>
\t\t\t\t\t\t
\t\t\t\t\t  </div>               
\t\t\t\t\t  <div class=\"info-date3\">
\t\t\t\t\t    <a href=\"";
            // line 133
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($context["entity"], "username", array()), "id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\" title=\"defi sportif entre amis\" class=\"btn btn-grinta\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("button.affiche"), "html", null, true);
            echo "</a>
\t\t\t\t\t    <form id=\"validate_field_types\" method=\"POST\" action=\"";
            // line 134
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_friend_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">
                            <input type=\"submit\" class=\"btn btn-refus\" value=\"";
            // line 135
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.delete"), "html", null, true);
            echo "\"/>
                        </form>
\t\t\t\t\t  </div>          

\t\t\t\t\t
\t\t\t\t  </div>       
                </div>                 

  



            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 148
        echo "        </div>
    </div>
  </div>
</div>
<!--Fin containt-->";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Friends:listFriend.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  348 => 148,  329 => 135,  325 => 134,  319 => 133,  313 => 129,  309 => 127,  299 => 124,  293 => 122,  290 => 121,  287 => 120,  282 => 119,  279 => 118,  277 => 117,  271 => 116,  268 => 115,  266 => 114,  263 => 113,  253 => 110,  247 => 108,  244 => 107,  241 => 106,  236 => 105,  233 => 104,  231 => 103,  227 => 102,  223 => 101,  208 => 97,  204 => 96,  199 => 93,  193 => 91,  186 => 89,  184 => 88,  180 => 87,  173 => 82,  169 => 81,  160 => 75,  154 => 72,  143 => 64,  139 => 63,  130 => 57,  126 => 56,  119 => 52,  115 => 51,  109 => 48,  102 => 44,  98 => 43,  90 => 38,  86 => 37,  77 => 33,  73 => 32,  59 => 23,  55 => 22,  48 => 20,  36 => 13,  30 => 10,  19 => 1,);
    }
}
