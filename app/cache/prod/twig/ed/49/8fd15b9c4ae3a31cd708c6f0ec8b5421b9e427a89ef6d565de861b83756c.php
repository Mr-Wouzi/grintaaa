<?php

/* ComparatorEventBundle:Event:listEventByUser.html.twig */
class __TwigTemplate_ed498fd15b9c4ae3a31cd708c6f0ec8b5421b9e427a89ef6d565de861b83756c extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt-->
<div class=\"container containt-dash\">
    <!--Debut left-block-->
    <div class=\"col-md-2 no-padding-right\">
        <div class=\"block profil-block\">
            <div class=\"profil-dash\">
                <div class=\"col-md-12 no-padding\">
                    <div class=\"grid\">
                        <figure class=\"effect-winston\">
                            ";
        // line 10
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorMultimediaBundle:File:logo"));
        echo "
                            <figcaption>
                                <p>
                                    <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("user_file_new");
        echo "\" title=\"organiser une journée sportive\"><i class=\"fa fa-fw\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("update.tof"), "html", null, true);
        echo "</i></a>
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class=\"col-md-12 no-padding\">
                    <p class=\"titre\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastname", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "firstname", array()), "html", null, true);
        echo "</p>
                    <p class=\"titre-user\">      
\t\t\t\t\t\t\t\t<a href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))), "html", null, true);
        echo "\"> 
\t\t\t\t\t\t\t\t\t<img src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/logo-login2.png"), "html", null, true);
        echo "\" class=\"img-loginnn\" />";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
        echo " 
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</p>   
                    <!--p class=\"pro\">Profil</p-->
                </div>

            </div>
            <ul class=\"list-unstyled\">
                <li>
                    <a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" title=\"organiser une journée sportive\">
                        <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/profil/actualite.png"), "html", null, true);
        echo "\" alt=\"organiser une journée sportive\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fil.act"), "html", null, true);
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("grintaaa_notification");
        echo "\" title=\"organiser une journée sportive\">
                        ";
        // line 38
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Notification:counNotification"));
        echo "
                    </a>
                </li>

                <li>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend");
        echo "\" title=\"organiser une journée sportive\">
                        ";
        // line 44
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:countListFriend"));
        echo "

                    </a>
                </li>
                <li>
                    ";
        // line 49
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:countEventByUser"));
        echo "
                </li>
                <li>
                    <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_image");
        echo "\" title=\"organiser une journée sportive\">
                        ";
        // line 53
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countImages"));
        echo "
                    </a>
                </li>
                <li>
                    <a href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("grintaaa_multimedia_video");
        echo "\" title=\"organiser une journée sportive\">
                        ";
        // line 58
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Mur:countVideos"));
        echo "
                    </a>
                </li>
            </ul>
        </div>

        ";
        // line 64
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Event:listEventFriendsHome"));
        echo "
        ";
        // line 65
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('sonata_page')->controller("ComparatorEventBundle:Friends:listFriends"));
        echo "

    <br><br>
    </div>

    <div class=\"col-md-10\">
    <div class=\"formulaire-search col-md-12\">
        <div class=\"img-search\">
           <i class=\"fa fa-list icon-puce green\" aria-hidden=\"true\"></i>
        </div>
        <div class=\"title-search\">
            <h4 class=\"green\">";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.sport.cours"), "html", null, true);
        echo "</h4>
        </div>
\t\t<br>
     <div class=\"result-search\">  

            ";
        // line 81
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
            // line 82
            echo "         ";
            if ((twig_date_format_filter($this->env, "now", "Y-m-d") < twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "startdate", array()), "Y-m-d"))) {
                // line 83
                echo "                <div class=\"block item-act\">
                    <div class=\"row\">
                        <div class=\"col-md-2\">
                            <a href=\"";
                // line 86
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()))), "html", null, true);
                echo "\" title=\"organiser une journée sportive\">
                                <div class=\"img-avatar\">
                                    ";
                // line 88
                if ((twig_length_filter($this->env, (isset($context["image"]) ? $context["image"] : null)) > 0)) {
                    // line 89
                    echo "                                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["image"]) ? $context["image"] : null), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"organiser une journée sportive\">
                                    ";
                } else {
                    // line 91
                    echo "                                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"organiser une journée sportive\">
                                    ";
                }
                // line 93
                echo "                                </div>
                                <div class=\"name-avatar\">
                                    <h5 class=\"text-center\">";
                // line 95
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                </div>
                            </a>
                        </div> 
                        <div class=\"col-md-4 text-center\">
                            <h3 class=\"name-match green\">";
                // line 100
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "title", array()), "html", null, true);
                echo "</h3>
                            ";
                // line 101
                if ((null === $this->getAttribute($context["entity"], "nbPlayer", array()))) {
                    // line 102
                    echo "                            <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.participants"), "html", null, true);
                    echo "</p>
                            ";
                } else {
                    // line 104
                    echo "                            <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nbPlayer", array()), "html", null, true);
                    echo "</p> ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.participants"), "html", null, true);
                    echo "
                            ";
                }
                // line 106
                echo "                        </div>  
                        <div class=\"col-md-4 info-date\"> 
                            <ul class=\"list-unstyled\">
                                <li class=\" size-date\"><img src=\"";
                // line 109
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/date.png"), "html", null, true);
                echo "\" alt=\"organiser une journée sportive\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "startdate", array()), "d-m-Y"), "html", null, true);
                echo "</li>
                                <li class=\"lieu\"><img src=\"";
                // line 110
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
                echo "\" alt=\"organiser une journée sportive\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "locality", array()), "html", null, true);
                echo " ( ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "country", array()), "html", null, true);
                echo ")</li>
                                <li><img src=\"";
                // line 111
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/heure.png"), "html", null, true);
                echo "\" alt=\"organiser une journée sportive\"> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "starttime", array()), "H:i"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "endtime", array()), "H:i"), "html", null, true);
                echo "</li>
                            </ul>
                        </div>
                        <div class=\"col-md-2 info-date\"> 
\t\t\t\t\t\t <h3 class=\"name-match green\"></h3>
                         <a href=\"";
                // line 116
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($context["entity"], "slug", array()))), "html", null, true);
                echo "\" title=\"organiser une journée sportive\" class=\"btn btn-grinta\" title=\"organiser une journée sportive\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fiche.match"), "html", null, true);
                echo "</a>

                         <form id=\"validate_field_types\" method=\"POST\" action=\"";
                // line 118
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\">
                            <input type=\"submit\" class=\"btn btn-refus btn-refus-invit\" value=\"";
                // line 119
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.delete"), "html", null, true);
                echo "\" />
                         </form>
\t\t\t\t\t\t</div>
                    </div>
                </div>
         ";
            }
            // line 125
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "        </div>
    </div>


        <div class=\"formulaire-search col-md-12\">
\t\t    <br><br>
            <div class=\"img-search\">
                <i class=\"fa fa-list icon-puce green\" aria-hidden=\"true\"></i>
            </div>
            <div class=\"title-search\">
                <h4 class=\"green\">";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mes.sport.arch"), "html", null, true);
        echo "</h4>
            </div>
            <br>
            <div class=\"result-search\">

                ";
        // line 141
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
            // line 142
            echo "                ";
            if ((twig_date_format_filter($this->env, "now", "Y-m-d") > twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "startdate", array()), "Y-m-d"))) {
                // line 143
                echo "                    <div class=\"block item-act\">
                        <div class=\"row\">
                            <div class=\"col-md-2\">
                                <a href=\"";
                // line 146
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "id", array()))), "html", null, true);
                echo "\" title=\"organiser une journée sportive\">
                                    <div class=\"img-avatar\">
                                        ";
                // line 148
                if ((twig_length_filter($this->env, (isset($context["image"]) ? $context["image"] : null)) > 0)) {
                    // line 149
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["image"]) ? $context["image"] : null), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"organiser une journée sportive\">
                                        ";
                } else {
                    // line 151
                    echo "                                            <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"organiser une journée sportive\">
                                        ";
                }
                // line 153
                echo "                                    </div>
                                    <div class=\"name-avatar\">
                                        <h5 class=\"text-center\">";
                // line 155
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user", array()), "firstname", array()), "html", null, true);
                echo "</h5>
                                    </div>
                                </a>
                            </div>
                            <div class=\"col-md-4 text-center\">
                                <h3 class=\"name-match green\">";
                // line 160
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "title", array()), "html", null, true);
                echo "</h3>
                                ";
                // line 161
                if ((null === $this->getAttribute($context["entity"], "nbPlayer", array()))) {
                    // line 162
                    echo "                                    <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.participants"), "html", null, true);
                    echo "</p>
                                ";
                } else {
                    // line 164
                    echo "                                    <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["nbparticipe"]) ? $context["nbparticipe"] : null), $context["key"], array(), "array"), "html", null, true);
                    echo "/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nbPlayer", array()), "html", null, true);
                    echo "</p> ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("mot.participants"), "html", null, true);
                    echo "
                                ";
                }
                // line 166
                echo "                            </div>
                            <div class=\"col-md-4 info-date\">
                                <ul class=\"list-unstyled\">
                                    <li><img src=\"";
                // line 169
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/date.png"), "html", null, true);
                echo "\" alt=\"organiser une journée sportive\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "startdate", array()), "d-m-Y"), "html", null, true);
                echo "</li>
                                    <li class=\"lieu\"><img src=\"";
                // line 170
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/lieu.png"), "html", null, true);
                echo "\" alt=\"organiser une journée sportive\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "locality", array()), "html", null, true);
                echo " ( ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "country", array()), "html", null, true);
                echo ")</li>
                                    <li><img src=\"";
                // line 171
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/heure.png"), "html", null, true);
                echo "\" alt=\"organiser une journée sportive\"> ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "starttime", array()), "H:i"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("lettre.a"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "endtime", array()), "H:i"), "html", null, true);
                echo "</li>
                                </ul>
                            </div>
                            <div class=\"col-md-2 info-date\">
                                <h3 class=\"name-match green\"></h3>
                                <a href=\"";
                // line 176
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_show", array("slug" => $this->getAttribute($context["entity"], "slug", array()))), "html", null, true);
                echo "\" title=\"organiser une journée sportive\" class=\"btn btn-grinta\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("fiche.match"), "html", null, true);
                echo "</a>

                                <form id=\"validate_field_types\" method=\"POST\" action=\"";
                // line 178
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("event_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
                echo "\">
                                    <input type=\"submit\" class=\"btn btn-refus btn-refus-invit\" value=\"";
                // line 179
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("msg.delete"), "html", null, true);
                echo "\" />
                                </form>
                            </div>
                        </div>
                    </div>
                ";
            }
            // line 185
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 186
        echo "            </div>
        </div>
 </div>
</div>
<!--end containt-->
";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Event:listEventByUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  444 => 186,  438 => 185,  429 => 179,  425 => 178,  418 => 176,  404 => 171,  396 => 170,  390 => 169,  385 => 166,  375 => 164,  367 => 162,  365 => 161,  361 => 160,  351 => 155,  347 => 153,  341 => 151,  334 => 149,  332 => 148,  327 => 146,  322 => 143,  319 => 142,  315 => 141,  307 => 136,  295 => 126,  289 => 125,  280 => 119,  276 => 118,  269 => 116,  255 => 111,  247 => 110,  241 => 109,  236 => 106,  226 => 104,  218 => 102,  216 => 101,  212 => 100,  202 => 95,  198 => 93,  192 => 91,  185 => 89,  183 => 88,  178 => 86,  173 => 83,  170 => 82,  166 => 81,  158 => 76,  144 => 65,  140 => 64,  131 => 58,  127 => 57,  120 => 53,  116 => 52,  110 => 49,  102 => 44,  98 => 43,  90 => 38,  86 => 37,  77 => 33,  73 => 32,  59 => 23,  55 => 22,  48 => 20,  36 => 13,  30 => 10,  19 => 1,);
    }
}
