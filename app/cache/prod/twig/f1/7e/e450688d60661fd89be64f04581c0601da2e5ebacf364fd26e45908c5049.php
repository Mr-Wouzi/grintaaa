<?php

/* ApplicationSonataPageBundle::layout.html.twig */
class __TwigTemplate_f17ee450688d60661fd89be64f04581c0601da2e5ebacf364fd26e45908c5049 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("ApplicationSonataPageBundle::base_layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'sonata_page_top_bar' => array($this, 'block_sonata_page_top_bar'),
            'page_top_bar' => array($this, 'block_page_top_bar'),
            'sonata_page_container' => array($this, 'block_sonata_page_container'),
            'page_content' => array($this, 'block_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ApplicationSonataPageBundle::base_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_sonata_page_top_bar($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayBlock('page_top_bar', $context, $blocks);
    }

    public function block_page_top_bar($context, array $blocks = array())
    {
        echo " ";
        // line 5
        echo "\t";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "presentation")) {
            // line 6
            echo "            ";
            echo call_user_func_array($this->env->getFunction('sonata_block_render')->getCallable(), array(array("type" => "sonata.page.block.header")));
            echo "
";
        }
        // line 8
        echo "
    ";
    }

    // line 11
    public function block_sonata_page_container($context, array $blocks = array())
    {
        // line 12
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "presentation")) {
            // line 13
            echo "<script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/js/lib.js"), "html", null, true);
            echo "\"></script>
    ";
        }
        // line 15
        echo "
            ";
        // line 16
        $this->displayBlock('page_content', $context, $blocks);
        // line 28
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "_route"), "method") != "presentation")) {
            // line 29
            echo "
        ";
            // line 30
            echo call_user_func_array($this->env->getFunction('sonata_block_render')->getCallable(), array(array("type" => "sonata.page.block.footer")));
            echo "
";
        }
    }

    // line 16
    public function block_page_content($context, array $blocks = array())
    {
        // line 17
        echo "                ";
        if (array_key_exists("content", $context)) {
            // line 18
            echo "                    ";
            echo (isset($context["content"]) ? $context["content"] : null);
            echo "
                ";
        } else {
            // line 20
            echo "                    ";
            $context["content"] = $this->renderBlock("content", $context, $blocks);
            // line 21
            echo "                    ";
            if ((twig_length_filter($this->env, (isset($context["content"]) ? $context["content"] : null)) > 0)) {
                // line 22
                echo "                        ";
                echo (isset($context["content"]) ? $context["content"] : null);
                echo "
                    ";
            } elseif (            // line 23
array_key_exists("page", $context)) {
                // line 24
                echo "                        ";
                echo $this->env->getExtension('sonata_page')->renderContainer("content", (isset($context["page"]) ? $context["page"] : null));
                echo "
                    ";
            }
            // line 26
            echo "                ";
        }
        // line 27
        echo "            ";
    }

    public function getTemplateName()
    {
        return "ApplicationSonataPageBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 27,  123 => 26,  117 => 24,  115 => 23,  110 => 22,  107 => 21,  104 => 20,  98 => 18,  95 => 17,  92 => 16,  85 => 30,  82 => 29,  80 => 28,  78 => 16,  75 => 15,  69 => 13,  67 => 12,  64 => 11,  59 => 8,  53 => 6,  50 => 5,  42 => 4,  39 => 3,  11 => 1,);
    }
}
