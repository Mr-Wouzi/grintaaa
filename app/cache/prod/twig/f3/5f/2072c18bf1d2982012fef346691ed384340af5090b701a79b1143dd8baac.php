<?php

/* ApplicationSonataUserBundle:User:complete.html.twig */
class __TwigTemplate_f35f2072c18bf1d2982012fef346691ed384340af5090b701a79b1143dd8baac extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"block2 recherche\">
    <div class=\"titre green\">

        <h4><img src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/users.png"), "html", null, true);
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("search.freind"), "html", null, true);
        echo "</h4>
    </div>


    <form action=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("grintaaa__list_friend_search");
        echo "\" method=\"post\" name=\"formsearch\" id=\"formsearch\">
        <input type=\"text\" name=\"friends\" placeholder=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("trouver.freind"), "html", null, true);
        echo "\" id=\"friends\">
        <input type=\"submit\" class=\"btn btn-search\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("search.avence"), "html", null, true);
        echo "\">
    </form>



</div>




<script type=\"text/javascript\">

   // \$(\"#formsearch\").submit();

    var termTemplate = \"<span class='ui-autocomplete-term'>%s</span>\";
    var availableTags = [
        ";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 27
            echo "        {label:\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["status"], "firstname", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["status"], "lastname", array()), "html", null, true);
            echo "   @";
            echo twig_escape_filter($this->env, $this->getAttribute($context["status"], "username", array()), "html", null, true);
            echo " \",the_link:\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($context["status"], "username", array()), "id" => $this->getAttribute($context["status"], "id", array()))), "html", null, true);
            echo "\" },
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "
    ];
    \$('#friends').autocomplete({
        source: availableTags,
        select:function(e,ui) {
            console.log(ui);    // When you click (select) the ui object is returned , as well as the event
            // You can get at the returned results ( the object via . (dot)  notation )
            location.href = ui.item.the_link;
            //console.log(ui.item.the_link);
        }
    });



</script>";
    }

    public function getTemplateName()
    {
        return "ApplicationSonataUserBundle:User:complete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 29,  64 => 27,  60 => 26,  41 => 10,  37 => 9,  33 => 8,  24 => 4,  19 => 1,);
    }
}
