<?php

/* ComparatorBaseBundle:Base:personne.html.twig */
class __TwigTemplate_f51d2542f3515d74125033be2e8d58df46d553b848e03a19cf9514bfe8aa570a extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!--Debut containt-->
<div class=\"containt-inscrit top-connect\">
    <div class=\"container\">
        <div class=\"col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 inscrit\">
            <div class=\"col-md-10 col-md-offset-1 cadre-blanc\">
                <h2 class=\"titre_connect\">Choisissez votre espace</h2>
\t\t\t\t<select id=\"personne\" name=\"personne\" onChange=\"getPersonne(this)\" class=\"form-control form-control\">
                    <option value='default' name=\"1\">-------------Choisir-------------</option>
                    <option value=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register");
        echo "\">Sportif</option>
                    <option value=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register1");
        echo "\">Coach</option>
                    <option value=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("fos_user_registration_register2");
        echo "\">Organisateur</option>
                </select>

            </div>
        </div>
    </div>
</div>
<!--Fin containt-->
</div>
</div>
<script type=\"text/javascript\">
    function getPersonne(theForm) {
        var url = \$('#personne').val();
        var hn = window.location.hostname;

        window.location.href = url;
    }
</script>";
    }

    public function getTemplateName()
    {
        return "ComparatorBaseBundle:Base:personne.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 12,  34 => 11,  30 => 10,  19 => 1,);
    }
}
