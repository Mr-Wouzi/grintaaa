<?php

/* SonataAdminBundle::standard_layout.html.twig */
class __TwigTemplate_fa25f5dc6b2b698f5270e43040b449f84447e6b780cae8ce1ff225fe3b6040ff extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'html_attributes' => array($this, 'block_html_attributes'),
            'meta_tags' => array($this, 'block_meta_tags'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'sonata_head_title' => array($this, 'block_sonata_head_title'),
            'body_attributes' => array($this, 'block_body_attributes'),
            'sonata_header' => array($this, 'block_sonata_header'),
            'sonata_header_noscript_warning' => array($this, 'block_sonata_header_noscript_warning'),
            'logo' => array($this, 'block_logo'),
            'sonata_nav' => array($this, 'block_sonata_nav'),
            'sonata_breadcrumb' => array($this, 'block_sonata_breadcrumb'),
            'sonata_top_nav_menu' => array($this, 'block_sonata_top_nav_menu'),
            'sonata_wrapper' => array($this, 'block_sonata_wrapper'),
            'sonata_left_side' => array($this, 'block_sonata_left_side'),
            'sonata_side_nav' => array($this, 'block_sonata_side_nav'),
            'sonata_sidebar_search' => array($this, 'block_sonata_sidebar_search'),
            'side_bar_before_nav' => array($this, 'block_side_bar_before_nav'),
            'side_bar_nav' => array($this, 'block_side_bar_nav'),
            'side_bar_after_nav' => array($this, 'block_side_bar_after_nav'),
            'sonata_page_content' => array($this, 'block_sonata_page_content'),
            'sonata_page_content_header' => array($this, 'block_sonata_page_content_header'),
            'sonata_page_content_nav' => array($this, 'block_sonata_page_content_nav'),
            'tab_menu_navbar_header' => array($this, 'block_tab_menu_navbar_header'),
            'sonata_admin_content' => array($this, 'block_sonata_admin_content'),
            'notice' => array($this, 'block_notice'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 11
        $context["_preview"] = $this->renderBlock("preview", $context, $blocks);
        // line 12
        $context["_form"] = $this->renderBlock("form", $context, $blocks);
        // line 13
        $context["_show"] = $this->renderBlock("show", $context, $blocks);
        // line 14
        $context["_list_table"] = $this->renderBlock("list_table", $context, $blocks);
        // line 15
        $context["_list_filters"] = $this->renderBlock("list_filters", $context, $blocks);
        // line 16
        $context["_tab_menu"] = $this->renderBlock("tab_menu", $context, $blocks);
        // line 17
        $context["_content"] = $this->renderBlock("content", $context, $blocks);
        // line 18
        $context["_title"] = $this->renderBlock("title", $context, $blocks);
        // line 19
        $context["_breadcrumb"] = $this->renderBlock("breadcrumb", $context, $blocks);
        // line 20
        $context["_actions"] = $this->renderBlock("actions", $context, $blocks);
        // line 21
        $context["_navbar_title"] = $this->renderBlock("navbar_title", $context, $blocks);
        // line 22
        $context["_list_filters_actions"] = $this->renderBlock("list_filters_actions", $context, $blocks);
        // line 23
        echo "
<!DOCTYPE html>
<html ";
        // line 25
        $this->displayBlock('html_attributes', $context, $blocks);
        echo ">
    <head>
        ";
        // line 27
        $this->displayBlock('meta_tags', $context, $blocks);
        // line 31
        echo "
        ";
        // line 32
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 39
        echo "
        ";
        // line 40
        $this->displayBlock('javascripts', $context, $blocks);
        // line 75
        echo "
        <title>
        ";
        // line 77
        $this->displayBlock('sonata_head_title', $context, $blocks);
        // line 97
        echo "        </title>
    </head>
    <body ";
        // line 99
        $this->displayBlock('body_attributes', $context, $blocks);
        echo ">
        ";
        // line 100
        $this->displayBlock('sonata_header', $context, $blocks);
        // line 188
        echo "
        ";
        // line 189
        $this->displayBlock('sonata_wrapper', $context, $blocks);
        // line 316
        echo "    </body>
</html>
";
    }

    // line 25
    public function block_html_attributes($context, array $blocks = array())
    {
        echo "class=\"no-js\"";
    }

    // line 27
    public function block_meta_tags($context, array $blocks = array())
    {
        // line 28
        echo "            <meta charset=\"UTF-8\">
            <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        ";
    }

    // line 32
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 33
        echo "            ";
        if (array_key_exists("admin_pool", $context)) {
            // line 34
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "stylesheets", 1 => array()), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["stylesheet"]) {
                // line 35
                echo "                        <link rel=\"stylesheet\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($context["stylesheet"]), "html", null, true);
                echo "\">
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stylesheet'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 37
            echo "            ";
        }
        // line 38
        echo "        ";
    }

    // line 40
    public function block_javascripts($context, array $blocks = array())
    {
        // line 41
        echo "            <script>
                window.SONATA_CONFIG = {
                    CONFIRM_EXIT: ";
        // line 43
        if ((array_key_exists("admin_pool", $context) && $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "confirm_exit"), "method"))) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                    USE_SELECT2: ";
        // line 44
        if ((array_key_exists("admin_pool", $context) && $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "use_select2"), "method"))) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                    USE_ICHECK: ";
        // line 45
        if ((array_key_exists("admin_pool", $context) && $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "use_icheck"), "method"))) {
            echo "true";
        } else {
            echo "false";
        }
        // line 46
        echo "                };
                window.SONATA_TRANSLATIONS = {
                    CONFIRM_EXIT:  '";
        // line 48
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("confirm_exit", array(), "SonataAdminBundle"), "js"), "html", null, true);
        echo "'
               };
            </script>

            ";
        // line 52
        if (array_key_exists("admin_pool", $context)) {
            // line 53
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "javascripts", 1 => array()), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["javascript"]) {
                // line 54
                echo "                    <script src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($context["javascript"]), "html", null, true);
                echo "\"></script>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['javascript'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "            ";
        }
        // line 57
        echo "
            ";
        // line 58
        $context["locale"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "locale", array());
        // line 59
        echo "            ";
        // line 60
        echo "            ";
        if ((twig_slice($this->env, (isset($context["locale"]) ? $context["locale"] : null), 0, 2) != "en")) {
            // line 61
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("bundles/sonatacore/vendor/moment/locale/" . strtr((isset($context["locale"]) ? $context["locale"] : null), array("_" => "-"))) . ".js")), "html", null, true);
            echo "\"></script>
            ";
        }
        // line 63
        echo "
            ";
        // line 65
        echo "            ";
        if ((array_key_exists("admin_pool", $context) && $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "use_select2"), "method"))) {
            echo "             
                ";
            // line 66
            if (((isset($context["locale"]) ? $context["locale"] : null) == "pt")) {
                $context["locale"] = "pt_PT";
            }
            // line 67
            echo "
                ";
            // line 69
            echo "                ";
            if ((twig_slice($this->env, (isset($context["locale"]) ? $context["locale"] : null), 0, 2) != "en")) {
                // line 70
                echo "                    <script src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("bundles/sonatacore/vendor/select2/select2_locale_" . strtr((isset($context["locale"]) ? $context["locale"] : null), array("_" => "-"))) . ".js")), "html", null, true);
                echo "\"></script>
                ";
            }
            // line 72
            echo "            ";
        }
        // line 73
        echo "
        ";
    }

    // line 77
    public function block_sonata_head_title($context, array $blocks = array())
    {
        // line 78
        echo "            ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Admin", array(), "SonataAdminBundle"), "html", null, true);
        echo "

            ";
        // line 80
        if ( !twig_test_empty((isset($context["_title"]) ? $context["_title"] : null))) {
            // line 81
            echo "                ";
            echo (isset($context["_title"]) ? $context["_title"] : null);
            echo "
            ";
        } else {
            // line 83
            echo "                ";
            if (array_key_exists("action", $context)) {
                // line 84
                echo "                    -
                    ";
                // line 85
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "breadcrumbs", array(0 => (isset($context["action"]) ? $context["action"] : null)), "method"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
                    // line 86
                    echo "                        ";
                    if ( !$this->getAttribute($context["loop"], "first", array())) {
                        // line 87
                        echo "                            ";
                        if (($this->getAttribute($context["loop"], "index", array()) != 2)) {
                            // line 88
                            echo "                                &gt;
                            ";
                        }
                        // line 90
                        echo "
                            ";
                        // line 91
                        echo twig_escape_filter($this->env, $this->getAttribute($context["menu"], "label", array()), "html", null, true);
                        echo "
                        ";
                    }
                    // line 93
                    echo "                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "                ";
            }
            // line 95
            echo "            ";
        }
        // line 96
        echo "        ";
    }

    // line 99
    public function block_body_attributes($context, array $blocks = array())
    {
        echo "class=\"sonata-bc skin-black fixed\"";
    }

    // line 100
    public function block_sonata_header($context, array $blocks = array())
    {
        // line 101
        echo "            <header class=\"header\">
                ";
        // line 102
        $this->displayBlock('sonata_header_noscript_warning', $context, $blocks);
        // line 109
        echo "                ";
        $this->displayBlock('logo', $context, $blocks);
        // line 123
        echo "                ";
        $this->displayBlock('sonata_nav', $context, $blocks);
        // line 186
        echo "            </header>
        ";
    }

    // line 102
    public function block_sonata_header_noscript_warning($context, array $blocks = array())
    {
        // line 103
        echo "                    <noscript>
                        <div class=\"noscript-warning\">
                            ";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("noscript_warning", array(), "SonataAdminBundle"), "html", null, true);
        echo "
                        </div>
                    </noscript>
                ";
    }

    // line 109
    public function block_logo($context, array $blocks = array())
    {
        // line 110
        echo "                    ";
        if (array_key_exists("admin_pool", $context)) {
            // line 111
            echo "                        ";
            ob_start();
            // line 112
            echo "                        <a class=\"logo\" href=\"";
            echo $this->env->getExtension('routing')->getPath("sonata_admin_dashboard");
            echo "\">
                            ";
            // line 113
            if ((("single_image" == $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "title_mode"), "method")) || ("both" == $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "title_mode"), "method")))) {
                // line 114
                echo "                                <img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "titlelogo", array())), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "title", array()), "html", null, true);
                echo "\">
                            ";
            }
            // line 116
            echo "                            ";
            if ((("single_text" == $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "title_mode"), "method")) || ("both" == $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getOption", array(0 => "title_mode"), "method")))) {
                // line 117
                echo "                                <span>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "title", array()), "html", null, true);
                echo "</span>
                            ";
            }
            // line 119
            echo "                        </a>
                        ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 121
            echo "                    ";
        }
        // line 122
        echo "                ";
    }

    // line 123
    public function block_sonata_nav($context, array $blocks = array())
    {
        // line 124
        echo "                    ";
        if (array_key_exists("admin_pool", $context)) {
            // line 125
            echo "                        <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                            <a href=\"#\" class=\"navbar-btn sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                                <span class=\"sr-only\">Toggle navigation</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </a>

                            <div class=\"navbar-left\">
                                ";
            // line 134
            $this->displayBlock('sonata_breadcrumb', $context, $blocks);
            // line 161
            echo "                            </div>

                            ";
            // line 163
            $this->displayBlock('sonata_top_nav_menu', $context, $blocks);
            // line 183
            echo "                        </nav>
                    ";
        }
        // line 185
        echo "                ";
    }

    // line 134
    public function block_sonata_breadcrumb($context, array $blocks = array())
    {
        // line 135
        echo "                                    <div class=\"hidden-xs\">
                                    ";
        // line 136
        if (( !twig_test_empty((isset($context["_breadcrumb"]) ? $context["_breadcrumb"] : null)) || array_key_exists("action", $context))) {
            // line 137
            echo "                                        <ol class=\"nav navbar-top-links breadcrumb\">
                                            ";
            // line 138
            if (twig_test_empty((isset($context["_breadcrumb"]) ? $context["_breadcrumb"] : null))) {
                // line 139
                echo "                                                ";
                if (array_key_exists("action", $context)) {
                    // line 140
                    echo "                                                    ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "breadcrumbs", array(0 => (isset($context["action"]) ? $context["action"] : null)), "method"));
                    $context['loop'] = array(
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    );
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
                        // line 141
                        echo "                                                        ";
                        if ( !$this->getAttribute($context["loop"], "last", array())) {
                            // line 142
                            echo "                                                            <li>
                                                                ";
                            // line 143
                            if ( !twig_test_empty($this->getAttribute($context["menu"], "uri", array()))) {
                                // line 144
                                echo "                                                                    <a href=\"";
                                echo twig_escape_filter($this->env, $this->getAttribute($context["menu"], "uri", array()), "html", null, true);
                                echo "\">";
                                echo $this->getAttribute($context["menu"], "label", array());
                                echo "</a>
                                                                ";
                            } else {
                                // line 146
                                echo "                                                                    ";
                                echo twig_escape_filter($this->env, $this->getAttribute($context["menu"], "label", array()), "html", null, true);
                                echo "
                                                                ";
                            }
                            // line 148
                            echo "                                                            </li>
                                                        ";
                        } else {
                            // line 150
                            echo "                                                            <li class=\"active\"><span>";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["menu"], "label", array()), "html", null, true);
                            echo "</span></li>
                                                        ";
                        }
                        // line 152
                        echo "                                                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 153
                    echo "                                                ";
                }
                // line 154
                echo "                                            ";
            } else {
                // line 155
                echo "                                                ";
                echo (isset($context["_breadcrumb"]) ? $context["_breadcrumb"] : null);
                echo "
                                            ";
            }
            // line 157
            echo "                                        </ol>
                                    ";
        }
        // line 159
        echo "                                    </div>
                                ";
    }

    // line 163
    public function block_sonata_top_nav_menu($context, array $blocks = array())
    {
        // line 164
        echo "                                <div class=\"navbar-right\">
                                    <ul class=\"nav navbar-nav\">
                                        <li class=\"dropdown\">
                                            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                <i class=\"fa fa-plus-square fa-fw\"></i> <i class=\"fa fa-caret-down\"></i>
                                            </a>
                                            ";
        // line 170
        $this->env->resolveTemplate($this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getTemplate", array(0 => "add_block"), "method"))->display($context);
        // line 171
        echo "                                        </li>
                                        <li class=\"dropdown user-menu\">
                                            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                <i class=\"fa fa-user fa-fw\"></i> <i class=\"fa fa-caret-down\"></i>
                                            </a>
                                            <ul class=\"dropdown-menu dropdown-user\">
                                                ";
        // line 177
        $this->env->resolveTemplate($this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getTemplate", array(0 => "user_block"), "method"))->display($context);
        // line 178
        echo "                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            ";
    }

    // line 189
    public function block_sonata_wrapper($context, array $blocks = array())
    {
        // line 190
        echo "            <div class=\"wrapper row-offcanvas row-offcanvas-left\">
                ";
        // line 191
        $this->displayBlock('sonata_left_side', $context, $blocks);
        // line 222
        echo "
                <aside class=\"right-side\">
                    ";
        // line 224
        $this->displayBlock('sonata_page_content', $context, $blocks);
        // line 312
        echo "                </aside>

            </div>
        ";
    }

    // line 191
    public function block_sonata_left_side($context, array $blocks = array())
    {
        // line 192
        echo "                    <aside class=\"left-side sidebar-offcanvas\">
                        <section class=\"sidebar\">
                            ";
        // line 194
        $this->displayBlock('sonata_side_nav', $context, $blocks);
        // line 219
        echo "                        </section>
                    </aside>
                ";
    }

    // line 194
    public function block_sonata_side_nav($context, array $blocks = array())
    {
        // line 195
        echo "                                ";
        $this->displayBlock('sonata_sidebar_search', $context, $blocks);
        // line 209
        echo "
                                ";
        // line 210
        $this->displayBlock('side_bar_before_nav', $context, $blocks);
        // line 211
        echo "                                ";
        $this->displayBlock('side_bar_nav', $context, $blocks);
        // line 216
        echo "                                ";
        $this->displayBlock('side_bar_after_nav', $context, $blocks);
        // line 218
        echo "                            ";
    }

    // line 195
    public function block_sonata_sidebar_search($context, array $blocks = array())
    {
        // line 196
        echo "                                    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "security", array()), "token", array()) && $this->env->getExtension('security')->isGranted("ROLE_SONATA_ADMIN"))) {
            // line 197
            echo "                                        <form action=\"";
            echo $this->env->getExtension('routing')->getUrl("sonata_admin_search");
            echo "\" method=\"GET\" class=\"sidebar-form\" role=\"search\">
                                            <div class=\"input-group custom-search-form\">
                                                <input type=\"text\" name=\"q\" value=\"";
            // line 199
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "get", array(0 => "q"), "method"), "html", null, true);
            echo "\" class=\"form-control\" placeholder=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("search_placeholder", array(), "SonataAdminBundle"), "html", null, true);
            echo "\">
                                                    <span class=\"input-group-btn\">
                                                        <button class=\"btn btn-flat\" type=\"submit\">
                                                            <i class=\"fa fa-search\"></i>
                                                        </button>
                                                    </span>
                                            </div>
                                        </form>
                                    ";
        }
        // line 208
        echo "                                ";
    }

    // line 210
    public function block_side_bar_before_nav($context, array $blocks = array())
    {
        echo " ";
    }

    // line 211
    public function block_side_bar_nav($context, array $blocks = array())
    {
        // line 212
        echo "                                    ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "security", array()), "token", array()) && $this->env->getExtension('security')->isGranted("ROLE_SONATA_ADMIN"))) {
            // line 213
            echo "                                        ";
            echo $this->env->getExtension('knp_menu')->render($this->env->getExtension('sonata_admin')->getKnpMenu($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array())), array("template" => $this->getAttribute((isset($context["admin_pool"]) ? $context["admin_pool"] : null), "getTemplate", array(0 => "knp_menu_template"), "method")));
            echo "
                                    ";
        }
        // line 215
        echo "                                ";
    }

    // line 216
    public function block_side_bar_after_nav($context, array $blocks = array())
    {
        // line 217
        echo "                                ";
    }

    // line 224
    public function block_sonata_page_content($context, array $blocks = array())
    {
        // line 225
        echo "                        <section class=\"content-header\">
                            ";
        // line 226
        $this->displayBlock('sonata_page_content_header', $context, $blocks);
        // line 272
        echo "                        </section>

                        <section class=\"content\">
                            ";
        // line 275
        $this->displayBlock('sonata_admin_content', $context, $blocks);
        // line 310
        echo "                        </section>
                    ";
    }

    // line 226
    public function block_sonata_page_content_header($context, array $blocks = array())
    {
        // line 227
        echo "                                ";
        $this->displayBlock('sonata_page_content_nav', $context, $blocks);
        // line 270
        echo "
                            ";
    }

    // line 227
    public function block_sonata_page_content_nav($context, array $blocks = array())
    {
        // line 228
        echo "                                    ";
        if ((( !twig_test_empty((isset($context["_tab_menu"]) ? $context["_tab_menu"] : null)) ||  !twig_test_empty((isset($context["_actions"]) ? $context["_actions"] : null))) ||  !twig_test_empty((isset($context["_list_filters_actions"]) ? $context["_list_filters_actions"] : null)))) {
            // line 229
            echo "                                        <nav class=\"navbar navbar-default\" role=\"navigation\">
                                            ";
            // line 230
            $this->displayBlock('tab_menu_navbar_header', $context, $blocks);
            // line 237
            echo "                                            <div class=\"container-fluid\">
                                                <div class=\"navbar-left\">
                                                    ";
            // line 239
            if ( !twig_test_empty((isset($context["_tab_menu"]) ? $context["_tab_menu"] : null))) {
                // line 240
                echo "                                                        ";
                echo (isset($context["_tab_menu"]) ? $context["_tab_menu"] : null);
                echo "
                                                    ";
            }
            // line 242
            echo "                                                </div>

                                                ";
            // line 244
            if ((((array_key_exists("admin", $context) && array_key_exists("action", $context)) && ((isset($context["action"]) ? $context["action"] : null) == "list")) && (twig_length_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "listModes", array())) > 1))) {
                // line 245
                echo "                                                    <div class=\"nav navbar-right btn-group\">
                                                        ";
                // line 246
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "listModes", array()));
                foreach ($context['_seq'] as $context["mode"] => $context["settings"]) {
                    // line 247
                    echo "                                                            <a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "generateUrl", array(0 => "list", 1 => twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "query", array()), "all", array()), array("_list_mode" => $context["mode"]))), "method"), "html", null, true);
                    echo "\" class=\"btn btn-default navbar-btn btn-sm";
                    if (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "getListMode", array(), "method") == $context["mode"])) {
                        echo " active";
                    }
                    echo "\"><i class=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["settings"], "class", array()), "html", null, true);
                    echo "\"></i></a>
                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['mode'], $context['settings'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 249
                echo "                                                    </div>
                                                ";
            }
            // line 251
            echo "
                                                ";
            // line 252
            if ( !twig_test_empty(trim(strtr((isset($context["_actions"]) ? $context["_actions"] : null), array("<li>" => "", "</li>" => ""))))) {
                // line 253
                echo "                                                    <ul class=\"nav navbar-nav navbar-right\">
                                                        <li class=\"dropdown sonata-actions\">
                                                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 255
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("link_actions", array(), "SonataAdminBundle"), "html", null, true);
                echo " <b class=\"caret\"></b></a>
                                                            <ul class=\"dropdown-menu\" role=\"menu\">
                                                                ";
                // line 257
                echo (isset($context["_actions"]) ? $context["_actions"] : null);
                echo "
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                ";
            }
            // line 262
            echo "
                                                ";
            // line 263
            if ( !twig_test_empty((isset($context["_list_filters_actions"]) ? $context["_list_filters_actions"] : null))) {
                // line 264
                echo "                                                    ";
                echo (isset($context["_list_filters_actions"]) ? $context["_list_filters_actions"] : null);
                echo "
                                                ";
            }
            // line 266
            echo "                                            </div>
                                        </nav>
                                    ";
        }
        // line 269
        echo "                                ";
    }

    // line 230
    public function block_tab_menu_navbar_header($context, array $blocks = array())
    {
        // line 231
        echo "                                                ";
        if ( !twig_test_empty((isset($context["_navbar_title"]) ? $context["_navbar_title"] : null))) {
            // line 232
            echo "                                                    <div class=\"navbar-header\">
                                                        <span class=\"navbar-brand\">";
            // line 233
            echo (isset($context["_navbar_title"]) ? $context["_navbar_title"] : null);
            echo "</span>
                                                    </div>
                                                ";
        }
        // line 236
        echo "                                            ";
    }

    // line 275
    public function block_sonata_admin_content($context, array $blocks = array())
    {
        // line 276
        echo "
                                ";
        // line 277
        $this->displayBlock('notice', $context, $blocks);
        // line 280
        echo "
                                ";
        // line 281
        if ( !twig_test_empty((isset($context["_preview"]) ? $context["_preview"] : null))) {
            // line 282
            echo "                                    <div class=\"sonata-ba-preview\">";
            echo (isset($context["_preview"]) ? $context["_preview"] : null);
            echo "</div>
                                ";
        }
        // line 284
        echo "
                                ";
        // line 285
        if ( !twig_test_empty((isset($context["_content"]) ? $context["_content"] : null))) {
            // line 286
            echo "                                    <div class=\"sonata-ba-content\">";
            echo (isset($context["_content"]) ? $context["_content"] : null);
            echo "</div>
                                ";
        }
        // line 288
        echo "
                                ";
        // line 289
        if ( !twig_test_empty((isset($context["_show"]) ? $context["_show"] : null))) {
            // line 290
            echo "                                    <div class=\"sonata-ba-show\">";
            echo (isset($context["_show"]) ? $context["_show"] : null);
            echo "</div>
                                ";
        }
        // line 292
        echo "
                                ";
        // line 293
        if ( !twig_test_empty((isset($context["_form"]) ? $context["_form"] : null))) {
            // line 294
            echo "                                    <div class=\"sonata-ba-form\">";
            echo (isset($context["_form"]) ? $context["_form"] : null);
            echo "</div>
                                ";
        }
        // line 296
        echo "
                                ";
        // line 297
        if (( !twig_test_empty((isset($context["_list_table"]) ? $context["_list_table"] : null)) ||  !twig_test_empty((isset($context["_list_filters"]) ? $context["_list_filters"] : null)))) {
            // line 298
            echo "                                    ";
            if (trim((isset($context["_list_filters"]) ? $context["_list_filters"] : null))) {
                // line 299
                echo "                                        <div class=\"row\">
                                            ";
                // line 300
                echo (isset($context["_list_filters"]) ? $context["_list_filters"] : null);
                echo "
                                        </div>
                                    ";
            }
            // line 303
            echo "
                                    <div class=\"row\">
                                        ";
            // line 305
            echo (isset($context["_list_table"]) ? $context["_list_table"] : null);
            echo "
                                    </div>

                                ";
        }
        // line 309
        echo "                            ";
    }

    // line 277
    public function block_notice($context, array $blocks = array())
    {
        // line 278
        echo "                                    ";
        $this->env->loadTemplate("SonataCoreBundle:FlashMessage:render.html.twig")->display($context);
        // line 279
        echo "                                ";
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::standard_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  949 => 279,  946 => 278,  943 => 277,  939 => 309,  932 => 305,  928 => 303,  922 => 300,  919 => 299,  916 => 298,  914 => 297,  911 => 296,  905 => 294,  903 => 293,  900 => 292,  894 => 290,  892 => 289,  889 => 288,  883 => 286,  881 => 285,  878 => 284,  872 => 282,  870 => 281,  867 => 280,  865 => 277,  862 => 276,  859 => 275,  855 => 236,  849 => 233,  846 => 232,  843 => 231,  840 => 230,  836 => 269,  831 => 266,  825 => 264,  823 => 263,  820 => 262,  812 => 257,  807 => 255,  803 => 253,  801 => 252,  798 => 251,  794 => 249,  779 => 247,  775 => 246,  772 => 245,  770 => 244,  766 => 242,  760 => 240,  758 => 239,  754 => 237,  752 => 230,  749 => 229,  746 => 228,  743 => 227,  738 => 270,  735 => 227,  732 => 226,  727 => 310,  725 => 275,  720 => 272,  718 => 226,  715 => 225,  712 => 224,  708 => 217,  705 => 216,  701 => 215,  695 => 213,  692 => 212,  689 => 211,  683 => 210,  679 => 208,  665 => 199,  659 => 197,  656 => 196,  653 => 195,  649 => 218,  646 => 216,  643 => 211,  641 => 210,  638 => 209,  635 => 195,  632 => 194,  626 => 219,  624 => 194,  620 => 192,  617 => 191,  610 => 312,  608 => 224,  604 => 222,  602 => 191,  599 => 190,  596 => 189,  588 => 178,  586 => 177,  578 => 171,  576 => 170,  568 => 164,  565 => 163,  560 => 159,  556 => 157,  550 => 155,  547 => 154,  544 => 153,  530 => 152,  524 => 150,  520 => 148,  514 => 146,  506 => 144,  504 => 143,  501 => 142,  498 => 141,  480 => 140,  477 => 139,  475 => 138,  472 => 137,  470 => 136,  467 => 135,  464 => 134,  460 => 185,  456 => 183,  454 => 163,  450 => 161,  448 => 134,  437 => 125,  434 => 124,  431 => 123,  427 => 122,  424 => 121,  420 => 119,  414 => 117,  411 => 116,  403 => 114,  401 => 113,  396 => 112,  393 => 111,  390 => 110,  387 => 109,  379 => 105,  375 => 103,  372 => 102,  367 => 186,  364 => 123,  361 => 109,  359 => 102,  356 => 101,  353 => 100,  347 => 99,  343 => 96,  340 => 95,  337 => 94,  323 => 93,  318 => 91,  315 => 90,  311 => 88,  308 => 87,  305 => 86,  288 => 85,  285 => 84,  282 => 83,  276 => 81,  274 => 80,  268 => 78,  265 => 77,  260 => 73,  257 => 72,  251 => 70,  248 => 69,  245 => 67,  241 => 66,  236 => 65,  233 => 63,  227 => 61,  224 => 60,  222 => 59,  220 => 58,  217 => 57,  214 => 56,  205 => 54,  200 => 53,  198 => 52,  191 => 48,  187 => 46,  181 => 45,  173 => 44,  165 => 43,  161 => 41,  158 => 40,  154 => 38,  151 => 37,  142 => 35,  137 => 34,  134 => 33,  131 => 32,  125 => 28,  122 => 27,  116 => 25,  110 => 316,  108 => 189,  105 => 188,  103 => 100,  99 => 99,  95 => 97,  93 => 77,  89 => 75,  87 => 40,  84 => 39,  82 => 32,  79 => 31,  77 => 27,  72 => 25,  68 => 23,  66 => 22,  64 => 21,  62 => 20,  60 => 19,  58 => 18,  56 => 17,  54 => 16,  52 => 15,  50 => 14,  48 => 13,  46 => 12,  44 => 11,);
    }
}
