<?php

/* FOSUserBundle:Resetting:request_content.html.twig */
class __TwigTemplate_faa57c79f73dfdd651738e997ad73895161c17efdce0987e063d8b5ee107a205 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--Debut containt-->
<div class=\"containt\">
    <div class=\"container\">

        <div class=\"row col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 login-error\">
            <div class=\"login-error-form\">

                <form class=\"form-inscription\" action=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" class=\"fos_user_resetting_request\">
                  <div class=\"row\">
                   <div class=\"col-md-12\">
\t\t    <h2 class=\"titre_connect\">Réinitialiser mot de passe</h2>
                    <div class=\"row\">
                      <div class=\"col-md-12\">
                        ";
        // line 14
        if (array_key_exists("invalid_username", $context)) {
            // line 15
            echo "                            <p>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.invalid_username", array("%username%" => (isset($context["invalid_username"]) ? $context["invalid_username"] : null)), "FOSUserBundle"), "html", null, true);
            echo "</p>
                        ";
        }
        // line 17
        echo "                         
                        <h4 class=\"titre-inscrit2\"><label for=\"username\">";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label></h4>
                        
                        <input type=\"text\" id=\"username\" class=\"form-control\" name=\"username\" required=\"required\" />
                      </div>
                    </div>
                    
                    <div class=\"row\">
                      <div class=\"col-md-12\">
                        <input type=\"submit\" class=\"btn btn-grinta\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
                      </div>
                    </div>
                   </div>
                  </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!--Fin containt-->
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 26,  48 => 18,  45 => 17,  39 => 15,  37 => 14,  28 => 8,  19 => 1,);
    }
}
