<?php

/* ComparatorEventBundle:Friends:listFriends.html.twig */
class __TwigTemplate_fda1c6c7b0e2506144c8d51b6f920bf8984bf2de582fc7303f731b7ab19043aa extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"block friends\">
    <div class=\"title-block\">  
        <h4 class=\"green\">";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("list.of.amis"), "html", null, true);
        echo "</h4>
    </div>
    <div class=\"body-friends\">

        ";
        // line 7
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["allFreinds"]) ? $context["allFreinds"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["entity"]) {
            // line 8
            echo "        <div class=\"item-friend\">
            ";
            // line 9
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()) == $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))) {
                // line 10
                echo "            <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user2", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user2", array()), "id", array()))), "html", null, true);
                echo "\" title=\"defi sportif entre amis\">
                <div class=\"img-friend\">
                    ";
                // line 12
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 13
                    echo "                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"defi sportif entre amis\">
                    ";
                } else {
                    // line 15
                    echo "                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"defi sportif entre amis\">
                    ";
                }
                // line 17
                echo "                </div>   
                <div class=\"info-friend\">
                    <h4>";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user2", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user2", array()), "firstname", array()), "html", null, true);
                echo "</h4>
                </div>
            </a>  
            ";
            } else {
                // line 23
                echo "                <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("grintaaa_user_show", array("username" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "username", array()), "id" => $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "id", array()))), "html", null, true);
                echo "\" title=\"defi sportif entre amis\">
                <div class=\"img-friend\">
                    ";
                // line 25
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array")) > 0)) {
                    // line 26
                    echo "                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("uploads/users/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["images"]) ? $context["images"] : null), $context["key"], array(), "array"), 0, array(), "array"), "logo", array()), "html", null, true);
                    echo "\" alt=\"defi sportif entre amis\">
                    ";
                } else {
                    // line 28
                    echo "                        <img  src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/comparatorbase/images/avatar.png"), "html", null, true);
                    echo "\" alt=\"defi sportif entre amis\">
                    ";
                }
                // line 30
                echo "                </div>
                <div class=\"info-friend\">
                    <h4>";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "lastname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["entity"], "user1", array()), "firstname", array()), "html", null, true);
                echo "</h4>
                </div>
            </a>
            ";
            }
            // line 35
            echo "   
        </div>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "ComparatorEventBundle:Friends:listFriends.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 39,  107 => 35,  98 => 32,  94 => 30,  88 => 28,  81 => 26,  79 => 25,  73 => 23,  64 => 19,  60 => 17,  54 => 15,  47 => 13,  45 => 12,  39 => 10,  37 => 9,  34 => 8,  30 => 7,  23 => 3,  19 => 1,);
    }
}
