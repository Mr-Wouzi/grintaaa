<?php

/* ApplicationSiteVoteBundle:Default:index.html.twig */
class __TwigTemplate_fe6e2fcb40285135aa19b17b7ba09201b19f0484a994bdc98dcfce0b51a49c73 extends Sonata\CacheBundle\Twig\TwigTemplate14
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Hello ";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo "!

";
    }

    public function getTemplateName()
    {
        return "ApplicationSiteVoteBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
