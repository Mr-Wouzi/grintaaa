$(document).ready(function () {
    //multiple select
    $(".interet").select2({
        placeholder: "Centre d'interet"
    });


    //Datepicker inscription    

    $('.datepicker').datepicker({

        format: 'dd/mm/yyyy',

        startDate: '-3d',
        orientation: "top auto"

    });



    //Wizard

    var $validator = $("#commentForm").validate({

          rules: {

            category: {

              required: true,

            },

            adresse: {

              required: true,

            },

            date:{

              required: true,

              date: true    

            },

            debut: {

              required: true

            },

            fin: {

              required: true

            }, 

            titre: {

              required: true

            },

            description: {

              required: true

            }


          }

        });

        

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {



        var $target = $(e.target);

    

        if ($target.parent().hasClass('disabled')) {

            return false;

        }

    });



    $(".next-step").click(function (e) {

            var $valid = $("#commentForm").valid();

                if(!$valid) {

                    $validator.focusInvalid();

                    return false;

                }

        var $active = $('.wizard .nav-tabs li.active');

        $active.next().removeClass('disabled');

        nextTab($active);



                



    });

    $(".prev-step").click(function (e) {



        var $active = $('.wizard .nav-tabs li.active');

        prevTab($active);



    });

});



    function nextTab(elem) {

        $(elem).next().find('a[data-toggle="tab"]').click();

    }

    function prevTab(elem) {

        $(elem).prev().find('a[data-toggle="tab"]').click();

    }





