<?php
/**
 * Created by PhpStorm.
 * User: dev1
 * Date: 27/02/15
 * Time: 17:40
 */
namespace Application\Site\VoteBundle\Entity;
use DCS\RatingBundle\Entity\Rating as BaseRating;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Rating extends BaseRating
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Application\Site\VoteBundle\Entity\Vote", mappedBy="rating")
     */
    protected $votes;
}