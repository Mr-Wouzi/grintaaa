<?php

namespace Application\Sonata\CommentBundle\Entity;

use FOS\CommentBundle\Model\SignedCommentInterface;
use Symfony\Component\Security\Core\User\UserInterface;

use Sonata\CommentBundle\Entity\BaseComment as BaseComment;

class Comment extends BaseComment implements SignedCommentInterface
{
    /**
     * @var integer $id
     */
    protected $id;

    /**
     * @var UserInterface
     */
    protected $author;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthor(UserInterface $author)
    {
        $this->author = $author;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorName()
    {
        return $this->getAuthor() ? $this->getAuthor()->getUsername() : 'Anonymous';
    }
}