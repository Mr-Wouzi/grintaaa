<?php





namespace Application\Sonata\UserBundle\Admin\Entity;





use Sonata\AdminBundle\Admin\Admin;


use Sonata\AdminBundle\Form\FormMapper;


use Sonata\AdminBundle\Datagrid\DatagridMapper;


use Sonata\AdminBundle\Datagrid\ListMapper;


use FOS\UserBundle\Model\UserManagerInterface;





/**


 * Class UserAdmin


 * @package Application\Sonata\UserBundle\Admin\Entity


 */


class UserAdmin extends Admin


{


    protected $formOptions = array(


        'validation_groups' => 'Profile'


    );





    /**


     * {@inheritdoc}


     */


    protected function configureListFields(ListMapper $listMapper)


    {


        $listMapper


            ->addIdentifier('username')


            ->add('email')


            ->add('roles')


            ->add('groups')


            ->add('enabled', null, array('editable' => true))


            ->add('locked', null, array('editable' => true))




            ->add(


                '_action',


                'actions',


                array(


                    'label' => 'Action',


                    'actions' => array('edit' => array(), 'delete' => array())


                )


            );


    }





    /**


     * {@inheritdoc}


     */


    protected function configureDatagridFilters(DatagridMapper $filterMapper)


    {


        $filterMapper


            ->add('firstname')


            ->add('lastname')


            ->add('username')


            ->add('email');


    }





    /**


     * {@inheritdoc}


     */


    protected function configureFormFields(FormMapper $formMapper)


    {


        $formMapper


            ->tab('User')


            ->with('Profile', array('class' => 'col-md-6'))->end()


            ->with('General', array('class' => 'col-md-6'))->end()


            ->end()


            ->tab('Security')


            ->with('Status', array('class' => 'col-md-4'))->end()


            ->with('Groups', array('class' => 'col-md-4'))->end()


            ->with('Keys', array('class' => 'col-md-4'))->end()


            ->with('Roles', array('class' => 'col-md-12'))->end()


            ->end();





        $formMapper


            ->tab('User')


            ->with('General')


            ->add('username')



            ->add('email')


            ->add(


                'plainPassword',


                'text',


                array(


                    'required' => (!$this->getSubject() || is_null($this->getSubject()->getId()))


                )





            )



            ->end()


            ->with('Profile')


            ->add('firstname', null, array('required' => false))


            ->add('lastname', null, array('required' => false))


            ->end()


            ->end();





        if ($this->getSubject() && !$this->getSubject()->hasRole('ROLE_SUPER_ADMIN') && !$this->getSubject()->hasRole(


                'ROLE_ADMIN_PARTNER'


            )


        ) {


            $formMapper


                ->tab('Security')


                ->with('Status')


                ->add('locked', null, array('required' => false))


                ->add('expired', null, array('required' => false))


                ->add('enabled', null, array('required' => false))


                ->add('credentialsExpired', null, array('required' => false))


                ->end()


                ->with('Groups')


                ->add(


                    'groups',


                    'sonata_type_model',


                    array(


                        'required' => false,


                        'expanded' => true,


                        'multiple' => true


                    )


                )


                ->end()


                ->with('Roles')


                ->add(


                    'realRoles',


                    'sonata_security_roles',


                    array(


                        'label' => 'form.label_roles',


                        'expanded' => true,


                        'multiple' => true,


                        'required' => false


                    )


                )


                ->end()


                ->end();


        }


    }





    /**


     * {@inheritdoc}


     */


    public function preUpdate($user)


    {


        $this->getUserManager()->updateCanonicalFields($user);


        $this->getUserManager()->updatePassword($user);


    }





    /**


     * @param UserManagerInterface $userManager


     */


    public function setUserManager(UserManagerInterface $userManager)


    {


        $this->userManager = $userManager;


    }





    /**


     * @return UserManagerInterface


     */


    public function getUserManager()


    {


        return $this->userManager;


    }


}


