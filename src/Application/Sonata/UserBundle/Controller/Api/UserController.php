<?php
/**
 * Created by PhpStorm.
 * User: developper
 * Date: 18/08/16
 * Time: 23:09
 */

namespace Application\Sonata\UserBundle\Controller\Api;

use Comparator\Bundle\EventBundle\Controller\CoachingController;
use Comparator\Bundle\EventBundle\Controller\ConfigurationController;
use Comparator\Bundle\EventBundle\Controller\InterestController;
use Comparator\Bundle\MultimediaBundle\Controller\FileController;
use \FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use Application\Sonata\UserBundle\Controller\UserController as User ;

class UserController extends FOSRestController
{

    public function getUsersCheckAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }
        
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $security_context = $this->get('security.context');

            $security_token = $security_context->getToken();
            $user = $security_token->getUser();
            $data = array(
                "userid" => $user->getId(),
                "auth" => "true"
            );
        }else{
            $data = array(
                "auth" => "false"
            );
        }

        $view = $this->view($data);
        return $this->handleView($view);

    }

    public function postUsersLoginFacebookAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $request = $this->get('request');

        $facebookid     = $request->request->get('facebookid');
        $facebooktname  = $request->request->get('facebookname');
        $facebooktemail = $request->request->get('facebookmail');

        $dm = $this->getDoctrine();
        $repo = $dm->getRepository('ApplicationSonataUserBundle:User');
        $user = $repo->findOneByFacebookUid($facebookid);

        if($user){
            $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());

            $context = $this->get('security.context');

            $context->setToken($token);
            $data = array(
                "login" => "true"
            );
        }else{


            $userManager = $this->container->get('fos_user.user_manager');

            $user = $userManager->createUser();
            $user->setUsername($facebooktname);
            $user->setEmail($facebooktemail);
            $user->setFirstname($facebooktname);
            $user->setFacebookUid($facebookid);
            $user->setLocked(false);
            $user->setEnabled(true);
            $user->setPlainPassword($facebooktname+$facebooktname);

            $userManager->updateUser($user, true);

            $user = $repo->findOneByFacebookUid($facebookid);
            $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
            $context = $this->get('security.context');
            $context->setToken($token);

            $data = array(
                "result" => "true",
            );
        }

        $view = $this->view($data);
        return $this->handleView($view);
    }
    
    public function postUsersLoginAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $request = $this->get('request');

        $username = $request->request->get('username');
        $password = $request->request->get('password');

        if(is_null($username) || is_null($password)) {

            $data = array(
                "login" => "false",
                "reason" => "empty Username/Password"
            );

        }else{

            $dm = $this->getDoctrine();
            $repo = $dm->getRepository('ApplicationSonataUserBundle:User');
            $user = $repo->findOneByEmail($username);

            $usern = $repo->findOneByUsername($username);

            if (!$user and !$usern) {
                $data = array(
                    "login" => "false",
                    "reason" => "Invalid user Name"
                );
            }else {

                $factory = $this->get('security.encoder_factory');
                if($usern)
                    $user = $usern;

                $encoder = $factory->getEncoder($user);


                $bool = ($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) ? "true" : "false";

                if ($bool == "false") {
                    $data = array(
                        "login" => "false",
                        "reason" => "Password incorrect"
                    );
                }else{

                    $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());

                    $context = $this->get('security.context');

                    $context->setToken($token);
                    $data = array(
                        "userid" => $user->getId(),
                        "login" => "true"
                    );
                }
            }
        }

        $view = $this->view($data);
        return $this->handleView($view);

    }
    
    public function getUsersLogoutAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }
        $providerKey = $this->container->getParameter('fos_user.firewall_name');
        $token = new AnonymousToken($providerKey, 'anon.');
        $this->get('security.context')->setToken($token);
        $this->get('request')->getSession()->invalidate();

        $data = array(
            "result" => "true"
        );

        $view = $this->view($data);
        return $this->handleView($view);

    }
    
    public function postUsersRegisterAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }
        
        $request = $this->get('request');
        $userManager = $this->container->get('fos_user.user_manager');

        $sexe = $request->request->get('sexe');
        $email = $request->request->get('email');
        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $rpassword = $request->request->get('rpassword');
        $nom = $request->request->get('nom');
        $prenom = $request->request->get('prenom');
        $pays = $request->request->get('pays');
        $ville = $request->request->get('ville');
        $tel = $request->request->get('tel');
        $naissancej = $request->request->get('naissancej');
        $naissancem = $request->request->get('naissancem');
        $naissancey = $request->request->get('naissancey');
        $handicap = $request->request->get('handicap');
        $coach = $request->request->get('coach');

        $email_exist = $userManager->findUserByEmail($email);
        $username_exist = $userManager->findUserByUsername($username);

        if($email_exist){
            
            $data = array(
                "result" => "false",
                "reason" => "l'adresse e-mail existe"
            );
            
        }elseif($username_exist){
            
            $data = array(
                "result" => "false",
                "reason" => "le pseudo existe"
            );
            
        }elseif($rpassword != $password){
            
            $data = array(
                "result" => "false",
                "reason" => "Invalide mot de passe"
            );
            
        }else{

            $anniversaire = $naissancey."-".$naissancem."-".$naissancej;
           
            $date = \DateTime::createFromFormat("Y-m-d", $naissancey.'-'.$naissancem.'-'.$naissancej);

            $user = $userManager->createUser();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setFirstname($nom);
            $user->setLastname($prenom);
            $user->setCivility($sexe);
            $user->setCountry($pays);
            $user->setCity($ville);
            $user->setPhone($tel);
            $user->setDateOfBirth($date);

            $user->setHandi($handicap);
            $user->setCoach($coach);

            $user->setLocked(false);
            $user->setEnabled(true);
            $user->setPlainPassword($password);
            $userManager->updateUser($user, true);
            
            $data = array(
                "result" => "true",
            );
        }



        $view = $this->view($data);
        return $this->handleView($view);

    }
    
    public function postUsersEditAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $request = $this->get('request');

        $form = $request->request->get('fos_user_profile_form');

        $userManager = $this->container->get('fos_user.user_manager');

        $user = $this->container->get('security.context')->getToken()->getUser();

        $date = \DateTime::createFromFormat("Y-m-d", $form['date_of_birth']['year'].'-'.$form['date_of_birth']['month'].'-'.$form['date_of_birth']['day']);

        
        $user->setFirstname($form['firstname']);
        $user->setLastname($form['lastname']);
        $user->setCountry($form['country']);
        $user->setCity($form['city']);
        $user->setPhone($form['phone']);
        $user->setDateOfBirth($date);
        $user->setHandi($form['handi']);
        $user->setSize($form['size']);
        $user->setWeight($form['weight']);
        $user->setParam($form['param']);
        $user->setCoach($form['coach']);

        $userManager->updateUser($user, true);

        $update = array(
            "succes" => "true"
        );
        
        $view = $this->view($update);
        return $this->handleView($view);
    }

    public function getUsersInfoAction(){
        
        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $user = $this->container->get('security.context')->getToken()->getUser();

        $users['firstname']  = $user->getFirstname();
        $users['lastname']  =$user->getLastname();
        $users['country']  =$user->getCountry();
        $users['city']  = $user->getCity();
        $users['phone']  = $user->getPhone();
        $users['date_of_birth']  =$user->getDateOfBirth();
        $users['handi']  =$user->getHandi();
        $users['size']  =$user->getSize();
        $users['weight']  =$user->getWeight();
        $users['param']  =$user->getParam();
        $users['coach']  =$user->getCoach();

        $view = $this->view($users);
        return $this->handleView($view);
    }

    public function getUsersInterestAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $request = $this->get('request');

        $data = InterestController::indexAction($request,true);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    public function postUsersInterestAction(Request $request){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = InterestController::showAction($request,true);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    public function getUsersCoachingAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $request = $this->get('request');

        $data = CoachingController::indexAction($request,true);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    public function postUsersCoachingAction(Request $request){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = CoachingController::showAction($request,true);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    public function getUsersNotifAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $request = $this->get('request');

        $data = ConfigurationController::indexAction($request,true);
        $view = $this->view($data);
        return $this->handleView($view);
        
    }

    public function postUsersNotifAction(Request $request){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = ConfigurationController::showAction($request,true);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    public function postUsersImageAction(Request $request){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }


        $data = FileController::createAction($request,true);
        $view = $this->view($data);
        return $this->handleView($view);
    }

    public function getUsersProfileAction($id){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = User::showAction($id,true);
        $view = $this->view($data);
        return $this->handleView($view);

    }

    public function getUsersFriendsAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = User::completeAction(true);
        $view = $this->view($data);
        return $this->handleView($view);

    }

}
