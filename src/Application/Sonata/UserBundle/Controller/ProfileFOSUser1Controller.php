<?php



/*

 * This file is part of the FOSUserBundle package.

 *

 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>

 *

 * For the full copyright and license information, please view the LICENSE

 * file that was distributed with this source code.

 */


namespace Application\Sonata\UserBundle\Controller;


use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Symfony\Component\HttpFoundation\Response;

use FOS\UserBundle\Model\UserInterface;

use FOS\UserBundle\Event\FormEvent;

use FOS\UserBundle\Event\FilterUserResponseEvent;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\UserBundle\FOSUserEvents;


use Sonata\UserBundle\Controller\ProfileFOSUser1Controller as BaseController;
use Symfony\Component\Validator\Constraints\Null;

/**
 * This class is inspired from the FOS Profile Controller, except :
 *   - only twig is supported
 *   - separation of the user authentication form with the profile form

 */
class ProfileFOSUser1Controller extends BaseController

{

    /**
     * @return Response
     *
     * @throws AccessDeniedException
     */

    public function showAction()

    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {

            throw new AccessDeniedException('This user does not have access to this section.');

        }

        $logo = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($user);

        $userInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $user));
        $ctr = 3;

        if($user->getEmail()!=Null)
        {
            $ctr++;;
        }
        if($user->getFirstname()!=Null)
        {
            $ctr++;
        }
        if($user->getLastname()!=Null)
        {
            $ctr++;
        }
        if($user->getPhone()!=Null)
        {
            $ctr++;
        }

        if($user->getWeight()!=0)
        {
            $ctr++;
        }
        if($user->getSize()!=0)
        {
            $ctr++;
        }


        if($user->getDateOfBirth()!=Null)
        {
            $ctr++;
        }
        if($user->getCivility()!=Null)
        {
            $ctr++;
        }
        if($user->getCity()!=Null)
        {
            $ctr++;
        }
        if($user->getCountry()!=Null)
        {
            $ctr++;
        }

        if(count($logo)>1)
        {
            $ctr++;
        }

        if(count($userInterest)>0)
        {
            $ctr++;
        }

        if($ctr-2==13)
        {
            $compte= true;
        }else{
            $compte=false;
        }
        $tatal = 15;
        $rest =$ctr;

        $percentages =intval((100*$ctr)/15);





        return $this->render('ApplicationSonataUserBundle:Profile:show.html.twig', array(

            'user' => $user,
            'logo' => $logo,
            'userInterest' => $userInterest,

            'percentages' => $percentages,
            'ctr' => $ctr,
            'compte' => $compte,

            'blocks' => $this->container->getParameter('sonata.user.configuration.profile_blocks')

        ));

    }


    /**
     * @return Response
     *
     * @throws AccessDeniedException
     */

    public function editAuthenticationAction()

    {

        $user = $this->container->get('security.context')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {

            throw new AccessDeniedException('This user does not have access to this section.');

        }


        $form = $this->container->get('sonata.user.authentication.form');

        $formHandler = $this->container->get('sonata.user.authentication.form_handler');


        $process = $formHandler->process($user);
        
        

        if ($process) {
            
            
            $this->setFlash('sonata_user_success', 'profile.flash.updated');


            return new RedirectResponse($this->generateUrl('sonata_user_profile_show'));

        }


        return $this->render('ApplicationSonataUserBundle:Profile:edit_authentication.html.twig', array(

            'form' => $form->createView(),

        ));

    }


    /**
     * @return Response
     *
     * @throws AccessDeniedException
     */

    public function editProfileAction()

    {

        $user = $this->container->get('security.context')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {

            throw new AccessDeniedException('This user does not have access to this section.');

        }


        $form = $this->container->get('sonata.user.profile.form');

        $formHandler = $this->container->get('sonata.user.profile.form.handler');


        $process = $formHandler->process($user);

        if ($process) {

            $this->setFlash('sonata_user_success', 'profile.flash.updated');


            return new RedirectResponse($this->generateUrl('sonata_user_profile_show'));


        }


        return $this->render('ApplicationSonataUserBundle:Profile:edit_profile.html.twig', array(

            'form' => $form->createView(),

            'breadcrumb_context' => 'user_profile',

        ));

    }

    /**
     * Generate the redirection url when the resetting is completed.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return string
     */
    protected function getRedirectionUrl(UserInterface $user)
    {

        return $this->container->get('router')->generate('fos_user_profile_show');
    }

    /**
     * @param string $action
     * @param string $value
     */

    protected function setFlash($action, $value)

    {

        $this->container->get('session')->getFlashBag()->set($action, $value);

    }

}

