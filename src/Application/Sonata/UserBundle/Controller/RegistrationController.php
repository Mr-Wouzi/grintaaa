<?php





/*


 * This file is part of the FOSUserBundle package.


 *


 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>


 *


 * For the full copyright and license information, please view the LICENSE


 * file that was distributed with this source code.


 */


namespace Application\Sonata\UserBundle\Controller;


use Symfony\Component\DependencyInjection\ContainerAware;


use Symfony\Component\HttpFoundation\Response;


use Symfony\Component\HttpFoundation\RedirectResponse;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


use Symfony\Component\Security\Core\Exception\AccessDeniedException;


use Symfony\Component\Security\Core\Exception\AccountStatusException;

use FOS\UserBundle\Model\UserInterface;


use FOS\UserBundle\Controller\RegistrationController as BaseController;

use Comparator\Bundle\MultimediaBundle\Entity\File;

/**
 * Controller managing the registration
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class RegistrationController extends BaseController


{


    public function registerAction()


    {


        $form = $this->container->get('fos_user.registration.form');


        $formHandler = $this->container->get('fos_user.registration.form.handler');


        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');


        $process = $formHandler->process($confirmationEnabled);


        if ($process) {


            $user = $form->getData();


            $authUser = false;


            if ($confirmationEnabled) {


                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());


                $route = 'fos_user_registration_check_email';


            } else {


                $authUser = true;
                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());

                $route = 'fos_user_registration_confirmed';


            }


            $this->setFlash('fos_user_success', 'registration.flash.user_created');


            $url = $this->container->get('router')->generate($route);


            $response = new RedirectResponse($url);


            if ($authUser) {


                $this->authenticateUser($user, $response);


            }


            return $response;


        }


        return $this->container->get('templating')->renderResponse('ApplicationSonataUserBundle:Registration:register.html.' . $this->getEngine(), array(


            'form' => $form->createView(),


        ));


    }

    public function register1Action()


    {


        $form = $this->container->get('fos_user.registration.form');


        $formHandler = $this->container->get('fos_user.registration.form.handler');


        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');


        $process = $formHandler->process($confirmationEnabled);


        if ($process) {


            $user = $form->getData();


            $authUser = false;


            if ($confirmationEnabled) {


                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());


                $route = 'fos_user_registration_check_email';


            } else {


                $authUser = true;
                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());

                $route = 'fos_user_registration_confirmed';


            }


            $this->setFlash('fos_user_success', 'registration.flash.user_created');


            $url = $this->container->get('router')->generate($route);


            $response = new RedirectResponse($url);


            if ($authUser) {


                $this->authenticateUser($user, $response);


            }


            return $response;


        }


        return $this->container->get('templating')->renderResponse('ApplicationSonataUserBundle:Registration:register1.html.' . $this->getEngine(), array(


            'form' => $form->createView(),


        ));


    }

    public function register2Action()


    {


        $form = $this->container->get('fos_user.registration.form');


        $formHandler = $this->container->get('fos_user.registration.form.handler');


        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');


        $process = $formHandler->process($confirmationEnabled);


        if ($process) {


            $user = $form->getData();


            $authUser = false;


            if ($confirmationEnabled) {


                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());


                $route = 'fos_user_registration_check_email';


            } else {


                $authUser = true;
                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());

                $route = 'fos_user_registration_confirmed';


            }


            $this->setFlash('fos_user_success', 'registration.flash.user_created');


            $url = $this->container->get('router')->generate($route);


            $response = new RedirectResponse($url);


            if ($authUser) {


                $this->authenticateUser($user, $response);


            }


            return $response;


        }


        return $this->container->get('templating')->renderResponse('ApplicationSonataUserBundle:Registration:register2.html.' . $this->getEngine(), array(


            'form' => $form->createView(),


        ));


    }


    /**
     * Tell the user to check his email provider


     */


    public function checkEmailAction()


    {


        $email = $this->container->get('session')->get('fos_user_send_confirmation_email/email');


        $this->container->get('session')->remove('fos_user_send_confirmation_email/email');


        $user = $this->container->get('fos_user.user_manager')->findUserByEmail($email);


        if (null === $user) {


            throw new NotFoundHttpException(sprintf('The user with email "%s" does not exist', $email));


        }


        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:checkEmail.html.' . $this->getEngine(), array(


            'user' => $user,


        ));


    }


    /**
     * Receive the confirmation token from user email provider, login the user


     */


    public function confirmAction($token)


    {


        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);


        if (null === $user) {


            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));


        }


        $user->setConfirmationToken(null);


        $user->setEnabled(true);


        $user->setLastLogin(new \DateTime());


        $this->container->get('fos_user.user_manager')->updateUser($user);


        $response = new RedirectResponse($this->container->get('router')->generate('fos_user_registration_confirmed'));


        $this->authenticateUser($user, $response);


        return $response;


    }


    /**
     * Tell the user his account is now confirmed


     */


    public function confirmedAction()


    {


        $user = $this->container->get('security.context')->getToken()->getUser();


        if (!is_object($user) || !$user instanceof UserInterface) {


            throw new AccessDeniedException('This user does not have access to this section.');


        }

		
	if($user->getCivility()=="Homme")	
		{
        if($user->getId()%5 ==0)
        {
            $em =  $this->container->get('doctrine')->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("3AvatarHommes.jpg");
            $em->persist($entity);
            $em->flush();
        }elseif($user->getId()%3 ==0){
            $em =  $this->container->get('doctrine')->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("5AvatarHommes.jpg");
            $em->persist($entity);
            $em->flush();

        }elseif($user->getId()%2 ==0){
            $em =  $this->container->get('doctrine')->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("6AvatarHommes.jpg");
            $em->persist($entity);
            $em->flush();

        }else{
            $em =  $this->container->get('doctrine')->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("11AvatarHommes.jpg");
            $em->persist($entity);
            $em->flush();

        }
		}
		
		if($user->getCivility()=="Femme")	
		{
        if($user->getId()%5 ==0)
        {
            $em =  $this->container->get('doctrine')->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("5AvatarFemme.jpg");
            $em->persist($entity);
            $em->flush();
        }elseif($user->getId()%3 ==0){
            $em =  $this->container->get('doctrine')->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("9AvatarFemme.jpg");
            $em->persist($entity);
            $em->flush();

        }elseif($user->getId()%2 ==0){
            $em =  $this->container->get('doctrine')->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("8AvatarFemme.jpg");
            $em->persist($entity);
            $em->flush();

        }else{
            $em =  $this->container->get('doctrine')->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("7AvatarFemme.jpg");
            $em->persist($entity);
            $em->flush();

        }
		}
        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:confirmed.html.' . $this->getEngine(), array(


            'user' => $user,


        ));


    }


    /**
     * Authenticate a user with Symfony Security
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     * @param \Symfony\Component\HttpFoundation\Response $response
     */


    protected function authenticateUser(UserInterface $user, Response $response)


    {


        try {


            $this->container->get('fos_user.security.login_manager')->loginUser(


                $this->container->getParameter('fos_user.firewall_name'),


                $user,


                $response);


        } catch (AccountStatusException $ex) {


            // We simply do not authenticate users which do not pass the user


            // checker (not enabled, expired, etc.).


        }


    }


    /**
     * @param string $action
     * @param string $value
     */


    protected function setFlash($action, $value)


    {


        $this->container->get('session')->getFlashBag()->set($action, $value);


    }


    protected function getEngine()


    {


        return $this->container->getParameter('fos_user.template.engine');


    }


}


