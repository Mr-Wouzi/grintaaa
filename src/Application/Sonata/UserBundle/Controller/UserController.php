<?php

namespace Application\Sonata\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Application\Sonata\UserBundle\Entity\User;
use Application\Sonata\UserBundle\Form\UserType;


/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $freind = $em->getRepository('ApplicationSonataUserBundle:User')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('ComparatorEventBundle:Friends')->testListFriend($user, $freind);
        $entitiess = $em->getRepository('ComparatorEventBundle:Friends')->testFriend($user, $freind);
        $send = $em->getRepository('ComparatorEventBundle:Friends')->sendFriend($user, $freind);
        $requestinvi = $em->getRepository('ComparatorEventBundle:Friends')->requestFriend($user, $freind);


        $allFreinds = array();

        foreach ($entities as $entity) {


            array_push($allFreinds, $entity);

        }


        foreach ($entitiess as $entitys) {


            array_push($allFreinds, $entitys);

        }

        $logo = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($freind);
        $friendsInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $freind));
        $friendsCoaching = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $freind));

        if (!$freind) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $participeFreinds =$em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeByUser($freind);

        $listentities = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($freind);

        $listentitiess = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($freind);


        $listallFreinds = array();
        $listimages = array();
        foreach ($listentities as $listentity) {
            $listimage = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($listentity->getUser1());

            array_push($listimages, $listimage);
            array_push($listallFreinds, $listentity);
        }


        foreach ($listentitiess as $listentitys) {
            $listimage = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($listentitys->getUser2());

            array_push($listimages, $listimage);
            array_push($listallFreinds, $listentitys);
        }



        return $this->render('ApplicationSonataUserBundle:User:index.html.twig', array(
            'entity' => $freind,
            'friends' => $allFreinds,
            'sends' => $send,
            'requestinvis' => $requestinvi,
            'logo' => $logo,
            'friendsInterest' => $friendsInterest,
            'friendsCoaching' => $friendsCoaching,
            'participeFreinds' => $participeFreinds,
            'listallFreinds' => $listallFreinds,
            'listimages' => $listimages,
        ));
    }

    /**
     * Lists all User entities.
     *
     */
    public function completeAction($api = false)
    {
        $em = $this->getDoctrine()->getManager();


        $user = $this->get('security.context')->getToken()->getUser();

        $entities = $em->getRepository('ApplicationSonataUserBundle:User')->CompleteFriends($user);


        $allFreinds = array();
        foreach ($entities as $entity) {

            if($entity->getParam()==1)

            {
                array_push($allFreinds, $entity);
            }else{
                if($entity->getCivility()==$user->getCivility())
                {
                    array_push($allFreinds, $entity);
                }
            }



        }

        if($api){
            return array(
                'entities' => $allFreinds,
            );
        }
        return $this->render('ApplicationSonataUserBundle:User:complete.html.twig', array(
            'entities' => $allFreinds,
        ));

    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id, $api =false)
    {
        $em = $this->getDoctrine()->getManager();
        $freind = $em->getRepository('ApplicationSonataUserBundle:User')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('ComparatorEventBundle:Friends')->testListFriend($user, $freind);
        $entitiess = $em->getRepository('ComparatorEventBundle:Friends')->testFriend($user, $freind);
        $send = $em->getRepository('ComparatorEventBundle:Friends')->sendFriend($user, $freind);
        $requestinvi = $em->getRepository('ComparatorEventBundle:Friends')->requestFriend($user, $freind);
        $events = $em->getRepository('ComparatorEventBundle:Event')->listEventUser($freind);


        $notification = $em->getRepository('ComparatorEventBundle:Notification')->listNotificationByUserProfile($user);

        if ($notification) {
            foreach ($notification as $notif) {

                $notif->setEnabled(true);


                $em->flush();
            }
        }

        $allFreinds = array();

        foreach ($entities as $entity) {


            array_push($allFreinds, $entity);

        }


        foreach ($entitiess as $entitys) {


            array_push($allFreinds, $entitys);

        }

        $logo = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($freind);
        $Mylogo = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($user);
        $friendsInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $freind));
        $friendsCoaching = $this->getDoctrine()->getRepository('ComparatorEventBundle:Coaching')->findBy(array('user' => $freind));

        if (!$freind) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $participeFreinds =$em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeByUser($freind);

        $listentities = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($freind);

        $listentitiess = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($freind);


        $listallFreinds = array();
        $listimages = array();
        foreach ($listentities as $listentity) {
            $listimage = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($listentity->getUser1());

            array_push($listimages, $listimage);
            array_push($listallFreinds, $listentity);
        }


        foreach ($listentitiess as $listentitys) {
            $listimage = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($listentitys->getUser2());

            array_push($listimages, $listimage);
            array_push($listallFreinds, $listentitys);
        }


        $listComment = $em->getRepository('ComparatorEventBundle:CommentUser')->findCommentByUser($freind);
        $testComment = $em->getRepository('ComparatorEventBundle:CommentUser')->findCommentByMyUser($freind,$user);



        $imagesComment = array();
        foreach ($listComment as $comment) {


            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($comment->getUser());

            array_push($imagesComment, $image);


        }

        $votes = array();

        foreach ($listComment as $comment) {


            $vote = $em->getRepository('ApplicationSiteVoteBundle:Vote')->oneVote($freind,$comment->getUser());

            array_push($votes, $vote);


        }

        $users['country'] = $freind->getCountry();
        $users['city'] = $freind->getCity();
        if($api)
            return array(
                'user' => $users,
                'entity' => $freind,
                'testComment' => $testComment,
                'votes' => $votes,
                'listComment' => $listComment,
                'imagesComment' => $imagesComment,
                'friends' => $allFreinds,
                'sends' => $send,
                'requestinvis' => $requestinvi,
                'logo' => $logo,
                'mylogo' => $Mylogo,
                'events' => $events,
                'friendsInterest' => $friendsInterest,
                'friendsCoaching' => $friendsCoaching,
                'participeFreinds' => $participeFreinds,
                'listallFreinds' => $listallFreinds,
                'listimages' => $listimages,
            );

        return $this->render('ApplicationSonataUserBundle:User:show.html.twig', array(
            'entity' => $freind,
            'testComment' => $testComment,
            'votes' => $votes,
            'listComment' => $listComment,
            'imagesComment' => $imagesComment,
            'friends' => $allFreinds,
            'sends' => $send,
            'requestinvis' => $requestinvi,
            'logo' => $logo,
            'mylogo' => $Mylogo,
            'events' => $events,
            'friendsInterest' => $friendsInterest,
            'friendsCoaching' => $friendsCoaching,
            'participeFreinds' => $participeFreinds,
            'listallFreinds' => $listallFreinds,
            'listimages' => $listimages,
        ));

    }


    /**
     * Lists all User entities.
     *
     */
    public function configAction()
    {


        return $this->render('ApplicationSonataUserBundle:User:config.html.twig');
    }

}
