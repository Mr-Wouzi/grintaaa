<?php





/*


 * This file is part of the FOSUserBundle package.


 *


 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>


 *


 * For the full copyright and license information, please view the LICENSE


 * file that was distributed with this source code.


 */


namespace Application\Sonata\UserBundle\Form\Type;


use Symfony\Component\Form\AbstractType;


use Symfony\Component\Form\FormBuilderInterface;


use Symfony\Component\OptionsResolver\OptionsResolverInterface;


use Symfony\Component\Security\Core\Validator\Constraint\UserPassword as OldUserPassword;


use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;


use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;


use Application\Sonata\UserBundle\Entity\User;

use Doctrine\ORM\EntityRepository;
use Comparator\Bundle\EventBundle\Entity\Category;
use Comparator\Bundle\EventBundle\Repository\CategoryRepository;


use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


class ProfileFormType extends BaseType


{


    private $class;


    /**
     * @param string $class The User class name
     */


    public function __construct($class)


    {


        $this->class = $class;

    }


    public function buildForm(FormBuilderInterface $builder, array $options)


    {


        if (class_exists('Symfony\Component\Security\Core\Validator\Constraints\UserPassword')) {


            $constraint = new UserPassword();


        } else {


            // Symfony 2.1 support with the old constraint class


            $constraint = new OldUserPassword();


        }


        $this->buildUserForm($builder, $options);


         $builder

           // ->add('current_password', 'password', array('label' => 'form.current_password', 'translation_domain' => 'FOSUserBundle', 'mapped' => false, 'constraints' => $constraint,))

        ;


    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)


    {


        $resolver->setDefaults(array(


            'data_class' => $this->class,


            'intention' => 'profile',


        ));


    }


    public function getName()


    {


        return 'fos_user_profile';


    }


    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */


    protected function buildUserForm(FormBuilderInterface $builder, array $options)


    {

        $builder
            ->add(
                'civility',
                'choice',
                array(
                    'required' => true,
                    'translation_domain' => 'BaseBundle',
                    'choices' => array('Homme' => 'form.homme' , 'Femme' => 'form.femme')
                )
            )
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'BaseBundle'))
            
            ->add('firstname', null, array('label' => 'form.firstname','required' => true, 'translation_domain' => 'BaseBundle'))
            ->add('lastname', null, array('label' => 'form.lastname','required' => true, 'translation_domain' => 'BaseBundle'))
            ->add('phone', null, array('label' => 'form.phone','required' => false, 'translation_domain' => 'BaseBundle'))

            ->add('coach', 'choice', array(
                'label' => 'form.etes.coach',
                'choices' => array(1 => 'coach.oui', 0 => 'coach.non'),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'translation_domain' => 'BaseBundle'
            ))
            ->add('handi', 'choice', array(
                'label' => 'form.andisport',
                'choices' => array(1 => 'handi.oui', 0 => 'handi.non'),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'translation_domain' => 'BaseBundle'

            ))
            ->add('param', 'choice', array(
                'label' => 'Visibilite par l\'autre sexe',
                'choices' => array(1 => 'param.oui', 0 => 'param.non'),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
				'translation_domain' => 'BaseBundle'
            ))
            ->add('weight', null, array('label' => 'form.poid','required' => true, 'translation_domain' => 'BaseBundle'))
            ->add('size', null, array('label' => 'form.taille','required' => true, 'translation_domain' => 'BaseBundle'))
            ->add('date_of_birth', 'birthday', array(
			    'years' => range(1930, date('Y')),
                'placeholder' => array('year' => 'form.annee', 'month' => 'form.mois', 'day' => 'form.day'), 'translation_domain' => 'BaseBundle'
            ))
            ->add('city', null, array('label' => 'form.adress','required' => true, 'translation_domain' => 'BaseBundle'))
            ->add(
                'country',
                'choice',
                array(
                    'required' => true,
                    'translation_domain' => 'ApplicationSonataUserBundle',
                    'choices' => User::$countryList
                )
            );

    }


}


