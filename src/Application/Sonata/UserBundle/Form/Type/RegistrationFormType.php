<?php



/*

 * This file is part of the FOSUserBundle package.

 *

 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>

 *

 * For the full copyright and license information, please view the LICENSE

 * file that was distributed with this source code.

 */


namespace Application\Sonata\UserBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\Extension\Core\Type\DateType;

use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Comparator\Bundle\EventBundle\Entity\Category;
use Comparator\Bundle\EventBundle\Repository\CategoryRepository;


class RegistrationFormType extends BaseType

{

    private $class;


    /**
     * @param string $class The User class name
     */

    public function __construct($class)

    {

        $this->class = $class;

    }


    public function buildForm(FormBuilderInterface $builder, array $options)

    {


        $builder
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'BaseBundle'))
            ->add(
                'civility',
                'choice',
                array(
                    'required' => true,
                    'translation_domain' => 'BaseBundle',
                    'choices' => array('Homme' => 'form.homme' , 'Femme' => 'form.femme')
                )
            )
            ->add('firstname', null, array('label' => 'form.firstname','required' => true, 'translation_domain' => 'BaseBundle'))
            ->add('lastname', null, array('label' => 'form.lastname','required' => true, 'translation_domain' => 'BaseBundle'))
            ->add('phone', null, array('label' => 'form.phone','required' => false, 'translation_domain' => 'BaseBundle'))
            ->add('coach', 'choice', array(
                'label' => 'form.etes.coach',
                'choices' => array(1 => 'coach.oui', 0 => 'coach.non'),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'translation_domain' => 'BaseBundle'
            ))
            ->add('handi', 'choice', array(
                'label' => 'form.andisport',
                'choices' => array(1 => 'handi.oui', 0 => 'handi.non'),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'translation_domain' => 'BaseBundle'
            ))
            ->add('date_of_birth', 'birthday', array(
			    'years' => range(1930, date('Y')),
                'placeholder' => array('year' => 'form.annee', 'month' => 'form.mois', 'day' => 'form.day'), 'translation_domain' => 'BaseBundle','required' => true,
            ))
            ->add(
                'country',
                'choice',
                array(
                    'required' => true,
                    'translation_domain' => 'ApplicationSonataUserBundle',
                    'choices' => User::$countryList,
                )
            )
            ->add('city', null, array('label' => 'form.adress','required' => true, 'translation_domain' => 'BaseBundle'))
            ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('plainPassword', 'repeated', array(

                'type' => 'password',

                'options' => array('translation_domain' => 'ApplicationSonataUserBundle'),

                'first_options' => array('label' => 'form.password'),

                'second_options' => array('label' => 'form.password_confirmation'),

                'invalid_message' => 'fos_user.password.mismatch',
                'required' => true,


            ));


    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)

    {

        $resolver->setDefaults(array(

            'data_class' => $this->class,

            'intention' => 'registration',

        ));

    }


    public function getName()

    {

        return 'fos_user_registration';

    }


}

