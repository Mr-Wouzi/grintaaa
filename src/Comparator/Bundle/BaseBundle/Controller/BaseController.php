<?php











namespace Comparator\Bundle\BaseBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;

use Comparator\Bundle\EventBundle\Entity\Notification;

use Comparator\Bundle\EventBundle\Form\NotificationType;

class BaseController extends Controller


{


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function homeAction(Request $request)


    {
        $locale = $request->getLocale();

        
		 if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
        $user = $this->container->get('security.context')->getToken()->getUser();
	 
            //($user->getUpdatedAt()==$user->getCreatedAt())
            if($user->getFacebookUid())
            {
                $em = $this->getDoctrine()->getManager();
                $notif =$em->getRepository('ComparatorEventBundle:Notification')->notificationRegister($user);

                
                if($notif)
                {
				$interest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $user));
				
				if(($user->getLastName())&&(count($interest)>0))
				  {
				  
				  $notif->setEnabled(true);
				  $em->persist($notif);

                    $em->flush();
				  }
				
				}else{
                    $notifEnable = $em->getRepository('ComparatorEventBundle:Notification')->notificationRegisterEnable($user);

                    if(!$notifEnable)
                    {
                        $notification = new Notification();
                        $notification->setEnabled(false);
                        $notification->setUser($user);

                        $notification->setUser1($user);
                        $notification->setTitle("Register");

                        $em->persist($notification);

                        $em->flush();
                    }


                }


            }else{
			
			    $em = $this->getDoctrine()->getManager();
                $notif =$em->getRepository('ComparatorEventBundle:Notification')->notificationRegister($user);

                
                if($notif)
                {
				$interest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $user));
				
				if(($user->getWeight())&&(count($interest)>0))
				  {
				  
				  $notif->setEnabled(true);
				  $em->persist($notif);

                    $em->flush();
				  }
				
				}else{
                    $notifEnable = $em->getRepository('ComparatorEventBundle:Notification')->notificationRegisterEnable($user);
                    if(!$notifEnable)
                    {
                        $notification = new Notification();
                        $notification->setEnabled(false);
                        $notification->setUser($user);

                        $notification->setUser1($user);
                        $notification->setTitle("Register");

                        $em->persist($notification);

                        $em->flush();
                    }



                }

			
			}
    }
		
		

        return $this->render('ComparatorBaseBundle:Base:home.html.twig');


    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function presentationAction()


    {


        return $this->render('ComparatorBaseBundle:Base:presentation.html.twig');


    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function personneAction()


    {


        return $this->render('ComparatorBaseBundle:Base:personne.html.twig');


    }
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function engagementAction()


    {


        return $this->render('ComparatorBaseBundle:Base:engagement.html.twig');


    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function mentionlegalesAction()


    {


        return $this->render('ComparatorBaseBundle:Base:mentionlegales.html.twig');


    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function aideAction()


    {


        return $this->render('ComparatorBaseBundle:Base:aide.html.twig');


    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function planSiteAction()


    {


        return $this->render('ComparatorBaseBundle:Base:plan.html.twig');


    }


}





