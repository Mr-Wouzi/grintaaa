<?php





namespace Comparator\Bundle\BaseBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class SiteController extends Controller


{


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */


    public function logoAction()


    {


        $em = $this->getDoctrine()->getManager();


        $entities = $em->getRepository('ComparatorBaseBundle:Site')->logoSite();


        return $this->render('ComparatorBaseBundle:Site:logo.html.twig', array(


            'entities' => $entities,


        ));


    }


}


