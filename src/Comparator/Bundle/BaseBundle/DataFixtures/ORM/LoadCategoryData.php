<?php











namespace Comparator\Bundle\BaseBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\OrderedFixtureInterface;


use Doctrine\Common\DataFixtures\AbstractFixture;


use Doctrine\Common\Persistence\ObjectManager;


use Comparator\Bundle\EventBundle\Entity\Category;


use Doctrine\ORM\Mapping as ORM;


class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface


{


    /**
     * @param ObjectManager $em
     */


    public function load(ObjectManager $em)


    {


        $category_1 = new Category();


        $category_1->setTitle('Football');


        $category_1->setSlug('football');


        $em->persist($category_1);


        $category_2 = new Category();


        $category_2->setTitle('Handball');


        $category_2->setSlug('handball');


        $em->persist($category_1);


        $em->persist($category_2);


        $em->flush();


    }


    public function getOrder()


    {


        return 10;


    }


}





