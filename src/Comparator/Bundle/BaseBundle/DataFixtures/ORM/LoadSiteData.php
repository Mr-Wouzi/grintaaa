<?php









namespace Comparator\Bundle\BaseBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\OrderedFixtureInterface;


use Doctrine\Common\DataFixtures\AbstractFixture;


use Doctrine\Common\Persistence\ObjectManager;


use Comparator\Bundle\BaseBundle\Entity\Site;


use Doctrine\ORM\Mapping as ORM;


class LoadSiteData extends AbstractFixture implements OrderedFixtureInterface


{


    /**
     * @param ObjectManager $em
     */


    public function load(ObjectManager $em)


    {


        $site_1 = new Site();


        $site_1->setTitle('Grintaaa');


        $site_1->setEmail('info@domain.com');


        $site_1->setNumTel('+2 95 01 88 821');


        $site_1->setEnabled(true);


        $em->persist($site_1);


        $em->flush();


    }


    public function getOrder()


    {


        return 10;


    }


}




