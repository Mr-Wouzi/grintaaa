<?php

namespace Comparator\Bundle\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Site
 */
class Site
{

	/**
	 * @var integer
	 */
	private $id;

	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var string
	 */
	private $numTel;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var boolean
	 */
	private $enabled;

       /**
        * @var string
        */
      private $facebook;
    
    /**
     * @var string
     */
    private $twitter;
    
    /**
     * @var string
     */

    private $google;
     /**
     * @var string
     */

    private $linkedin;
      /**
	* @return string
      */

	/**
	 * Hook timestampable behavior
	 * updates createdAt, updatedAt fields
	 */
	use TimestampableEntity;

	/**
	 * @var \Application\Sonata\MediaBundle\Entity\Media
	 */
	private $image;

	/**
	 * @return string
	 */
	public function __toString()
	{
		return (string)$this->getTitle();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Site
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Set numTel
	 *
	 * @param string $numTel
	 * @return Site
	 */
	public function setNumTel($numTel)
	{
		$this->numTel = $numTel;

		return $this;
	}

	/**
	 * Get numTel
	 *
	 * @return string
	 */
	public function getNumTel()
	{
		return $this->numTel;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 * @return Site
	 */
	public function setEmail($email)
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set enabled
	 *
	 * @param boolean $enabled
	 * @return Site
	 */
	public function setEnabled($enabled)
	{
		$this->enabled = $enabled;

		return $this;
	}

	/**
	 * Get enabled
	 *
	 * @return boolean
	 */
	public function getEnabled()
	{
		return $this->enabled;
	}

	/**
	 * Set image
	 *
	 * @param \Application\Sonata\MediaBundle\Entity\Media $image
	 * @return Site
	 */
	public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
	{
		$this->image = $image;

		return $this;
	}

	/**
	 * Get image
	 *
	 * @return \Application\Sonata\MediaBundle\Entity\Media
	 */
	public function getImage()
	{
		return $this->image;
	}


    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Site
     */

    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }



    /**
     * Get facebook
     *
     * @return string 
     */

    public function getFacebook()
    {
        return $this->facebook;
    }
        
        
        
    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Site
     */

    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
        return $this;
    }



    /**
     * Get twitter
     *
     * @return string 
     */

    public function getTwitter()
    {
        return $this->twitter;
    }  
    
    /**
     * Set google
     *
     * @param string $google
     * @return Site
     */

    public function setGoogle($google)
    {
        $this->google = $google;
        return $this;
    }



    /**
     * Get google
     *
     * @return string 
     */

    public function getGoogle()
    {
        return $this->google;
    }
        

    
    /**
     * Set linkedin
     *
     * @param string $linkedin
     * @return Site
     */

    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;
        return $this;
    }



    /**
     * Get linkedin
     *
     * @return string 
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }
}
