<?php

namespace Comparator\Bundle\BaseBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BaseControllerTest extends WebTestCase
{
    public function testPresentation()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/presentation');
    }

    public function testEngagement()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/engagement');
    }

    public function testMentionlegales()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/mentionlegales');
    }

    public function testAide()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/aide');
    }

    public function testContact()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact');
    }

}
