<?php





namespace Comparator\Bundle\EventBundle\Admin;





use Sonata\AdminBundle\Admin\Admin;


use Sonata\AdminBundle\Datagrid\DatagridMapper;


use Sonata\AdminBundle\Datagrid\ListMapper;


use Sonata\AdminBundle\Form\FormMapper;


use Sonata\AdminBundle\Show\ShowMapper;





class CategoryAdmin extends Admin


{


    /**


     * @param DatagridMapper $datagridMapper


     */


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)


    {


        $datagridMapper


            ->add('title')

            ->add('parent')

        ;


    }





    /**


     * @param ListMapper $listMapper


     */


    protected function configureListFields(ListMapper $listMapper)


    {


        $listMapper


            ->add('title')
            ->add('parent')


            ->add('_action', 'actions', array(


                'actions' => array(


                    'show' => array(),


                    'edit' => array(),


                    'delete' => array(),


                )


            ))


        ;


    }





    /**


     * @param FormMapper $formMapper


     */


    protected function configureFormFields(FormMapper $formMapper)


    {



        $entity = new \Comparator\Bundle\EventBundle\Entity\Category();
        $query = $this->modelManager->getEntityManager($entity)->createQuery('SELECT s FROM Comparator\Bundle\EventBundle\Entity\Category s WHERE s.parent IS NULL ORDER BY s.title');
        $formMapper


            ->add('title')
            ->add('parent', 'sonata_type_model', array('required' => false, 'query' => $query), array('edit' => 'standard'))


        ;


    }





    /**


     * @param ShowMapper $showMapper


     */


    protected function configureShowFields(ShowMapper $showMapper)


    {


        $showMapper


            ->add('title')

            ->add('parent')

            ->add('createdAt')


            ->add('updatedAt')


        ;


    }


}


