<?php
/**
 * Created by PhpStorm.
 * User: developper
 * Date: 18/08/16
 * Time: 23:09
 */

namespace Comparator\Bundle\EventBundle\Controller\Api;

use \FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\UserBundle\Controller\SecurityController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Comparator\Bundle\EventBundle\Controller\NotificationController as Notification;
use Comparator\Bundle\EventBundle\Controller\ParticipeEventController as Participate;


class CounterController extends FOSRestController
{

    public function getCounterAllAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data =  array();
        $data['notification'] = Notification::counNotificationAction(true);

        $view = $this->view($data);
        return $this->handleView($view);

    }

    public function getCounterNotificationAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }
        
        
        $data = Notification::counNotificationAction(true);
        
        $view = $this->view($data);
        return $this->handleView($view);

    }

    public function getCounterNotificationUserAction($userid){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = Notification::counNotificationAction(true,$userid);
        $view = $this->view($data);
        return $this->handleView($view);

    }


}
