<?php
/**
 * Created by PhpStorm.
 * User: developper
 * Date: 18/08/16
 * Time: 23:09
 */

namespace Comparator\Bundle\EventBundle\Controller\Api;

use \FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\UserBundle\Controller\SecurityController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Comparator\Bundle\EventBundle\Controller\EventController as Event;
use Comparator\Bundle\EventBundle\Controller\ParticipeEventController as Participate;


class EventController extends FOSRestController
{


    public function postEventCreateAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        
        $request = $this->get('request');
        $data = $this->get('request')->request->get('comparator_bundle_eventbundle_event');
        
        //$data = Event::searchAction(true);
        $data = Event::createAction($request , true);
        $view = $this->view($data);
        return $this->handleView($view);

    }

    
    public function postEventSearchAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = Event::searchAction(true);

        $view = $this->view($data);
        return $this->handleView($view);

    }

    public function getEventShowAction($slug){


        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }
        $data = Event::showAction($slug,true);

        $view = $this->view($data);
        return $this->handleView($view);

    }

    public function postEventParticipeAction($id,$enable){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = Participate::addParticipeAction($enable, $id, true);

        $view = $this->view($data);
        return $this->handleView($view);

    }

    public function postEventDelParticipeAction($id){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }
        
        $request = $this->get('request');
        $data = Participate::deleteAction($request,$id, true);
        
        $view = $this->view($data);
        return $this->handleView($view);

    }



}
