<?php

/**
 * Created by PhpStorm.
 * User: developper
 * Date: 16/09/16
 * Time: 16:50
 */

namespace Comparator\Bundle\EventBundle\Controller\Api;

use \FOS\RestBundle\Controller\FOSRestController;
use Comparator\Bundle\EventBundle\Controller\FriendsController as Friends;

/**
 * Friends controller.
 *
 */
class FriendsController extends FOSRestController
{

    public function getFriendAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = Friends::listFriendAction(true);

        $view = $this->view($data);
        return $this->handleView($view);

    }

}
