<?php
/**
 * Created by PhpStorm.
 * User: developper
 * Date: 18/08/16
 * Time: 23:09
 */

namespace Comparator\Bundle\EventBundle\Controller\Api;

use \FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\UserBundle\Controller\SecurityController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Comparator\Bundle\EventBundle\Controller\NotificationController as Notification;
use Comparator\Bundle\EventBundle\Controller\LikemController as Likem;


class LikemController extends FOSRestController
{


    public function getLikemAction($userid){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = Likem::indexAction($userid,true);
        $view = $this->view($data);
        return $this->handleView($view);

    }
    
    public function getLikemCountAction($userid){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = Likem::testLikemAction($userid,true);
        $view = $this->view($data);
        return $this->handleView($view);

    }

    public function getLikemDelAction($userid){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $request = $this->get('request');

        $data = Likem::delLikemAction($userid,$request,true);
        
        $view = $this->view($data);
        return $this->handleView($view);

    }
    
    public function getLikemCountlikAction($userid){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $data = Likem::addLikemAction(true,$userid);
        
        $view = $this->view($data);
        return $this->handleView($view);

    }
    


}
