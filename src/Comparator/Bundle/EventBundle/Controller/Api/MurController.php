<?php

namespace Comparator\Bundle\EventBundle\Controller\Api;

use \FOS\RestBundle\Controller\FOSRestController;
use Comparator\Bundle\EventBundle\Controller\MurController as Mur;



class MurController extends FOSRestController
{

    public function getMurAction(){

        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $request = $this->get('request');
        $data = Mur::murEventFriendsAction($request,true);

        $view = $this->view($data);
        return $this->handleView($view);

    }

}
