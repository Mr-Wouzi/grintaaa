<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\Coaching;
use Comparator\Bundle\EventBundle\Form\CoachingType;

/**
 * Coaching controller.
 *
 */
class CoachingController extends Controller
{

    /**
     * Lists all Coaching entities.
     *
     */
    public function indexAction(Request $request, $api = false)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ComparatorEventBundle:Category')->findAll();
        $disciplines = $em->getRepository('ComparatorEventBundle:Category')->findLevel1();
        $user = $this->get('security.context')->getToken()->getUser();

        $oldcoaching = $this->getDoctrine()->getRepository('ComparatorEventBundle:Coaching')->findBy(array('user' => $user));


        $coachingUser = array();
        foreach ($oldcoaching as $coaching) {
            array_push($coachingUser, ($coaching->getCategory()->getId()));
        }

        if($api)
            return array(
                'disciplines' => $disciplines,
                'coachingUser' => $coachingUser,
            );
        
        return $this->render('ComparatorEventBundle:Coaching:index.html.twig', array(
            'entities' => $entities,
            'disciplines' => $disciplines,
            'coachingUser' => $coachingUser,
        ));

    }


    /**
     * Finds and displays a Coaching entity.
     *
     */
    public function showAction(Request $request, $api = false)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ComparatorEventBundle:Category')->findAll();
        $user = $this->get('security.context')->getToken()->getUser();

        $oldcoaching = $this->getDoctrine()->getRepository('ComparatorEventBundle:Coaching')->findBy(array('user' => $user));


        $coachingUser = array();
        foreach ($oldcoaching as $coaching) {
            array_push($coachingUser, $coaching->getCategory()->getId());
        }

        if ($request->getMethod() == 'POST' or $api) {



            foreach ($oldcoaching as $key => $value) {

                $entity = $em->getRepository('ComparatorEventBundle:Coaching')->find($value);
                $em->remove($entity);
                $em->flush();
            }

            //$coaching = $request->request->get("coaching");

            $diciplines = $request->request->get("discipline");
            if(count($diciplines)>0)
            {
                foreach ($diciplines as $key => $value) {
                    $coaching = new Coaching();

                    $coaching->setUser($user);
                    $category = $this->getDoctrine()->getRepository('ComparatorEventBundle:Category')->find($value);
                    $coaching->setCategory($category);
                    $em->persist($coaching);
                    $em->flush();
                }
            }

            if($api)
                return array(
                    'result' => "success",
                );
            
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }
        return $this->render('ComparatorBaseBundle:Base:home.html.twig');

    }

    /**
     * Creates a new Coaching entity.
     *
     */
    public function createAction(Request $request)
    {

        $entity = new Coaching();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('home'));
        }

        return $this->render('ComparatorEventBundle:Coaching:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Creates a form to create a Coaching entity.
     *
     * @param Coaching $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Coaching $entity)
    {

        $form = $this->createForm(new CoachingType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_coaching_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;

    }

    /**
     * Displays a form to create a new Coaching entity.
     *
     */
    public function newAction()
    {

        $entity = new Coaching();
        $form = $this->createCreateForm($entity);

        return $this->render('ComparatorEventBundle:Coaching:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }


    /**
     * Displays a form to edit an existing Coaching entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Coaching')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Coaching entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Coaching:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Creates a form to edit a Coaching entity.
     *
     * @param Coaching $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Coaching $entity)
    {

        $form = $this->createForm(new CoachingType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_coaching_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;

    }

    /**
     * Edits an existing Coaching entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Coaching')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Coaching entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa_coaching_edit', array('id' => $id)));
        }

        return $this->render('ComparatorEventBundle:Coaching:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a Coaching entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorEventBundle:Coaching')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Coaching entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grintaaa_coaching'));


    }

    /**
     * Creates a form to delete a Coaching entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grintaaa_coaching_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();


    }
}
