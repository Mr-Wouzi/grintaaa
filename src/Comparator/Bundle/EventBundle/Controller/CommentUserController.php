<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\CommentUser;
use Comparator\Bundle\EventBundle\Form\CommentUserType;
use Comparator\Bundle\EventBundle\Entity\Notification;
use Comparator\Bundle\EventBundle\Form\NotificationType;

use Comparator\Bundle\EventBundle\Entity\Mur;
use Comparator\Bundle\EventBundle\Form\MurType;

/**
 * Comment controller.
 *
 */
class CommentUserController extends Controller
{


    public function addCommentAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user1 = $em->getRepository('ApplicationSonataUserBundle:User')->find($id);
        $text = $this->get('request')->request->get('textarea');


        $user = $this->get('security.context')->getToken()->getUser();

        $comment = new CommentUser();
        $comment->setUser($user);
        $comment->setUser1($user1);
        $comment->setDescription($text);
        $em->persist($comment);
        $em->flush();

        $notification = new Notification();
        $notification->setEnabled(false);
        $notification->setUser($user);

        $notification->setUser1($user1);
        $notification->setTitle("Commente Profil");

        $em->persist($notification);

        $em->flush();


        $config = $em->getRepository('ComparatorEventBundle:Configuration')->configByUser($user1);

        if($config)
        {
            if($config->getProfile())
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Grintaaa')
                    ->setFrom($user->getEmail())
                    ->setTo($user1->getEmail())
                    ->setBody(
                        $this->renderView(
                            'ComparatorEventBundle:Emails:commentuser.txt.twig',
                            array('user' => $user)

                        ),
                        'text/html'
                    )
                ;
                $this->get('mailer')->send($message);
            }
        }







        return $this->redirect($this->generateUrl('grintaaa_user_show', array('username' => $user1->getUsername(),'id' => $user1->getId())));


    }


    /**
     * Lists all Comment entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ComparatorEventBundle:Comment')->findAll();

        return $this->render('ComparatorEventBundle:Comment:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Comment entity.
     *
     */
    public function createAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = new Comment();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa_comment_show', array('id' => $entity->getId())));
        }

        return $this->render('ComparatorEventBundle:Comment:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Creates a form to create a Comment entity.
     *
     * @param Comment $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Comment $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new CommentType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_comment_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;


    }

    /**
     * Displays a form to create a new Comment entity.
     *
     */
    public function newAction()
    {
        $entity = new Comment();
        $form = $this->createCreateForm($entity);

        return $this->render('ComparatorEventBundle:Comment:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Finds and displays a Comment entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ComparatorEventBundle:Comment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Comment:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));


    }

    /**
     * Displays a form to edit an existing Comment entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ComparatorEventBundle:Comment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Comment:notif.txt.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Creates a form to edit a Comment entity.
     *
     * @param Comment $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Comment $entity)
    {
        $form = $this->createForm(new CommentType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_comment_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;

    }

    /**
     * Edits an existing Comment entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ComparatorEventBundle:Comment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa_comment_edit', array('id' => $id)));
        }

        return $this->render('ComparatorEventBundle:Comment:notif.txt.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a Comment entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorEventBundle:Comment')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Comment entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grintaaa_comment'));


    }

    /**
     * Creates a form to delete a Comment entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grintaaa_comment_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }

}
