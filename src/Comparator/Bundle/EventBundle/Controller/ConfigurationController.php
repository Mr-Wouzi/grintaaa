<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\Configuration;
use Comparator\Bundle\EventBundle\Form\ConfigurationType;

/**
 * Configuration controller.
 *
 */
class ConfigurationController extends Controller
{

    /**
     * Lists all Configuration entities.
     *
     */
    public function indexAction(Request $request ,$api = false)
    {
        $em = $this->getDoctrine()->getManager();


        $user = $this->get('security.context')->getToken()->getUser();

        $entities = $this->getDoctrine()->getRepository('ComparatorEventBundle:Configuration')->findBy(array('user' => $user));

        if($api)
            return array(
                'entities' => $entities,

            );
        
        
        return $this->render('ComparatorEventBundle:Configuration:index.html.twig', array(
            'entities' => $entities,

        ));

    }


    /**
     * Finds and displays a Configuration entity.
     *
     */
    public function showAction(Request $request, $api = false)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();

        $oldConfiguration = $this->getDoctrine()->getRepository('ComparatorEventBundle:Configuration')->findBy(array('user' => $user));


        if ($request->getMethod() == 'POST' or $api) {


            foreach ($oldConfiguration as $key => $value) {

                $entity = $em->getRepository('ComparatorEventBundle:Configuration')->find($value);
                $em->remove($entity);
                $em->flush();
            }

            //$interested = $request->request->get("interet");


            $comment = $request->request->get("comment");
            $profile = $request->request->get("profile");
            $participe = $request->request->get("participe");
            $confirmer = $request->request->get("confirmer");
            $supprimer = $request->request->get("supprimer");


            $config = new Configuration();

            $config->setUser($user);
            if ($comment == "on") {
                $config->setComment(true);

            } else {

                $config->setComment(false);

            }

            if ($profile == "on") {
                $config->setProfile(true);

            } else {

                $config->setProfile(false);

            }
            if ($participe == "on") {
                $config->setParticipe(true);

            } else {

                $config->setParticipe(false);

            }

            if ($confirmer == "on") {
                $config->setSupprimer(true);

            } else {

                $config->setSupprimer(false);

            }

            if ($supprimer == "on") {
                $config->setConfirmer(true);

            } else {
                $config->setConfirmer(false);


            }
            $em->persist($config);
            $em->flush();

            if($api)
                return array(
                    'result' => "success",
                );
            
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }
        return $this->render('ComparatorBaseBundle:Base:home.html.twig');

    }

}
