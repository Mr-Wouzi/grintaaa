<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\Notification;
use Comparator\Bundle\EventBundle\Entity\Event;
use Comparator\Bundle\EventBundle\Entity\Mur;
use Comparator\Bundle\EventBundle\Entity\OptionEvent;
use Comparator\Bundle\EventBundle\Form\EventType;
use Comparator\Bundle\EventBundle\Form\MurType;
use Comparator\Bundle\StatutBundle\Form\FileType;
use Comparator\Bundle\StatutBundle\Entity\File;
/**
 * Event controller.
 *
 */
class EventController extends Controller
{

    public function listEventAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();


        $city = $user->getCity();


        $locacl = $user->getCountry();

        $address = $locacl . '' . $city;
        $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=true');
        $coordinates = json_decode($coordinates);
        $lat = $coordinates->results[0]->geometry->location->lat;
        $lng = $coordinates->results[0]->geometry->location->lng;
        $events = $em->getRepository('ComparatorEventBundle:Event')->listEvent();
        $entities = array();

        $category = $this->getDoctrine()->getRepository('ComparatorEventBundle:Category')->findLevel2();

        $interests = array();

        $userInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $user));
        foreach ($userInterest as $interest) {
            array_push($interests, $interest->getCategory()->getTitle());
        }

        $images = array();

        $array_count = array();
        foreach ($events as $event) {




                if (round($this->get_distance_m($lat, $lng, $event->getLat(), $event->getLng()) / 1000, 3) <= 100) {
                    $listParticipeEvent = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeEvents($event);
                    $nbparticipe = count($listParticipeEvent);
                    array_push($array_count, $nbparticipe);
                    $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($event->getUser());
                    array_push($images, $image);
                    array_push($entities, $event);


                }


        }

        $villes = $em->getRepository('ComparatorEventBundle:Ville')->findAll();
		$seoPage = $this->container->get('sonata.seo.page');

        $seoPage
        ->setTitle("Grintaaa | Reseau des sports")
        ->addMeta('name', 'description', "Grintaaa est le meilleur reseau des sports et de rencontre sportive entre sportif et sportive en france")
	    ->addMeta('name', 'keywords', "Reseau des sports")
        ->addMeta('property', 'og:title', "Grintaaa | Reseau des sports")
        ->addMeta('property', 'og:type', 'site')
        ->addMeta('property', 'og:description', 'Grintaaa | Reseau des sports');

        return $this->render('ComparatorEventBundle:Event:listEvent.html.twig', array('entities' => $entities,'villes' => $villes, 'images' => $images, 'userInterest' => $category, 'nbparticipe' => $array_count));


    }
    /**
     * Creates a new Event entity.
     *
     */
    public function createAction(Request $request , $api = null)
    {

        header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');


        $entity = new Event();
        $form = Self::createCreateForm($entity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isValid() or $api) {

            $options = $request->request->get("option");

            $data = $this->get('request')->request->get('comparator_bundle_eventbundle_event');
            

            $address = $data['address']['address'];
            $locality = $data['locality']['address'];
            $country = $data['country']['address'];
            $lat = $data['lat']['address'];
            $lng = $data['lng']['address'];



            $descipline = $this->get('request')->request->get('articleID');
            $sport = $em->getRepository('ComparatorEventBundle:Category')->find($descipline);


            //$startTime = $data['startTime'];
            //$endTime = $data['endTime'];

            $em = $this->getDoctrine()->getManager();
            $entity->setUser($this->get('security.context')->getToken()->getUser());
            $entity->setAddress($address);
            $entity->setLocality($locality);
            $entity->setCountry($country);
            $entity->setLat($lat);
            $entity->setLng($lng);
            $entity->setCategory($sport);



            if($api){
                $startdate = $data['startDate'];
                $tmp = explode("-",$startdate);
                $date = \DateTime::createFromFormat("Y-m-d", $tmp[0].'-'.$tmp[1].'-'.$tmp[2]);
                $entity->setStartDate($date);
            }

            // $entity->setStartTime($startTime);
            //$entity->setEndTime($endTime);

            $em->persist($entity);


            $mur = new Mur();
            $mur->setEvent($entity);
            $mur->setUser1($this->get('security.context')->getToken()->getUser());
            $mur->setUser2($this->get('security.context')->getToken()->getUser());
            $mur->setType('newevent');
            $em->persist($mur);
            $em->flush();


            if($options!=null)
            {
                foreach($options as $option)
                {
                    $optionEvent= new OptionEvent();

                    $optionEvent->setEvent($entity);
                    $optionEvent->setTitle($option);
                    $em->persist($optionEvent);
                    $em->flush();
                }

            }
            
            if($api){
                return array(
                    'slug' => $entity->getSlug(),
                    'result' => "success"
                );
            }
            
            
        $seoPage = $this->container->get('sonata.seo.page');

        $seoPage
        ->setTitle("Grintaaa | organiser une journée sportive")
        ->addMeta('name', 'description', "Si vous voulez créer des rencontres sportives, organiser une journée sportive ou participer à des compétitions. Vous êtes au bon endroit !")
	    ->addMeta('name', 'keywords', "Reseau des sports")
        ->addMeta('property', 'og:title', "Grintaaa | organiser une journée sportive")
        ->addMeta('property', 'og:type', 'site')
        ->addMeta('property', 'og:description', 'Si vous voulez créer des rencontres sportives, organiser une journée sportive ou participer à des compétitions. Vous êtes au bon endroit !');

            return $this->redirect($this->generateUrl('event_confirmed', array('slug' => $entity->getSlug())));
        }

        return $this->render('ComparatorEventBundle:Event:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));


    }

    /**
     * Creates a form to create a Event entity.
     *
     * @param Event $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Event $entity)
    {

        $form = $this->createForm(new EventType(), $entity, array(
            'action' => $this->generateUrl('event_create'),
            'method' => 'POST',
        ));


        return $form;


    }

    /**
     * Displays a form to create a new Event entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('ComparatorEventBundle:Category')->findAll();
        $disciplinesC = $em->getRepository('ComparatorEventBundle:Category')->findLevel1();
        $disciplinesSA = $em->getRepository('ComparatorEventBundle:Category')->findLevel2();
        $entity = new Event();
        $form = $this->createCreateForm($entity);
		$seoPage = $this->container->get('sonata.seo.page');

        $seoPage
        ->setTitle("Grintaaa | Reseau des sports")
        ->addMeta('name', 'description', "Grintaaa est le meilleur reseau des sports et de rencontre sportive entre sportif et sportive en france")
	    ->addMeta('name', 'keywords', "Reseau des sports")
        ->addMeta('property', 'og:title', "Grintaaa | Reseau des sports")
        ->addMeta('property', 'og:type', 'site')
        ->addMeta('property', 'og:description', 'Grintaaa | Reseau des sports');

        return $this->render('ComparatorEventBundle:Event:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
            'categorie' => $category,
            'disciplinesc' => $disciplinesC,
            'disciplinessa' => $disciplinesSA,
        ));

    }

    /**
     * Finds and displays a Event entity.
     *
     */
    public function showAction($slug, $api = null )
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Event')->findOneBy(array('slug' => $slug));

        $likes = $em->getRepository('ComparatorEventBundle:Liked')->findBy(array('event' => $entity));


        $nblikes=count($likes);
        $entityImageUser = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($entity->getUser());

        $optionEvent= $em->getRepository('ComparatorEventBundle:OptionEvent')->listOptionEvent($entity);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }


        $dteDiff = $entity->getStartTime()->diff($entity->getEndTime());
        //$nbHeure = $dteDiff->format("%H:%I:%S");
        $nbHeure = $dteDiff->format("%h");

        $user = $this->get('security.context')->getToken()->getUser();

        $likesuser = $em->getRepository('ComparatorEventBundle:Liked')->findBy(array('event' => $entity,'user' => $user));

        $nblikesuser =count($likesuser);

        $notification = $em->getRepository('ComparatorEventBundle:Notification')->listNotificationByUserAndEvent($user, $entity);

        if ($notification) {
            foreach ($notification as $notif) {

                $notif->setEnabled(true);


                $em->flush();
            }
        }


        $logo = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($user);

        $testparticipe = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->findParticipeByUserAndEvent($entity, $user);

        $participes = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeEvent($entity);
        $participesConfirm = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeConfirmEvent($entity);
        $testparticipesConfirm = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->testParticipeConfirmEventUser($entity,$user);

        if($testparticipesConfirm)
        {
            $participeUser= true;
        }else{
            $participeUser= false;
        }

        $imagesParticipes = array();
        foreach ($participes as $participe) {


            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($participe->getUser());

            array_push($imagesParticipes, $image);


        }

        $imagesParticipesConfirm = array();
        foreach ($participesConfirm as $participeConfirm) {


            $imageConfirm = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($participeConfirm->getUser());

            array_push($imagesParticipesConfirm, $imageConfirm);


        }
        $maybe = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listMaybeEvent($entity);


        $nbparticipe = count($participes);

        $nbparticipeConfirm = count($participesConfirm);
        $nbmaybe = count($maybe);


        $listComment = $em->getRepository('ComparatorEventBundle:Comment')->findCommentByEvent($entity);
        $listStatut = $em->getRepository('ComparatorStatutBundle:File')->findStatutByEvent($entity);

        $imagesStatut =array();

        foreach ($listStatut as $stat) {


            $images = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($stat->getUser());

            array_push($imagesStatut, $images);


        }

        $imagesComment = array();
        foreach ($listComment as $comment) {


            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($comment->getUser());

            array_push($imagesComment, $image);


        }



        $fentities = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($user);

        $fentitiess = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($user);


        $fallFreinds = array();
        foreach ($fentities as $fentity) {

            array_push($fallFreinds, $fentity);
        }


        foreach ($fentitiess as $fentitys) {

            array_push($fallFreinds, $fentitys);
        }



        
        if($api){
            return array(
                'entity' => $entity,
                'participeUser' => $participeUser,
                'completes' => $fallFreinds,
                'entityImageUser' => $entityImageUser,
                'testparticipe' => $testparticipe,
                'nbparticipe' => $nbparticipe,
                'nbparticipeConfirm' => $nbparticipeConfirm,
                'nbmaybe' => $nbmaybe,
                'participes' => $participes,
                'participesConfirm' => $participesConfirm,
                'imagesParticipes' => $imagesParticipes,
                'imagesParticipesConfirm' => $imagesParticipesConfirm,
                'listComment' => $listComment,
                'listStatut' => $listStatut,
                'imagesComment' => $imagesComment,
                'imagesStatut' => $imagesStatut,
                'nbHeure' => $nbHeure,
                'logo' => $logo,
                'optionEvent' => $optionEvent,
                'nblikes' => $nblikes,
                'nblikesuser' => $nblikesuser,
            );
        }else{


            $entities = new File();
            $form = $this->createCreateFileForm($entities);
            
            return $this->render(
                'ComparatorEventBundle:Event:show.html.twig',
                array(
                    'entity' => $entity,
                    'participeUser' => $participeUser,
                    'completes' => $fallFreinds,
                    'entityImageUser' => $entityImageUser,
                    'testparticipe' => $testparticipe,
                    'nbparticipe' => $nbparticipe,
                    'nbparticipeConfirm' => $nbparticipeConfirm,
                    'nbmaybe' => $nbmaybe,
                    'participes' => $participes,
                    'participesConfirm' => $participesConfirm,
                    'imagesParticipes' => $imagesParticipes,
                    'imagesParticipesConfirm' => $imagesParticipesConfirm,
                    'listComment' => $listComment,
                    'listStatut' => $listStatut,
                    'imagesComment' => $imagesComment,
                    'imagesStatut' => $imagesStatut,
                    'nbHeure' => $nbHeure,
                    'logo' => $logo,
                    'optionEvent' => $optionEvent,
                    'nblikes' => $nblikes,
                    'nblikesuser' => $nblikesuser,
                    'form' => $form->createView(),

                )
            );
        }
        


    }

    /**
     * Finds and displays a Event entity.
     *
     */
    public function sendNotifEventAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $event = $em->getRepository('ComparatorEventBundle:Event')->findOneBy(array('slug' => $slug));




        $user = $this->get('security.context')->getToken()->getUser();


        $participants = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeConfirmEvent($event);

        foreach($participants as $participant)
        {
            $notification = new Notification();
            $notification->setEnabled(false);
            $notification->setEvent($event);
            $notification->setUser($user);

            $notification->setUser1($participant->getUser());
            $notification->setTitle("Fin Evenement");

            $em->persist($notification);

            $em->flush();
        }
		$seoPage = $this->container->get('sonata.seo.page');

        $seoPage
        ->setTitle("Grintaaa | Reseau des sports")
        ->addMeta('name', 'description', "Grintaaa est le meilleur reseau des sports et de rencontre sportive entre sportif et sportive en france")
	    ->addMeta('name', 'keywords', "Reseau des sports")
        ->addMeta('property', 'og:title', "Grintaaa | Reseau des sports")
        ->addMeta('property', 'og:type', 'site')
        ->addMeta('property', 'og:description', 'Grintaaa | Reseau des sports');


        return $this->redirect($this->generateUrl('event_show', array('slug' => $event->getSlug())));



    }

    /**
     * Displays a form to edit an existing Event entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('ComparatorEventBundle:Event:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ));


    }


    /**
     * Creates a form to edit a Event entity.
     *
     * @param Event $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Event $entity)
    {

        $form = $this->createForm(new EventType(), $entity, array(
            'action' => $this->generateUrl('event_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;

    }

    /**
     * Edits an existing Event entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Event')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('event_edit', array('id' => $id)));
        }

        return $this->render('ComparatorEventBundle:Event:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));


    }

    /**
     * Deletes a Event entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        //$form = $this->createDeleteForm($id);
        //$form->handleRequest($request);
        $user = $this->get('security.context')->getToken()->getUser();

        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorEventBundle:Event')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Event entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('event_user', array('username' => $user->getUsername())));

    }

    /**
     * Creates a form to delete a Event entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('event_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function confirmedAction($slug)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Event')->findOneBy(array('slug' => $slug));
        return $this->render('ComparatorEventBundle:Event:confirmed.html.twig', array(
            'entity' => $entity,
        ));

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction( $api = null)
    {
        $em = $this->getDoctrine()->getManager();

        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $user = $this->get('security.context')->getToken()->getUser();
        $city = $user->getCity();
        $userInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Category')->findLevel2();
        $userInterests = $this->getDoctrine()->getRepository('ComparatorEventBundle:Category')->findLevel2();

        if ($request->getMethod() == 'POST') {
            $locale = $request->request->get("genre");
            $ville = $request->request->get("ville");
            $interestSearch = $request->request->get("interest");
            $handi = $request->request->get("handi");

            if($handi=="on")
            {
                $handi =true;
            }
            else{
                $handi =false;
            }
            if ($ville == '') {
                $ville = $city;
            }
            $rayon = $request->request->get("rayon");
            $address = $locale . ' ' . $ville;

            $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=true');
            $coordinates = json_decode($coordinates);
            $lat = $coordinates->results[0]->geometry->location->lat;
            $lng = $coordinates->results[0]->geometry->location->lng;


            $interests = array();

            if ($interestSearch == '') {
                $userInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $user));
                foreach ($userInterest as $interest) {
                    array_push($interests, $interest->getCategory()->getTitle());
                }

            } else {
                $categorySearch = $this->getDoctrine()->getRepository('ComparatorEventBundle:Category')->find($interestSearch);
                array_push($interests, $categorySearch->getTitle());

            }


            $events = $em->getRepository('ComparatorEventBundle:Event')->listEventSearch($handi);

            $images = array();

            $array_count = array();
            $entities = array();
            foreach ($events as $event) {

                    if (round(Self::get_distance_m($lat, $lng, $event->getLat(), $event->getLng()) / 1000, 3) <= $rayon) {
                        $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($event->getUser());
                        $listParticipeEvent = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeEvents($event);
                        $nbparticipe = count($listParticipeEvent);
                        array_push($array_count, $nbparticipe);
                        array_push($images, $image);
                        array_push($entities, $event);
                    }

            }

        }


        
        
        if($api == true){
            return array('entities' => $entities,'handi' => $handi, 'loca' => $locale, 'ville' => $ville, 'rayon' => $rayon, 'images' => $images, 'userInterest' => $userInterest, 'userInterests' => $userInterests,'interestSearch' => $interestSearch, 'nbparticipe' => $array_count);
        }else{

            $villes = $em->getRepository('ComparatorEventBundle:Ville')->findAll();
            $seoPage = $this->container->get('sonata.seo.page');

            $seoPage
                ->setTitle("Grintaaa | Reseau des sports")
                ->addMeta('name', 'description', "Grintaaa est le meilleur reseau des sports et de rencontre sportive entre sportif et sportive en france")
                ->addMeta('name', 'keywords', "Reseau des sports")
                ->addMeta('property', 'og:title', "Grintaaa | Reseau des sports")
                ->addMeta('property', 'og:type', 'site')
                ->addMeta('property', 'og:description', 'Grintaaa | Reseau des sports');
            return $this->render(
                'ComparatorEventBundle:Event:search.html.twig',
                array('entities' => $entities,'handi' => $handi,'villes' => $villes, 'loca' => $locale, 'ville' => $ville, 'rayon' => $rayon, 'images' => $images, 'userInterest' => $userInterest, 'userInterests' => $userInterests,'interestSearch' => $interestSearch, 'nbparticipe' => $array_count)
            );
        }
    }

    /*
     * calcul distance
     */
    public static function get_distance_m($lat1, $lng1, $lat2, $lng2)
    {
        $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
        $rlo1 = deg2rad($lng1);
        $rla1 = deg2rad($lat1);
        $rlo2 = deg2rad($lng2);
        $rla2 = deg2rad($lat2);
        $dlo = ($rlo2 - $rlo1) / 2;
        $dla = ($rla2 - $rla1) / 2;
        $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin($dlo
                ));
        $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
        return ($earth_radius * $d);
    }

    public function listEventFriendsAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $friends = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($user);


        $friendss = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($user);


        $allFriends = array();
        foreach ($friends as $friend) {

            array_push($allFriends, $friend->getUser1()->getEmail());

        }


        foreach ($friendss as $friend) {

            array_push($allFriends, $friend->getUser2()->getEmail());

        }

        array_push($allFriends, $user->getEmail());
        $events = $em->getRepository('ComparatorEventBundle:Event')->listEvent();
        $entities = array();


        $array_count = array();
        $images = array();

        foreach ($events as $event) {

            if (in_array($event->getUser()->getEmail(), $allFriends)) {
                $listParticipeEvent = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeConfirmEvent($event);

                $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($event->getUser());
               
                $nbparticipe = count($listParticipeEvent);
                array_push($array_count, $nbparticipe);
                array_push($images, $image);
                array_push($entities, $event);
            }


        }

        $seoPage
        ->setTitle("Grintaaa | Reseau des sports")
        ->addMeta('name', 'description', "Grintaaa est le meilleur reseau des sports et de rencontre sportive entre sportif et sportive en france")
	    ->addMeta('name', 'keywords', "Reseau des sports")
        ->addMeta('property', 'og:title', "Grintaaa | Reseau des sports")
        ->addMeta('property', 'og:type', 'site')
        ->addMeta('property', 'og:description', 'Grintaaa | Reseau des sports');
        return $this->render('ComparatorEventBundle:Event:listEventFriends.html.twig', array('entities' => $entities, 'nbparticipe' => $array_count, 'images' => $images));

    }






    public function listEventFriendsHomeAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();




        $entities = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->findBy(array('user' => $user,'enabled'=> "3"));



        return $this->render('ComparatorEventBundle:Event:listEventFriendsHome.html.twig', array('entities' => $entities));

    }

    public function listEventByUserAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();


        $events = $em->getRepository('ComparatorEventBundle:Event')->listEventUser($user);
        $entities = array();


        $array_count = array();
        $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($user);
        foreach ($events as $event) {
            $listParticipeEvent = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeConfirmEvent($event);
            $nbparticipe = count($listParticipeEvent);
            array_push($array_count, $nbparticipe);
            array_push($entities, $event);

        }


        return $this->render('ComparatorEventBundle:Event:listEventByUser.html.twig', array('entities' => $entities, 'nbparticipe' => $array_count, 'image' => $image));

    }

    public function countEventByUserAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();


        $events = $em->getRepository('ComparatorEventBundle:Event')->listEventUser($user);

        $nbEvent = count($events);

        return $this->render('ComparatorEventBundle:Event:countEventByUser.html.twig', array('nb' => $nbEvent,'user' => $user ));

    }



    private function createCreateFileForm(File $entity)
    {

        $form = $this->createForm(new FileType(), $entity, array(
            'action' => $this->generateUrl('event_file_create'),
            'method' => 'POST',
        ));


        return $form;

    }

    /**
     * Creates a new File entity.
     *
     */
    public function createFileAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new File();
        $form = $this->createCreateFileForm($entity);
        $form->handleRequest($request);
        $event = $em->getRepository('ComparatorEventBundle:Event')->find($id);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setUser($this->get('security.context')->getToken()->getUser());
            $entity->setEvent($event);
            $entity->setType("image");
            $em->persist($entity);
            $em->flush();

            $mur = new Mur();
            $mur->setEvent($event);
            $mur->setUser1($this->get('security.context')->getToken()->getUser());
            $mur->setUser2($event->getUser());
            $mur->setFile($entity);
            $mur->setType('imageevent');
            $em->persist($mur);
            $em->flush();

            return $this->redirect($this->generateUrl('event_show', array('slug' => $event->getSlug())));
        }



    }


    public function addStatutAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('ComparatorEventBundle:Event')->find($id);
        $text = $this->get('request')->request->get('textarea');

        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setEvent($event);
        $entity->setType("statut");
        $entity->setDescreption($text);
        $em->persist($entity);
        $em->flush();

        $mur = new Mur();
        $mur->setEvent($event);
        $mur->setUser1($this->get('security.context')->getToken()->getUser());
        $mur->setUser2($event->getUser());
        $mur->setFile($entity);
        $mur->setType('statutevent');
        $em->persist($mur);
        $em->flush();

        return $this->redirect($this->generateUrl('event_show', array('slug' => $event->getSlug())));


    }

    public function addVideoAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('ComparatorEventBundle:Event')->find($id);
        $video = $this->get('request')->request->get('video');

        $youtube_id = $this->getYouTubeIdFromURL($video);

        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setEvent($event);
        $entity->setType("video");
        $entity->setUrl($youtube_id);
        $em->persist($entity);
        $em->flush();

        $mur = new Mur();
        $mur->setEvent($event);
        $mur->setUser1($this->get('security.context')->getToken()->getUser());
        $mur->setUser2($event->getUser());
        $mur->setFile($entity);
        $mur->setType('videoevent');
        $em->persist($mur);
        $em->flush();

        return $this->redirect($this->generateUrl('event_show', array('slug' => $event->getSlug())));


    }

    /**
     * Finds and displays a Event entity.
     *
     */
    public function sendEventAction(Request $request,$slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Event')->findOneBy(array('slug' => $slug));
        $user = $this->get('security.context')->getToken()->getUser();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $url = $request->getUri();
        $lien=str_replace("send","show",$url);

        $nom = $request->request->get("nom");

        foreach( $nom as $key=>$value){
            $entities = $em->getRepository('ApplicationSonataUserBundle:User')->SendFriends($value);

            if($entities)
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Grintaaa')
                    ->setFrom($user->getEmail())
                    ->setTo($entities->getEmail())
                    ->setBody(
                        $this->renderView(
                            'ComparatorEventBundle:Emails:invitation.txt.twig',
                            array('user' => $user,'lien' => $lien,'entity' => $entity)

                        ),
                        'text/html'
                    )
                ;
                $this->get('mailer')->send($message);


            }


        }




        return $this->redirect($this->generateUrl('event_show', array('slug' => $entity->getSlug())));


    }

    /**
     * Finds and displays a Event entity.
     *
     */
    public function sendEventMailAction(Request $request,$slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Event')->findOneBy(array('slug' => $slug));
        $user = $this->get('security.context')->getToken()->getUser();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $url = $request->getUri();
        $lien=str_replace("sendMail","show",$url);

        $mails = $request->request->get("mail");

        foreach( $mails as $key=>$value){

                $message = \Swift_Message::newInstance()
                    ->setSubject('Grintaaa')
                    ->setFrom($user->getEmail())
                    ->setTo($value)
                    ->setBody(
                        $this->renderView(
                            'ComparatorEventBundle:Emails:invitationMail.txt.twig',
                            array('user' => $user,'lien' => $lien,'entity' => $entity)

                        ),
                        'text/html'
                    )
                ;
                $this->get('mailer')->send($message);





        }




        return $this->redirect($this->generateUrl('event_show', array('slug' => $entity->getSlug())));


    }

    public function sendMailFriend(Request $request, $data)
    {
        $user = $this->container->get('security.context')->getToken()->getUser()->getEmail();
        $url = $request->getUri();
        $message = \Swift_Message::newInstance()
            ->setSubject($data["subject"])
            ->setFrom($user)
            ->setTo($data["email"])
            ->setBody($url.$data["message"]);
        $this->get('mailer')->send($message);
    }



    function difheure($heuredeb, $heurefin)
    {
        $hd = explode(":", $heuredeb);
        $hf = explode(":", $heurefin);
        $ts_hd = mktime($hd[0], $hd[1], $hd[2]);
        $ts_hf = mktime($hf[0], $hf[1], $hf[2]);
        $diff = $ts_hf - $ts_hd;
        $float = $diff / 3600;
        return $float;
    }

    function getYouTubeIdFromURL($url)
    {
        $url_string = parse_url($url, PHP_URL_QUERY);
        parse_str($url_string, $args);
        return isset($args['v']) ? $args['v'] : false;
    }

}
