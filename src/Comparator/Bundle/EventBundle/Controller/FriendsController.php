<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\Friends;
use Comparator\Bundle\EventBundle\Form\FriendsType;

/**
 * Friends controller.
 *
 */
class FriendsController extends Controller
{

    public function addFriendAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $enabled = false;

        $user = $this->get('security.context')->getToken()->getUser();
        $user2 = $em->getRepository('ApplicationSonataUserBundle:User')->find($id);

        $friend = new Friends();
        $friend->setUser1($user);
        $friend->setEnabled($enabled);
        $friend->setUser2($user2);
        $em->persist($friend);
        $em->flush();
        return $this->redirect($this->generateUrl('grintaaa__list_friend'));

    }


    /**
     * Lists all Friends entities.
     *
     */
    public function indexAction($api = false)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $images = array();
        $interest = array();
        $coaching = array();
        $entities = $em->getRepository('ComparatorEventBundle:Friends')->listInvitation($user);
        foreach ($entities as $entity) {
            $friendsInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $entity->getUser1()));
            $friendsCoaching = $this->getDoctrine()->getRepository('ComparatorEventBundle:Coaching')->findBy(array('user' => $entity->getUser1()));


            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($entity->getUser1());
            array_push($images, $image);
            array_push($interest, $friendsInterest);
            array_push($coaching, $friendsCoaching);


        }
        
        if($api){
            return array(
                'entities' => $entities,
                'images' => $images,
                'interest' => $interest,
                'coaching' => $coaching,
            );
        }
        
         $seoPage = $this->container->get('sonata.seo.page');

        $seoPage
           ->setTitle("Grintaaa | defi sportif entre amis")
           ->addMeta('name', 'description', "rien de mieux qu un defi sportif entre amis. Grintaaa vous aide à organiser une journée sportive et réaliser un defi sportif entre amis")
           ->addMeta('property', 'og:title', "Grintaaa | defi sportif entre amis")
           ->addMeta('property', 'og:type', 'site')
           ->addMeta('property', 'og:description', 'rien de mieux qu un defi sportif entre amis. Grintaaa vous aide à organiser une journée sportive et réaliser un defi sportif entre amis');
		   
        return $this->render('ComparatorEventBundle:Friends:index.html.twig', array(
            'entities' => $entities,
            'images' => $images,
            'interest' => $interest,
            'coaching' => $coaching,
        ));

    }

    /**
     * Lists all Friends entities.
     *
     */
    public function countIndexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('ComparatorEventBundle:Friends')->listInvitation($user);

        $invitation = count($entities);
        return $this->render('ComparatorEventBundle:Friends:countIndex.html.twig', array(
            'invitation' => $invitation,
        ));

    }

    /**
     * Creates a new Friends entity.
     *
     */
    public function createAction(Request $request)
    {

        $entity = new Friends();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa_friend_show', array('id' => $entity->getId())));
        }

        return $this->render('ComparatorEventBundle:Friends:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Creates a form to create a Friends entity.
     *
     * @param Friends $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Friends $entity)
    {

        $form = $this->createForm(new FriendsType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_friend_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;

    }

    /**
     * Displays a form to create a new Friends entity.
     *
     */
    public function newAction()
    {

        $entity = new Friends();
        $form = $this->createCreateForm($entity);

        return $this->render('ComparatorEventBundle:Friends:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Finds and displays a Friends entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Friends')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Friends entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Friends:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Displays a form to edit an existing Friends entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Friends')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Friends entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Friends:notif.txt.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Creates a form to edit a Friends entity.
     *
     * @param Friends $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Friends $entity)
    {

        $form = $this->createForm(new FriendsType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_friend_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;

    }

    /**
     * Edits an existing Friends entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Friends')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Friends entity.');
        }
        $form = $this->createForm(new FriendsType(), $entity);
        //$deleteForm = $this->createDeleteForm($id);
        //$editForm = $this->createEditForm($entity);
        // $editForm->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            // $form->bindRequest($request);

            $entity->setEnabled(true);


            // perform some action, such as save the object to the database
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa__list_friend_search'));

        }

        return $this->redirect($this->generateUrl('grintaaa__list_friend_search'));

    }

    /**
     * Deletes a Friends entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        //$form = $this->createDeleteForm($id);
        //$form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorEventBundle:Friends')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Friends entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grintaaa__list_friend'));

    }

    /**
     * Creates a form to delete a Friends entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grintaaa_friend_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }


    /**
     * Lists all Friends entities.
     *
     */
    public function listFriendsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($user);

        $entitiess = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($user);


        $allFreinds = array();
        $images = array();
        foreach ($entities as $entity) {
            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($entity->getUser1());

            array_push($images, $image);
            array_push($allFreinds, $entity);
        }


        foreach ($entitiess as $entitys) {
            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($entitys->getUser2());

            array_push($images, $image);
            array_push($allFreinds, $entitys);
        }


        return $this->render('ComparatorEventBundle:Friends:listFriends.html.twig', array(
            'allFreinds' => $allFreinds,
            'images' => $images,
        ));

    }

    /**
     * Lists all Friends entities.
     *
     */
    public function listFriendAction($api = false)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($user);


        $entitiess = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($user);

        $allFreinds = array();

        foreach ($entities as $entity) {

            $test = $em->getRepository('ApplicationSonataUserBundle:User')->find($entity->getUser1()->getId());
            array_push($allFreinds, $test);
        }


        $images = array();
        foreach ($entities as $friends) {


            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($friends->getUser1());

            array_push($images, $image);


        }


        foreach ($entitiess as $entitys) {
            $test1 = $em->getRepository('ApplicationSonataUserBundle:User')->find($entitys->getUser2()->getId());
            array_push($allFreinds, $test1);
        }
        foreach ($entitiess as $friendss) {


            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($friendss->getUser2());

            array_push($images, $image);


        }

        $interest = array();
        $coaching = array();


        foreach ($allFreinds as $entity) {


            $resultfriend = $em->getRepository('ApplicationSonataUserBundle:User')->find($entity->getId());

            $friendsInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $resultfriend));
            $friendsCoaching = $this->getDoctrine()->getRepository('ComparatorEventBundle:Coaching')->findBy(array('user' => $resultfriend));
            array_push($interest, $friendsInterest);
            array_push($coaching, $friendsCoaching);



        }
        if($api){
            return array(
                'allFreinds' => $allFreinds,
                'images' => $images,
                'interest' => $interest,
                'coaching' => $coaching,
            );
        }
        
		$seoPage = $this->container->get('sonata.seo.page');

$seoPage
    ->setTitle("Grintaaa | site de rencontre football")
    ->addMeta('name', 'description', "Si vous souhaitez créer un match, organiser une compétition ou encore un rencontre football. Inscrivez vous gratuitement à notre site de rencotre football...")
    ->addMeta('property', 'og:title', "Grintaaa | site de rencontre football")
    ->addMeta('property', 'og:type', 'site')
    ->addMeta('property', 'og:description', 'Grintaaa | site de rencontre football');

        return $this->render('ComparatorEventBundle:Friends:listFriend.html.twig', array(
            'allFreinds' => $allFreinds,
            'images' => $images,
            'interest' => $interest,
            'coaching' => $coaching,
        ));

    }


    public function countListFriendAction()
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($user);
        $entitiess = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($user);

        $allFreinds = array();

        foreach ($entities as $entity) {
            array_push($allFreinds, $entity);
        }


        foreach ($entitiess as $entitys) {
            array_push($allFreinds, $entitys);
        }

        $nbrFriends = count($allFreinds);
		$seoPage = $this->container->get('sonata.seo.page');

$seoPage
    ->setTitle("Grintaaa | site de rencontre football")
    ->addMeta('name', 'description', "Si vous souhaitez créer un match, organiser une compétition ou encore un rencontre football. Inscrivez vous gratuitement à notre site de rencotre football...")
    ->addMeta('property', 'og:title', "Grintaaa | site de rencontre football")
    ->addMeta('property', 'og:type', 'site')
    ->addMeta('property', 'og:description', 'Grintaaa | site de rencontre football');

        return $this->render('ComparatorEventBundle:Friends:countListFriend.html.twig', array(
            'nbrFriends' => $nbrFriends,
        ));

    }

    /**
     * Lists all Friends entities.
     *
     */
    public function searchFriendAction()
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $mot = $request->request->get("friends");

        $friends = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($user);
        $friendss = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($user);
        $disciplines = $this->getDoctrine()->getRepository('ComparatorEventBundle:Category')->findAll();


        $allFreinds = array();

        foreach ($friends as $entity) {

                array_push($allFreinds, $entity);

        }

        foreach ($friendss as $entitys) {
            array_push($allFreinds, $entitys);
        }


        $friendsinviattente = $em->getRepository('ComparatorEventBundle:Friends')->listFriendsAttente($user);


        $friendssattente = $em->getRepository('ComparatorEventBundle:Friends')->listFriendssAttente($user);


        $idAll = array();
        foreach ($friendsinviattente as $entity) {
            array_push($idAll, $entity->getUser1()->getId());
        }


        foreach ($friendssattente as $entity) {
            array_push($idAll, $entity->getUser2()->getId());
        }


        foreach ($allFreinds as $entity) {
            array_push($idAll, $entity->getUser1()->getId());
        }

        foreach ($allFreinds as $entity) {
            array_push($idAll, $entity->getUser2()->getId());
        }


        $entitiess = $em->getRepository('ApplicationSonataUserBundle:User')->SearchFriends($mot);


        $entities = array();
        foreach($entitiess as $entitiesy)
        {
            if($entitiesy->getParam()==1)
            {
                array_push($entities, $entitiesy);
            }else{
                if($user->getCivility()==$entitiesy->getCivility())
                {
                    array_push($entities, $entitiesy);
                }
            }
        }
        $images = array();
        $interest = array();
        foreach ($entities as $entity) {


            $resultfriend = $em->getRepository('ApplicationSonataUserBundle:User')->find($entity->getId());

            $friendsInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $resultfriend));
            array_push($interest, $friendsInterest);
            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($resultfriend);

            array_push($images, $image);


        }
        return $this->render('ComparatorEventBundle:Friends:searchFriend.html.twig', array(
            'entities' => $entities,
            'images' => $images,
            'interest' => $interest,
            'allFreinds' => $allFreinds,
            'friendssattente' => $friendssattente,
            'friendsinviattente' => $friendsinviattente,
            'idAlls' => $idAll,
            'disciplines' => $disciplines,
            'mot' => $mot,
        ));

    }
    public function searchFriendsAction()
    {




        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $pays = $request->request->get("loca");
        $interestSearch = $request->request->get("interest");
        $coach = $request->request->get("coach");
        $handi = $request->request->get("handi");

        $size_min = $request->request->get("sizemin");
        $size_max = $request->request->get("sizemax");
        $weight_min = $request->request->get("weightmin");
        $weight_max = $request->request->get("weightmax");

        $age_min = $request->request->get("agemin");
        $age_max = $request->request->get("agemax");

        if($size_min=='')
        {
            $size_min=0;
        }

        if($size_max=='')
        {
            $size_max=1000;
        }

        if($age_min=='')
        {
            $age_min=0;
        }

        if($age_max=='')
        {
            $age_max=100;
        }


        if($weight_min=='')
        {
            $weight_min=0;
        }

        if($weight_max=='')
        {
            $weight_max=1000;
        }

        if($coach=="on")
        {
            $coach =true;
        }
        else{
            $coach =false;
        }

        if($handi=="on")
        {
            $handi =true;
        }
        else{
            $handi =false;
        }
       if($interestSearch!='')
       {
           $category = $em->getRepository('ComparatorEventBundle:Category')->find($interestSearch);
       }
       else{
           $category = $interestSearch;
       }

        if($pays!='')
        {
            $resultSearch = $em->getRepository('ApplicationSonataUserBundle:User')->SearchFriendPays($coach,$pays,$handi);
        }
        else
        {
            $resultSearch = $em->getRepository('ApplicationSonataUserBundle:User')->SearchFriend($coach,$handi);
        }


        $entities = array();
        foreach ($resultSearch as $result) {

            $d1 = $result->getDateOfBirth();
            $d2 = new \DateTime();
            $diff = $d1->diff($d2)->y;

            if(($age_min<$diff)&& ($diff< $age_max)) {

            if ($result->getParam() == 1) {
                if (($result->getWeight() > $weight_min) && ($result->getWeight() < $weight_max) && ($result->getSize() > $size_min) && ($result->getSize() < $size_max)) {

                    if ($category == "") {
                        array_push($entities, $result);
                    } else {
                        $interestserch = $em->getRepository('ComparatorEventBundle:Interest')->interestByUserAndCategory($result, $category);
                        if ($interestserch != null) {
                            array_push($entities, $result);
                        }


                    }

                }
            } else {

                if ($user->getCivility() == $result->getCivility()) {
                    if (($result->getWeight() > $weight_min) && ($result->getWeight() < $weight_max) && ($result->getSize() > $size_min) && ($result->getSize() < $size_max)) {

                        if ($category == "") {
                            array_push($entities, $result);
                        } else {
                            $interestserch = $em->getRepository('ComparatorEventBundle:Interest')->interestByUserAndCategory($result, $category);
                            if ($interestserch != null) {
                                array_push($entities, $result);
                            }


                        }
                    }
                }


            }
        }
        }


        $friends = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($user);
        $friendss = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($user);
        $disciplines = $this->getDoctrine()->getRepository('ComparatorEventBundle:Category')->findAll();


        $allFreinds = array();

        foreach ($friends as $entity) {

                array_push($allFreinds, $entity);

        }

        foreach ($friendss as $entitys) {

                array_push($allFreinds, $entitys);


        }


        $friendsinviattente = $em->getRepository('ComparatorEventBundle:Friends')->listFriendsAttente($user);


        $friendssattente = $em->getRepository('ComparatorEventBundle:Friends')->listFriendssAttente($user);


        $idAll = array();
        foreach ($friendsinviattente as $entity) {
            array_push($idAll, $entity->getUser1()->getId());
        }


        foreach ($friendssattente as $entity) {
            array_push($idAll, $entity->getUser2()->getId());
        }


        foreach ($allFreinds as $entity) {
            array_push($idAll, $entity->getUser1()->getId());
        }

        foreach ($allFreinds as $entity) {
            array_push($idAll, $entity->getUser2()->getId());
        }


        $images = array();
        $interest = array();
        foreach ($entities as $entity) {


            $resultfriend = $em->getRepository('ApplicationSonataUserBundle:User')->find($entity->getId());

            $friendsInterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $resultfriend));
            array_push($interest, $friendsInterest);
            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($resultfriend);

            array_push($images, $image);


        }
        $agemax = $request->request->get("agemax");
        $agemin = $request->request->get("agemin");
        $sizemin = $request->request->get("sizemin");
        $sizemax = $request->request->get("sizemax");
        $weightmin = $request->request->get("weightmin");
        $weightmax = $request->request->get("weightmax");

        return $this->render('ComparatorEventBundle:Friends:searchFriends.html.twig', array(
            'entities' => $entities,
            'images' => $images,
            'interest' => $interest,
            'allFreinds' => $allFreinds,
            'friendssattente' => $friendssattente,
            'friendsinviattente' => $friendsinviattente,
            'idAlls' => $idAll,
            'disciplines' => $disciplines,
            'interestSearch' => $interestSearch,
            'coach' => $coach,
            'handi' => $handi,
            'loca' => $pays,
            'sizemin' => $sizemin,
            'sizemax' => $sizemax,
            'weightmin' => $weightmin,
            'weightmax' => $weightmax,
            'agemax' => $agemax,
            'agemin' => $agemin,
        ));

    }

}
