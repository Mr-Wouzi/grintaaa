<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\Interest;
use Comparator\Bundle\EventBundle\Form\InterestType;

/**
 * Interest controller.
 *
 */
class InterestController extends Controller
{

    /**
     * Lists all Interest entities.
     *
     */
    public function indexAction(Request $request , $api = false)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ComparatorEventBundle:Category')->findAll();
        $disciplines = $em->getRepository('ComparatorEventBundle:Category')->findLevel1();
        $user = $this->get('security.context')->getToken()->getUser();

        $oldinterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $user));


        $interestUser = array();
        foreach ($oldinterest as $interest) {
            array_push($interestUser, ($interest->getCategory()->getId()));
        }

        if($api)
            return array(
                'disciplines' => $disciplines,
                'interestUser' => $interestUser,
            );
        
        return $this->render('ComparatorEventBundle:Interest:index.html.twig', array(
            'entities' => $entities,
            'disciplines' => $disciplines,
            'interestUser' => $interestUser,
        ));

    }


    /**
     * Finds and displays a Interest entity.
     *
     */
    public function showAction(Request $request , $api = false)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ComparatorEventBundle:Category')->findAll();
        $user = $this->get('security.context')->getToken()->getUser();

        $oldinterest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $user));


        $interestUser = array();
        foreach ($oldinterest as $interest) {
            array_push($interestUser, $interest->getCategory()->getId());
        }

        if ($request->getMethod() == 'POST' or $api) {



            foreach ($oldinterest as $key => $value) {

                $entity = $em->getRepository('ComparatorEventBundle:Interest')->find($value);
                $em->remove($entity);
                $em->flush();
            }

            //$interested = $request->request->get("interet");

            $diciplines = $request->request->get("discipline");
            if(count($diciplines)>0)
            {
                foreach ($diciplines as $key => $value) {
                    $interest = new Interest();

                    $interest->setUser($user);
                    $category = $this->getDoctrine()->getRepository('ComparatorEventBundle:Category')->find($value);
                    $interest->setCategory($category);
                    $em->persist($interest);
                    $em->flush();
                }
            }

            if($api)
                return array(
                    'result' => "success",
                );
            return $this->redirect($this->generateUrl('fos_user_profile_show'));
        }
        return $this->render('ComparatorBaseBundle:Base:home.html.twig');

    }

    /**
     * Creates a new Interest entity.
     *
     */
    public function createAction(Request $request)
    {

        $entity = new Interest();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('home'));
        }

        return $this->render('ComparatorEventBundle:Interest:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Creates a form to create a Interest entity.
     *
     * @param Interest $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Interest $entity)
    {

        $form = $this->createForm(new InterestType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_interest_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;

    }

    /**
     * Displays a form to create a new Interest entity.
     *
     */
    public function newAction()
    {

        $entity = new Interest();
        $form = $this->createCreateForm($entity);

        return $this->render('ComparatorEventBundle:Interest:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }


    /**
     * Displays a form to edit an existing Interest entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Interest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Interest entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Interest:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Creates a form to edit a Interest entity.
     *
     * @param Interest $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Interest $entity)
    {

        $form = $this->createForm(new InterestType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_interest_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;

    }

    /**
     * Edits an existing Interest entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Interest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Interest entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa_interest_edit', array('id' => $id)));
        }

        return $this->render('ComparatorEventBundle:Interest:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a Interest entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorEventBundle:Interest')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Interest entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grintaaa_interest'));


    }

    /**
     * Creates a form to delete a Interest entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grintaaa_interest_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();


    }
}
