<?php



namespace Comparator\Bundle\EventBundle\Controller;


use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Comparator\Bundle\EventBundle\Entity\Liked;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


/**
 * Liked controller.
 *

 */
class LikedController extends Controller

{


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function addLikedAction($id)

    {

        $em = $this->getDoctrine()->getManager();


        $events = $em->getRepository('ComparatorEventBundle:Event')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();

        $likes = new Liked();

        $likes->setEvent($events);

        $likes->setUser($user);

        $em->persist($likes);
        $em->flush();





        return $this->redirect($this->generateUrl('event_show', array('slug' => $events->getSlug())));


    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function delLikedAction($id,Request $request)

    {



        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository('ComparatorEventBundle:Event')->find($id);
        $user = $this->get('security.context')->getToken()->getUser();
        $entity = $em->getRepository('ComparatorEventBundle:Liked')->findOneBy(array('event' => $events, 'user' => $user));



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $em->remove($entity);
        $em->flush();





        return $this->redirect($this->generateUrl('home'));


    }


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function testLikedAction($id)

    {

        $em = $this->getDoctrine()->getManager();


        $events = $em->getRepository('ComparatorEventBundle:Event')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();

        $likesuser = $em->getRepository('ComparatorEventBundle:Liked')->findBy(array('event' => $events,'user' => $user));

        return new Response(count($likesuser));





    }

    /**
     * Lists all Liked entities.
     *

     */

    public function indexAction($id)

    {

        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('ComparatorEventBundle:Event')->find($id);

        $entities = $em->getRepository('ComparatorEventBundle:Liked')->findBy(array('event' => $events));


        return new Response(count($entities));

    }

}

