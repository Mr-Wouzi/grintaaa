<?php



namespace Comparator\Bundle\EventBundle\Controller;


use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Comparator\Bundle\EventBundle\Entity\Likem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


/**
 * Likem controller.
 *

 */
class LikemController extends Controller

{


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function addLikemAction($id,$api = false)

    {

        $em = $this->getDoctrine()->getManager();


        $mur = $em->getRepository('ComparatorEventBundle:Mur')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();

        $likes = new Likem();

        $likes->setMur($mur);

        $likes->setUser($user);

        $em->persist($likes);
        $em->flush();


        if($api)
            return array(
                'like' => "1"
            );


        return $this->redirect($this->generateUrl('home'));


    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function testLikemAction($id,$api =false)

    {

        $em = $this->getDoctrine()->getManager();


        $mur = $em->getRepository('ComparatorEventBundle:Mur')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();

        $likesuser = $em->getRepository('ComparatorEventBundle:Likem')->findBy(array('mur' => $mur,'user' => $user));

        
        if($api)
            return array(
                'like' => count($likesuser)
            );
        
        
        return new Response(count($likesuser));
        

    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function delLikemAction($id,Request $request,$api =false)

    {


        $em = $this->getDoctrine()->getManager();
        $mur = $em->getRepository('ComparatorEventBundle:Mur')->find($id);
        $user = $this->get('security.context')->getToken()->getUser();
        $entity = $em->getRepository('ComparatorEventBundle:Likem')->findOneBy(array('mur' => $mur, 'user' => $user));



        if (!$entity) {
                throw $this->createNotFoundException('Unable to find Event entity.');
            }

            $em->remove($entity);
            $em->flush();


        if($api)
            return array(
                'like' => '0'
            );


        return $this->redirect($this->generateUrl('home'));


    }

    /**
     * Lists all Likem entities.
     *

     */

    public function indexAction($id,$api =false)

    {
        if($_SERVER['REMOTE_ADDR'] != "192.168.1.200"){
            header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }

        $em = $this->getDoctrine()->getManager();

        $mur = $em->getRepository('ComparatorEventBundle:Mur')->find($id);

        $entities = $em->getRepository('ComparatorEventBundle:Likem')->findBy(array('mur' => $mur));

        if($api)
            return array(
                'like' => count($entities)
            );
        return new Response(count($entities));

    }

}

