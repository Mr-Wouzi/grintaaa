<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\Messages;
use Comparator\Bundle\EventBundle\Form\MessagesType;
use Comparator\Bundle\BaseBundle\Common\Tools;


/**
 * Messages controller.
 *
 */
class MessagesController extends Controller
{


    /*
     *
     */

    public function sendMessageAction(Request $request, $userid)
    {

        $em = $this->getDoctrine()->getManager();
        $text = $this->get('request')->request->get('textarea');
        $friend = $em->getRepository('ApplicationSonataUserBundle:User')->find($userid);
        $user = $this->get('security.context')->getToken()->getUser();
        $messages = new Messages();
        $messages->setUser1($user);
        $messages->setUser2($friend);
        $messages->setDescription($text);
        $messages->setEnabled(false);
        $em->persist($messages);
        $em->flush();

        return $this->redirect($this->generateUrl('friend_message', array('userid' => $userid)));

    }

    /*
     *
     */
    public function receiveMessageAction()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->redirect($this->generateUrl('home'));

    }

    /*
     *
     */
    public function listMessageAction($userid)
    {
        $em = $this->getDoctrine()->getManager();


        $user = $this->get('security.context')->getToken()->getUser();
        $friend = $em->getRepository('ApplicationSonataUserBundle:User')->find($userid);
        $imagesUser = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($user);
        $imagesFriend = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($friend);

        $messages = $em->getRepository('ComparatorEventBundle:Messages')->listMessagesByUserAndFriend($user, $friend);
        $msgs = $em->getRepository('ComparatorEventBundle:Messages')->listMessagesByUserAndFriend($friend, $user);


        $allMessages = array();

        foreach ($messages as $message) {
            array_push($allMessages, $message);
        }

        foreach ($msgs as $msg) {
            if (!in_array($msg, $allMessages)) {
                array_push($allMessages, $msg);
            }


        }

        $allMessages = Tools::array_sort($allMessages, "createdAt", SORT_DESC);

        foreach ($messages as $msgf) {

            $msgf->setEnabled(true);


            $em->flush();
        }
        return $this->render(
            'ComparatorEventBundle:Messages:message.html.twig',
            array(
                'messages' => $allMessages,
                'friend' => $friend,
                'imagesUser' => $imagesUser,
                'imagesFriend' => $imagesFriend,
            )
        );


    }


    /**
     * Lists all Messages entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('ComparatorEventBundle:Messages')->listMessage($user);
        $imagesUser = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($user);
        $messages = array();
        $users = array();
        $imagesFriend = array();

        foreach ($entities as $entity) {


            if (in_array(([$entity->getUser1()->getId(), $entity->getUser2()->getId()]), $users)) {
                if (in_array(([$entity->getUser2()->getId(), $entity->getUser1()->getId()]), $users)) {
                    $messages = $messages;
                }
            } else {
                array_push($messages, $entity);
                array_push($users, ([$entity->getUser1()->getId(), $entity->getUser2()->getId()]));
                array_push($users, ([$entity->getUser2()->getId(), $entity->getUser1()->getId()]));
                if ($user->getId() == $entity->getUser1()->getId()) {
                    $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($entity->getUser2());

                }

                if ($user->getId() == $entity->getUser2()->getId()) {
                    $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($entity->getUser1());

                }
                array_push($imagesFriend, $image);


            }


        }


        return $this->render('ComparatorEventBundle:Messages:index.html.twig', array(
            'messages' => $messages,
            'imagesUser' => $imagesUser,
            'imagesFriend' => $imagesFriend,
        ));

    }

    /**
     * Creates a new Messages entity.
     *
     */
    public function createAction(Request $request)
    {

        $entity = new Messages();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa_msg_show', array('id' => $entity->getId())));
        }

        return $this->render('ComparatorEventBundle:Messages:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Creates a form to create a Messages entity.
     *
     * @param Messages $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Messages $entity)
    {

        $form = $this->createForm(new MessagesType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_msg_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;


    }

    /**
     * Displays a form to create a new Messages entity.
     *
     */
    public function newAction()
    {

        $entity = new Messages();
        $form = $this->createCreateForm($entity);

        return $this->render('ComparatorEventBundle:Messages:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));


    }

    /**
     * Finds and displays a Messages entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Messages')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Messages entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Messages:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Displays a form to edit an existing Messages entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Messages')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Messages entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Messages:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));


    }

    /**
     * Creates a form to edit a Messages entity.
     *
     * @param Messages $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Messages $entity)
    {

        $form = $this->createForm(new MessagesType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_msg_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;

    }

    /**
     * Edits an existing Messages entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Messages')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Messages entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa_msg_edit', array('id' => $id)));
        }

        return $this->render('ComparatorEventBundle:Messages:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a Messages entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorEventBundle:Messages')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Messages entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grintaaa_msg'));

    }

    /**
     * Creates a form to delete a Messages entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grintaaa_msg_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }

    /*
     *
     */
    public function countMessageAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();


        $messages = $em->getRepository('ComparatorEventBundle:Messages')->countMessagesByUserAndFriend($user);

        $nbmessages = count($messages);


        return $this->render(
            'ComparatorEventBundle:Messages:countMessage.html.twig',
            array(
                'message' => $nbmessages,
            )
        );

    }

    /*
     *
     */
    public function countMessagesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();


        $messages = $em->getRepository('ComparatorEventBundle:Messages')->countMessagesByUserAndFriend($user);

        $nbmessages = count($messages);


        return $this->render(
            'ComparatorEventBundle:Messages:countMessages.html.twig',
            array(
                'message' => $nbmessages,
            )
        );

    }


}
