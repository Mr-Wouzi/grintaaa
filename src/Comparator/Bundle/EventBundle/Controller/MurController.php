<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\Event;
use Comparator\Bundle\EventBundle\Entity\Mur;
use Comparator\Bundle\EventBundle\Entity\OptionEvent;
use Comparator\Bundle\EventBundle\Form\EventType;
use Comparator\Bundle\EventBundle\Form\MurType;
use Comparator\Bundle\StatutBundle\Form\FileType;
use Comparator\Bundle\StatutBundle\Entity\File;
/**
 * Event controller.
 *
 */
class MurController extends Controller
{
    public function murEventFriendsAction(Request $request, $api = false)
    {


        $offset=$request->request->get("offset");


        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $friends = $em->getRepository('ComparatorEventBundle:Friends')->listFriends($user);


        $friendss = $em->getRepository('ComparatorEventBundle:Friends')->listFriendss($user);


        $allFriends = array();
        foreach ($friends as $friend) {

            array_push($allFriends, $friend->getUser1()->getEmail());

        }


        foreach ($friendss as $friend) {

            array_push($allFriends, $friend->getUser2()->getEmail());

        }

        array_push($allFriends, $user->getEmail());


        $userInterst =$em->getRepository('ComparatorEventBundle:Interest')->interestByUser($user);

        $arruserInterst =array();
        foreach ($userInterst as $arruserinter) {

            array_push($arruserInterst, $arruserinter->getCategory()->getTitle());
        }


        $murs = $em->getRepository('ComparatorEventBundle:Mur')->listMurs();

        $entities = array();


        $array_count = array();
        $images = array();

        foreach ($murs as $mur) {

            if($mur->getEvent())
            {
                if (in_array($mur->getUser1()->getEmail(), $allFriends)||in_array($mur->getUser2()->getEmail(), $allFriends)||(in_array($mur->getEvent()->getCategory(), $arruserInterst) && ($user->getCountry()==$mur->getEvent()->getCountry())))
                {


                    if($mur->getEvent())
                    {
                        $listParticipeEvent = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeConfirmEvent($mur->getEvent());
                        $nbparticipe = count($listParticipeEvent);
                    }else{

                        $nbparticipe= 0;
                    }

                    $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($mur->getUser1());



                    array_push($array_count, $nbparticipe);
                    array_push($images, $image);
                    array_push($entities, $mur);
                }



            }else{
                if (in_array($mur->getUser1()->getEmail(), $allFriends)||in_array($mur->getUser2()->getEmail(), $allFriends))
                {


                    if($mur->getEvent())
                    {
                        $listParticipeEvent = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->listParticipeConfirmEvent($mur->getEvent());
                        $nbparticipe = count($listParticipeEvent);
                    }else{

                        $nbparticipe= 0;
                    }

                    $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($mur->getUser1());

                    array_push($array_count, $nbparticipe);
                    array_push($images, $image);
                    array_push($entities, $mur);
                }


            }


        }
        $nbocc= count($entities);
        $nbpage= (int)($nbocc/10);

        if($offset==NULL)
        {
            $offset=1;
        }

        $derocc= $nbpage*10;
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25/*limit per page*/
        );
        $countentities= count($entities);
		if($countentities>0)
		{
        $length= $entities[$countentities-1]->getId();
        }else{
		
		$length =NULL;
		}
        
        if($api)
            return array(
                'entities' => $entities,
                'nbparticipe' => $array_count,
                'images' => $images,
                'offset' => $offset,
                'length' => $length,
                'nbpage' => $nbpage,
                'derocc' => $derocc
            );
        
        return $this->render('ComparatorEventBundle:Mur:murEventFriends.html.twig', array('entities' => $entities, 'nbparticipe' => $array_count, 'images' => $images, 'offset' => $offset, 'length' => $length, 'nbpage' => $nbpage, 'derocc' => $derocc));

    }

    /*
    *
    */
    public function countImagesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();


        $images = $em->getRepository('ComparatorEventBundle:Mur')->listImage($user);


        $nbimages = count($images);


        return $this->render(
            'ComparatorEventBundle:Mur:countImages.html.twig',
            array(
                'nbimages' => $nbimages,
            )
        );

    }

    /*
    *
    */
    public function countVideosAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();


        $videos = $em->getRepository('ComparatorEventBundle:Mur')->listVideo($user);

        $nbvideos = count($videos);


        return $this->render(
            'ComparatorEventBundle:Mur:countVideos.html.twig',
            array(
                'nbvideos' => $nbvideos,
            )
        );

    }

    /*
    *
    */
    public function listImagesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();


        $images = $em->getRepository('ComparatorEventBundle:Mur')->listImage($user);


        return $this->render(
            'ComparatorEventBundle:Mur:listImages.html.twig',
            array(
                'images' => $images,
            )
        );

    }

    /*
    *
    */
    public function listVideosAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();


        $videos = $em->getRepository('ComparatorEventBundle:Mur')->listVideo($user);



        return $this->render(
            'ComparatorEventBundle:Mur:listVideos.html.twig',
            array(
                'videos' => $videos,
            )
        );

    }
    /**
     * Deletes a StatutEvent entity.
     *
     */
    public function deleteAction($id)
    {

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorEventBundle:Mur')->find($id);
            $em->remove($entity);
            $em->flush();

        return $this->redirect($this->generateUrl('home'));
    }

}
