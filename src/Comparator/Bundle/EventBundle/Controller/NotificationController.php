<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\Notification;
use Comparator\Bundle\EventBundle\Form\NotificationType;

/**
 * Notification controller.
 *
 */
class NotificationController extends Controller
{

    /**
     * Lists all Notification entities.
     *
     */
    public function indexAction(Request $request, $api = false)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $entities = $em->getRepository('ComparatorEventBundle:Notification')->listNotificationByUsers($user);
        $imagesUsers = array();

        foreach ($entities as $entity) {


            $image = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($entity->getUser());

            array_push($imagesUsers, $image);


        }

        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            20/*limit per page*/
        );
        
        if($api)
            return array(
                'entities' => $entities,
                'imagesUsers' => $imagesUsers,

            );

        return $this->render('ComparatorEventBundle:Notification:index.html.twig', array(
            'entities' => $entities,
            'imagesUsers' => $imagesUsers,

        ));

    }

    /**
     * Creates a new Notification entity.
     *
     */
    public function createAction(Request $request)
    {

        $entity = new Notification();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa_notification_show', array('id' => $entity->getId())));
        }

        return $this->render('ComparatorEventBundle:Notification:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Creates a form to create a Notification entity.
     *
     * @param Notification $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Notification $entity)
    {

        $form = $this->createForm(new NotificationType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_notification_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;

    }

    /**
     * Displays a form to create a new Notification entity.
     *
     */
    public function newAction()
    {

        $entity = new Notification();
        $form = $this->createCreateForm($entity);

        return $this->render('ComparatorEventBundle:Notification:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Finds and displays a Notification entity.
     *
     */
    public function showAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Notification')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notification entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Notification:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Displays a form to edit an existing Notification entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Notification')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notification entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:Notification:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Creates a form to edit a Notification entity.
     *
     * @param Notification $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Notification $entity)
    {

        $form = $this->createForm(new NotificationType(), $entity, array(
            'action' => $this->generateUrl('grintaaa_notification_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;

    }

    /**
     * Edits an existing Notification entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:Notification')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Notification entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grintaaa_notification_edit', array('id' => $id)));
        }

        return $this->render('ComparatorEventBundle:Notification:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a Notification entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorEventBundle:Notification')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Notification entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grintaaa_notification'));

    }

    /**
     * Creates a form to delete a Notification entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grintaaa_notification_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }

    
    
    public function counNotificationAction($api = null, $userid = null)
    {
        $em = $this->getDoctrine()->getManager();
        
        if($userid){
            $dm = $this->getDoctrine();
            $repo = $dm->getRepository('ApplicationSonataUserBundle:User');
            $user = $repo->findOneById($userid);
        }else
            
         $user = $this->get('security.context')->getToken()->getUser();


        $notification = $em->getRepository('ComparatorEventBundle:Notification')->countNotificationByUser($user);

        $nbnotification = count($notification);

        if($api)
            return $nbnotification;

$seoPage = $this->container->get('sonata.seo.page');

$seoPage
    ->setTitle("Grintaaa | Reseau social sport")
	->addMeta('name', 'keywords', "Reseau social sport")
    ->addMeta('name', 'description', "Grintaaa est un reseau social pour tous les amateurs et passionnees de sport. organiser une journ&eacute;e sportive, planifier un evenement sportif, lancer un defi sportif entre amis...")
    ->addMeta('property', 'og:title', "Grintaaa | Reseau social sport")
    ->addMeta('property', 'og:type', 'site')
    ->addMeta('property', 'og:description', 'Grintaaa est un reseau social pour tous les amateurs et passionnees de sport. organiser une journée sportive, planifier un evenement sportif, lancer un defi sportif entre amis...');       
	   return $this->render(
            'ComparatorEventBundle:Notification:countNotification.html.twig',
            array(
                'notification' => $nbnotification,
            )
        );

    }


    public function counNotificationsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();


        $notification = $em->getRepository('ComparatorEventBundle:Notification')->countNotificationByUser($user);

        $nbnotification = count($notification);


        return $this->render(
            'ComparatorEventBundle:Notification:countNotifications.html.twig',
            array(
                'notification' => $nbnotification,
            )
        );

    }

    public function counNotificationssAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();


        $notification = $em->getRepository('ComparatorEventBundle:Notification')->countNotificationByUser($user);

        $nbnotification = count($notification);


        return $this->render(
            'ComparatorEventBundle:Notification:countNotificationss.html.twig',
            array(
                'notification' => $nbnotification,
            )
        );

    }

}
