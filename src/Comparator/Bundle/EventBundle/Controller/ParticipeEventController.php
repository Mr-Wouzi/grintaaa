<?php



namespace Comparator\Bundle\EventBundle\Controller;


use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Comparator\Bundle\EventBundle\Entity\Mur;
use Comparator\Bundle\EventBundle\Form\MurType;
use Comparator\Bundle\EventBundle\Form\ParticipeEventType;


use Comparator\Bundle\EventBundle\Entity\ParticipeEvent;

use Comparator\Bundle\EventBundle\Entity\Notification;
use Comparator\Bundle\EventBundle\Form\NotificationType;


/**
 * ParticipeEvent controller.
 *

 */
class ParticipeEventController extends Controller

{


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function addParticipeAction($enabled, $id, $api = null)

    {

        $em = $this->getDoctrine()->getManager();


        if ($enabled == "true") {

            $enabled = 1;

        } else {

            $enabled = 0;

        }

        $events = $em->getRepository('ComparatorEventBundle:Event')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();

        $favorites = new ParticipeEvent();

        $favorites->setEvent($events);

        $favorites->setEnabled($enabled);

        $favorites->setUser($user);

        $em->persist($favorites);
        $em->flush();
        $notification = new Notification();
        $notification->setEnabled(false);
        $notification->setEvent($events);
        $notification->setUser($user);
        $notification->setUser1($events->getUser());
        $notification->setTitle("Participe");

        $em->persist($notification);

        $em->flush();

        $config = $em->getRepository('ComparatorEventBundle:Configuration')->configByUser($events->getUser());


        if($config)
        {
            if($config->getParticipe())
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Grintaaa')
                    ->setFrom($user->getEmail())
                    ->setTo($events->getUser()->getEmail())
                    ->setBody(
                        $this->renderView(
                            'ComparatorEventBundle:Emails:notif.txt.twig',
                            array('user' => $user)

                        ),
                        'text/html'
                    )
                ;
                $this->get('mailer')->send($message);
            }
        }

        if($api)
            return array(
                'result' => "success",
            );
        else
            return $this->redirect($this->generateUrl('event_show', array('slug' => $events->getSlug())));


    }


    /**
     * Lists all ParticipeEvent entities.
     *

     */

    public function indexAction()

    {

        $em = $this->getDoctrine()->getManager();


        $entities = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->findAll();


        return $this->render('ComparatorEventBundle:ParticipeEvent:index.html.twig', array(

            'entities' => $entities,

        ));

    }

    /**
     * Creates a new ParticipeEvent entity.
     *

     */

    public function createAction(Request $request)

    {

        $entity = new ParticipeEvent();

        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);

            $em->flush();


            return $this->redirect($this->generateUrl('favoris_show', array('id' => $entity->getId())));

        }


        return $this->render('ComparatorEventBundle:ParticipeEvent:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));

    }


    /**
     * Creates a form to create a ParticipeEvent entity.
     *
     * @param ParticipeEvent $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createCreateForm(ParticipeEvent $entity)

    {

        $form = $this->createForm(new ParticipeEventType(), $entity, array(

            'action' => $this->generateUrl('favoris_create'),

            'method' => 'POST',

        ));


        $form->add('submit', 'submit', array('label' => 'Create'));


        return $form;

    }


    /**
     * Displays a form to create a new ParticipeEvent entity.
     *

     */

    public function newAction()

    {

        $entity = new ParticipeEvent();

        $form = $this->createCreateForm($entity);


        return $this->render('ComparatorEventBundle:ParticipeEvent:new.html.twig', array(

            'entity' => $entity,

            'form' => $form->createView(),

        ));


    }


    /**
     * Finds and displays a ParticipeEvent entity.
     *

     */

    public function showAction($id)

    {

        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find ParticipeEvent entity.');

        }


        $deleteForm = $this->createDeleteForm($id);


        return $this->render('ComparatorEventBundle:ParticipeEvent:show.html.twig', array(

            'entity' => $entity,

            'delete_form' => $deleteForm->createView(),

        ));


    }


    /**
     * Displays a form to edit an existing ParticipeEvent entity.
     *

     */

    public function editAction($id)

    {

        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find ParticipeEvent entity.');

        }


        $editForm = $this->createEditForm($entity);


        return $this->render('ComparatorEventBundle:ParticipeEvent:notif.txt.twig', array(

            'entity' => $entity,

            'edit_form' => $editForm->createView(),

        ));


    }


    /**
     * Creates a form to edit a ParticipeEvent entity.
     *
     * @param ParticipeEvent $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createEditForm(ParticipeEvent $entity)

    {

        $form = $this->createForm(new ParticipeEventType(), $entity, array(

            'action' => $this->generateUrl('favoris_update', array('id' => $entity->getId())),

            'method' => 'PUT',

        ));


        $form->add('submit', 'submit', array('label' => 'Rejoindre'));


        return $form;


    }

    /**
     * Edits an existing ParticipeEvent entity.
     *

     */

    public function updateAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find ParticipeEvent entity.');

        }

        $form = $this->createForm(new ParticipeEventType(), $entity);

        //  $editForm = $this->createEditForm($entity);

        //$editForm->handleRequest($request);


        if ($request->getMethod() == 'POST') {

            $entity->setEnabled(1);

            $em->flush();


            return $this->redirect($this->generateUrl('event_show', array('slug' => $entity->getEvent()->getSlug())));

        }


        return $this->redirect($this->generateUrl('event_show', array('slug' => $entity->getEvent()->getSlug())));

    }

    /**
     * Edits an existing ParticipeEvent entity.
     *

     */

    public function confirmAction(Request $request, $id)

    {

        $em = $this->getDoctrine()->getManager();
        $user= $this->get('security.context')->getToken()->getUser();

        $entity = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->find($id);


        if (!$entity) {

            throw $this->createNotFoundException('Unable to find ParticipeEvent entity.');

        }

        $form = $this->createForm(new ParticipeEventType(), $entity);

        //  $editForm = $this->createEditForm($entity);

        //$editForm->handleRequest($request);


        if ($request->getMethod() == 'POST') {

            $entity->setEnabled(3);

            $em->flush();


                $mur = new Mur();
                $mur->setEvent($entity->getEvent());
                $mur->setUser1($this->get('security.context')->getToken()->getUser());
                $mur->setUser2($entity->getEvent()->getUser());
                $mur->setParticipeevent($entity);
                $mur->setType('participeevent');
                $em->persist($mur);
                $em->flush();

            $notification = new Notification();
            $notification->setEnabled(false);
            $notification->setEvent($entity->getEvent());
            $notification->setUser($user);
            $notification->setUser1($entity->getUser());
            $notification->setTitle("Confirme");

            $em->persist($notification);

            $em->flush();

      $testparticipesConfirm = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->testParticipeConfirmEventUser($entity->getEvent(),$user);
	  
	  $nbtestparticipesConfirm = count($testparticipesConfirm);
      if($entity->getEvent()->getNbPlayer())
          {
             if($nbtestparticipesConfirm==$entity->getEvent()->getNbPlayer())
             {
             $notifications = new Notification();
            $notifications->setEnabled(false);
            $notifications->setEvent($entity->getEvent());
            $notifications->setUser($user);
            $notifications->setUser1($entity->getUser());
            $notifications->setTitle("Confirme Attend");

            $em->persist($notifications);

            $em->flush();
             }
          }

            $config = $em->getRepository('ComparatorEventBundle:Configuration')->configByUser($entity->getUser());

            if($config)
            {
                if($config->getConfirmer())
                {
                    $message = \Swift_Message::newInstance()
                        ->setSubject('Grintaaa')
                        ->setFrom($user->getEmail())
                        ->setTo($entity->getUser()->getEmail())
                        ->setBody(
                            $this->renderView(
                                'ComparatorEventBundle:Emails:notifs.txt.twig',
                                array('user' => $user)

                            ),
                            'text/html'
                        )
                    ;
                    $this->get('mailer')->send($message);
                }
            }






            return $this->redirect($this->generateUrl('event_show', array('slug' => $entity->getEvent()->getSlug())));

        }


        return $this->redirect($this->generateUrl('event_show', array('slug' => $entity->getEvent()->getSlug())));

    }


    /**
     * Deletes a ParticipeEvent entity.
     *

     */

    public function deleteAction(Request $request, $id,$api = null)

    {


        //$form = $this->createDeleteForm($id);

        //$form->handleRequest($request);
        $user = $this->get('security.context')->getToken()->getUser();

        if ($request->getMethod() == 'POST' or $api) {
            $em = $this->getDoctrine()->getManager();
            $desc = $this->get('request')->request->get('descsupp');
            $entity = $em->getRepository('ComparatorEventBundle:ParticipeEvent')->find($id);


            if (!$entity) {

                throw $this->createNotFoundException('Unable to find ParticipeEvent entity.');

            }


            $em->remove($entity);

            $em->flush();

            if($entity->getEvent()->getUser()->getId()==$user->getId())
            {
                $notification = new Notification();
                $notification->setEnabled(false);
                $notification->setEvent($entity->getEvent());
                $notification->setUser($user);
                $notification->setDescription($desc);
                $notification->setUser1($entity->getUser());
                $notification->setTitle("Suppression Participe");

                $em->persist($notification);
                $em->flush();


                $config = $em->getRepository('ComparatorEventBundle:Configuration')->configByUser($entity->getUser());

                if($config)
                {
                    if($config->getSupprimer())
                    {
                        $message = \Swift_Message::newInstance()
                            ->setSubject('Grintaaa')
                            ->setFrom($user->getEmail())
                            ->setTo($entity->getUser()->getEmail())
                            ->setBody(
                                $this->renderView(
                                    'ComparatorEventBundle:Emails:deletes.txt.twig',
                                    array('user' => $user)

                                ),
                                'text/html'
                            )
                        ;
                        $this->get('mailer')->send($message);
                    }
                }


            }


        }

        if($api)
            return array(
                'result' => "success"
            );
        else
            return $this->redirect($this->generateUrl('event_show', array('slug' => $entity->getEvent()->getSlug())));

    }


    /**
     * Creates a form to delete a ParticipeEvent entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */

    private function createDeleteForm($id)

    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('favoris_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Annuler'))
            ->getForm();

    }

}

