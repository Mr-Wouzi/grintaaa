<?php

namespace Comparator\Bundle\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\EventBundle\Entity\StatutEvent;
use Comparator\Bundle\EventBundle\Form\StatutEventType;

/**
 * StatutEvent controller.
 *
 */
class StatutEventController extends Controller
{

    /**
     * Lists all StatutEvent entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ComparatorEventBundle:StatutEvent')->findAll();

        return $this->render('ComparatorEventBundle:StatutEvent:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new StatutEvent entity.
     *
     */
    public function createAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = new StatutEvent();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //$files = $request->request->get("photo");

            $entity->setUser($this->get('security.context')->getToken()->getUser());
            //$entity->setUser($this->get('security.context')->getToken()->getUser());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('home'));
        }

        return $this->render('ComparatorEventBundle:StatutEvent:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a StatutEvent entity.
     *
     * @param StatutEvent $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(StatutEvent $entity)
    {
        $form = $this->createForm(new StatutEventType(), $entity, array(
            'action' => $this->generateUrl('event_statut_create'),
            'method' => 'POST',
        ));


        return $form;
    }

    /**
     * Displays a form to create a new StatutEvent entity.
     *
     */
    public function newAction()
    {
        $entity = new StatutEvent();
        $form   = $this->createCreateForm($entity);

        return $this->render('ComparatorEventBundle:StatutEvent:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a StatutEvent entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:StatutEvent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StatutEvent entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:StatutEvent:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing StatutEvent entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:StatutEvent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StatutEvent entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorEventBundle:StatutEvent:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a StatutEvent entity.
    *
    * @param StatutEvent $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(StatutEvent $entity)
    {
        $form = $this->createForm(new StatutEventType(), $entity, array(
            'action' => $this->generateUrl('event_statut_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing StatutEvent entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorEventBundle:StatutEvent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StatutEvent entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('event_statut_edit', array('id' => $id)));
        }

        return $this->render('ComparatorEventBundle:StatutEvent:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a StatutEvent entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorEventBundle:StatutEvent')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find StatutEvent entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('event_statut'));
    }

    /**
     * Creates a form to delete a StatutEvent entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('event_statut_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
