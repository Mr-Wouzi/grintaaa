<?php



namespace Comparator\Bundle\EventBundle\Entity;



use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;

use Gedmo\Timestampable\Traits\TimestampableEntity;



/**

 * Category

 *

 * @ORM\Table()

 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\CategoryRepository")

 */

class Category

{

    /**

     * @var integer

     *

     * @ORM\Column(name="id", type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */



    private $id;



    /**

     * @var string

     *

     *

     * @ORM\Column(name="title", type="string", length=255 , nullable=true)

     */



    private $title;



  /**

   * @Gedmo\Slug(fields={"title"})

   * @ORM\Column(length=128, unique=true)

   */

    private $slug;



    /**

     * @var \DateTime $createdAt

     *

     * @Gedmo\Timestampable(on="create")

     * @ORM\Column(type="datetime")

     */

    private $createdAt;



    /**

     * @var \DateTime $updatedAt

     *

     * @Gedmo\Timestampable(on="update")

     * @ORM\Column(type="datetime")

     */

    private $updatedAt;


    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     **/
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     **/
    protected $parent;

    /**

     * @return string

     */

    public function __toString()

    {

        return (string)$this->getTitle();

    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**

     * Get id

     *

     * @return integer 

     */

    public function getId()

    {

        return $this->id;

    }



    /**

     * Set title

     *

     * @param string $title

     * @return Category

     */

    public function setTitle($title)

    {

        $this->title = $title;



        return $this;

    }



    /**

     * Get title

     *

     * @return string 

     */

    public function getTitle()

    {

        return $this->title;

    }


    /**

     * Set slug

     *

     * @param string $slug

     * @return Category

     */

    public function setSlug($slug)

    {

        $this->slug = $slug;



        return $this;

    }



    /**

     * Get slug

     *

     * @return string 

     */

    public function getSlug()

    {

        return $this->slug;

    }

    

    

     /**

     * Set createdAt

     *

     * @param \DateTime $createdAt

     * @return Event

     */

    public function setCreatedAt($createdAt)

    {

        $this->createdAt = $createdAt;



        return $this;

    }



    /**

     * Get createdAt

     *

     * @return \DateTime 

     */

    public function getCreatedAt()

    {

        return $this->createdAt;

    }



    /**

     * Set updatedAt

     *

     * @param \DateTime $updatedAt

     * @return Event

     */

    public function setUpdatedAt($updatedAt)

    {

        $this->updatedAt = $updatedAt;



        return $this;

    }



    /**

     * Get updatedAt

     *

     * @return \DateTime 

     */

    public function getUpdatedAt()

    {

        return $this->updatedAt;

    }

    /**
     * Add children
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Category $children
     * @return Category
     */
    public function addChild(\Comparator\Bundle\EventBundle\Entity\Category $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Category $children
     */
    public function removeChild(\Comparator\Bundle\EventBundle\Entity\Category $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Category $parent
     * @return Category
     */
    public function setParent(\Comparator\Bundle\EventBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Comparator\Bundle\EventBundle\Entity\Category
     */
    public function getParent()
    {
        return $this->parent;
    }



}

