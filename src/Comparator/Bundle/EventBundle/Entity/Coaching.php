<?php





namespace Comparator\Bundle\EventBundle\Entity;





use Doctrine\ORM\Mapping as ORM;


use Gedmo\Mapping\Annotation as Gedmo;


use Comparator\Bundle\EventBundle\Entity\AbstractGMapEntity;


use Symfony\Component\Validator\Constraints as Assert;


/**


 * Coaching


 *


 * @ORM\Table()


 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\CoachingRepository")


 */





class Coaching


{


    /**

     * @var integer

     *

     * @ORM\Column(name="id", type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */

    private $id;






    /**


     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Category", inversedBy="event")


     * @ORM\JoinColumn(name="category_id", referencedColumnName="id",onDelete="CASCADE")


     */



    private $category;



     /**


     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="event")


     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")


     */


    private $user;




    /**

     * Get id

     *

     * @return integer

     */

    public function getId()

    {

        return $this->id;

    }




    /**


     * Set category


     *


     * @param \Comparator\Bundle\EventBundle\Entity\Category $category


     * @return Coaching


     */


    public function setCategory(\Comparator\Bundle\EventBundle\Entity\Category $category = null)


    {


        $this->category = $category;





        return $this;


    }





    /**


     * Get category


     *


     * @return \Comparator\Bundle\EventBundle\Entity\Category 


     */


    public function getCategory()


    {


        return $this->category;


    }





    /**


     * Set user


     *


     * @param \Application\Sonata\UserBundle\Entity\User $user


     * @return Coaching


     */


    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)


    {


        $this->user = $user;





        return $this;


    }





    /**


     * Get user


     *


     * @return \Application\Sonata\UserBundle\Entity\User 


     */


    public function getUser()


    {


        return $this->user;


    }


}


