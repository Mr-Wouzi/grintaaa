<?php



namespace Comparator\Bundle\EventBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;


use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\CommentRepository")
 */
class Comment

{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;


    /**
     * @var string
     *
     *
     * @ORM\Column(name="description", type="text" , nullable=true)
     */


    private $description;


    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */

    private $createdAt;


    /**
     * @var \DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */

    private $updatedAt;


    /**
     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Event", inversedBy="comment")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",onDelete="CASCADE")
     */

    private $event;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="comment")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $user;


    /**
     * Get id
     *
     * @return integer
     */

    public function getId()

    {

        return $this->id;

    }


    /**
     * @return string
     */

    public function __toString()

    {

        return (string)$this->getDescription();

    }

    /**
     * Set description
     *
     * @param string $description
     * @return Comment
     */

    public function setDescription($description)

    {

        $this->description = $description;


        return $this;

    }


    /**
     * Get description
     *
     * @return string
     */

    public function getDescription()

    {

        return $this->description;

    }


    /**
     * Set event
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Event $event
     * @return Comment
     */

    public function setEvent(\Comparator\Bundle\EventBundle\Entity\Event $event = null)

    {

        $this->event = $event;


        return $this;

    }


    /**
     * Get event
     *
     * @return \Comparator\Bundle\EventBundle\Entity\Event
     */

    public function getEvent()

    {

        return $this->event;

    }


    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Comment
     */

    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)

    {

        $this->user = $user;


        return $this;

    }


    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */

    public function getUser()

    {

        return $this->user;

    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Comment
     */

    public function setCreatedAt($createdAt)

    {

        $this->createdAt = $createdAt;


        return $this;

    }


    /**
     * Get createdAt
     *
     * @return \DateTime
     */

    public function getCreatedAt()

    {

        return $this->createdAt;

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Comment
     */

    public function setUpdatedAt($updatedAt)

    {

        $this->updatedAt = $updatedAt;


        return $this;

    }


    /**
     * Get updatedAt
     *
     * @return \DateTime
     */

    public function getUpdatedAt()

    {

        return $this->updatedAt;

    }


}

