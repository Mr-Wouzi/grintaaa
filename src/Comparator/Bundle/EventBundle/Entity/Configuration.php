<?php





namespace Comparator\Bundle\EventBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


use Gedmo\Mapping\Annotation as Gedmo;


use Comparator\Bundle\EventBundle\Entity\AbstractGMapEntity;


use Symfony\Component\Validator\Constraints as Assert;


/**
 * Configuration
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\ConfigurationRepository")
 */
class Configuration


{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comment", type="boolean" , nullable=true)
     */

    private $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="profile", type="boolean" , nullable=true)
     */

    private $profile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="participe", type="boolean" , nullable=true)
     */

    private $participe;

    /**
     * @var boolean
     *
     * @ORM\Column(name="confirmer", type="boolean" , nullable=true)
     */

    private $confirmer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="supprimer", type="boolean" , nullable=true)
     */

    private $supprimer;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="event")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $user;


    /**
     * Get id
     *
     * @return integer
     */

    public function getId()

    {

        return $this->id;

    }


    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Interest
     */


    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)

    {
        $this->user = $user;

        return $this;

    }


    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */


    public function getUser()

    {
        return $this->user;

    }

    /**
     * Set comment
     *
     * @param boolean $comment
     * @return Configuration
     */

    public function setComment($comment)

    {

        $this->comment = $comment;


        return $this;

    }


    /**
     * Get comment
     *
     * @return boolean
     */

    public function getComment()

    {

        return $this->comment;

    }

    /**
     * Set profile
     *
     * @param boolean $profile
     * @return Configuration
     */

    public function setProfile($profile)

    {

        $this->profile = $profile;


        return $this;

    }


    /**
     * Get profile
     *
     * @return boolean
     */

    public function getProfile()

    {

        return $this->profile;

    }

    /**
     * Set participe
     *
     * @param boolean $participe
     * @return Configuration
     */

    public function setParticipe($participe)

    {

        $this->participe = $participe;


        return $this;

    }


    /**
     * Get participe
     *
     * @return boolean
     */

    public function getParticipe()

    {

        return $this->participe;

    }

    /**
     * Set supprimer
     *
     * @param boolean $supprimer
     * @return Configuration
     */

    public function setSupprimer($supprimer)

    {

        $this->supprimer = $supprimer;


        return $this;

    }


    /**
     * Get supprimer
     *
     * @return boolean
     */

    public function getSupprimer()

    {

        return $this->supprimer;

    }

    /**
     * Set confirmer
     *
     * @param boolean $confirmer
     * @return Configuration
     */

    public function setConfirmer($confirmer)

    {

        $this->confirmer = $confirmer;


        return $this;

    }


    /**
     * Get confirmer
     *
     * @return boolean
     */

    public function getConfirmer()

    {

        return $this->confirmer;

    }

}


