<?php





namespace Comparator\Bundle\EventBundle\Entity;





use Doctrine\ORM\Mapping as ORM;


use Gedmo\Mapping\Annotation as Gedmo;


use Comparator\Bundle\EventBundle\Entity\AbstractGMapEntity;


use Symfony\Component\Validator\Constraints as Assert;


/**


 * Event


 *


 * @ORM\Table()


 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\EventRepository")


 */





class Event extends AbstractGMapEntity


{





    /**


     * @var string


     *


     *


     * @ORM\Column(name="title", type="string", length=255 , nullable=true)


     */





    private $title;





    /**


     * @var integer


     *


     * @ORM\Column(name="nbPlayer", type="integer", nullable=true)


     */





    private $nbPlayer;





    /**


     * @var \Date


     *


     * @ORM\Column(name="startDate", type="date", nullable=true)


     */





    private $startDate;





    /**


     * @var string


     *


     * @ORM\Column(name="startTime", type="time", nullable=true)


     */





    private $startTime;





    /**


     * @var string


     *


     * @ORM\Column(name="endTime", type="time", nullable=true)


     */





    private $endTime;





    /**


     * @var string


     *


     *


     * @ORM\Column(name="description", type="text" , nullable=true)


     */





    private $description;





  /**


   * @Gedmo\Slug(fields={"title"})


   * @ORM\Column(length=128, unique=true)


   */


    private $slug;





    /**


     * @var \DateTime $createdAt


     *


     * @Gedmo\Timestampable(on="create")


     * @ORM\Column(type="datetime")


     */


    private $createdAt;





    /**


     * @var \DateTime $updatedAt


     *


     * @Gedmo\Timestampable(on="update")


     * @ORM\Column(type="datetime")


     */


    private $updatedAt;





     /**


     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Category", inversedBy="event")


     * @ORM\JoinColumn(name="category_id", referencedColumnName="id",onDelete="CASCADE")


     */



    private $category;



     /**


     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="event")


     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")


     */








    private $user;


    /**
     * @var boolean
     *
     * @ORM\Column(name="handi", type="boolean" , nullable=true)
     */

    private $handi;


    /**


     * @return string


     */


    public function __toString()


    {


        return (string)$this->getTitle();


    }








    /**


     * Set title


     *


     * @param string $title


     * @return Event


     */


    public function setTitle($title)


    {


        $this->title = $title;





        return $this;


    }





    /**


     * Get title


     *


     * @return string


     */


    public function getTitle()


    {


        return $this->title;


    }


    /**
     * Set handi
     *
     * @param boolean $handi
     * @return Event
     */

    public function setHandi($handi)

    {

        $this->handi = $handi;


        return $this;

    }


    /**
     * Get handi
     *
     * @return boolean
     */

    public function getHandi()

    {

        return $this->handi;

    }



    /**


     * Set nbPlayer


     *


     * @param integer $nbPlayer


     * @return Event


     */


    public function setNbPlayer($nbPlayer)


    {


        $this->nbPlayer = $nbPlayer;





        return $this;


    }





    /**


     * Get nbPlayer


     *


     * @return integer


     */


    public function getNbPlayer()


    {


        return $this->nbPlayer;


    }





    /**


     * Set startDate


     *


     * @param \DateTime $startDate


     * @return Event


     */


    public function setStartDate($startDate)


    {


        $this->startDate = $startDate;





        return $this;


    }





    /**


     * Get startDate


     *


     * @return \DateTime


     */


    public function getStartDate()


    {


        return $this->startDate;


    }





    /**


     * Set startTime


     *


     * @param string $startTime


     * @return Event


     */


    public function setStartTime($startTime)


    {


        $this->startTime = $startTime;





        return $this;


    }





    /**


     * Get startTime


     *


     * @return string


     */


    public function getStartTime()


    {


        return $this->startTime;


    }





    /**


     * Set endTime


     *


     * @param string $endTime


     * @return Event


     */


    public function setEndTime($endTime)


    {


        $this->endTime = $endTime;





        return $this;


    }





    /**


     * Get endTime


     *


     * @return string


     */


    public function getEndTime()


    {


        return $this->endTime;


    }





    /**


     * Set description


     *


     * @param string $description


     * @return Event


     */


    public function setDescription($description)


    {


        $this->description = $description;





        return $this;


    }





    /**


     * Get description


     *


     * @return string


     */


    public function getDescription()


    {


        return $this->description;


    }





    /**


     * Set slug


     *


     * @param string $slug


     * @return Event


     */


    public function setSlug($slug)


    {


        $this->slug = $slug;





        return $this;


    }





    /**


     * Get slug


     *


     * @return string


     */


    public function getSlug()


    {


        return $this->slug;


    }





    /**


     * Set category


     *


     * @param \Comparator\Bundle\EventBundle\Entity\Category $category


     * @return Event


     */


    public function setCategory(\Comparator\Bundle\EventBundle\Entity\Category $category = null)


    {


        $this->category = $category;





        return $this;


    }





    /**


     * Get category


     *


     * @return \Comparator\Bundle\EventBundle\Entity\Category


     */


    public function getCategory()


    {


        return $this->category;


    }





    /**


     * Set user


     *


     * @param \Application\Sonata\UserBundle\Entity\User $user


     * @return Event


     */


    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)


    {


        $this->user = $user;





        return $this;


    }





    /**


     * Get user


     *


     * @return \Application\Sonata\UserBundle\Entity\User


     */


    public function getUser()


    {


        return $this->user;


    }





     /**


     * Set createdAt


     *


     * @param \DateTime $createdAt


     * @return Event


     */


    public function setCreatedAt($createdAt)


    {


        $this->createdAt = $createdAt;





        return $this;


    }





    /**


     * Get createdAt


     *


     * @return \DateTime


     */


    public function getCreatedAt()


    {


        return $this->createdAt;


    }





    /**


     * Set updatedAt


     *


     * @param \DateTime $updatedAt


     * @return Event


     */


    public function setUpdatedAt($updatedAt)


    {


        $this->updatedAt = $updatedAt;





        return $this;


    }





    /**


     * Get updatedAt


     *


     * @return \DateTime


     */


    public function getUpdatedAt()


    {


        return $this->updatedAt;


    }





}


