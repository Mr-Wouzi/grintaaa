<?php



namespace Comparator\Bundle\EventBundle\Entity;



use Doctrine\ORM\Mapping as ORM;



/**

 * Friends

 *

 * @ORM\Table()

 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\FriendsRepository")

 */

class Friends

{



    /**

     * @var integer

     *

     * @ORM\Column(name="id", type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */

    private $id;



    /**

     * @var boolean

     *

     * @ORM\Column(name="enabled", type="boolean" , nullable=true)

     */

    private $enabled;






      /**

     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="friends")

     * @ORM\JoinColumn(name="user1_id", referencedColumnName="id",onDelete="CASCADE")

     */


    private $user1;


    /**

     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="friends")

     * @ORM\JoinColumn(name="user2_id", referencedColumnName="id",onDelete="CASCADE")

     */


    private $user2;


    /**

     * Get id

     *

     * @return integer 

     */

    public function getId()

    {

        return $this->id;

    }




    /**

     * Set user1

     *

     * @param \Application\Sonata\UserBundle\Entity\User $user1

     * @return Friends

     */

    public function setUser1(\Application\Sonata\UserBundle\Entity\User $user1 = null)

    {

        $this->user1 = $user1;



        return $this;

    }



    /**

     * Get user1

     *

     * @return \Application\Sonata\UserBundle\Entity\User 

     */

    public function getUser1()

    {

        return $this->user1;

    }




    /**

     * Set user2

     *

     * @param \Application\Sonata\UserBundle\Entity\User $user2

     * @return Friends

     */

    public function setUser2(\Application\Sonata\UserBundle\Entity\User $user2 = null)

    {

        $this->user2 = $user2;



        return $this;

    }



    /**

     * Get user2

     *

     * @return \Application\Sonata\UserBundle\Entity\User

     */

    public function getUser2()

    {

        return $this->user2;

    }


    /**

     * Set enabled

     *

     * @param boolean $enabled

     * @return Friends

     */

    public function setEnabled($enabled)

    {

        $this->enabled = $enabled;



        return $this;

    }



    /**

     * Get enabled

     *

     * @return boolean

     */

    public function getEnabled()

    {

        return $this->enabled;

    }



    

}

