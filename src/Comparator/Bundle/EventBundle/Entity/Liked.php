<?php





namespace Comparator\Bundle\EventBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Liked
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\LikedRepository")
 */
class Liked
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Event", inversedBy="liked")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $event;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="liked")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set event
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Event $event
     * @return Liked
     */
    public function setEvent(\Comparator\Bundle\EventBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;

    }


    /**
     * Get event
     *
     * @return \Comparator\Bundle\EventBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;

    }


    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Liked
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;

    }


    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */

    public function getUser()
    {
        return $this->user;
    }


}


