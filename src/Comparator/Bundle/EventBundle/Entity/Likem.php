<?php





namespace Comparator\Bundle\EventBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Likem
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\LikemRepository")
 */
class Likem
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Mur", inversedBy="likem")
     * @ORM\JoinColumn(name="mur_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $mur;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="likem")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set mur
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Mur $mur
     * @return Likem
     */
    public function setMur(\Comparator\Bundle\EventBundle\Entity\Mur $mur = null)
    {
        $this->mur = $mur;

        return $this;

    }


    /**
     * Get mur
     *
     * @return \Comparator\Bundle\EventBundle\Entity\Mur
     */
    public function getMur()
    {
        return $this->mur;

    }


    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Likem
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;

    }


    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */

    public function getUser()
    {
        return $this->user;
    }


}


