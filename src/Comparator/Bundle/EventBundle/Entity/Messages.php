<?php



namespace Comparator\Bundle\EventBundle\Entity;



use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;


/**

 * Messages

 *

 * @ORM\Table()

 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\MessagesRepository")

 */

class Messages

{



    /**

     * @var integer

     *

     * @ORM\Column(name="id", type="integer")

     * @ORM\Id

     * @ORM\GeneratedValue(strategy="AUTO")

     */

    private $id;



    /**
     * @var string
     *
     *
     * @ORM\Column(name="description", type="text" , nullable=true)
     */


    private $description;



      /**

     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="messages")

     * @ORM\JoinColumn(name="user1_id", referencedColumnName="id",onDelete="CASCADE")

     */


    private $user1;


    /**

     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="messages")

     * @ORM\JoinColumn(name="user2_id", referencedColumnName="id",onDelete="CASCADE")

     */


    private $user2;


    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */

    private $createdAt;


    /**
     * @var \DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */

    private $updatedAt;


    /**

     * @var boolean

     *

     * @ORM\Column(name="enabled", type="boolean" , nullable=true)

     */

    private $enabled;

    /**

     * Get id

     *

     * @return integer 

     */

    public function getId()

    {

        return $this->id;

    }




    /**

     * Set user1

     *

     * @param \Application\Sonata\UserBundle\Entity\User $user1

     * @return Messages

     */

    public function setUser1(\Application\Sonata\UserBundle\Entity\User $user1 = null)

    {

        $this->user1 = $user1;



        return $this;

    }



    /**

     * Get user1

     *

     * @return \Application\Sonata\UserBundle\Entity\User 

     */

    public function getUser1()

    {

        return $this->user1;

    }




    /**

     * Set user2

     *

     * @param \Application\Sonata\UserBundle\Entity\User $user2

     * @return Messages

     */

    public function setUser2(\Application\Sonata\UserBundle\Entity\User $user2 = null)

    {

        $this->user2 = $user2;



        return $this;

    }



    /**

     * Get user2

     *

     * @return \Application\Sonata\UserBundle\Entity\User

     */

    public function getUser2()

    {

        return $this->user2;

    }

    /**
     * Set description
     *
     * @param string $description
     * @return Messages
     */

    public function setDescription($description)

    {

        $this->description = $description;


        return $this;

    }


    /**
     * Get description
     *
     * @return string
     */

    public function getDescription()

    {

        return $this->description;

    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Messages
     */

    public function setCreatedAt($createdAt)

    {

        $this->createdAt = $createdAt;


        return $this;

    }


    /**
     * Get createdAt
     *
     * @return \DateTime
     */

    public function getCreatedAt()

    {

        return $this->createdAt;

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Messages
     */

    public function setUpdatedAt($updatedAt)

    {

        $this->updatedAt = $updatedAt;


        return $this;

    }


    /**
     * Get updatedAt
     *
     * @return \DateTime
     */

    public function getUpdatedAt()

    {

        return $this->updatedAt;

    }

    /**

     * Set enabled

     *

     * @param boolean $enabled

     * @return Messages

     */

    public function setEnabled($enabled)

    {

        $this->enabled = $enabled;



        return $this;

    }



    /**

     * Get enabled

     *

     * @return boolean

     */

    public function getEnabled()

    {

        return $this->enabled;

    }

}

