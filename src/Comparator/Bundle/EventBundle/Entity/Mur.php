<?php





namespace Comparator\Bundle\EventBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Mur
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\MurRepository")
 */
class Mur


{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */


    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Event", inversedBy="mur")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $event;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="mur")
     * @ORM\JoinColumn(name="user1_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $user1;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="mur")
     * @ORM\JoinColumn(name="user2_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $user2;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="type", type="string", length=255 , nullable=true)
     */


    private $type;


    /**
     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\StatutBundle\Entity\File", inversedBy="mur")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $file;


    /**
     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\ParticipeEvent", inversedBy="mur")
     * @ORM\JoinColumn(name="participeevent_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $participeevent;


    /**
     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Comment", inversedBy="mur")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\StatutBundle\Entity\Statut", inversedBy="mur")
     * @ORM\JoinColumn(name="statut_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $statut;

    /**


     * @var \DateTime $createdAt


     *


     * @Gedmo\Timestampable(on="create")


     * @ORM\Column(type="datetime")


     */


    private $createdAt;





    /**


     * @var \DateTime $updatedAt


     *


     * @Gedmo\Timestampable(on="update")


     * @ORM\Column(type="datetime")


     */


    private $updatedAt;


    /**
     * Get id
     *
     * @return integer
     */


    public function getId()


    {


        return $this->id;


    }


    /**
     * Set event
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Event $event
     * @return Mur
     */


    public function setEvent(\Comparator\Bundle\EventBundle\Entity\Event $event = null)


    {


        $this->event = $event;


        return $this;


    }


    /**
     * Get event
     *
     * @return \Comparator\Bundle\EventBundle\Entity\Event
     */


    public function getEvent()


    {


        return $this->event;


    }


    /**
     * Set user1
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user1
     * @return Mur
     */


    public function setUser1(\Application\Sonata\UserBundle\Entity\User $user1 = null)


    {


        $this->user1 = $user1;


        return $this;


    }


    /**
     * Get user1
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */


    public function getUser1()


    {


        return $this->user1;


    }

    /**
     * Set user2
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user2
     * @return Mur
     */


    public function setUser2(\Application\Sonata\UserBundle\Entity\User $user2 = null)


    {


        $this->user2 = $user2;


        return $this;


    }


    /**
     * Get user2
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */


    public function getUser2()


    {


        return $this->user2;


    }

    /**
     * Set type
     *
     * @param string $type
     * @return Mur
     */


    public function setType($type)

    {

        $this->type = $type;

        return $this;


    }


    /**
     * Get type
     *
     * @return string
     */


    public function getType()

    {

        return $this->type;

    }

    /**
     * Set file
     *
     * @param \Comparator\Bundle\StatutBundle\Entity\File $file
     * @return Mur
     */


    public function setFile(\Comparator\Bundle\StatutBundle\Entity\File $file = null)


    {


        $this->file = $file;


        return $this;


    }


    /**
     * Get file
     *
     * @return \Comparator\Bundle\StatutBundle\Entity\File
     */


    public function getFile()


    {


        return $this->file;


    }

    /**
     * Set participeevent
     *
     * @param \Comparator\Bundle\EventBundle\Entity\ParticipeEvent $participeevent
     * @return Mur
     */


    public function setParticipeevent(\Comparator\Bundle\EventBundle\Entity\ParticipeEvent $participeevent = null)


    {


        $this->participeevent = $participeevent;


        return $this;


    }


    /**
     * Get participeevent
     *
     * @return \Comparator\Bundle\EventBundle\Entity\ParticipeEvent
     */


    public function getParticipeevent()


    {


        return $this->participeevent;


    }

    /**
     * Set comment
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Comment $comment
     * @return Mur
     */


    public function setComment(\Comparator\Bundle\EventBundle\Entity\Comment $comment = null)


    {


        $this->comment = $comment;


        return $this;


    }


    /**
     * Get comment
     *
     * @return \Comparator\Bundle\EventBundle\Entity\Comment
     */


    public function getComment()


    {


        return $this->comment;


    }
    /**
     * Set statut
     *
     * @param \Comparator\Bundle\StatutBundle\Entity\Statut $statut
     * @return Mur
     */


    public function setStatut(\Comparator\Bundle\StatutBundle\Entity\Statut $statut = null)


    {


        $this->statut = $statut;


        return $this;


    }


    /**
     * Get statut
     *
     * @return \Comparator\Bundle\StatutBundle\Entity\Statut
     */


    public function getStatut()


    {


        return $this->statut;


    }
    /**


     * Set createdAt


     *


     * @param \DateTime $createdAt


     * @return Mur


     */


    public function setCreatedAt($createdAt)


    {


        $this->createdAt = $createdAt;





        return $this;


    }





    /**


     * Get createdAt


     *


     * @return \DateTime


     */


    public function getCreatedAt()


    {


        return $this->createdAt;


    }





    /**


     * Set updatedAt


     *


     * @param \DateTime $updatedAt


     * @return Mur


     */


    public function setUpdatedAt($updatedAt)


    {


        $this->updatedAt = $updatedAt;





        return $this;


    }





    /**


     * Get updatedAt


     *


     * @return \DateTime


     */


    public function getUpdatedAt()


    {


        return $this->updatedAt;


    }



}


