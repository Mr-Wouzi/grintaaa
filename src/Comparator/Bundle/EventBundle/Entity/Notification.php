<?php



namespace Comparator\Bundle\EventBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Notification
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\NotificationRepository")
 */
class Notification

{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="title", type="string", length=255 , nullable=true)
     */


    private $title;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="description", type="string", length=255 , nullable=true)
     */


    private $description;

    /**


     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Event", inversedBy="notification")


     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",onDelete="CASCADE")


     */

    private $event;


    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */


    private $createdAt;


    /**
     * @var \DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */

    private $updatedAt;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="notification")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="notification")
     * @ORM\JoinColumn(name="user1_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $user1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean" , nullable=true)
     */

    private $enabled;

    /**
     * Get id
     *
     * @return integer
     */

    public function getId()

    {

        return $this->id;

    }


    /**
     * @return string
     */

    public function __toString()

    {

        return (string)$this->getTitle();

    }


    /**
     * Set title
     *
     * @param string $title
     * @return Notification
     */

    public function setTitle($title)

    {

        $this->title = $title;


        return $this;

    }


    /**
     * Get title
     *
     * @return string
     */

    public function getTitle()

    {

        return $this->title;

    }

    /**
     * Set description
     *
     * @param string $description
     * @return Notification
     */

    public function setDescription($description)

    {

        $this->description = $description;


        return $this;

    }


    /**
     * Get description
     *
     * @return string
     */

    public function getDescription()

    {

        return $this->description;

    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Notification
     */

    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)

    {

        $this->user = $user;


        return $this;

    }


    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */

    public function getUser()

    {

        return $this->user;

    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Notification
     */

    public function setCreatedAt($createdAt)

    {

        $this->createdAt = $createdAt;


        return $this;

    }


    /**
     * Get createdAt
     *
     * @return \DateTime
     */

    public function getCreatedAt()

    {

        return $this->createdAt;

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Notification
     */

    public function setUpdatedAt($updatedAt)

    {

        $this->updatedAt = $updatedAt;


        return $this;

    }


    /**
     * Get updatedAt
     *
     * @return \DateTime
     */

    public function getUpdatedAt()

    {

        return $this->updatedAt;

    }

    /**
     * Set user1
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user1
     * @return Notification
     */

    public function setUser1(\Application\Sonata\UserBundle\Entity\User $user1 = null)

    {

        $this->user1 = $user1;


        return $this;

    }


    /**
     * Get user1
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */

    public function getUser1()

    {

        return $this->user1;

    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Notification
     */

    public function setEnabled($enabled)

    {

        $this->enabled = $enabled;


        return $this;

    }


    /**
     * Get enabled
     *
     * @return boolean
     */

    public function getEnabled()

    {

        return $this->enabled;

    }


    /**
     * Set event
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Event $event
     * @return Notification
     */


    public function setEvent(\Comparator\Bundle\EventBundle\Entity\Event $event = null)

    {

        $this->event = $event;

        return $this;


    }





    /**

     * Get event

     *

     * @return \Comparator\Bundle\EventBundle\Entity\Event


     */


    public function getEvent()


    {


        return $this->event;


    }


}

