<?php





namespace Comparator\Bundle\EventBundle\Entity;





use Doctrine\ORM\Mapping as ORM;





/**


 * OptionEvent


 *


 * @ORM\Table()


 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\OptionEventRepository")


 */


class OptionEvent


{





    /**


     * @var integer


     *


     * @ORM\Column(name="id", type="integer")


     * @ORM\Id


     * @ORM\GeneratedValue(strategy="AUTO")


     */


    private $id;


    /**
     * @var string
     *
     *
     * @ORM\Column(name="title", type="string", length=255 , nullable=true)
     */

    private $title;


     /**


     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Event", inversedBy="optionevent")


     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",onDelete="CASCADE")


     */



    private $event;





    /**


     * Get id


     *


     * @return integer 


     */


    public function getId()


    {


        return $this->id;


    }





    /**


     * Set event


     *


     * @param \Comparator\Bundle\EventBundle\Entity\Event $event


     * @return OptionEvent


     */


    public function setEvent(\Comparator\Bundle\EventBundle\Entity\Event $event = null)


    {


        $this->event = $event;





        return $this;


    }





    /**


     * Get event


     *


     * @return \Comparator\Bundle\EventBundle\Entity\Event 


     */


    public function getEvent()


    {


        return $this->event;


    }







    /**


     * Set title


     *


     * @param string $title


     * @return OptionEvent


     */


    public function setTitle($title)
    {

        $this->title = $title;

        return $this;


    }





    /**

     * Get title

     *

     * @return string
     */


    public function getTitle()
    {
        return $this->title;

    }



}


