<?php





namespace Comparator\Bundle\EventBundle\Entity;





use Doctrine\ORM\Mapping as ORM;





/**


 * ParticipeEvent


 *


 * @ORM\Table()


 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\ParticipeEventRepository")


 */


class ParticipeEvent


{





    /**


     * @var integer


     *


     * @ORM\Column(name="id", type="integer")


     * @ORM\Id


     * @ORM\GeneratedValue(strategy="AUTO")


     */


    private $id;





    /**


     * @var integer


     *


     * @ORM\Column(name="enabled", type="integer" , nullable=true)


     */


    private $enabled;








     /**


     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Event", inversedBy="participeevent")


     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",onDelete="CASCADE")


     */








    private $event;





      /**


     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="participeevent")


     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")


     */





       


    private $user;








    /**


     * Get id


     *


     * @return integer 


     */


    public function getId()


    {


        return $this->id;


    }





    /**


     * Set event


     *


     * @param \Comparator\Bundle\EventBundle\Entity\Event $event


     * @return ParticipeEvent


     */


    public function setEvent(\Comparator\Bundle\EventBundle\Entity\Event $event = null)


    {


        $this->event = $event;





        return $this;


    }





    /**


     * Get event


     *


     * @return \Comparator\Bundle\EventBundle\Entity\Event 


     */


    public function getEvent()


    {


        return $this->event;


    }





    /**


     * Set user


     *


     * @param \Application\Sonata\UserBundle\Entity\User $user


     * @return ParticipeEvent


     */


    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)


    {


        $this->user = $user;





        return $this;


    }





    /**


     * Get user


     *


     * @return \Application\Sonata\UserBundle\Entity\User 


     */


    public function getUser()


    {


        return $this->user;


    }





    /**


     * Set enabled


     *


     * @param integer $enabled


     * @return ParticipeEvent


     */


    public function setEnabled($enabled)


    {


        $this->enabled = $enabled;





        return $this;


    }





    /**


     * Get enabled


     *


     * @return integer


     */


    public function getEnabled()


    {


        return $this->enabled;


    }





    


}


