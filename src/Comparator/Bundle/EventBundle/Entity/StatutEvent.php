<?php





namespace Comparator\Bundle\EventBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * StatutEvent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\StatutEventRepository")
 */
class StatutEvent
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */


    private $id;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="type", type="string", length=255 , nullable=true)
     */

    private $type;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="url", type="string", length=255 , nullable=true)
     */

    private $url;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="description", type="text" , nullable=true)
     */


    private $description;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */

    private $createdAt;


    /**
     * @var \DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */

    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Comparator\Bundle\EventBundle\Entity\Event", inversedBy="statutevent")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $event;


    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User", inversedBy="statutevent")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE")
     */


    private $user;


    /**
     * @var string
     *
     *
     * @ORM\Column(name="logo", type="text" , nullable=true)
     */
    private $logo;

    public $file;
    /**
     * Get id
     *
     * @return integer
     */


    public function getId()


    {


        return $this->id;


    }


    /**
     * Set event
     *
     * @param \Comparator\Bundle\EventBundle\Entity\Event $event
     * @return StatutEvent
     */


    public function setEvent(\Comparator\Bundle\EventBundle\Entity\Event $event = null)


    {


        $this->event = $event;


        return $this;


    }


    /**
     * Get event
     *
     * @return \Comparator\Bundle\EventBundle\Entity\Event
     */


    public function getEvent()


    {


        return $this->event;


    }


    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return StatutEvent
     */


    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)


    {


        $this->user = $user;


        return $this;


    }


    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */


    public function getUser()


    {


        return $this->user;


    }

    /**
     * Set description
     *
     * @param string $description
     * @return StatutEvent
     */

    public function setDescription($description)

    {

        $this->description = $description;


        return $this;

    }


    /**
     * Get description
     *
     * @return string
     */

    public function getDescription()

    {

        return $this->description;

    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return StatutEvent
     */

    public function setCreatedAt($createdAt)

    {

        $this->createdAt = $createdAt;


        return $this;

    }


    /**
     * Get createdAt
     *
     * @return \DateTime
     */

    public function getCreatedAt()

    {

        return $this->createdAt;

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return StatutEvent
     */

    public function setUpdatedAt($updatedAt)

    {

        $this->updatedAt = $updatedAt;


        return $this;

    }


    /**
     * Get updatedAt
     *
     * @return \DateTime
     */

    public function getUpdatedAt()

    {

        return $this->updatedAt;

    }






    /**
     * Set type
     *
     * @param string $type

     * @return StatutEvent
     */


    public function setType($type)

    {
        $this->type = $type;

        return $this;

    }


    /**
     * Get type
     *
     * @return string
     */

    public function getType()
    {
        return $this->type;


    }






    /**
     * Set url
     *
     * @param string $url

     * @return StatutEvent
     */


    public function setUrl($url)

    {
        $this->url = $url;

        return $this;

    }


    /**
     * Get url
     *
     * @return string
     */

    public function getUrl()
    {
        return $this->url;


    }


    /**
     * Set logo
     *
     * @param string $logo
     * @return StatutEvent
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    protected function getUploadDir()
    {
        return 'uploads/murs';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../../web/'.$this->getUploadDir();
    }

    public function getWebPath()
    {
        return null === $this->logo ? null : $this->getUploadDir().'/'.$this->logo;
    }

    public function getAbsolutePath()
    {
        return null === $this->logo ? null : $this->getUploadRootDir().'/'.$this->logo;
    }

    /**
     * @ORM\PrePersist
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // do whatever you want to generate a unique name
            $this->logo = uniqid().'.'.$this->file->guessExtension();
        }
    }
    /**
     * @ORM\postPersist
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->logo);

        unset($this->file);
    }
    /**
     * @ORM\postRemove
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}


