<?php



namespace Comparator\Bundle\EventBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;

use Gedmo\Timestampable\Traits\TimestampableEntity;


/**
 * Ville
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Comparator\Bundle\EventBundle\Repository\VilleRepository")
 */
class Ville

{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */


    private $id;


    /**
     * @var string
     *
     *
     * @ORM\Column(name="name", type="string", length=255 , nullable=true)
     */


    private $name;


    /**
     * @var string
     *
     *
     * @ORM\Column(name="cp", type="string", length=255 , nullable=true)
     */


    private $cp;


    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */

    private $createdAt;


    /**
     * @var \DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */

    private $updatedAt;


    public function __toString()

    {

        return (string)$this->getName();

    }


    /**
     * Get id
     *
     * @return integer
     */

    public function getId()

    {

        return $this->id;

    }


    /**
     * Set name
     *
     * @param string $name
     * @return Ville
     */

    public function setName($name)

    {

        $this->name = $name;


        return $this;

    }


    /**
     * Get name
     *
     * @return string
     */

    public function getName()

    {

        return $this->name;

    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return Ville
     */

    public function setCp($cp)

    {

        $this->cp = $cp;


        return $this;

    }


    /**
     * Get cp
     *
     * @return string
     */

    public function getCp()

    {

        return $this->cp;

    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Event
     */

    public function setCreatedAt($createdAt)

    {

        $this->createdAt = $createdAt;


        return $this;

    }


    /**
     * Get createdAt
     *
     * @return \DateTime
     */

    public function getCreatedAt()

    {

        return $this->createdAt;

    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Event
     */

    public function setUpdatedAt($updatedAt)

    {

        $this->updatedAt = $updatedAt;


        return $this;

    }


    /**
     * Get updatedAt
     *
     * @return \DateTime
     */

    public function getUpdatedAt()

    {

        return $this->updatedAt;

    }

}

