<?php



namespace Comparator\Bundle\EventBundle\Form;



use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityRepository;


class EventType extends AbstractType

{

    /**

     * @param FormBuilderInterface $builder

     * @param array $options

     */

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder

            ->add('title')

            ->add('nbPlayer', null, array('required' => false))

            ->add('startDate', 'date', array('widget' => 'single_text',

                                               'input' => 'datetime',

                                               'format' => 'dd/MM/yyyy',

                                               'attr' => array('class' => 'date'),

                                                ))

            //->add('startTime', 'time', array('input'  => 'timestamp',  'widget' => 'single_text',))

            //->add('endTime', 'time', array('input'  => 'timestamp', 'widget' => 'single_text',))

                

           //->add('startTime', 'time', array('widget' => 'choice', 'input' => 'timestamp', 'widget' => 'single_text', 'with_seconds' => false))

            ->add('startTime')

            ->add('endTime')



            ->add('description','textarea')

            ->add('address', 'gmap_address', array('data_class' => 'Comparator\Bundle\EventBundle\Entity\Event',))


            ->add('handi', NULL, array('label' =>' ', 'required' => false))


          ->add('locality', 'gmap_address', array('data_class' => 'Comparator\Bundle\EventBundle\Entity\Event',))

            ->add('country', 'gmap_address', array('data_class' => 'Comparator\Bundle\EventBundle\Entity\Event',))

            ->add('lat', 'gmap_address', array('data_class' => 'Comparator\Bundle\EventBundle\Entity\Event',))

            ->add('lng', 'gmap_address', array('data_class' => 'Comparator\Bundle\EventBundle\Entity\Event',))


            //->add('user')



        ;

    }

    

    /**

     * @param OptionsResolverInterface $resolver

     */

    public function setDefaultOptions(OptionsResolverInterface $resolver)

    {

        $resolver->setDefaults(array(

            'data_class' => 'Comparator\Bundle\EventBundle\Entity\Event'

        ));

    }



    /**

     * @return string

     */

    public function getName()

    {

        return 'comparator_bundle_eventbundle_event';

    }

}

