<?php

namespace Comparator\Bundle\EventBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StatutEventType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           // ->add('type')
           // ->add('url')
           // ->add('description')
          //  ->add('createdAt')
          //  ->add('updatedAt')
           // ->add('event')
           // ->add('user')
           ->add('file', 'file', array('label' => 'logo', 'required' => false))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Comparator\Bundle\EventBundle\Entity\StatutEvent'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'comparator_bundle_eventbundle_statutevent';
    }
}
