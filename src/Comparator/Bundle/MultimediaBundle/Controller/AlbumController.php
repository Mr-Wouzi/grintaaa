<?php

namespace Comparator\Bundle\MultimediaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\MultimediaBundle\Entity\Album;
use Comparator\Bundle\MultimediaBundle\Form\AlbumType;

use Comparator\Bundle\MultimediaBundle\Entity\File;
use Comparator\Bundle\MultimediaBundle\Form\FileType;

/**
 * Album controller.
 *
 */
class AlbumController extends Controller
{

    /**
     * Lists all Album entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ComparatorMultimediaBundle:Album')->findAll();

        return $this->render('ComparatorMultimediaBundle:Album:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Album entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Album();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUser($this->get('security.context')->getToken()->getUser());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user_album_show', array('slug' => $entity->getSlug())));
        }

        return $this->render('ComparatorMultimediaBundle:Album:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Album entity.
     *
     * @param Album $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Album $entity)
    {
        $form = $this->createForm(new AlbumType(), $entity, array(
            'action' => $this->generateUrl('user_album_create'),
            'method' => 'POST',
        ));

       // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Album entity.
     *
     */
    public function newAction()
    {
        $entity = new Album();
        $form   = $this->createCreateForm($entity);

        return $this->render('ComparatorMultimediaBundle:Album:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Album entity.
     *
     */
    public function showAction($slug)
    {

        $entity = new File();
        $form = $this->createsCreateForm($entity,$slug);
        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $album = $em->getRepository('ComparatorMultimediaBundle:Album')->findOneBy(array('slug' => $slug));
        $images = $em->getRepository('ComparatorMultimediaBundle:File')->findBy(array('user' => $user,'album' => $album));

        return $this->render('ComparatorMultimediaBundle:Album:show.html.twig', array(
            'entity' => $entity,
            'images' => $images,
            'album' => $album,
            'form' => $form->createView(),

        ));
    }

    /**
     * Displays a form to edit an existing Album entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorMultimediaBundle:Album')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Album entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorMultimediaBundle:Album:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Album entity.
    *
    * @param Album $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Album $entity)
    {
        $form = $this->createForm(new AlbumType(), $entity, array(
            'action' => $this->generateUrl('user_album_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Album entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorMultimediaBundle:Album')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Album entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('user_album_edit', array('id' => $id)));
        }

        return $this->render('ComparatorMultimediaBundle:Album:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Album entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorMultimediaBundle:Album')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Album entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('user_album'));
    }

    /**
     * Creates a form to delete a Album entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_album_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Creates a new File entity.
     *
     */
    public function createsAction(Request $request, $slug)
    {

        $entity = new File();
        $form = $this->createsCreateForm($entity, $slug);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $album = $em->getRepository('ComparatorMultimediaBundle:Album')->findOneBy(array('slug' => $slug));
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();


            $entity->setUser($this->get('security.context')->getToken()->getUser());
            $entity->setAlbum($album);
            $em->persist($entity);
            $em->flush();

            if($album->getLogo()==NULL)
            {
                $album ->setLogo($entity->getLogo());
                $em->persist($entity);
                $em->flush();
            }
            return $this->redirect($this->generateUrl('user_album_show', array('slug' => $album->getSlug())));
        }

        return $this->render('ComparatorMultimediaBundle:Album:news.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }


    /**
     * Creates a form to create a File entity.
     *
     * @param File $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createsCreateForm(File $entity, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $album = $em->getRepository('ComparatorMultimediaBundle:Album')->findOneBy(array('slug' => $slug));
        $form = $this->createForm(new FileType(), $entity, array(
            'action' => $this->generateUrl('user_file_creates', array('slug' => $album->getSlug())),
            'method' => 'POST',
        ));


        return $form;

    }

    /**
     * Displays a form to create a new File entity.
     *
     */
    public function newsAction($slug)
    {

        $entity = new File();
        $form = $this->createsCreateForm($entity,$slug);
        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $album = $em->getRepository('ComparatorMultimediaBundle:Album')->findOneBy(array('slug' => $slug));
        return $this->render('ComparatorMultimediaBundle:Album:news.html.twig', array(
            'entity' => $entity,
            'album' => $album,
            'form' => $form->createView(),
        ));


    }

}
