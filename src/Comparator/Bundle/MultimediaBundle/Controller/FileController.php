<?php

namespace Comparator\Bundle\MultimediaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Comparator\Bundle\EventBundle\Entity\Notification;

use Comparator\Bundle\EventBundle\Form\NotificationType;

use Comparator\Bundle\MultimediaBundle\Entity\File;
use Comparator\Bundle\MultimediaBundle\Form\FileType;

/**
 * File controller.
 *
 */
class FileController extends Controller
{

    /**
     * Lists all File entities.
     *
     */
    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($user->getId() % 5 == 0) {
            $em = $this->getDoctrine()->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("3AvatarHommes.jpg");
            $em->persist($entity);
            $em->flush();
        } elseif ($user->getId() % 3 == 0) {
            $em = $this->getDoctrine()->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("5AvatarHommes.jpg");
            $em->persist($entity);
            $em->flush();

        } elseif ($user->getId() % 2 == 0) {
            $em = $this->getDoctrine()->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("6AvatarHommes.jpg");
            $em->persist($entity);
            $em->flush();

        } else {
            $em = $this->getDoctrine()->getManager();
            $entity = new File();
            $entity->setUser($user);
            $entity->setLogo("11AvatarHommes.jpg");
            $em->persist($entity);
            $em->flush();

        }

        return $this->redirect($this->generateUrl('home'));

    }


    /**
     * Creates a new File entity.
     *
     */
    public function createAction(Request $request , $api = false)
    {

        $entity = new File();

        $form = self::createCreateForm($entity);
        $form->handleRequest($request);

        if($api){
            $em = $this->getDoctrine()->getManager();

            $entity->setUser($this->get('security.context')->getToken()->getUser());
            $em->persist($entity);
            $em->flush();

            if($api)
                return array(
                    'result' => "success",
                );

        }else if (!empty($form)) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();


                $entity->setUser($this->get('security.context')->getToken()->getUser());
                $em->persist($entity);
                $em->flush();


                return $this->redirect($this->generateUrl('home'));
            }
        }

        return $this->render('ComparatorMultimediaBundle:File:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Creates a form to create a File entity.
     *
     * @param File $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(File $entity)
    {

        $form = $this->createForm(new FileType(), $entity, array(
            'action' => $this->generateUrl('user_file_create'),
            'method' => 'POST',
        ));


        return $form;

    }

    /**
     * Displays a form to create a new File entity.
     *
     */
    public function newAction()
    {

        $entity = new File();
        $form = $this->createCreateForm($entity);
        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        if (!($user->getFacebookUid())) {

            $em = $this->getDoctrine()->getManager();
            $notif = $em->getRepository('ComparatorEventBundle:Notification')->notificationRegister($user);


            if ($notif) {
                $interest = $this->getDoctrine()->getRepository('ComparatorEventBundle:Interest')->findBy(array('user' => $user));

                if (($user->getWeight()) && (count($interest) > 0)) {

                    $notif->setEnabled(true);
                    $em->persist($notif);

                    $em->flush();
                }

            } else {
                $notifEnable = $em->getRepository('ComparatorEventBundle:Notification')->notificationRegisterEnable($user);

                if (!$notifEnable) {
                    $notification = new Notification();
                    $notification->setEnabled(false);
                    $notification->setUser($user);

                    $notification->setUser1($user);
                    $notification->setTitle("Register");

                    $em->persist($notification);

                    $em->flush();
                }


            }


        }
        $albums = $em->getRepository('ComparatorMultimediaBundle:Album')->findBy(array('user' => $user));


        return $this->render('ComparatorMultimediaBundle:File:new.html.twig', array(
            'entity' => $entity,
            'albums' => $albums,
            'form' => $form->createView(),
        ));


    }

    /**
     * Finds and displays a File entity.
     *
     */
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        //$images = $em->getRepository('ComparatorMultimediaBundle:File')->findBy(array('user' => $user,'album' => NULL));
        $images = $em->getRepository('ComparatorMultimediaBundle:File')->listLogoProfile($user);

        return $this->render('ComparatorMultimediaBundle:File:show.html.twig', array(
            'images' => $images,
        ));

    }

    /**
     * Displays a form to edit an existing File entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorMultimediaBundle:File')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find File entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorMultimediaBundle:File:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Creates a form to edit a File entity.
     *
     * @param File $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(File $entity)
    {

        $form = $this->createForm(new FileType(), $entity, array(
            'action' => $this->generateUrl('user_file_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;

    }


    /**
     * Creates a new File entity.
     *
     */
    public function profileLogoAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $profile = $em->getRepository('ComparatorMultimediaBundle:File')->find($id);
        $profile->setUpdatedAt(new \DateTime('now'));
        $em->persist($profile);

        $em->flush();

        return $this->redirect($this->generateUrl('home'));


    }

    /**
     * Edits an existing File entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorMultimediaBundle:File')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find File entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('user_file_edit', array('id' => $id)));
        }

        return $this->render('ComparatorMultimediaBundle:File:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a File entity.
     *
     */
    public function deleteAction($id)
    {


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ComparatorMultimediaBundle:File')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find File entity.');
        }

        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('user_file_new'));

    }

    /**
     * Creates a form to delete a File entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_file_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }

    public function avatarFemme1Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("1AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme2Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("2AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme3Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("3AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme4Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("4AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme5Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("5AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme6Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("6AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme7Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("7AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme8Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("8AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme9Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("9AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme10Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("10AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarFemme11Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("11AvatarFemme.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }


    public function avatarHommes1Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("1AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes2Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("2AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes3Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("3AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes4Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("4AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes5Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("5AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes6Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("6AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes7Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("7AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes8Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("8AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes9Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("9AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes10Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("10AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }

    public function avatarHommes11Action(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = new File();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setLogo("11AvatarHommes.jpg");
        $em->persist($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('fos_user_profile_show'));


    }


    public function logoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();

        $logo = $em->getRepository('ComparatorMultimediaBundle:File')->listLogo($user);

        return $this->render('ComparatorMultimediaBundle:File:logo.html.twig', array(
            'logo' => $logo

        ));

    }

}
