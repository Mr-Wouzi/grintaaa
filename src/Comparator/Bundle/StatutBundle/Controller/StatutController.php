<?php

namespace Comparator\Bundle\StatutBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Comparator\Bundle\StatutBundle\Entity\Statut;
use Comparator\Bundle\StatutBundle\Form\StatutType;
use Comparator\Bundle\EventBundle\Entity\Mur;

/**
 * Statut controller.
 *
 */
class StatutController extends Controller
{

    /**
     * Lists all Statut entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ComparatorStatutBundle:Statut')->findAll();

        return $this->render('ComparatorStatutBundle:Statut:index.html.twig', array(
            'entities' => $entities,
        ));

    }

    /**
     * Creates a new Statut entity.
     *
     */
    public function createAction(Request $request, $api = false)
    {

        $entity = new Statut();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid() or $api) {
            $em = $this->getDoctrine()->getManager();


            $entity->setUser($this->get('security.context')->getToken()->getUser());
            $entity->setType("imagemur");
            $em->persist($entity);
            $em->flush();

            $mur = new Mur();
            $mur->setUser1($this->get('security.context')->getToken()->getUser());
            $mur->setUser2($this->get('security.context')->getToken()->getUser());
            $mur->setStatut($entity);
            $mur->setType('imagemur');
            $em->persist($mur);
            $em->flush();
            if($api)
                return array(
                    "result" => "success"
                );
            
            return $this->redirect($this->generateUrl('home'));
        }

        return $this->render('ComparatorStatutBundle:Statut:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));

    }

    /**
     * Creates a form to create a Statut entity.
     *
     * @param Statut $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Statut $entity)
    {

        $form = $this->createForm(new StatutType(), $entity, array(
            'action' => $this->generateUrl('user_statut_create'),
            'method' => 'POST',
        ));


        return $form;

    }

    /**
     * Displays a form to create a new Statut entity.
     *
     */
    public function newAction()
    {

        $entity = new Statut();
        $form = $this->createCreateForm($entity);

        return $this->render('ComparatorStatutBundle:Statut:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));


    }

    /**
     * Finds and displays a Statut entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorStatutBundle:Statut')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Statut entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorStatutBundle:Statut:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Displays a form to edit an existing Statut entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorStatutBundle:Statut')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Statut entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ComparatorStatutBundle:Statut:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Creates a form to edit a Statut entity.
     *
     * @param Statut $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Statut $entity)
    {

        $form = $this->createForm(new StatutType(), $entity, array(
            'action' => $this->generateUrl('user_statut_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;

    }

    /**
     * Edits an existing Statut entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ComparatorStatutBundle:Statut')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Statut entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('user_statut_edit', array('id' => $id)));
        }

        return $this->render('ComparatorStatutBundle:Statut:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a Statut entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ComparatorStatutBundle:Statut')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Statut entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('user_statut'));

    }

    /**
     * Creates a form to delete a Statut entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_statut_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();

    }

    public function addStatutAction(Request $request, $api = false)
    {
        $em = $this->getDoctrine()->getManager();
        $text = $this->get('request')->request->get('textarea');

        $entity = new Statut();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setType("statutmur");
        $entity->setDescreption($text);
        $em->persist($entity);
        $em->flush();

        $mur = new Mur();
        $mur->setUser1($this->get('security.context')->getToken()->getUser());
        $mur->setUser2($this->get('security.context')->getToken()->getUser());
        $mur->setStatut($entity);
        $mur->setType('statutmur');
        $em->persist($mur);
        $em->flush();

        if($api)
            return array(
                "result" => "success",
            );
        
        return $this->redirect($this->generateUrl('home'));


    }

    public function addVideoAction(Request $request, $api = false)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $this->get('request')->request->get('video');

        $youtube_id = Self::getYouTubeIdFromURL($video);

        $entity = new Statut();
        $entity->setUser($this->get('security.context')->getToken()->getUser());
        $entity->setType("videomur");
        $entity->setUrl($youtube_id);
        $em->persist($entity);
        $em->flush();

        $mur = new Mur();
        $mur->setUser1($this->get('security.context')->getToken()->getUser());
        $mur->setUser2($this->get('security.context')->getToken()->getUser());
        $mur->setStatut($entity);
        $mur->setType('videomur');
        $em->persist($mur);
        $em->flush();
        if($api)
            return array(
                "result" => "success",
            );
        return $this->redirect($this->generateUrl('home'));


    }

    function getYouTubeIdFromURL($url)
    {
        $url_string = parse_url($url, PHP_URL_QUERY);
        parse_str($url_string, $args);
        return isset($args['v']) ? $args['v'] : false;
    }


}
