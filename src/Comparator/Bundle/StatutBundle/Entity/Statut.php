<?php

namespace Comparator\Bundle\StatutBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Statut
 */
class Statut
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $logo;
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $descreption;

    /**
     * @var string
     */
    private $url;
    /**

     * Hook timestampable behavior

     * updates createdAt, updatedAt fields

     */

    use TimestampableEntity;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $user;

    public $file;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Statut
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Statut
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Statut
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set descreption
     *
     * @param string $descreption
     * @return Statut
     */
    public function setDescreption($descreption)
    {
        $this->descreption = $descreption;

        return $this;
    }

    /**
     * Get descreption
     *
     * @return string
     */
    public function getDescreption()
    {
        return $this->descreption;
    }


    /**
     * Set url
     *
     * @param string $url
     * @return Statut
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    protected function getUploadDir()
    {
        return 'uploads/murs';
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../../public_html/'.$this->getUploadDir();
    }

    public function getWebPath()
    {
        return null === $this->logo ? null : $this->getUploadDir().'/'.$this->logo;
    }

    public function getAbsolutePath()
    {
        return null === $this->logo ? null : $this->getUploadRootDir().'/'.$this->logo;
    }

    /**
     * @ORM\PrePersist
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // do whatever you want to generate a unique name
            $this->logo = uniqid().'.'.$this->file->guessExtension();
        }
    }
    /**
     * @ORM\PostPersist
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->logo);

        unset($this->file);
    }
    /**
     * @ORM\PostRemove
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}
